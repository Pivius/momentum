
-------------------------------------
-- Idea SECTION
-------------------------------------
-- SPEED UI
-------------------------------------
-- IDEA The limit should be all time top -- Idea BY THEO

-------------------------------------
-- LOBBIES
-------------------------------------
-- IDEA Hard mode, worse acceleration (higher grav?), no shift

-------------------------------------
-- HEALTH UI
-------------------------------------
--IDEA Light/White UI style
--IDEA Fill up health heart and speed guy idea by metaloexdxdxdxd
-------------------------------------
-- UI.
-------------------------------------
hud = hud or {}
hud.module = include( 'modules/cl_hudmodule.lua' )
hud.ease_module = include( 'modules/cl_ease.lua' )
Topspeed     = 0

topstartlimit= 5
toplimit     = topstartlimit
-------------------------------------
-- Commands.
-- Different console commands for the UI.
-------------------------------------
local keyechoes   = CreateClientConVar("grp_keys", "0", true, true, "Hides/shows key echoes. 1 = fancy echoes, 2 = quake style echoes")
local outlineecho   = CreateClientConVar("grp_keys_outline", "0", true, true, "Enables outlined key echoes")
local hudtoggle   = CreateClientConVar("grp_hud", "1", true, true, "Hides/shows hud")
local indicators   = CreateClientConVar("grp_indicators", "1", true, true, "Hides/shows player indicators")
local crosshairtoggle = CreateClientConVar( "grp_crosshair", "1", true, true, "Hides/shows crosshair")
local sGraph = CreateClientConVar( "grp_speedometer", "1", true, true, "Hides/shows graph")
local LagSens = CreateClientConVar( "grp_hud_lag_sensitivity", "10", true, true, "Hud lag sensitivity. Default: 10")
local velo = CreateClientConVar( "grp_2Dvel", "0", true, true, "0 = Normal, 1 = 2D Velocity")

cvars.AddChangeCallback( "grp_hud_lag_sensitivity", function( convar_name, value_old, value_new )
  if tonumber(value_new) > 30 then
    LagSens:SetInt(30)
    return
  elseif tonumber(value_new) < 1 then
    LagSens:SetInt(1)
    return
  end
  local ls = math.Clamp(LagSens:GetInt(),1,30)

	leftSide.tiltRate = math.Clamp(ls,1,30)/4
  leftSide.resetRate = 40/((ls/4)/2)
	rightSide.tiltRate = math.Clamp(ls,1,30)/4
  rightSide.resetRate = 40/((ls/4)/2)
	top.tiltRate = math.Clamp(ls,1,30)/4
  top.resetRate = 40/((ls/4)/2)
  keyEchoes.tiltRate = ls/4
  keyEchoes.resetRate = 40/((ls/4)/2)
end )
-------------------------------------
-- The main UI colors.
-------------------------------------
hud.color = Color(255, 255, 255, 220)
hud.customColor = Color(255,255,255)
hud.customColor2 = Color(200,200,200)

hud.Team = "None"
-------------------------------------
-- Easing
-------------------------------------
hud.ease = {}
hud.ease.HP = {}
hud.ease.HP.FadeTime = 0

hud.ease.HP.Alpha = 255
hud.ease.HP.W = 0
hud.ease.HP.Ease = hud.ease_module.new(1, {alpha = 0, W = 0}, {alpha = hud.ease.HP.Alpha, W = hud.ease.HP.W}, "inOutCubic")

hud.ease.vel = {}
hud.ease.vel.FadeTime = 0

hud.ease.vel.Alpha = 255
hud.ease.vel.W = 0
hud.ease.vel.Ease = hud.ease_module.new(1, {alpha = 0, W = 0}, {alpha = hud.ease.vel.Alpha, W = hud.ease.vel.W}, "inOutCubic")

hud.keys = {
  ["W"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_FORWARD},
  ["A"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_MOVELEFT},
  ["S"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_BACK},
  ["D"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_MOVERIGHT},
  ["JUMP"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_JUMP},
  ["DUCK"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_DUCK},
  ["M1"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_ATTACK},
  ["M2"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_ATTACK2}
}

-------------------------------------
-- INIT UI.
-------------------------------------
function hud.Init()
  local ls = math.Clamp(LagSens:GetFloat(),1,30)
  --Speedometer

  --KeyEchoes
  if keyEchoes then
    keyEchoes:Remove()
  end


  keyEchoes = hud.module.CreatePanel(  )

  keyEchoes.Distance = 1.65
  keyEchoes.tiltRate = ls/4
  keyEchoes.resetRate = 40/((ls/4)/2)
  keyEchoes.sideAng = 0
  keyEchoes.moveUpdate = false

  if leftSide then
    leftSide:Remove()
  end
  leftSide = vgui.Create( "3DPanel" )

  leftSide.Distance = 1.65
  leftSide.tiltRate = ls/4
  leftSide.resetRate = 40/((ls/4)/2)
  leftSide.sideAng = 0
  leftSide.moveUpdate = false

  if cgaz then
    cgaz:Remove()
  end
  cgaz = hud.module.CreatePanel( nil, ls / 4, 40 / ( ( ls / 4 ) / 2 ) )

  if rightSide then
    rightSide:Remove()
  end
  rightSide = vgui.Create( "3DPanel" )

  rightSide.Distance = 1.65
  rightSide.tiltRate = ls/4
  rightSide.resetRate = 40/((ls/4)/2)
  rightSide.sideAng = 0
  rightSide.moveUpdate = false

  if top then
    top:Remove()
  end
  top = vgui.Create( "3DPanel" )

  top.Distance = 1.65
  top.tiltRate = ls/4
  top.resetRate = 40/((ls/4)/2)
  top.sideAng = 0
  top.moveUpdate = false
end
hud.Init()
-------------------------------------
-- Removes standard UI.
-------------------------------------
function GM:HUDDrawTargetID()
end
function GM:HUDShouldDraw(name)
    local draw = true
    if(name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudSecondaryAmmo" or name == "CHudCrosshair" ) then
    draw = false;
    end
return draw;
end


-------------------------------------
-- Draw Player behind walls.
-------------------------------------
function drawPlayer(ent)

    local ang = LocalPlayer():EyeAngles()
    local pos = LocalPlayer():EyePos()+ang:Forward()*10

            render.ClearStencil()
            render.SetStencilEnable(true)
            render.SetStencilReferenceValue( 1 )
            render.SetStencilWriteMask(1)
            render.SetStencilTestMask(1)
            render.SetStencilZFailOperation(STENCILOPERATION_REPLACE)
            render.SetStencilPassOperation(STENCILOPERATION_KEEP)
            render.SetStencilFailOperation(STENCILOPERATION_KEEP)
            render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_ALWAYS)


            render.SetBlend( 0 )
            /*
              local scale = Vector(1,1,0.9)
              local mat = Matrix()
              mat:Scale(Vector(scale))

              ent:EnableMatrix("RenderMultiply", mat)


              */
              local tran = Vector(0,0,0)
              local mat = Matrix()
              mat:Scale(Vector(1,1,1))
              mat:Translate( tran )
              ent:EnableMatrix("RenderMultiply", mat)

              ent:DrawModel()
              mat:Translate( Vector(0,0,0) )
              mat:Scale(Vector(1,1,1))
              ent:EnableMatrix("RenderMultiply", mat)
            render.SetBlend( 1 )
            render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
            cam.Start3D2D(Vector(pos.x, pos.y, pos.z),Angle(ang.p+90,ang.y,0),1)
              cam.IgnoreZ(false)
                surface.SetDrawColor(hud.customColor)
                surface.DrawRect(-ScrW(),-ScrH(),ScrW()*2,ScrH()*2)
                cam.IgnoreZ(false)
            cam.End3D2D()

              ent:DrawModel()
            render.SetStencilEnable(false)

end

-------------------------------------
-- Render player.
-------------------------------------
hook.Add("PostDrawOpaqueRenderables","PlayerStencil",function()
  for k, ply in pairs(player.GetAll()) do
    if ply != LocalPlayer() and indicators:GetBool() then
      drawPlayer( ply )
    end
  end
end)

-------------------------------------
-- Calculate the distance between two elements. http://www.purplemath.com/modules/distform.htm
-------------------------------------
function GetDistanceSqr(pp ,cx, cy)

    return (pp.x - cx) ^ 2 + (pp.y - cy) ^ 2
end

-------------------------------------
-- Find the  root.
-------------------------------------
function GetDistance(pp, cx, cy)
    return math.sqrt(GetDistanceSqr(pp, cx ,cy))
end

-------------------------------------
-- Indicator UI.
-------------------------------------
function hud.haha()
  if lobby:getPlayerLobby(LocalPlayer()) != "None" then
    if lobby:getPlayerLobby(LocalPlayer()).lobbyMode== "Gay" then
        surface.SetDrawColor( 255, 255, 1, 255 )
        --draw.DrawText("Moo5e GAY HAHAH", "HUD Speed", (ScrW()/2)*1.005, (ScrH()/2), Color(255,255,255, 255), TEXT_ALIGN_CENTER)
    end
end
end
hook.Add("HUDPaint", "haha", hud.haha)

function hud.indicator_info()

    for k, v in pairs (player.GetAll()) do
      if v != LocalPlayer() then

        local    NameTag = (v:LocalToWorld(Vector(0,0,77))):ToScreen()
        local    Middle = (v:LocalToWorld(Vector(0,0,77/2))):ToScreen()
        local    bot = (v:LocalToWorld(Vector(0,0,0))):ToScreen()
        local    speedTag = (v:LocalToWorld(Vector(0,0,10))):ToScreen()
        local    speed   = v:Speed()
        local playername = ""
        local      alpha = 255
        local     cx, cy = input.GetCursorPos()
        playername       = v:Name()

        local dist = LocalPlayer():GetPos():Distance(v:GetPos())
        local size = 15/(math.Clamp(dist/1200, 1, 1.5))

        alpha = Lerp(GetDistance(Middle, cx, cy)/(math.Clamp(alpha, 0, 255)/3) , 255, 0)
        local color = ColorAlpha(hud.color, alpha)
        if lobby:getPlayerLobby(v) == lobby:getPlayerLobby(LocalPlayer()) and lobby:getPlayerLobby(v) != "None" then
          surface.SetDrawColor(hud.customColor )
          surface.SetMaterial(Material("materials/trick/triangle.png", "noclamp"))
          surface.DrawTexturedRectRotated( bot.x, bot.y, size, size, 0 )
          color = ColorAlpha(hud.customColor, alpha)
        end

        if hudtoggle:GetBool() and indicators:GetBool() and v:Alive() then
          local color = ColorAlpha(hud.customColor, alpha)
          draw.DrawText(playername, "HUD Player", NameTag.x, NameTag.y-10 , color, TEXT_ALIGN_CENTER)
          draw.DrawText(math.Round(speed/10)*10, "HUD Player", speedTag.x+20, speedTag.y+10, color, TEXT_ALIGN_CENTER)
        end
      end
    end
end
hook.Add("HUDPaint", "Indicator info", hud.indicator_info)

local Vector2D = {}
Vector2D.__index = Vector2D

function Vector2D.new(x, y)
  local self = setmetatable({}, Vector2D)
  self.x = x or 0
  self.y = y or 0
  return self
end

function Vector2D:update(x, y)
  self.x = x or self.x
  self.y = y or self.y
end

function Vector2D:size()
  return math.sqrt(math.pow(self.x,2) + math.pow(self.y,2))
end

function Vector2D:dotProduct(vec2)
  assert(type(vec2) == "table", "vec2 must be a table")
  assert(type(vec2.x) ~= "nil", "vec2 must have an x value")
  assert(type(vec2.y) ~= "nil", "vec2 must have an y value")
  return (self.x*vec2.x)+(self.y*vec2.y)
end

function Vector2D:angle(vec2)
  assert(type(vec2) == "table", "vec2 must be a table")
  assert(type(vec2.x) ~= "nil", "vec2 must have an x value")
  assert(type(vec2.y) ~= "nil", "vec2 must have an y value")
  return math.acos(self:dotProduct(vec2)/(self:size()*vec2:size()))
end

local function posAngle(angle)
  -- angle = math.modf(angle, 360) -- this causes bad jitter
  if(angle < 0) then angle = angle + 360 end
  return angle
end
local col = Color(255,255,255)
local accel, prevAng, timer, timer2
local playerSpeed = Vector2D.new(0,0)
local deltaSpeed = Vector2D.new(0,0)
local playerAccel = Vector2D.new(0,0)
local cGazR = hud.ease_module.new(1, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, "outInCubic")
local cGazL = hud.ease_module.new(1, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, "outInCubic")
local cGazB = hud.ease_module.new(1, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, "outInCubic")

local accelAvg = {
      ["totalAccel"] = 0,
      ["Samples"] = 0,
      ["averageAccel"] = 0
}
local ticksOnGround = 0

function hud.accelmeter()
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  local ply = LocalPlayer()
  local vel = ply:GetVelocity() --ply.SpeedTest
  local eyeAng = ply:EyeAngles().y
  local acceldir = 0
  if eyeAng > 0 then
    eyeAng = 360-eyeAng
    --eyeAng = 360- math.abs(eyeAng)
  else
    eyeAng = 360+ math.abs(eyeAng)
  end

  local backwards = false
  if (vel:GetNormalized()*Vector(1,1,0)):Dot(ply:EyeAngles():Forward()) < 0 then
    backwards = true
  end

  if ply:OnGround() then
    if ticksOnGround > 5 then
      return
    else
      ticksOnGround = ticksOnGround+1
    end
  else
    ticksOnGround = 0
  end
  if prevAng == nil then prevAng = eyeAng end
  if accel == nil then accel = 0 end
    acceldir = vel:Length2D()-playerSpeed:size()
    deltaSpeed:update(vel.x-playerSpeed.x, vel.y-playerSpeed.y)
    playerAccel:update(deltaSpeed.x/1000, deltaSpeed.y/1000)

    accel = playerAccel:size()
    if playerAccel.x < 0 and playerAccel.y < 0 then
      accel = accel * -1
    end
    playerSpeed:update(vel.x, vel.y)

    --accel = ply.accelTest

  local vec_x = Vector2D.new(1,0)
  local vec_nx = Vector2D.new(-1,0)

  local pl_ang = posAngle(eyeAng-180)
  local vel_ang

  if playerSpeed.y >= 0 then
    vel_ang = playerSpeed:angle(vec_nx)
  else
    vel_ang = math.pi + playerSpeed:angle(vec_x)
  end
  if backwards then
    vel_ang = -vel_ang
    pl_ang = -pl_ang
  end

  if ply:KeyDown(IN_BACK) and !(ply:KeyDown(IN_FORWARD) or ply:KeyDown(IN_MOVELEFT) or ply:KeyDown(IN_MOVERIGHT) ) then
    --vel_ang = vel_ang
    pl_ang = pl_ang +180
  elseif ply:KeyDown(IN_MOVELEFT) and !(ply:KeyDown(IN_FORWARD) or ply:KeyDown(IN_BACK)) then
    if backwards then
      vel_ang = vel_ang
      pl_ang = pl_ang -45
    else
      vel_ang = vel_ang +math.rad(90)
      pl_ang = pl_ang +45
    end
  elseif ply:KeyDown(IN_MOVERIGHT) and !(ply:KeyDown(IN_FORWARD) or ply:KeyDown(IN_BACK)) then
    if backwards then
      vel_ang = vel_ang --math.rad(45)
      pl_ang = pl_ang +45
    else
      vel_ang = vel_ang -math.rad(90)
      pl_ang = pl_ang -45
    end
  elseif ply:KeyDown(IN_MOVELEFT) and ply:KeyDown(IN_FORWARD) and !ply:KeyDown(IN_BACK) then
    if backwards then

      vel_ang = vel_ang -math.rad(90)
      pl_ang = pl_ang +180
    end

  elseif ply:KeyDown(IN_MOVERIGHT) and ply:KeyDown(IN_FORWARD) and !ply:KeyDown(IN_BACK) then
    if backwards then
      vel_ang = vel_ang +math.rad(90)
      pl_ang = pl_ang -180
    end
  elseif ply:KeyDown(IN_MOVELEFT) and ply:KeyDown(IN_BACK) and !ply:KeyDown(IN_FORWARD) then
    if !backwards then

      vel_ang = vel_ang -math.rad(90)
      pl_ang = pl_ang +180
    end

  elseif ply:KeyDown(IN_MOVERIGHT) and ply:KeyDown(IN_BACK) and !ply:KeyDown(IN_FORWARD) then
    if !backwards then
      vel_ang = vel_ang +math.rad(90)
      pl_ang = pl_ang -180
    end
  end

  local wishspeed = ply.wishspeedTest or 320
  if (ply.movementDir == 2 || ply.movementDir == 6) then
    --wishspeed = ply:WishSpeed()
  end
  local min_ang = math.asin((wishspeed-accel)/playerSpeed:size())
  local opt_ang = math.acos((wishspeed-accel)/playerSpeed:size())
  local o = math.atan((accel*math.sqrt(math.pow(playerSpeed:size(), 2) - math.pow(wishspeed-accel, 2)))/(math.pow(playerSpeed:size(), 2) + accel*(wishspeed-accel)))
  local a = o/2+opt_ang
  local e = a-math.pi/4
  local min_a = o/2+min_ang
  local min_e = min_a-math.pi/4
  local t_ang_min = vel_ang
  local t_ang_op_m = vel_ang
  local t_ang_op = vel_ang
  local test1 = vel_ang
  local test2 = vel_ang

  if ply:KeyDown(IN_MOVERIGHT) then
    t_ang_op_m = t_ang_op + 1*e
    t_ang_op = t_ang_op + 1.2*e
    test1 = test1 + 1*min_e
    test2 = test2 + 1.2*min_e
  elseif ply:KeyDown(IN_MOVELEFT) then
    t_ang_op_m = t_ang_op - 1*e
    t_ang_op = t_ang_op - 1.2*e
    test1 = test1 - 1*min_e
    test2 = test2 - 1.2*min_e
  elseif ply:KeyDown(IN_BACK) then
    t_ang_op_m = t_ang_op - 1*e
    t_ang_op = t_ang_op - 1.2*e
    test1 = test1 - 1*min_e
    test2 = test2 - 1.2*min_e
  end
    prevAng = eyeAng


  local ang_diff_min = t_ang_min-math.rad(pl_ang)
  local ang_diff_op_m = t_ang_op_m-math.rad(pl_ang)
  local ang_diff_op = t_ang_op-math.rad(pl_ang)
  local ang_diff_test1 = test1-math.rad(pl_ang)
  local ang_diff_test2 = test2-math.rad(pl_ang)

  local radiusC = 100
  local NVG_CW = 2
  local NVG_CCW = 1
  local lineScaleL = 1
  local lineScaleC = 1
  local invertC = false
  local drawAccelLine = true
  local lineH = 10
  local lineHOff = 7

  local lineSize = radiusC
  local dir = NVG_CW
  if ang_diff_min < ang_diff_op then dir = NVG_CW else dir = NVG_CCW end

  if invertC then
    if dir == NVG_CW then dir = NVG_CCW
    else dir = NVG_CW end

    ang_diff_min = ang_diff_min + math.rad(180)
    ang_diff_op_m = ang_diff_op_m + math.rad(180)
    ang_diff_op = ang_diff_op + math.rad(180)
  end

  local cgazA1 = ( radiusC * math.cos( ang_diff_op_m - math.pi / 2 ) ) * lineScaleL
  local cgazA2 = ( radiusC * math.cos( ang_diff_op   - math.pi / 2 ) ) * lineScaleL
  local cgazB1 = ( radiusC * math.cos( ang_diff_min  - math.pi / 2 ) ) * lineScaleL
  local cgazB2 = ( radiusC * math.cos( ang_diff_op   - math.pi / 2 ) ) * lineScaleL
  local cgazC1 = ( radiusC * math.cos( ang_diff_test1  - math.pi / 2 ) ) * lineScaleL
  local cgazC2 = ( radiusC * math.cos( ang_diff_test2   - math.pi / 2 ) ) * lineScaleL
  local cgazD1 = ( radiusC * math.sin( ang_diff_test1  - math.pi / 2 ) ) * lineScaleL
  local cgazD2 = ( radiusC * math.sin( ang_diff_test2   - math.pi / 2 ) ) * lineScaleL

  local guideCircleWidth = 6
  local guideLineWidth = 4

  ang_diff_min = ang_diff_min * lineScaleC
  ang_diff_op_m = ang_diff_op_m * lineScaleC
  ang_diff_op = ang_diff_op * lineScaleC

  local maxWidth = 0
  local maxPos = 0

  local optWidth = 0
  local optPos = 0

  local rWidth = 0
  local rPos = 0
  --if ply:KeyDown(IN_FORWARD) or ply:KeyDown(IN_BACK) then
  if drawAccelLine then

    local cGazRUpdate = cGazR:update(0.1)
    local cGazLUpdate = cGazL:update(0.1)
    local cGazBUpdate = cGazB:update(0.1)
    /*
    if acceldir > 0 then
      col = Color(40, 231, 9, 255)
    elseif acceldir < 0 then
      col = Color(249, 71, 95)
    elseif accel == 0 then
      --col = Color(255,255,255)
    end
    surface.SetDrawColor(col)
    surface.DrawRect(ScrW()/2, ScrH()/2-(lineH/2)+lineHOff+lineH, 10, 5)
    */
    if ( ply:KeyDown(IN_MOVELEFT) and !backwards ) or  ( ply:KeyDown(IN_MOVERIGHT) and backwards) then
      maxWidth = math.abs(math.min(cgazC1-cgazC2, 0))
      maxPos = cgazA1-maxWidth

      optWidth = math.abs(math.max(cgazA1-cgazA2, 0))-1
      optPos = cgazA1-optWidth-1

      rWidth = math.abs(math.max(cgazD1,cgazD2))-1
      rPos = cgazA1-rWidth-1

      // Accel Range
      surface.SetDrawColor(Color(240, 211, 20) )
      surface.DrawRect((ScrW()/2)+cGazL:get().rPos,ScrH()/2-(lineH/2)+lineHOff, cGazL:get().rWidth, lineH)
      // Optimal accel
      surface.SetDrawColor( Color(40, 231, 9, 255) )
      surface.DrawRect((ScrW()/2)+cGazL:get().optPos,ScrH()/2-(lineH/2)+lineHOff, cGazL:get().optWidth, lineH)
      // Max accel
      surface.SetDrawColor( Color(32, 95, 255) )
      surface.DrawRect((ScrW()/2)+cGazL:get().maxPos,ScrH()/2-(lineH/2)+lineHOff, cGazL:get().maxWidth, lineH)
      cGazL:Target({optWidth = optWidth, optPos = optPos, maxWidth = maxWidth, maxPos = maxPos, rWidth = rWidth, rPos = rPos})
    elseif ( ply:KeyDown(IN_MOVERIGHT) and !backwards) or  ( ply:KeyDown(IN_MOVELEFT) and backwards)  then
      maxWidth = math.abs(math.max(cgazC1-cgazC2, 0))
      maxPos = cgazA1

      optWidth = math.abs(math.min(cgazA1-cgazA2, 0))
      optPos = cgazA1

      rWidth = math.abs(math.min(cgazD1,cgazD2))
      rPos = cgazA1

      // Accel Range
      surface.SetDrawColor(Color(240, 211, 20) )
      surface.DrawRect((ScrW()/2)+cGazR:get().rPos,ScrH()/2-(lineH/2)+lineHOff, cGazR:get().rWidth, lineH)
      --surface.DrawRect((ScrW()/2)+cgazA1,ScrH()/2-(lineH/2)+lineHOff, width, lineH)  --math.abs(cgazC1 + cgazA2)+math.min(cgazA1, cgazA2)
      // Optimal accel
      surface.SetDrawColor(Color(40, 231, 9, 255) )
      surface.DrawRect((ScrW()/2)+cGazR:get().optPos,ScrH()/2-(lineH/2)+lineHOff, cGazR:get().optWidth, lineH)
      // Max accel
      surface.SetDrawColor( Color(32, 95, 201) )
      surface.DrawRect((ScrW()/2)+cGazR:get().maxPos,ScrH()/2-(lineH/2)+lineHOff, cGazR:get().maxWidth, lineH)
      cGazR:Target({optWidth = optWidth, optPos = optPos, maxWidth = maxWidth, maxPos = maxPos, rWidth = rWidth, rPos = rPos})

    elseif ( ply:KeyDown(IN_BACK) ) and !(ply:KeyDown(IN_FORWARD) or ply:KeyDown(IN_MOVERIGHT) or ply:KeyDown(IN_MOVELEFT)) then
      /*maxWidth = math.abs(math.max(cgazC1-cgazC2, 0))
      maxPos = cgazA1

      optWidth = math.abs(math.min(cgazA1-cgazA2, 0))
      optPos = cgazA1

      rWidth = math.abs(math.min(cgazA1-cgazA2))
      rPos = cgazA1

      // Accel Range
      surface.SetDrawColor(Color(240, 211, 20) )
      surface.DrawRect((ScrW()/2)+rPos,ScrH()/2-(lineH/2)+lineHOff, rWidth, lineH)
      --surface.DrawRect((ScrW()/2)+cgazA1,ScrH()/2-(lineH/2)+lineHOff, width, lineH)  --math.abs(cgazC1 + cgazA2)+math.min(cgazA1, cgazA2)
      // Optimal accel
      --surface.SetDrawColor(Color(40, 231, 9, 255) )
      --surface.DrawRect((ScrW()/2)+cGazB:get().optPos,ScrH()/2-(lineH/2)+lineHOff, cGazB:get().optWidth, lineH)
      // Max accel
      --surface.SetDrawColor( Color(32, 95, 201) )
      --surface.DrawRect((ScrW()/2)+cGazB:get().maxPos,ScrH()/2-(lineH/2)+lineHOff, cGazB:get().maxWidth, lineH)
      cGazB:Target({optWidth = optWidth, optPos = optPos, maxWidth = maxWidth, maxPos = maxPos, rWidth = rWidth, rPos = rPos})
*/
    end

    if UT:IsNan(maxWidth, maxPos, optWidth, optPos, rWidth, rPos) then
      maxWidth = 0
      maxPos = 0
      optWidth = 0
      optPos = 0
      rWidth = 0
      rPos = 0
      cGazL:Target({optWidth = optWidth, optPos = optPos, maxWidth = maxWidth, maxPos = maxPos, rWidth = rWidth, rPos = rPos})
      cGazR:Target({optWidth = optWidth, optPos = optPos, maxWidth = maxWidth, maxPos = maxPos, rWidth = rWidth, rPos = rPos})
    end
    --surface.DrawRect((ScrW()/2)+math.min(cgazA1, cgazA2),ScrH()/2-(lineH/2)+lineHOff, math.abs(cgazA2 - cgazA1), lineH)
    --surface.DrawRect((ScrW()/2)+math.min(math.abs(cgazA1, cgazA2),ScrH()/2-(lineH/2)+lineHOff, math.abs(cgazA1 - cgazA2), lineH)
  end
  --end
  --surface.SetDrawColor(Color(231, 222, 9) )
  --surface.DrawLine( ScrW()/2, ScrH()/2, (ScrW()/2) + lineSize*math.cos(ang_diff_min-math.pi/2), (ScrH()/2)+lineSize*math.sin(ang_diff_min-math.pi/2) )
  --surface.SetDrawColor(Color(9, 85, 231) )
  --surface.DrawLine( ScrW()/2, ScrH()/2, (ScrW()/2) + lineSize*math.cos(ang_diff_op-math.pi/2), (ScrH()/2)+lineSize*math.sin(ang_diff_op-math.pi/2) )
end
hook.Add("HUDPaint", "Accelmeter test", hud.accelmeter)

function hud.indicator_offscreen()
  /*
  for k, v in pairs (player.GetAll()) do
    if v != LocalPlayer() then
      local       plypos = (v:LocalToWorld(Vector(0,0,73/2))):ToScreen()
      local screenCenter = Vector(ScrW(), ScrH(), 0)/2

      plypos.x = plypos.x-screenCenter.x
      plypos.y = plypos.y-screenCenter.y

      local angle = math.deg(math.atan2(plypos.y, plypos.x))
      angle =  (math.rad(angle-90))
      local cos = math.cos(angle)
      local sin = -math.sin(angle)
      local radius = 10
      local m = cos/sin
      local screenBounds = screenCenter*0.8
      --screenBounds.x =  radius * math.cos(-angle*math.pi/180) * 1
      --screenBounds.y =  radius * math.sin(-angle*math.pi/180) * 1
      plypos.x= screenCenter.x+ (sin*150)
      plypos.y= screenCenter.y+ (cos*150)


      --if then

      --end

      --if plypos.x >screenBounds.x or plypos.x <-screenBounds.x or plypos.y >screenBounds.y or plypos.y <-screenBounds.y then


        if cos >0 then
          plypos.x = screenBounds.y/m
          plypos.y = screenBounds.y
        else
          plypos.x = -screenBounds.y/m
          plypos.y = -screenBounds.y
        end

        if plypos.x >screenBounds.x then
          plypos.x = screenBounds.x
          plypos.y = screenBounds.x*m
        elseif plypos.x <-screenBounds.x then
          plypos.x = -screenBounds.x
          plypos.y = -screenBounds.x*m
        end

        plypos.x = plypos.x+screenCenter.x
        plypos.y = plypos.y+screenCenter.y

        --surface.DrawLine( plypos.x, plypos.y, screenCenter.x, screenCenter.y )
        surface.SetMaterial(Material("materials/trick/triangle.png", "noclamp"))
        angle = math.deg(angle)

        surface.DrawTexturedRectRotated( plypos.x, plypos.y, 15, 15, -angle )

      --end
    end
  end*/
end
hook.Add("HUDPaint", "Indicator offscreen", hud.indicator_offscreen)

hook.Add("PostDrawTranslucentRenderables", "ch", hud.crosshair)
function hud.crosshair()
  if !console.CmdVar("hud_enabled") then
    return
  end
  local ply      = LocalPlayer()
  local speed   = ply:Speed()

  surface.SetDrawColor( 255, 255, 255, 255 )
  surface.SetMaterial(Material("materials/trick/hud/Dot.png", "noclamp"))
  surface.DrawTexturedRect( (ScrW()/2)-1, (ScrH()/2)-1, 2, 2 )
  surface.SetDrawColor( 255, 1, 1, 255 )
  --surface.DrawCircle( ScrW()/2, ScrH()/2, 4, 255, 255, 255, 255 )
  --draw.DrawText(math.Round(speed/10)*10, "HUD Speed", (ScrW()/2)*1.005, (ScrH()/2), Color(255,255,255, 255), TEXT_ALIGN_CENTER)

end

hook.Add("HUDPaint", "ch", hud.crosshair)
-------------------------------------
-- Spectators UI.
-------------------------------------
function hud.spectators()
  local ply = LocalPlayer():GetObserverTarget() or LocalPlayer()
  if !Spectate.Players[ply] then return end
  if #Spectate.Players[ply]["Spectators"] > 0 then
    surface.SetFont( "HUD Spectators" )
    surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )

    local tx, ty = surface.GetTextSize( "Spectators")

    surface.SetTextPos( ScrW()-tx, 0)
    surface.DrawText( "Spectators" )

    for k, v in pairs(Spectate.Players[ply]["Spectators"]) do
      surface.SetFont( "HUD Spectators" )
      surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )

      local tx, ty = surface.GetTextSize( v:Nick() )

      surface.SetTextPos( ScrW()-tx, (k*(ty+1)))
      surface.DrawText( v:Nick() )
    end
  end
end
hook.Add("HUDPaint", "spectators", hud.spectators)


function surface.DrawTexturedRectRotatedPoint( x, y, w, h, rot, x0, y0 )

	local c = math.cos( math.rad( rot ) )
	local s = math.sin( math.rad( rot ) )

	local newx = y0 * s - x0 * c
	local newy = y0 * c + x0 * s

	surface.DrawTexturedRectRotated( x + newx, y + newy, w, h, rot )

end


-------------------------------------
-- Draws a outlined box.
-------------------------------------
function hud.OutlinedBox( x, y, w, h, thickness, color)
    surface.SetDrawColor( color )

    for i=0, thickness  do

        surface.DrawRect(x, y, w, h-h+thickness)
        surface.DrawRect(x, y+h-thickness, w, h-h+thickness)
        surface.DrawRect(x, y, w-w+thickness, h)
        surface.DrawRect(x+w-thickness+.5, y, w-w+thickness, h)
    end

end

-------------------------------------
-- Draws a box.
-------------------------------------
function hud.drawBox(corner, x, y, w, h, color, key, font, txtcolor, enum, keoutline, thickness, outlinecolor)
  local ply = Spectate.IsSpectating(LocalPlayer()) or LocalPlayer()
  surface.SetFont( font )
  local width, height = surface.GetTextSize( key )
  centerW, centerH = x + ( w/2 ), (y + (h/2))-(height/2)

  if  ( keyEnum:KeyDown(ply, enum ) and keoutline == true and outlineecho:GetBool()) then
    --surface.DrawRect(x, y, w, h) maybe
    hud.OutlinedBox(x, y, w, h, thickness, outlinecolor)
  end
  if keyEnum:KeyDown(ply, enum ) and !outlineecho:GetBool() then
    color.a = color.a and Lerp(0.1, color.a, 230) or 230
  else
    color.a = color.a and Lerp(0.1, color.a, hud.color.a) or hud.color.a
  end

  draw.RoundedBox(corner, x, y, w, h, color)
  draw.DrawText( key, font, centerW, centerH, txtcolor, TEXT_ALIGN_CENTER )
end

-------------------------------------
-- KeyEcho UI.
-------------------------------------
function hud.keyEcho()

  if !hudtoggle:GetBool() then return end
    local ply      = Spectate.IsSpectating(LocalPlayer()) or LocalPlayer()
    if console.CmdVar("hud_keyecho") == 2 then

      surface.SetFont( "HUD Echo2" )
      if keyEnum:KeyDown(ply, IN_FORWARD ) then

        local width, height = surface.GetTextSize( "˄" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("˄", "HUD Echo2", centerW, centerH-30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_BACK ) then
        local width, height = surface.GetTextSize( "˅" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("˅", "HUD Echo2", centerW, centerH+30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_MOVERIGHT ) then
        local width, height = surface.GetTextSize( ">" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText(">", "HUD Echo2", centerW+30, centerH, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_MOVELEFT ) then
        local width, height = surface.GetTextSize( "<" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("<", "HUD Echo2", centerW-30, centerH, hud.customColor, TEXT_ALIGN_CENTER)
      end

        surface.SetFont( "HUD Echo2txt" )
      if keyEnum:KeyDown(ply, IN_JUMP ) then

        local width, height = surface.GetTextSize( "J" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("J", "HUD Echo2txt", centerW+30, centerH+30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_DUCK ) then
        local width, height = surface.GetTextSize( "D" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("D", "HUD Echo2txt", centerW-30, centerH+30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_ATTACK ) then
        local width, height = surface.GetTextSize( "M1" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("M1", "HUD Echo2txt", centerW-30, centerH-30, hud.customColor, TEXT_ALIGN_CENTER)
      end

      if keyEnum:KeyDown(ply, IN_ATTACK2 ) then
        local width, height = surface.GetTextSize( "M2" )
        local centerW, centerH = (ScrW()/2), (ScrH()/2)-(height/2)-2
        draw.DrawText("M2", "HUD Echo2txt", centerW+32, centerH-30, hud.customColor, TEXT_ALIGN_CENTER)
      end
    end


end

hook.Add("HUDPaint", "kE", hud.keyEcho)

function CreateGraph( tbl, x, y, width, height, vel, outlineWidth, scrollspeed, outline )
  table.Empty( tbl.Poly )        -- Reset the poly table
  table.Empty( tbl.polyOutline ) -- Reset the poly table
  if tbl ~= nil then
    if #tbl.Points == 0 then -- Fill the table to prevent the graph from standing still in the beginning
      for i = 1, ( width*scrollspeed ) do
        table.insert( tbl.Points, 0 )
      end
    end

    if math.Round((vel*10)/10)*10 >= tbl.Points[#tbl.Points] then
      table.insert( tbl.Points, Lerp( 0.005, tbl.Points[#tbl.Points],math.Round((vel*10)/100)*100 ))  -- Insert a new point
    else -- else if lower than previous point
      table.insert( tbl.Points, Lerp( 0.005, tbl.Points[#tbl.Points],math.Round((vel*10)/100)*100 ))
    end
    if #tbl.Points > ( width*scrollspeed ) then -- remove if the point has reached the end
        table.remove( tbl.Points, 1 )
    end

  end

  -- Have not tested this outside my own 3d hud module, but positive numbers go down in y axis
  table.insert( tbl.Poly, { x = ( x+width/2 ), y = y+height*10 } )         -- This makes it so the graph is drawn from the bottom, but that part is hidden with stencil
  table.insert( tbl.polyOutline, { x = ( x+width/2 ), y = y+height*10 } )

  -- Draw the graph
  for k, v in pairs( tbl.Points ) do
    local barh = math.Round( ( ( v/tbl.topGraph ) )*( height/10 ), 1 )
    table.insert( tbl.Poly, { x = ( x+1 ) + ( k/scrollspeed ), y = y+height-barh, hi = barh } )
    if outline then
      table.insert( tbl.polyOutline, { x = x + ( k/scrollspeed ), y = y+height-barh-1-outlineWidth, hi = barh } )
      table.insert( tbl.polyOutline, { x = ( x+outlineWidth ) + ( k/scrollspeed ), y = y+height-barh-1, hi = barh } )
    end
    if k == 1 then
      table.insert( tbl.Poly, { x = x, y = y+height } ) -- Fills the first poly
      if outline then
        table.insert( tbl.polyOutline, { x = x, y = y+height } )
      end
    end
  end
  table.insert( tbl.Poly, { x = x+width, y = y+height } ) -- Fills the rest of the graph
  table.insert( tbl.polyOutline, { x = x+width, y = y+height } )
end

function draw.Circle( x, y, radius, seg, deg, rot, color )
	local cir = {}
  local c = math.cos( math.rad( rot ) )
	local s = math.sin( math.rad( rot ) )

	table.insert( cir, { x = x, y = y, u = 0.5, v = 0.5 } )
	for i = 0, seg do
		local a = math.rad( rot + ( i / seg ) * -deg )
		table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )
	end

	local a = math.rad( rot ) -- This is needed for non absolute segment counts
	table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )
  surface.SetDrawColor( color.r, color.g, color.b, color.a )
	draw.NoTexture()
	surface.DrawPoly( cir )
end

function draw.SoM(x, y, rad, rot, gear, alpha, active)
    render.ClearStencil()
    render.SetStencilEnable( true )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )

    -- The graph fill
    render.SetStencilReferenceValue( 10 )

      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
      render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
      render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
      render.SetStencilPassOperation( STENCILOPERATION_KEEP )
      draw.Circle( x, y, rad-9, 50, 360, rot, Color(255,255,255,alpha) )
      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_GREATER )
      render.SetStencilFailOperation( STENCILOPERATION_KEEP )
      draw.Circle( x, y, rad, 50, 90, rot, Color(255,255,255,alpha) )
    render.SetStencilEnable(false)

    //Cur Gear
    local a = math.rad( rot+8 )
    local tx, ty = surface.GetTextSize( gear )


    surface.SetFont( "HUD Gear" )
    surface.SetTextColor( Color(255,255,255,alpha) )
    surface.SetTextPos( x+(math.sin( a )*(rad-5))-(tx/2), y+(math.cos( a )*(rad))-(ty/2)  )
    surface.DrawText( gear )
    a = math.rad( rot )
    surface.DrawTexturedRectRotated( x+(math.sin( a )*(rad-5)), y+(math.cos( a )*(rad)), 40, 5, rot-90 )

    //Next gear
    a = math.rad( (rot-8) + 1 * -90  )
    tx, ty = surface.GetTextSize( gear+1  )


    surface.SetTextPos( x+(math.sin( a )*(rad))-(tx/2), y+(math.cos( a )*(rad-5))-(ty/2) )
    surface.DrawText( gear+1 )
    a = math.rad( rot + 1 * -90 )
    surface.DrawTexturedRectRotated( x+(math.sin( a )*(rad)), y+(math.cos( a )*(rad-5)), 40, 5, rot )

    /*
    for i=0, 10 do
      local a = math.rad( rot + ( i / 10 ) * -90 )

      surface.DrawTexturedRectRotated( x+ math.sin( a )*(rad-5), y+ math.cos( a )*(rad-5), 40, 5, 90/ )
    end*/
end

function hud.keyEchoes( ply, x, y, w, h, thickness)
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  //KeyEcho
  local kE_w, kE_h = w, h
  local V_ROW_1, V_ROW_2, V_ROW_3 = y-kE_h+thickness, y-(kE_h*2), y-(kE_h*3)-thickness
  local H_ROW_1, H_ROW_2, H_ROW_3 = x+thickness, x+((kE_w))+(thickness*2), x+((kE_w*2))+(thickness*3)

  if console.CmdVar("hud_keyecho") == 1 then
    kE_TOP = kE_TOP and math.Round(UT:Hermite(0.1, kE_TOP, (kE_h*3)+(thickness*2)),2) or 0
    kE_MID_2_U = kE_MID_2_U and math.Round(UT:Hermite(0.1, kE_MID_2_U, (kE_h*2)+(thickness*2)),2) or -kE_h
    if kE_TOP >= (kE_h) then
      kE_ROW_1 = kE_ROW_1 and math.Round(UT:Hermite(1, kE_ROW_1, kE_h*1),2) or 0
    end
    if kE_TOP >= (kE_h*2)+thickness then
      kE_ROW_2 = kE_ROW_2 and math.Round(UT:Hermite(1, kE_ROW_2, (kE_h*2)+thickness),2) or 0
    end
    if kE_TOP >= (kE_h/1.5) then
      kE_DUCK_A = kE_DUCK_A and math.Round(UT:Hermite(0.1, kE_DUCK_A, 255),2) or 0
      kE_JUMP_A = kE_JUMP_A and math.Round(UT:Hermite(0.1, kE_JUMP_A, 255),2) or 0
    end

    if kE_TOP >= (kE_h)+(kE_h/1.5) then
      kE_A_A = kE_A_A and math.Round(UT:Hermite(0.1, kE_A_A, 255),2) or 0
      kE_S_A = kE_S_A and math.Round(UT:Hermite(0.1, kE_S_A, 255),2) or 0
      kE_D_A = kE_D_A and math.Round(UT:Hermite(0.1, kE_D_A, 255),2) or 0
    end
    if kE_TOP >= (kE_h*2)+(kE_h/1.5) then
      kE_W_A = kE_W_A and math.Round(UT:Hermite(0.1, kE_W_A, 255),2) or 0
      kE_M1_A = kE_M1_A and math.Round(UT:Hermite(0.1, kE_M1_A, 255),2) or 0
      kE_M2_A = kE_M2_A and math.Round(UT:Hermite(0.1, kE_M2_A, 255),2) or 0
    end

    if console.CmdVar("hud_keyecho_press") == 0 then
      for k, v in pairs(keyEnum.keys) do
        if math.Round(kE_TOP) == math.Round((kE_h*3)+(thickness*2)) then
          if !kE_Flash then
            kE_W_PRESS = 255
            kE_A_PRESS = 255
            kE_S_PRESS = 255
            kE_D_PRESS = 255
            kE_M1_PRESS = 255
            kE_M2_PRESS = 255
            kE_DUCK_PRESS = 255
            kE_JUMP_PRESS = 255
            kE_Flash = true
          else
            if k == "W" then
              kE_W_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_W_PRESS, 100) or UT:Hermite(0.25, kE_W_PRESS, 0)
            elseif k == "A" then
              kE_A_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_A_PRESS, 100) or UT:Hermite(0.25, kE_A_PRESS, 0)
            elseif k == "S" then
              kE_S_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_S_PRESS, 100) or UT:Hermite(0.25, kE_S_PRESS, 0)
            elseif k == "D" then
              kE_D_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_D_PRESS, 100) or UT:Hermite(0.25, kE_D_PRESS, 0)
            elseif k == "M1" then
              kE_M1_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_M1_PRESS, 100) or UT:Hermite(0.25, kE_M1_PRESS, 0)
            elseif k == "M2" then
              kE_M2_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_M2_PRESS, 100) or UT:Hermite(0.25, kE_M2_PRESS, 0)
            elseif k == "DUCK" then
              kE_DUCK_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_DUCK_PRESS, 100) or UT:Hermite(0.25, kE_DUCK_PRESS, 0)
            elseif k == "JUMP" then
              kE_JUMP_PRESS = keyEnum:KeyDown(ply, v ) and UT:Hermite(0.1, kE_JUMP_PRESS, 100) or UT:Hermite(0.25, kE_JUMP_PRESS, 0)
            end
          end
        end
      end
      local col = hud.customColor2
      if math.Round(kE_TOP) == math.Round((kE_h*3)+(thickness*2)) then
        // M1
        surface.SetDrawColor( Color(col.r, col.g, col.b, kE_M1_PRESS) )
        surface.DrawRect(H_ROW_1, V_ROW_3, kE_w, kE_h)
        // W
        surface.SetDrawColor( Color(col.r, col.g, col.b, kE_W_PRESS) )
        surface.DrawRect(H_ROW_2, V_ROW_3, kE_w, kE_h)
        // M2
        surface.SetDrawColor( Color(col.r, col.g, col.b, kE_M2_PRESS) )
        surface.DrawRect(H_ROW_3, V_ROW_3, kE_w, kE_h)
        // A
        surface.SetDrawColor( Color(col.r, col.g, col.b, kE_A_PRESS) )
        surface.DrawRect(H_ROW_1, V_ROW_2, kE_w, kE_h)
        // S
        surface.SetDrawColor( Color(col.r, col.g, col.b, kE_S_PRESS) )
        surface.DrawRect(H_ROW_2, V_ROW_2, kE_w, kE_h)
        // D
        surface.SetDrawColor( Color(col.r, col.g, col.b, kE_D_PRESS) )
        surface.DrawRect(H_ROW_3, V_ROW_2, kE_w, kE_h)
        // DUCK
        surface.SetDrawColor( Color(col.r, col.g, col.b, kE_DUCK_PRESS) )
        surface.DrawRect(H_ROW_1, V_ROW_1, kE_w, kE_h)
        // JUMP
        surface.SetDrawColor( Color(col.r, col.g, col.b, kE_JUMP_PRESS) )
        surface.DrawRect(H_ROW_2, V_ROW_1, kE_w*2+thickness, kE_h)
      end
    elseif console.CmdVar("hud_keyecho_press") == 1 then

    end
  elseif console.CmdVar("hud_keyecho") == 0 then
    kE_TOP = kE_TOP and math.Round(UT:Hermite(0.1, kE_TOP, 0),2) or 0
    kE_MID_2_U = kE_MID_2_U and math.Round(UT:Hermite(0.1, kE_MID_2_U, -kE_h),2) or -kE_h
    if kE_TOP <= (kE_h) then
      kE_ROW_1 = kE_ROW_1 and math.Round(UT:Hermite(1, kE_ROW_1, 0),2) or 0
    end
    if kE_TOP <= (kE_h*2)+thickness then
      kE_ROW_2 = kE_ROW_2 and math.Round(UT:Hermite(1, kE_ROW_2, 0),2) or 0
    end
    if kE_TOP <= (kE_h) then
      kE_DUCK_A = kE_DUCK_A and math.Round(UT:Hermite(0.25, kE_DUCK_A, 0),2) or 0
      kE_JUMP_A = kE_JUMP_A and math.Round(UT:Hermite(0.25, kE_JUMP_A, 0),2) or 0
    end
    if kE_TOP <= (kE_h)+(kE_h) then
      kE_A_A = kE_A_A and math.Round(UT:Hermite(0.25, kE_A_A, 0),2) or 0
      kE_S_A = kE_S_A and math.Round(UT:Hermite(0.25, kE_S_A, 0),2) or 0
      kE_D_A = kE_D_A and math.Round(UT:Hermite(0.25, kE_D_A, 0),2) or 0
    end
    if kE_TOP <= (kE_h*2)+(kE_h) then
      kE_W_A = kE_W_A and math.Round(UT:Hermite(0.25, kE_W_A, 0),2) or 0
      kE_M1_A = kE_M1_A and math.Round(UT:Hermite(0.25, kE_M1_A, 0),2) or 0
      kE_M2_A = kE_M2_A and math.Round(UT:Hermite(0.25, kE_M2_A, 0),2) or 0
    end
    if kE_Flash then
      kE_W_PRESS = kE_W_PRESS and UT:Hermite(0.25, kE_W_PRESS, 0) or 0
      kE_A_PRESS = kE_A_PRESS and UT:Hermite(0.25, kE_A_PRESS, 0) or 0
      kE_S_PRESS = kE_S_PRESS and UT:Hermite(0.25, kE_S_PRESS, 0) or 0
      kE_D_PRESS = kE_D_PRESS and UT:Hermite(0.25, kE_D_PRESS, 0) or 0
      kE_M1_PRESS = kE_M1_PRESS and UT:Hermite(0.25, kE_M1_PRESS, 0) or 0
      kE_M2_PRESS = kE_M2_PRESS and UT:Hermite(0.25, kE_M2_PRESS, 0) or 0
      kE_DUCK_PRESS = kE_DUCK_PRESS and UT:Hermite(0.25, kE_DUCK_PRESS, 0) or 0
      kE_JUMP_PRESS = kE_JUMP_PRESS and UT:Hermite(0.25, kE_JUMP_PRESS, 0) or 0
      kE_Flash = false
    end
  end
  surface.SetDrawColor( hud.color )
  if kE_TOP > 0 then
    --surface.DrawRect(x, y-thickness/2, (kE_w*3), thickness)                     -- BOTTOM
    surface.DrawRect(x, y-(kE_TOP or 0), (kE_w*3)+(thickness*3), thickness)              -- TOP
    surface.DrawRect(x, y-(kE_ROW_1 or 0), (kE_w*3)+(thickness*3), thickness)            -- ROW_1
    surface.DrawRect(x, y-(kE_ROW_2 or 0), (kE_w*3)+(thickness*3), thickness)            -- ROW_2
    surface.DrawRect(x, y, thickness, -(kE_TOP or 0))                                    -- LEFT
    surface.DrawRect(x+(kE_w*3)+(thickness*3), y, thickness, -(kE_TOP or 0))             -- RIGHT
    surface.DrawRect(x+kE_w+thickness, y, thickness, -(kE_TOP or 0))                     -- MID_1 Up
    if kE_TOP >= (kE_h) then
      surface.DrawRect(x+(kE_w*2)+(thickness*2), y-kE_h, thickness, -kE_MID_2_U)  -- MID_2 Up
    end
  end

  SetFont("HUD Echo", {
    font = "Ostrich Sans",
    weight = 700,
    size = (kE_w/2.5),
    antialias = true,

  })

  local kE_keyW, kE_keyH = surface.GetTextSize( "W" )
  local kE_keyCW, kE_keyCH = (H_ROW_2+(kE_w/2))-(kE_keyW/2), (V_ROW_3+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_W_A) )
  surface.DrawText( "W" )

  kE_keyW, kE_keyH = surface.GetTextSize( "M1" )
  kE_keyCW, kE_keyCH = (H_ROW_1+(kE_w/2))-(kE_keyW/2), (V_ROW_3+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_M1_A) )
  surface.DrawText( "M1" )

  kE_keyW, kE_keyH = surface.GetTextSize( "M2" )
  kE_keyCW, kE_keyCH = (H_ROW_3+(kE_w/2))-(kE_keyW/2), (V_ROW_3+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_M2_A) )
  surface.DrawText( "M2" )

  kE_keyW, kE_keyH = surface.GetTextSize( "S" )
  kE_keyCW, kE_keyCH = (H_ROW_2+(kE_w/2))-(kE_keyW/2), (V_ROW_2+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_S_A) )
  surface.DrawText( "S" )

  kE_keyW, kE_keyH = surface.GetTextSize( "A" )
  kE_keyCW, kE_keyCH = (H_ROW_1+(kE_w/2))-(kE_keyW/2), (V_ROW_2+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_A_A) )
  surface.DrawText( "A" )

  kE_keyW, kE_keyH = surface.GetTextSize( "D" )
  kE_keyCW, kE_keyCH = (H_ROW_3+(kE_w/2))-(kE_keyW/2), (V_ROW_2+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_D_A) )
  surface.DrawText( "D" )

  kE_keyW, kE_keyH = surface.GetTextSize( "DUCK" )
  kE_keyCW, kE_keyCH = (H_ROW_1+(kE_w/2))-(kE_keyW/2), (V_ROW_1+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_DUCK_A) )
  surface.DrawText( "DUCK" )

  kE_keyW, kE_keyH = surface.GetTextSize( "JUMP" )
  kE_keyCW, kE_keyCH = (H_ROW_2+((kE_w*2)/2))-(kE_keyW/2), (V_ROW_1+(kE_h/2))-(kE_keyH/2)
  surface.SetTextPos( kE_keyCW, kE_keyCH )
  surface.SetTextColor( Color(hud.color.r, hud.color.g, hud.color.b, kE_JUMP_A) )
  surface.DrawText( "JUMP" )

end

-------------------------------------
-- Left Side UI.
-------------------------------------

function leftSide:Draw3D(ply, scrw, scrh)
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  local localPly = Spectate.IsSpectating(ply) or ply
  local x, y     = 0, scrh-200      // X and Y position
  local w, h     = -scrw, 5            // Width and Height
  local hp       = math.Clamp(ply:Health(), 0, 100)
  local hpPos    = x
  local hpWidth  = hud.ease.HP.Ease:get().W
  local hpHeight = 20
  local alpha = hud.ease.HP.Ease:get().alpha
  local color = ColorAlpha(hud.customColor, alpha)
  // BackDraw
  --PrintTable(hud.ease.HP.Ease)

  local a = hud.ease.HP.Ease:update(1*FrameTime())

  if hp <= 99 then
    hud.ease.HP.FadeTime = CurTime()+5
  end
  if hud.ease.HP.FadeTime <= CurTime() and hp == 100 then
    hud.ease.HP.W = 0
    hud.ease.HP.Alpha = 0
    --if a and hud.ease.HP.Ease.target.alpha != 0 then
      --hud.ease.HP.Ease:reset()
    --end
  elseif hp < 100 or hud.ease.HP.FadeTime > CurTime() then
    hud.ease.HP.W = 500
    hud.ease.HP.Alpha = 255

  end
  hud.ease.HP.Ease:Target({alpha = hud.ease.HP.Alpha, W = hud.ease.HP.W})


  surface.SetDrawColor( Color(0,0,0,math.Clamp(alpha, 0, 100) ))
  surface.DrawRect( x, y+(h/2)-(hpHeight/2), -hpWidth, hpHeight )
  hpHeight    = 10
  local healthWidth    = hp*(hpWidth/100)-5
  hpBar = hpBar and UT:Hermite(0.075, hpBar, healthWidth) or healthWidth
  surface.SetDrawColor( color )
  surface.DrawRect( hpPos, y+(h/2)-(hpHeight/2), -hpBar, hpHeight )

  local delaycol = Color(color.r, color.g, color.b, math.Clamp(color.a - 105, 0 ,255))
  surface.SetDrawColor( delaycol )
  delayedHpBar = delayedHpBar and UT:Hermite(0.06, delayedHpBar, healthWidth) or healthWidth
  surface.DrawRect( hpPos, y+(h/2)-(hpHeight/2), -delayedHpBar, hpHeight )

  SetFont("HUD Health", {
    font = "Ostrich Sans",
    weight = 600,
    size = 75,
    antialias = true
  })
  surface.SetTextColor( color )
  local hpX, hpY = surface.GetTextSize( math.Round( hp ) )
  surface.SetTextPos( math.Clamp(hpPos-(hpWidth/2)-(hpX/2), -9999, -hpX),  y-hpY-10)
  surface.DrawText( math.Round( hp ) )

  surface.SetDrawColor( hud.color )


end

function cgaz:Draw3D(ply, scrw, scrh)
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  local localPly = Spectate.IsSpectating(ply) or ply
  local x, y     = -500, scrh-200      // X and Y position
  local w, h     = -scrw, 5            // Width and Height
  local bar_width     = 1000
  local bar_height    = 20
  local color = ColorAlpha(hud.customColor, alpha)
  surface.SetDrawColor( Color(0,0,0,100) )
  surface.DrawRect( x, y+(h/2)+(bar_height/2), bar_width, bar_height )

  if ( ply:KeyDown(IN_MOVERIGHT) and !backwards) or  ( ply:KeyDown(IN_MOVELEFT) and backwards)  then
      local maxWidth = cGazR:get().maxWidth
      local maxPos = cGazR:get().maxPos

      local optWidth = cGazR:get().optWidth
      local optPos = cGazR:get().optPos

      local rWidth = cGazR:get().rWidth
      local rPos = cGazR:get().rPos

      // Accel Range
      surface.SetDrawColor(Color(240, 211, 20) )
      surface.DrawRect( x+( bar_width / 2 ) + rPos, y+(h/2)+(bar_height/2), rWidth, bar_height)
      // Optimal accel
      surface.SetDrawColor(Color(40, 231, 9, 255) )
      surface.DrawRect( x+( bar_width / 2 ) + optPos, y+(h/2)+(bar_height/2), optWidth, bar_height)
      // Max accel
      surface.SetDrawColor( Color(32, 95, 201) )
      surface.DrawRect( x+( bar_width / 2 ) + maxPos, y+(h/2)+(bar_height/2), maxWidth, bar_height)
  end
end

-------------------------------------
-- Right Side UI.
-------------------------------------
function rightSide:Draw3D(ply, scrw, scrh)
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  local localPly = Spectate.IsSpectating(ply) or ply
  local x, y        = 0, scrh-200      // X and Y position
  local w, h        = scrw, 5            // Width and Height
  local vel         = ply:Speed2D()
  local velPos      = x
  local vBWidth     = 500
  local vBHeight    = 20
  local alpha = 255
  local color = ColorAlpha(hud.customColor, alpha)
  local kEPos       = x+w-(95*3)-(h*4)
  local kE_w, kE_h = 95, 95

  local a = hud.ease.vel.Ease:update(1*FrameTime())

  if vel >= 1 then
    hud.ease.vel.FadeTime = CurTime()+2.5
  end
  if hud.ease.vel.FadeTime <= CurTime() and vel == 0 then
    hud.ease.vel.W = 0
    hud.ease.vel.Alpha = 0
  elseif vel > 0 or hud.ease.vel.FadeTime > CurTime() then
    hud.ease.vel.W = vBWidth
    hud.ease.vel.Alpha = alpha

  end
  hud.ease.vel.Ease:Target({alpha = hud.ease.vel.Alpha, W = hud.ease.vel.W})

  vBWidth = hud.ease.vel.Ease:get().W
  alpha = hud.ease.vel.Ease:get().alpha
  color = ColorAlpha(hud.customColor, alpha)

  // Default line
  surface.SetDrawColor( Color(0,0,0,100) )
  surface.DrawRect( x, y+(h/2)-(vBHeight/2), vBWidth, vBHeight )
  --surface.DrawRect( x, y, w, h )

  if velo:GetInt() == 1 then
    vel = ply:Speed()
  end
  vBWidth     = vBWidth-5
  vBHeight    = vBHeight-10
  local TSS = math.Clamp(localPly:TSS(), 1000, localPly:TSS())
  local maxVelWidth = (vBWidth)/TSS
  local velWidth    = math.Clamp(vel,0, TSS)*maxVelWidth
  local hundVel = math.floor((math.floor( vel/50 )*50)/100)
  velBar = velWidth or UT:Hermite(0.25, velBar, velWidth) and velBar
  surface.SetDrawColor( color )
  surface.DrawRect( velPos, y+(h/2)-(vBHeight/2), velBar, vBHeight )

  local delaycol = Color(color.r, color.g, color.b, math.Clamp(color.a - 105, 0 ,255))
  surface.SetDrawColor( delaycol )
  delayedvelBar = delayedvelBar and math.Clamp(UT:Hermite(0.15, delayedvelBar, velWidth), velBar, 999) or velWidth
  surface.DrawRect( velPos, y+(h/2)-(vBHeight/2), delayedvelBar, vBHeight )

  SetFont("HUD Speed", {
    font = "Ostrich Sans",
    weight = 600,
    size = 75,
    antialias = true
  })
  surface.SetTextColor( color )
  local velX, velY = surface.GetTextSize( math.Round( vel ) )
  surface.SetTextPos( velPos+(vBWidth/2)-(velX/2),  y-velY-10)
  surface.DrawText( math.Round( vel ) )

  hud.keyEchoes(ply, kEPos, y, kE_w, kE_h, h)
  // Aestethics

  surface.SetDrawColor( Color(0,0,0,200) )
  surface.DrawRect( x-5, y-15-4, 10, 60+8 )
  surface.SetDrawColor( hud.color )
  surface.DrawRect( x-2, y-15, 4, 60 )

end

-------------------------------------
-- Top UI.
-------------------------------------
function top:Draw3D(ply, scrw, scrh)
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  local x, y      = -scrw+200, -scrh+100      // X and Y position
  local w, h      = ((scrw+100)*2), 5            // Width and Height
  local localPly = Spectate.IsSpectating(LocalPlayer()) or ply
  local combo = localPly:Combo()
  local pts = Combo:Calc(combo)
  local rank = calcRank(localPly:Points())
  // Default line
  surface.SetDrawColor( hud.customColor )
  --surface.DrawRect( x, y, w, h )
  if lobby:getPlayerLobby(localPly) != "None" then


    local teams = hud.Team
    local player = lobby:getPlayerLobby(localPly).minigamePlayers[localPly]
    local teams = lobby:getPlayerLobby(localPly).minigameTeams[player]
    local teamCol = lobby:getPlayerLobby(localPly).minigameColors[player]
    SetFont("HUD Team", {
      font = "Ostrich Sans",
      weight = 600,
      size = 75,
      antialias = true
    })
    //PrintTable(lobby:getPlayerLobby(localPly).minigamePlayers)
    local txtx, txty = surface.GetTextSize( teams )
    surface.SetTextColor( teamCol )
    surface.SetTextPos( x+ScrW()*2-(txtx/2),  y)
    surface.DrawText( teams )
  end

  SetFont("HUD Rank", {
    font = "SIMPLIFICA",
    weight = 1000,
    size = 60,
    antialias = true
  })
  local txtx, txty = surface.GetTextSize( rank )
  surface.SetTextColor( rankColor(rank) )
  surface.SetTextPos( x,  y-txty)
  surface.DrawText( rank )
  SetFont("HUD Combo", {
    font = "SIMPLIFICA",
    weight = 1000,
    size = 55,
    antialias = true
  })
  local combos = {}
  for i=#combo, 1, -1 do

    if #combo > 0 then

      local line = table.GetKeys( combo[i] )[1]
      local prev = ""
      if #combos > 0 then

        prev = table.GetKeys( combos[#combos] )[1]
      end
      if prev != line then
        combos[ #combos+1] = {[line] = 1}
      else

        combos[#combos] = {[line] = combos[#combos][line]+1}
      end
      if table.Count(combos) >= 5 then
        break
      end
    end
  end
  --PrintTable(combos)
  for k, line in pairs(combos) do
    local comboName = table.GetKeys( line )[1]
    local repeats = line[comboName]
    local txtx, txty = surface.GetTextSize( comboName )
    surface.SetTextColor( hud.customColor )
    surface.SetTextPos( x,  y+((10+txty)*k)-txty)
    surface.DrawText( repeats .. "X " .. comboName )
  end
  if #combo > 0 then
    local txtx, txty = surface.GetTextSize( pts )
    surface.SetTextColor( hud.customColor )
    surface.SetTextPos( x,  y+((10+txty)*math.min(#combos+1, 6))-txty)
    surface.DrawText( "PTS: " .. math.Round(pts+ply:StrafePts(),2) )

    local timer = ply:ComboTimer()
    local txtx, txty = surface.GetTextSize( timer )
    surface.SetTextColor( hud.customColor )
    surface.SetTextPos( x,  y+((10+(txty))*math.min(#combos+1, 6)))
    surface.DrawText( "Time: " .. math.Round(math.max(timer-CurTime(), 0 ),2) )
  end
end
--[=[
-------------------------------------
-- Speedometer UI.
-------------------------------------
SpeedoMeter = {}

local blur = Material("pp/blurscreen")
function speedDisplay:Draw3D(ply, w, h)
  local x      = -910       -- x position of the panel
  local y      = 310        -- y position of the panel
  local localPly = ply
  local plyLobby = lobby:getPlayerLobby(ply)
  /*
  if plyLobby != "None" then
    hud.customColor = plyLobby["minigameColors"][plyLobby["minigamePlayers"][ply]]
  else
    hud.customColor = Color(255,255,255)
  end*/
  if ply:GetObserverTarget() then
    ply = ply:GetObserverTarget()
  end
  local vel    = ply:Speed()
  local height = 70                           -- height of the panel
  local width  = 200                          -- width of the panel

  if !hudtoggle:GetBool() then return end
  local scrollspeed = 4                       -- How fast the graph scrolls, higher = slower
  local topGraph = 100                        -- Same as self.topGraph Decides how many units/10 you can fit in the the graph
  local fillCol = Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, 200 )      -- fill color

  if sGraph:GetBool() then

  	--surface.DrawTexturedRect( 25, 25, 100, 100 )
    /*
    local gear = 1
    if vel < 200 then
      curGear = 1
    elseif vel >= 200 then
      curGear = 2
    elseif vel >= 300 then
      curGear = 3
    elseif vel >= 400 then
      curGear = 4
    elseif vel >= 500 then
      curGear = 5
    end
    x = x+150
    if !spinning or math.Round(spinning) >= 360 then
      spinning = 0
    end
    spinning = Lerp( 0.0005, math.Round(spinning, 1), 360*2 ) -- Slowly increase graph height

    draw.SoM(x, y, 200, -90, 1, 255, curGear==1)
    draw.SoM(x, y, 175, -205, 2, 20, curGear==2)
    draw.SoM(x, y, 150, -310, 3, 15, curGear==3)
    draw.SoM(x, y, 125, -70, 4, 10, curGear==4)
    draw.SoM(x, y, 100, -190, 5, 5, curGear==5)*/
    --
    if !SpeedoMeter.Points then
      SpeedoMeter.Points = {}                    -- All the points in the graph which stores speed per point.
      SpeedoMeter.Poly = {}                      -- The graph poly
      SpeedoMeter.polyOutline = {}               -- Graph poly outline
      SpeedoMeter.topGraph = 100                 -- The number equals the speed which is required to reach the top of the graph
    end

    if localPly:TSS() and localPly:TSS() > 100 then -- If topspeed exists
      SpeedoMeter.topGraph = Lerp( 0.01, SpeedoMeter.topGraph, math.Round( localPly:TSS()/50 )*50 ) -- Slowly increase graph height
    end

    if velo:GetInt() == 1 then
      vel = ply:Speed2D()
    end

    CreateGraph( SpeedoMeter, x, y, width, height, vel, outlineWidth, scrollspeed, outline )

    // Graph Background
    surface.SetDrawColor( hud.color )
    surface.DrawRect( x, y, width, height )

    -- Stencil start
    render.ClearStencil()
    render.SetStencilEnable( true )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )

    -- The graph fill
    render.SetStencilReferenceValue( 10 )


      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
      render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
      render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
      render.SetStencilPassOperation( STENCILOPERATION_KEEP )

    render.SetBlend( 0 )

      surface.SetDrawColor( Color(255,0,0,255) )
      draw.NoTexture()
      surface.DrawPoly( SpeedoMeter.Poly )


    render.SetBlend( 1 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
    render.SetStencilFailOperation( STENCILOPERATION_KEEP )

    --surface.DrawRect( x, y, width, height )
    /* -- This turned out to be some cool looking waveform
    local i = 1
    local gradients = 20
    for i = 1, gradients do
      surface.DrawRect( x+i*gradients, y, width/gradients, height )
    end*/
    /*
    local i = 1
    local smooth = 100
    local normalAlpha = 255
    local rate = normalAlpha/(smooth/6) //Full alpha at about 1/6th of the way

    for i = 1, smooth do
      if i <= smooth/2 then
        surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, math.Clamp(rate*i, 0, normalAlpha ) ) )
      else

        surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, math.Clamp(rate*(smooth-i), 0, normalAlpha ) ) )
      end
      surface.DrawRect( x+((width/smooth)*(i)), y, width/smooth, height )
    end*/

    local normalAlpha = 120
    local segments = 1
    local smooth = 20


    for i = 1, smooth, segments do
      local alpha = 0
      local rate = normalAlpha/(width/(smooth*segments))
      local length = math.Clamp(width-(i*2), 0, width-(i*2))

      surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, rate) )
      surface.DrawRect( x+i  , y, length, height )
    end

    render.SetStencilReferenceValue( 11 )
    render.SetStencilWriteMask( 255  )
    render.SetStencilTestMask( 255 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )

    render.SetBlend( 0 )

      surface.SetDrawColor( Color(255,0,0,255) )
      draw.NoTexture()
      surface.DrawPoly( SpeedoMeter.Poly )


    render.SetBlend( 1 )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_GREATER )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )

    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( hud.customColor2 )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )
    /*
    render.SetStencilReferenceValue( 11 )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
    render.SetStencilFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilZFailOperation( STENCILOPERATION_REPLACE )
    render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( Color(hud.customColor.r-225, hud.customColor.g-225, hud.customColor.b-225, 255) )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )*/

  render.SetStencilEnable(false)


else
    // Graph Background
    surface.SetDrawColor( hud.color )
    surface.DrawRect( x, y, width, height )
    surface.SetDrawColor( hud.customColor )
    surface.DrawRect( x, y+height, width, 1 )


        -- Stencil start
    render.ClearStencil()
    render.SetStencilEnable( true )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )

    -- The graph fill
    render.SetStencilReferenceValue( 10 )


    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )


    local normalAlpha = 120
    local segments = 1
    local smooth = 20

    local maxVelWidth = (width)/math.Clamp(localPly:TSS(), 100, localPly:TSS())
    local velWidth    = math.Clamp(vel,0, localPly:TSS())*maxVelWidth
    velBar = velWidth or Lerp(0.25, velBar, velWidth) and velBar
    --velAlpha = velAlpha and Lerp(0.25, velBar, velWidth) or velWidth

    for i = 1, smooth, segments do
      local alpha = 0
      local rate = normalAlpha/(width/(smooth*segments))
      local length = math.Clamp(velBar-(i*2), 0, velBar-(i*2))


      /*
      if i <= width/2 then
         alpha = Lerp(0.25, math.Clamp(rate*(i-1), 0, normalAlpha ), math.Clamp(rate*i, 0, normalAlpha ))
      else

        alpha = Lerp(0.25, math.Clamp(rate*(width-(i-1)), 0, normalAlpha ), math.Clamp(rate*(width-i), 0, normalAlpha ) )
      end*/


      surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, rate) )
      surface.DrawRect( x+i  , y, length, height )
    end
    --surface.DrawRect( x , y, vel*maxVelWidth, height )


render.SetStencilReferenceValue( 11 )
    render.SetStencilWriteMask( 255  )
    render.SetStencilTestMask( 255 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )

    render.SetBlend( 0 )

      surface.SetDrawColor( Color(255,0,0,255) )
      draw.NoTexture()
      surface.DrawRect( x, y,velBar , height )


    render.SetBlend( 1 )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_GREATER )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( hud.customColor2 )
    local tx, ty = surface.GetTextSize( math.Round( vel ) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round( vel ) )
  render.SetStencilEnable(false)
end
    surface.SetDrawColor( hud.customColor2 )
    --surface.DrawRect( x, y+height-3, width, 3 )
  /* --UNUSED
    -- The graph outline
    if outline then
      render.SetStencilReferenceValue( 10 )
      render.SetStencilFailOperation( STENCILOPERATION_KEEP )
      render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
      render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )

      render.SetBlend( 0 )
        surface.SetDrawColor( Color(0,0,0,1) )
        draw.NoTexture()
        surface.DrawPoly( SpeedoMeter.polyOutline )

        render.SetStencilPassOperation( STENCILOPERATION_ZERO )
        render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
        surface.SetDrawColor( Color(0,0,0,1) )
        draw.NoTexture()
        surface.DrawPoly( SpeedoMeter.Poly )

      render.SetBlend( 1 )
      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
      surface.SetDrawColor( outlineCol )
      surface.DrawRect( x+2, y, width-4, height )
    end*/
end

HPWave = {}
prevHP = 0
function healthDisplay:Draw3D(ply, w, h)
  if !hudtoggle:GetBool() then return end
  local height = 80              -- height of the panel
  local width  = 200             -- width of the panel
  local x      = -910            -- x position of the panel
  local y      = 310+height-10      -- y position of the panel
  local localPly = ply
  local scrollspeed = 4
  local outlineWidth = 2
  if ply:GetObserverTarget() then
    ply = ply:GetObserverTarget()
  end
  local hp    = Lerp(0.1, prevHP, ply:Health())
  prevHP = hp
  local fillCol = Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, 200 )      -- fill color



    surface.SetDrawColor( hud.color )
    surface.DrawRect( x, y, width, height )

        -- Stencil start
    render.ClearStencil()
    render.SetStencilEnable( true )
    render.SetStencilWriteMask( 255 )
    render.SetStencilTestMask( 255 )

    -- The graph fill
    render.SetStencilReferenceValue( 10 )


    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )


    local normalAlpha = 120
    local segments = 1
    local smooth = 20
    local maxHPWidth = (width)/100
    local hpWidth    = math.Clamp(hp,0, 100)*maxHPWidth
    hpBar = hpWidth or Lerp(0.25, hpBar, hpWidth) and hpBar

    for i = 1, smooth, segments do
      local alpha = 0
      local rate = normalAlpha/(width/(smooth*segments))
      local length = math.Clamp(hpBar-(i*2), 0, hpBar-(i*2))

      surface.SetDrawColor( Color( hud.customColor.r, hud.customColor.g, hud.customColor.b, rate) )
      surface.DrawRect( x+i  , y, length, height )
    end
    --surface.DrawRect( x , y, vel*maxVelWidth, height )


render.SetStencilReferenceValue( 11 )
    render.SetStencilWriteMask( 255  )
    render.SetStencilTestMask( 255 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
    render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
    render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
    render.SetStencilPassOperation( STENCILOPERATION_KEEP )

    render.SetBlend( 0 )

      surface.SetDrawColor( Color(255,0,0,255) )
      draw.NoTexture()
      surface.DrawRect( x, y,hpBar , height )


    render.SetBlend( 1 )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_GREATER )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )
    local tx, ty = surface.GetTextSize( math.Round(hp) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round(hp) )

    render.SetStencilReferenceValue( 11 )
    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )


    surface.SetFont( "HUD Speed" )
    surface.SetTextColor( hud.customColor2 )
    local tx, ty = surface.GetTextSize( math.Round(hp) )
    surface.SetTextPos( x+(width/2)-(tx/2), y +(height/2)-(ty/2))
    surface.DrawText( math.Round(hp) )
  render.SetStencilEnable(false)

end
--]=]
