local LoadedSounds
if CLIENT then
	LoadedSounds = {} -- this table caches existing CSoundPatches
end

function ReadSound( FileName, rf, ent )
	local sound
	local filter
	if SERVER then
		filter = RecipientFilter()
    if !rf then
		  filter:AddAllPlayers()
    else
      if type(rf) == "table" then
        for k, v in pairs(rf) do
          filter:AddPlayer(v)
        end
      else
        filter:AddPlayer(rf)
      end
    end
	end
	if SERVER or !LoadedSounds[FileName] then
		-- The sound is always re-created serverside because of the RecipientFilter
    if !ent then
      ent = game.GetWorld()
    end
		sound = CreateSound( ent, FileName, filter ) -- create the new sound, parented to the worldspawn ( which always exists )
		if sound then
			sound:SetSoundLevel( 75 ) -- play everywhere
			if CLIENT then
				LoadedSounds[FileName] = { sound, filter } -- cache the CSoundPatch
			end
		end
	else
		sound = LoadedSounds[FileName][1]
		filter = LoadedSounds[FileName][2]
	end
	if sound then
    /*
		if CLIENT then
			sound:Stop() -- it won't play again otherwise
		end
		sound:Play()*/
	end
	return sound -- useful if you want to stop the sound yourself
end
