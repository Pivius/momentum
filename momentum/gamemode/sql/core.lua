include("my_sql.lua")
SQL = {}
SQLdata = SQLdata or {}
SQL.Use = true -- Set this to false if you don't want MySQL

local data = grapple.database

mysql:Disconnect()
if SQL.Use then
	mysql:Connect(data.Host, data.User, data.Pass, data.Database, data.Port)
end



hook.Add("DatabaseConnected", "Create Tables", function()

	local qadmins = mysql:Create(SQLDetails.tables.users)
		qadmins:Create("sID", "VARCHAR(255)")
    qadmins:Create("name", "VARCHAR(255)")
    qadmins:Create("rank", "VARCHAR(255)")
		qadmins:Create("flags", "VARCHAR(255)")
    qadmins:PrimaryKey("sID")
  qadmins:Execute()

	local qranks = mysql:Create(SQLDetails.tables.ranks)
		qranks:Create("name", "VARCHAR(255)")
		qranks:Create("inheritance", "VARCHAR(255)")
		qranks:Create("cmds", "VARCHAR(255)")
		qranks:Create("power", "TINYINT(255)")
		qranks:PrimaryKey("name")
	qranks:Execute()

	local stats = mysql:Create(SQLDetails.tables.stats)
		stats:Create("sID", "VARCHAR(255)")
		stats:Create("name", "VARCHAR(255)")
		stats:Create("speed", "INT(255)")
		stats:Create("pts", "INT(255)")
		stats:Create("strafepts", "INT(255)")
		stats:Create("strafetime", "INT(255)")
		stats:Create("airtime", "INT(255)")
		stats:Create("rank", "VARCHAR(255)")
		stats:Create("placement", "INT(255)")
		stats:Create("date", "VARCHAR(255)")
		stats:PrimaryKey("sID")
	stats:Execute()

	local qzones = mysql:Create(SQLDetails.tables.zones)
		qzones:Create("map", "VARCHAR(255)")
		qzones:Create("id", "INT(255)")
		qzones:Create("name", "VARCHAR(255)")
		qzones:Create("vert", "VARCHAR(255)")
		qzones:Create("mode", "VARCHAR(255)")
		qzones:PrimaryKey("id")
	qzones:Execute()

	local qtricks = mysql:Create(SQLDetails.tables.tricks)
		qtricks:Create("map", "VARCHAR(255)")
		qtricks:Create("id", "INT(255)")
		qtricks:Create("name", "VARCHAR(255)")
		qtricks:Create("sequence", "VARCHAR(255)")
		qtricks:Create("points", "INT(255)")
		qtricks:Create("ignore", "VARCHAR(255)")
		qtricks:Create("limit", "INT(255)")
		qtricks:PrimaryKey("id")
	qtricks:Execute()

	Admins:Init()
end)

timer.Create( "mysql.Timer", 1, 0, function()
    mysql:Think()
end )
