UT = UT or {}
UT.math = UT.math or {}


/*---------------
  SECTION - General
*/---------------

/*
  NAME - Lerp
  FUNCTION - Linear interpolation
*/
function UT:Lerp(fraction, from, to)
	return ((1.0 - fraction) * from) + (fraction * to)
end

/*
  NAME - NoEase
  FUNCTION - NoEase
*/
function UT:NoEase(frac, from, to)
	return ((1.0 - frac) * from) + (frac * to)
end

/*
  NAME - Hermite
  FUNCTION - Ease in and out
*/
function UT:Hermite(val, from, to)
	return UT:Lerp(val * val * (3 - 2 * val), from, to)
end

/*
  NAME - Smoothnum
  FUNCTION - Uses Lerp to smoothly increment or decrement a number
*/

function UT:Smoothnum(frac, old, new)
  sm2 = sm2 and UT:Lerp(frac, sm2 , new) or new
  if old > new then
    return math.Clamp(sm2, new, old)
  elseif old < new then
    return math.Clamp(sm2, old, new)
  end

end

function UT:Approx(val, about, range)
	return ((math.abs(val-about) < range))
end


/*
  NAME - wrapText
  FUNCTION - Wraps text according to the width. Don worry about lines its for callback
*/

function wrapText( font, string, width )
	local tbl = string.Explode( " ", string )
  local finaltbl = {}
  local text = {}
  local lines = 1

  for k, v in pairs(tbl) do
    if !text[lines] then
      text[lines] = {}
    end
    table.insert(text[lines], v)
    surface.SetFont(font)
    local w, h = surface.GetTextSize( string.Implode(" ", text[lines]) )
    if w >= width then
      table.remove(text[lines])
      lines = lines+1
      table.insert(finaltbl, text)
      text[lines] = {}
      table.insert(text[lines], v)
    elseif w < width and v == "//" then
      table.remove(text[lines])
      lines = lines+1
    end
    if k == #tbl and w < width then
      table.insert(finaltbl, text)
    end
  end

  --PrintTable(finaltbl)
return finaltbl[1]
end

/*
  NAME - SmoothVector
  FUNCTION - Uses Lerp to smoothly increment or decrement a vector
*/

function UT:SmoothVector(frac, oldpos, newpos)
    return Vector(UT:Lerp(frac, oldpos, newpos), UT:Lerp(frac, oldpos, newpos), UT:Lerp(frac, oldpos, newpos))
end

/*
  NAME - LerpColor
  FUNCTION - Lerps a color
*/

function UT:LerpColor(frac, oldcol, newcol)
  local oR, oG, oB, oA = UT:ExplodeColor(oldcol)
  local nR, nG, nB, nA = UT:ExplodeColor(newcol)
    return Color(UT:Lerp(frac, oR, nR), UT:Lerp(frac, oG, nG), UT:Lerp(frac, oB, nB), UT:Lerp(frac, oA, nA))
end

/*
  NAME - ClampAngle
  FUNCTION - Clamps an angle
*/

function UT:ClampAngle(input, min, max)
    return Angle(math.Clamp(input.p, min, max), math.Clamp(input.y, min, max), math.Clamp(input.r, min, max))
end

/*
  NAME - isEqual
  FUNCTION - Checks if two tables is equal
*/
function UT:isEqual(a,b)

   local function isEqualTable(t1,t2)

      if t1 == t2 then
         return true
      end

      for k,v in pairs(t1) do

         if type(t1[k]) ~= type(t2[k]) then
            return false
         end

         if type(t1[k]) == "table" then
            if not isEqualTable(t1[k], t2[k]) then
               return false
            end
         else
            if t1[k] ~= t2[k] then
               return false
            end
         end
      end

      for k,v in pairs(t2) do

         if type(t2[k]) ~= type(t1[k]) then
            return false
         end

         if type(t2[k]) == "table" then
            if not isEqualTable(t2[k], t1[k]) then
               return false
            end
         else
            if t2[k] ~= t1[k] then
               return false
            end
         end
      end

      return true
   end

   if type(a) ~= type(b) then
      return false
   end

   if type(a) == "table" then
      return isEqualTable(a,b)
   else
      return (a == b)
   end

end

/*
  NAME - TblContain
  FUNCTION - Check if table contain element
*/
function UT:TblContain(table, element)
	if type(table) != "table" then
		table = {table}
	end
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

/*
  NAME - TblContain
  FUNCTION - Check if table contain element
*/
function UT:TblgetKey(table, key)
  for _, value in pairs(table) do
    if _ == key then
      return true
    end
  end
  return false
end

/*
  NAME - TblCompare
  FUNCTION - Compares two tables
*/

function UT:TblCompare(tbl1,tbl2)
	for k, v in pairs( tbl1 ) do
		if ( type(v) == "table" and type(tbl2[k]) == "table" ) then
			if ( !UT:TblCompare( v, tbl2[k] ) ) then return false end
		else
			if ( v != tbl2[k] ) then return false end
		end
	end
	for k, v in pairs( tbl2 ) do
		if ( type(v) == "table" and type(tbl1[k]) == "table" ) then
			if ( !UT:TblCompare( v, tbl1[k] ) ) then return false end
		else
			if ( v != tbl1[k] ) then return false end
		end
	end
	return true
end

/*
  NAME - RemoveByKey
  FUNCTION - Removes key from table
*/
function UT:RemoveByKey(table, key)
    local element = table[key]
    table[key] = nil
    return element
end

/*
  NAME - RemoveByKey
  FUNCTION - Removes key from table
*/
function UT:ErrorWarning(str)
  MsgC( Color( 255, 0, 0 ), str, "\n" )
end

/*
  NAME - TraceVec
  FUNCTION - Detects the vector under players feet. Makes sure not to trace under world.
*/

function UT:TraceVec(ply)
  local pos = ply:GetShootPos()
  local tracedata = {}
  tracedata.start = pos
  tracedata.endpos = pos - Vector(0,0,75)
  tracedata.filter = ply
  local trace = util.TraceLine(tracedata)
  return trace.HitPos;
end

/*
  NAME - TraceVec
  FUNCTION - Detects the vector under players feet. Makes sure not to trace under world.
*/

function UT:OnGround(ply)
	local Pos = ply:GetPos()
  local Mins = ply:OBBMins()
  local Maxs = ply:OBBMaxs()
  local endPos = Pos * 1
  endPos.z = endPos.z -1

  local tr = util.TraceHull{
    start = Pos,
    endpos = endPos,
    mins = Mins,
    maxs = Maxs,
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }
  return tr.Hit;
end

/*
  NAME - TraceAim
  FUNCTION - Traces the aim
*/

function UT:TraceAim(ply)
	local pos = ply:GetShootPos()
	local ang = ply:GetAimVector()
	local tracedata = {}
	tracedata.start = pos
	tracedata.endpos = pos+(ang*100000)
	tracedata.filter = ply
	local trace = util.TraceLine(tracedata)
  return trace.HitPos
end

/*
  NAME - HasKey
  FUNCTION - Checks if a table has given key
*/
function UT:HasKey( t, key )
	for k, v in pairs( t ) do
		if ( k == key ) then return true end
	end
	return false
end

/*
  NAME - AddToTable
  FUNCTION - Adds a value to chosen table
*/

function UT:AddToTable(tbl,val)
	if (!table.HasValue(tbl,val)) then
		table.insert(tbl,val)
	end
end

/*
  NAME - MergeTableTo
  FUNCTION - Merges a table with another one
*/

function UT:MergeTableTo(tbl, src)
  table.Merge(tbl, src)
end



/*
  NAME - RemoveFromTable
  FUNCTION - Removes a value from a table
*/

function UT:RemoveFromTable(tbl,val)
	for k,v in pairs(tbl) do
		if (v == val) then
			table.remove(tbl,k)
			return
		end
	end
end

/*
  NAME - IsNan
  FUNCTION - Checks if values are nan
*/

function UT:IsNan(...)
  local args = {...}
	for k, v in pairs(args) do
		if string.lower(v) == "nan" then
			return true
		end
	end
	return false
end

/*
  NAME - FastExplode
  FUNCTION - Explodes a string
*/

function UT:FastExplode( str, sep )
	local k, t

	t = { }

	for k in str:gmatch( "[^" .. sep .. "]+" ) do
		table.insert( t, k )
	end

	return t
end

/*
  NAME - plyFromNick
  FUNCTION - Gets player entity from nick
*/
function plyFromNick(nick)
	for k, v in ipairs( player.GetAll() ) do

		if ( string.find(string.lower( v:Nick() ), string.lower(nick)) ) then
			return v
		end
	end
	return NULL
end
/*
	NAME - Berp/Boing-like interpolation
  FUNCTION - This method will first overshoot, then waver back and forth around the end value before coming to a rest.
*/

function UT:Berp(frac, start, endvar)
		frac = math.Clamp(frac,0, 1)
		frac = (math.sin(frac * math.pi * (0.2 + 2.5 * (frac ^ 3))) * math.pow(1 - frac, 2.2) + frac) * (1 + (1.2 * (1 - frac)))
		return start + (endvar - start) * frac
end

/*
  NAME - Berp/Boing-like interpolation for Vectors
  FUNCTION -
*/
function UT:BerpV3(frac, start, endvar)

		return Vector(Berp(frac, start.x, endvar.x), Berp(frac, start.y, endvar.y), Berp(frac, start.z, endvar.z))
end

/*
  NAME - Bounce interpolation
  FUNCTION - Returns a value between 0 and 1.
*/
function UT:Bounce(x )
    return math.abs(math.sin(6.28*(x+1)*(x+1)) * (1-x));
end

function UT:SmoothStep(frac, min, max)
	frac = math.Clamp(frac, min, max)
	local v1 = (frac - min) / (max - min)
	local v2 = (frac - min) / (max - min)
	return -2*v1 * v1 * v1 + 3*v2 * v2
end

/*
  NAME - ExplodeColor
  FUNCTION - Returns a color structure
*/
function UT:ExplodeColor(color)
	return color.r, color.g, color.b, color.a
end

/*
  NAME - Conversion
  FUNCTION - Converts source units to mph or kph
	VARIABLEs -
							units - The unit you want to convert
							unit  - Which conversion you want to use
											MPH or KPH
*/
function UT:Conversion(units, unit)
  if string.lower(unit) == "mph" then
    local mph = 6.4/150
    return mph*units
  elseif string.lower(unit) == "kph" then
    local kph = (6.4/150)*1.609344
    return kph*units
  end
end

/*---------------
  SECTION - Player targeting
*/---------------

/*
  NAME - OnRamp
  FUNCTION - Checks whether a player is on a flat surface or a slanted surface
*/
local PLYR = FindMetaTable( "Player" )
function PLYR:onRamp( )
        local Pos = self:GetPos()

        local Mins = self:OBBMins()

        local Maxs = self:OBBMaxs()

        local EndPos = Pos * 1
        EndPos.z = EndPos.z -1

        local tr = util.TraceHull{
            start = Pos,
            endpos = EndPos,
            mins = Mins,
            maxs = Maxs,
            mask = MASK_PLAYERSOLID_BRUSHONLY,

            filter = function(e1, e2)
                return not e1:IsPlayer()
            end

        }
        if(tr.Fraction ~= 1) then
            local Plane, Last = tr.HitNormal, Vector()

            if(0.7 <= Plane.z and Plane.z < 1) then
							return true
						else
							return false
            end
					else
						return false
        end
end

/*---------------
  SECTION - Movedata
*/---------------

local MOVE = FindMetaTable( "CMoveData" )

function MOVE:SendToGround()
	local tracedata = {}
	tracedata.start = self:GetOrigin()
	tracedata.endpos = self:GetOrigin()-Vector(0,0,10000)
	tracedata.filter = player.GetAll()
	local trace = util.TraceLine(tracedata)
	if trace.HitWorld then
		self:SetOrigin(trace.HitPos)
	end
end


/*---------------
  SECTION - Math
*/---------------


/*
  NAME - vectorClamp
  FUNCTION - Clamps an vector
*/

function UT.math:vectorClamp(input, min, max)
    return Vector(math.Clamp(input.x, min.x, max.x), math.Clamp(input.y, min.y, max.y), math.Clamp(input.z, min.z, max.z))
end

/*
  NAME - vectorSubtract
  FUNCTION - Subtracts vector a and b and returns c
*/

function UT.math:vectorSubtract(a, b ,c)
	c = a-b
	return c
end

/*
  NAME - vectorAdd
  FUNCTION - Adds vector a and b and returns c
*/

function UT.math:vectorAdd(a, b ,c)
	c = a+b
	return c
end


/*
  NAME - vectorClear
  FUNCTION - Clears vector a
*/

function UT.math:vectorClear(a)
	a = Vector()
	return a
end

/*
  NAME - vectorMax
  FUNCTION - Returns the highest number in vector a
*/

function UT.math:vectorMax(a)
	return math.max(a.x, math.max(a.y, a.z))
end

/*
  NAME - vectorMin
  FUNCTION - Returns the lowest number in vector a
*/

function UT.math:vectorMin(a)
	return math.min(a.x, math.min(a.y, a.z))
end

/*
  NAME - vectorScale
  FUNCTION - Scales vector a with b and returns c
*/

function UT.math:vectorScale(a, b ,c)
	c.x = a.x * b
	c.y = a.y * b
	c.z = a.z * b
	return c
end

/*
  NAME - vectorRound
  FUNCTION - Rounds vector a with n decimals
*/

function UT.math:vectorRound(a, n)
	if !a then return end
	if !n then n =0 end
	a.x = math.Round(a.x, n)
	a.y = math.Round(a.y, n)
	a.z = math.Round(a.z, n)
	return a
end

/*
  NAME - vectorFill
  FUNCTION - Fills a vector a with b
*/

function UT.math:vectorFill(a, b)
	a.x = b
	a.y = b
	a.z = b
	return a
end

/*
  NAME - vectorNegate
  FUNCTION - Scales vector a with b and returns c
*/

function UT.math:vectorNegate(a)
	a = a*-1
	return a
end

/*
  NAME - vectorNormalize2
  FUNCTION -
*/
function UT.math:vectorNormalize2(inn, out)

	local	length, ilength

	length = math.sqrt(inn.x*inn.x + inn.y*inn.y + inn.z*inn.z);
	if (length == 0) then

		UT.math:vectorClear(out)
		return 0;
	end

	ilength = 1.0/length;
	out.x = inn.x * ilength
	out.y = inn.y * ilength
	out.z = inn.z * ilength
	return length;
end

/*
  NAME - vectorMA
  FUNCTION -
*/
function UT.math:vectorMA(start, scale, dir, dest)
	dest.x=start.x+dir.x*scale;
	dest.y=start.y+dir.y*scale;
	dest.z=start.z+dir.z*scale;
	return dest
end


function UT.math:CrossProduct(v1, v2, cross)
	if v1 != cross and v2 != cross then
		cross.x = v1.y*v2.z - v1.z*v2.y;
		cross.y = v1.z*v2.x - v1.x*v2.z;
		cross.z = v1.x*v2.y - v1.y*v2.x;
		return cross
	end
end
