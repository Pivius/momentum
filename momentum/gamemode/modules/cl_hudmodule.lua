
local mod = {}

-- View table
mod.View = {}
mod.View.zFar = 0
mod.View.zNear = 0
mod.View.FOV = 0
mod.View.Ang = Angle(0,0,0)
mod.View.Pos = Vector(0,0,0)

mod.Panels = {}

-- Get view info
function viewUpdate( ply, pos, ang, fov, znear, zfar )
  if IsValid(LocalPlayer()) then
    mod.View.zFar = zfar
    mod.View.zNear = znear
    mod.View.FOV = fov
    mod.View.Ang = ang
    mod.View.Pos = pos
  end
end
hook.Add( 'CalcView', 'view_Update', viewUpdate )


function mod.finishMove(ply, mv)
  for name, pnl in pairs(mod.Panels) do
    if IsValid( pnl ) and pnl.moveUpdate then
      pnl:Move( ply, mv )
    end
  end
end
hook.Add( 'FinishMove', 'mod.finishMove', mod.finishMove)

function mod.getZFar()
  return mod.View.zFar or 0
end

function mod.getZNear()
  return mod.View.zNear or 0
end

function mod.getFOV()
  return mod.View.FOV or 70
end

function mod.getViewAngle()
  return mod.View.Ang or Angle( 0, 0, 0 )
end

function mod.getViewPos()
  return mod.View.Pos or Vector( 0, 0, 0 )
end

function mod.getPanel(pnl)
  return mod.Panels[pnl]
end

function mod.Render()

  for name, pnl in pairs(mod.Panels) do

    pnl:DrawCam()
  end
end
hook.Add( "RenderScreenspaceEffects", "PANEL.Render", mod.Render )

/*
  3D Rendering
*/

local PANEL = {}

function PANEL:Init()
    self:SetSize( ScrW(), ScrH() )
    self:SetPos( 0, 0 )
    self.Name = #mod.Panels+1
    -- Tilt
    self.Tiltable = true
    self.tiltDelta = Angle(0,0,0)
    self.oldTiltAngle = Angle(0,0,0)
    self.resetRate = 3
    self.tiltRate = 50

    self.Distance = 1
    self.offsetUp = 0
    self.offsetSide = 0

    self.sideAng = 0
    self.upAng = 0

    -- Offset according to movement
    self.move = false

    self.moveUpdate = false

    self.Alpha = 255

    mod.Panels[self.Name] = self

end

function PANEL:setName(name)
  local oldName = self.Name
  if !name then
    self.Name = #mod.Panels
    mod.Panels[oldName] = false
    mod.Panels[self.Name] = self
  end
  self.Name = name
end

function PANEL:Move(ply,mv)
end

function PANEL:setDistance(dist)
  if not dist then self.Distance = 1 end
  self.Distance = dist
end

function PANEL:setAngle(side, up)
  if !side then self.sideAng = 0 end
  if !up then self.upAng = 0 end
  self.upAng = up
  self.sideAng = side
end

function PANEL:OnRemove()
    self.moveUpdate = false
    mod.Panels[self.Name] = false
end

function PANEL:PerformLayout( w, h )

end

function PANEL:Tilt()
  self.tiltDelta = mod.getViewAngle() - self.oldTiltAngle
  if not ((self.tiltDelta.y > 90) or (self.tiltDelta.y < -90)) then
    self.offsetSide = self.offsetSide + ((self.tiltDelta.y - self.offsetSide*(self.resetRate/10)) * (self.tiltRate/100))
  end

  if not ((self.tiltDelta.p > 90) or (self.tiltDelta.p < -90)) then
    self.offsetUp = self.offsetUp + ((self.tiltDelta.p - self.offsetUp*(self.resetRate/10)) * (self.tiltRate/100))
  end
  self.oldTiltAngle = mod.getViewAngle()
end

function PANEL:DrawCam()
  local x, y = self:GetPos()
  local camPos = mod.getViewPos()
  local camAng = mod.getViewAngle()
  local aspectRatio = ScrW()/ScrH()
  local distance = mod.getZNear() * self.Distance * 160
  local frustH = 2 * (distance) * (math.tan( math.rad(90*0.5) ))
  local frustW  = frustH * aspectRatio
  --local aRatio = (frustW-frustH)/(ScrW()-ScrH())
  local fARatio    = frustW / frustH
  --local w, h = frustW / ScrW() / aRatio, frustH / ScrH() / aRatio
  self:Tilt()
  local  ang = Angle( camAng.p-(self.offsetUp*2), (camAng.y)-(self.offsetSide/2),  0)
  -- Center the 3D2D cam in 3D cam and move forward
  --local xOffset = ang:Right() * -((frustW) / (ScrW()) )
  --local yOffset = ang:Up() * ((frustH) / (ScrH()))
  --local zOffset = (ang:Forward()*self.Distance) * ((mod.getZNear())*aspectRatio)
  --camPos = camPos+zOffset+xOffset+yOffset
  --local xOffset = ang:Right()

  local xOffset = ang:Right()
  local yOffset = ang:Up()
  local zOffset = (ang:Forward()*distance) * fARatio
  camPos = camPos+zOffset+yOffset+xOffset
  --pos = pos - self.movementOffset
  ang:RotateAroundAxis( ang:Right(), 90+(self.upAng-self.offsetUp*2.5))
  ang:RotateAroundAxis( ang:Forward(), 0+(self.sideAng-self.offsetSide*2.5))
  ang:RotateAroundAxis( ang:Up(), -90)

  cam.Start3D( mod.getViewPos(), camAng, 90)
    surface.DisableClipping(true)
      cam.Start3D2D( camPos, ang, 1)
        cam.IgnoreZ(true)
          self:Draw3D(LocalPlayer(), frustW/2, frustH/2)
        cam.IgnoreZ(false)
      cam.End3D2D()
    surface.DisableClipping(false)
  cam.End3D()
end

function PANEL:Draw3D(ply, scrw, scrh)
  --print("a")
  -- Draw Function
end
vgui.Register( '3DPanel', PANEL )

function mod.CreatePanel( dist, tilt, reset, side, up, move )
  local panel = vgui.Create( "3DPanel" )

  panel.Distance = dist or 1.65
  panel.tiltRate = tilt or 50
  panel.resetRate = reset or 3
  panel.sideAng = side or 0
  panel.upAng = up or 0
  panel.moveUpdate = move or false
  return panel
end


return mod
