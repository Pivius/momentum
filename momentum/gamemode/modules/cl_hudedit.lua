hEdit = {}
hEdit.language = {}
hEdit.CurEditor = "Standard"

// Style
local MENU = {}
local STANDARD = {}
local BLUEPRINTS = {}

function MENU:CreateMenu()
  self.Menus = self:Add( "DFrame" )

  self.Menus:SetPos( 0, 0 )
  self.Menus:SetSize(190, 31)
  self.Menus:SetTitle("")
  self.Menus:SetDraggable(false)
  self.Menus:ShowCloseButton( false )
  self.Menus.Paint = function(self, w, h )
    surface.SetDrawColor( Color(15,15,15,220) )
    surface.DrawRect(0, 0, w, h)
  end

  self.MenusFill = self:Add( "DFrame" )
  self.MenusFill:SetPos( 193, 0 )
  self.MenusFill:SetSize(ScrW(), 31)
  self.MenusFill:SetTitle("")
  self.MenusFill:SetDraggable(false)
  self.MenusFill:ShowCloseButton( false )
  self.MenusFill.Paint = function(self, w, h )
    surface.SetDrawColor( Color(15,15,15,220) )
    surface.DrawRect(0, 0, w, h)
  end

  self.Menus.Exit = self.Menus:Add( "DButton" )
  self.Menus.Exit:SetPos( 3, 3 )
  self.Menus.Exit:SetSize(25, 25)
  self.Menus.Exit:SetText("")

  self.Menus.Exit.Paint = function(self, w, h)
    surface.SetDrawColor( Color(255,100,100,220) )
    surface.DrawRect(0, 0, w, h)

    surface.SetFont( "HUD Menu2" )
    local tw, th = surface.GetTextSize("X")
    surface.SetTextColor( Color(255,255,255,255) )
    surface.SetTextPos( (w/2) - tw/2,(h/2) - th/2 )
    surface.DrawText( "X" )
  end

  self.Menus.Exit.DoClick = function()
    self:AlphaTo(0, 0.15, 0)
    timer.Simple(0.16, function() self:Hide() end)
  end

  self.Menus.Editor = self.Menus:Add( "DButton" )
  self.Menus.Editor:SetPos( (3*2)+(25), 3 )
  self.Menus.Editor:SetSize(50, 25)
  self.Menus.Editor:SetText("")
  self.Menus.Editor.Paint = function(self, w, h)
    surface.SetDrawColor( Color(25,25,25,170) )
    surface.DrawRect(0, 0, w, h)

    surface.SetFont( "HUD Menu2" )
    local tw, th = surface.GetTextSize("Editor")
    surface.SetTextColor( Color(255,255,255,255) )
    surface.SetTextPos( (w/2) - tw/2,(h/2) - th/2 )
    surface.DrawText( "Editor" )
  end

  self.Menus.Editor.DoClick = function()
    if hEdit.CurEditor == "Standard" then
      hEdit.CurEditor = "Blueprint"
    else
      hEdit.CurEditor = "Standard"
    end
  end

  self.Menus.Save = self.Menus:Add( "DButton" )
  self.Menus.Save:SetPos( (3*3)+(25+50*1), 3 )
  self.Menus.Save:SetSize(50, 25)
  self.Menus.Save:SetText("")
  self.Menus.Save.Paint = function(self, w, h)
    surface.SetDrawColor( Color(25,25,25,220) )
    surface.DrawRect(0, 0, w, h)

    surface.SetFont( "HUD Menu2" )
    local tw, th = surface.GetTextSize("Save")
    surface.SetTextColor( Color(255,255,255,255) )
    surface.SetTextPos( (w/2) - tw/2,(h/2) - th/2 )
    surface.DrawText( "Save" )
  end

  self.Menus.Save.DoClick = function()

  end

  self.Menus.Load = self.Menus:Add( "DButton" )
  self.Menus.Load:SetPos( (3*4)+(25+50*2), 3 )
  self.Menus.Load:SetSize(50, 25)
  self.Menus.Load:SetText("")
  self.Menus.Load.Paint = function(self, w, h)
    surface.SetDrawColor( Color(25,25,25,170) )
    surface.DrawRect(0, 0, w, h)

    surface.SetFont( "HUD Menu2" )
    local tw, th = surface.GetTextSize("Load")
    surface.SetTextColor( Color(255,255,255,255) )
    surface.SetTextPos( (w/2) - tw/2,(h/2) - th/2 )
    surface.DrawText( "Load" )
  end

  self.Menus.Load.DoClick = function()
  end

end

function MENU:OnEnter()

end

function MENU:OnErase()

end



function MENU:UpdateLine(line)

  if !IsValid(self.TEdit.DrawLines[line]) then
    self.TEdit.DrawLines[line] = self.TEdit:Add( "DFrame" )
  end
  self.TEdit.DrawLines[line]:SetPos( 0, (self.TEdit.LineWidth*line) )
  self.TEdit.DrawLines[line]:SetSize(ScrW(), self.TEdit.LineWidth)
  self.TEdit.DrawLines[line]:SetTitle("")
  self.TEdit.DrawLines[line]:SetDraggable(false)
  self.TEdit.DrawLines[line]:ShowCloseButton( false )
  self.TEdit.DrawLines[line].Paint = function(self, w, h )
    if self:GetParent().CurLine[1] == line then
      surface.SetDrawColor( self:GetParent().Colors["highlight"] )
      surface.DrawRect(0, 0, w, h)
    end

    surface.SetFont( "Editor" )
    --local tw, th = surface.GetTextSize(self:GetParent().Lines[self:GetParent().CurLine])
    surface.SetTextColor( Color(255,255,255,255) )
    surface.SetTextPos( 0,0 )
    surface.DrawText( self:GetParent().Lines[self:GetParent().CurLine[1]] )
  end
end

function MENU:CreateEditor()

  self.FileName = self:Add( "DFrame" )
  self.FileName:SetPos( 0, 34 )
  self.FileName:SetSize(ScrW(), 20)
  self.FileName:SetTitle("")
  self.FileName:SetDraggable(false)
  self.FileName:ShowCloseButton( false )
  self.FileName.Paint = function(self, w, h )
    surface.SetDrawColor( Color(15,15,15,220) )
    surface.DrawRect(0, 0, w, h)
  end

  self.TEdit = self:Add( "DFrame" )
  self.TEdit:SetPos( 0, 34+20+3 )
  self.TEdit:SetSize(ScrW(), ScrH())
  self.TEdit:SetTitle("")
  self.TEdit:SetDraggable(false)
  self.TEdit:ShowCloseButton( false )
  self.TEdit.Paint = function(self, w, h )
    surface.SetDrawColor( Color(15,15,15,220) )
    surface.DrawRect(0, 0, w, h)
  end

  self.TEdit:SetCursor("beam")

	self.TEdit.Lines = {[1] = ""}
	self.TEdit.CurLine = {1, 1}
	self.TEdit.Scroll = {1, 1}
  self.TEdit.DrawLines = {}
  self.TEdit.Colors = {
		["highlight"] = Color(0, 100, 0, 100),
	}

	self.TEdit.LineWidth = 20

	self.TEdit.Blink = RealTime()

	self.ScrollBar = self.TEdit:Add( "DVScrollBar" )
	self.ScrollBar:SetUp(1, 1)

	self.TextEntry = self.TEdit:Add( "DTextEntry" )
  --self.TextEntry:SetFont( "Editor" )
  self.TextEntry:SetPos( 0, 0 )
  self.TextEntry:SetSize( ScrW(), ScrH() )
	self.TextEntry:SetMultiline(true)
  self.TextEntry:SetUpdateOnType( true )
  self.TextEntry.Paint = function(self, w, h)
  end

  self.TextEntry.OnValueChange = function(entry, key)
    self.TEdit.Lines[self.TEdit.CurLine[1]] = key
    self:UpdateLine(self.TEdit.CurLine[1])
    print(key)
  end
  --self.TextEntry:RequestFocus()

end

function MENU:CreateBlueprint()

end


function MENU:Init()
  self:CreateMenu()
  self:CreateEditor()
end


function MENU:PerformLayout()
	self:SetSize( ScrW()-20, ScrH()-20 )
	self:SetPos( 10,10 )
end

vgui.Register( "HudEditor", MENU, "EditablePanel" )

if ( IsValid( Editor ) ) then
	Editor:Remove()
end

function hEdit.Editor(show)
  if show then
  	if ( !IsValid( Editor ) ) then
  		Editor = vgui.CreateFromTable( MENU )
      Editor:SetAlpha(0)
  	end

  	if ( IsValid( Editor ) ) then
      Editor:AlphaTo(255, 0.15)
  		Editor:Show()
  		Editor:MakePopup()
      Editor:SetMouseInputEnabled( true )

  	end
  end
end
