freeCam = Angle(0,0,0)
local smooth = Angle()
local lagBack = Vector()
vpos = Vector()
vangle = Angle()

playerAim = Vector()
local thirdperson = CreateClientConVar("grp_3p", "0", true, true, "Enables/disables thirdperson view")
hook.Add( "ShouldDrawLocalPlayer", "ThirdPersonDrawPlayer", function()
	return thirdperson:GetBool()
end )

function FreeCam( cmd, x, y, ang )

    if !IsValid( LocalPlayer( ) ) or !thirdperson:GetBool() then return false end
    local ply = LocalPlayer( )

    freeCam.p = math.Clamp( math.NormalizeAngle( freeCam.p + y / 12 ), -90, 90 )
    freeCam.y = math.NormalizeAngle( freeCam.y - x / 12 )

    return true

end

hook.Add( "InputMouseApply", "ThirdpersonFreeCam", FreeCam )

hook.Add( "CalcView", "ThirdPersonView", function( ply, pos, angles, fov )

	if ply:Alive() and thirdperson:GetBool() then
		local view = {}
		local aim = LerpAngle(17*FrameTime(), smooth, freeCam)
		smooth = aim
		view.pos = ply:GetPos() + Vector(0,0,60) - (aim:Forward() * 90) + ( aim:Right() * 30 ) + ( aim:Up() * 5 )
		view.angles = freeCam

		local lag = LerpVector(15*FrameTime(), lagBack, view.pos)
		lagBack = lag
		view.pos = lagBack
		local crosshair =   util.TraceLine( {
			start  = view.pos,
			endpos = view.pos + ( (freeCam:Forward( )) * 99999 ),
			filter = ply
		} )
		playerAim = crosshair.HitPos
		view.fov = fov
		local t = {}
		t.start = ply:GetPos() + Vector(0,0,60)
		t.endpos = view.pos
		t.filter = ply
		local tr = util.TraceLine(t)
		view.pos = tr.HitPos
		if tr.Fraction < 1.0 then
			view.pos = view.pos + tr.HitNormal * 5
		end
		vpos = view.pos
		return GAMEMODE:CalcView( ply, view.pos, view.angles, view.fov )

	end

end )
local aimDir = Angle()
function MoveAngle(ply, cmd)
	--local ply = LocalPlayer()
  --ply:SetEyeAngles( (playerAim):Angle() )
	if !thirdperson:GetBool() then return end
	if cmd:KeyDown(IN_SPEED) then
		cmd:SetViewAngles( aimDir )
	else
		cmd:SetViewAngles( ( playerAim - ply:GetShootPos( ) ):Angle( ) )
	end

	aimDir = freeCam

end
hook.Add("StartCommand", "Angle movement", MoveAngle)
/*
function test()
	local ply = LocalPlayer()

	render.DrawLine( ply:GetShootPos(), ply:GetShootPos()+(playerAim - ply:GetShootPos( )), Color(255,0,0), true )
end
hook.Add( "PostDrawTranslucentRenderables", "tetetttt", test )*/
