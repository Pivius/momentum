
Trails = {} || Trails

local meta = FindMetaTable( "Player" )


function meta:TrailColor(col)
  if !col then self.TrailCol = Color(255, 255, 255, 255) return end
  self.TrailCol = col
  if IsValid(self.CurTrail) then
   --self.CurTrail:SetColor(self.TrailCol)
   self.CurTrail:SetTrailColor(Vector(self.TrailCol.r, self.TrailCol.g, self.TrailCol.b))
   self.CurTrail:SetTrailAlpha(self.TrailCol.a)
 end
end

function meta:TrailMat(mat)
  if !mat then self.TrailMaterial = "trails/laser.vmt" return end
  self.TrailMaterial = mat
end

function meta:TrailAdditive(add)
  if !add then self.TrailAdditive = false return end
  self.TrailAdditive = add
end

function meta:TrailLife(int)
  if !int then self.TrailLifetime = 1 return end
  self.TrailLifetime = int

  if IsValid(self.CurTrail) then
   --self.CurTrail:SetKeyValue('lifetime', self.TrailLifetime)
   self.CurTrail:SetlifeTime(self.TrailLifetime)
 end
end

function meta:StartFade(int)
  if !int then self.Fade = 0 return end
  self.Fade = int

  if IsValid(self.CurTrail) then
   self.CurTrail:SetstartFade(self.Fade)
 end
end

function meta:NodeLength(int)
  if !int then self.Length = 0 return end
  self.Length = int
  if IsValid(self.CurTrail) then
   self.CurTrail:SetnodeLength(self.Length)
 end
end

function meta:TrailSize(startw, endw)
  if !startw || !endw then self.TrailStartWidth = 4 self.TrailEndWidth = 0 else self.TrailStartWidth = startw self.TrailEndWidth = endw end
  if IsValid(self.CurTrail) then
   self.CurTrail:SetstartWidth(self.TrailStartWidth)
   self.CurTrail:SetendWidth(self.TrailEndWidth)
 end
end

function meta:RemoveTrail()

  if IsValid(self.CurTrail) then
    self.CurTrail.Removing = true
    timer.Simple(1.5, function()
      if !IsValid(self.CurTrail) then return end
      if self.CurTrail.Removing == false then return end
      self.CurTrail.Removing = false
      self.CurTrail:Remove()
    end)
  end
end

function meta:TrailRes(res)
  if !res then self.TrailResolution = 1/(16)*0.5 return end
  self.TrailResolution = res
end

function meta:CreateTrail()

  if IsValid(self.CurTrail) then
    self.CurTrail:Remove()
  end
  if !self.TrailCol then
    self:TrailColor(Color(255,255,255))
  end
  if !self.TrailLifetime then
  	self:TrailLife(75)
  end
  if !self.TrailAdditive then
  	self:TrailAdditive(true)
  end
  if !self.TrailStartWidth then
  	self:TrailSize(5, 0)
  end
  if !self.TrailResolution then
  	self:TrailRes(0.01)
  end
  if !self.Fade then
    self:StartFade(0)
  end
  if !self.Length then
    self:NodeLength(10)
  end
  if !self.TrailMaterial then
    self:TrailMat("trails/laser.vmt")--trick/test.vmt
  end

  self.CurTrail = ents.Create("bettertrail")
  self.CurTrail:SetParent(self, 0)
  self.CurTrail:SetPos(self:GetPos())
  self.CurTrail:Spawn()
  self.CurTrail:startWidth(self.TrailStartWidth)
  self.CurTrail:endWidth(self.TrailEndWidth)
  self.CurTrail:lifeTime(self.TrailLifetime)

  self.CurTrail:Color(Color(self.TrailCol.r,self.TrailCol.g,self.TrailCol.b, self.TrailCol.a))
  self.CurTrail:startFade(self.Fade)
  self.CurTrail:nodeLength(10)
  self.CurTrail:SetDashed(false)
  self.CurTrail:SetDashDist(30)
  self.CurTrail:SetMaterial(self.TrailMaterial)
  self.CurTrail.Removing = false


end

function TrailThink()
  for _, ply in pairs(player.GetAll()) do
    if !IsValid(ply.CurTrail) then return end
    if ply.CurTrail.Removing then
        local num2 = Lerp( 7*FrameTime(), ply.CurTrail:GetStartWidth(), 0 )
        local num = Lerp( 7*FrameTime(), ply.CurTrail:GetTrailAlpha(), 0 )
        --ply.CurTrail:SetStartWidth(num2)
        --print(num)
        ply.CurTrail:SetTrailAlpha(num)
        ply.CurTrail:SetStartWidth(num2)

    end

  end
end
hook.Add("Think", "Trailaestethics", TrailThink)

function meta:UpdateTrail()
  self.CurTrail:Remove()
  self:CreateTrail()
end


//LUA REFRESH
for k, v in pairs(ents.FindByClass( "bettertrail" )) do
  v:Remove()
end
for k, v in pairs(player.GetAll()) do

    v:CreateTrail()


end
