local UI = {}
local Jump_Pattern = {}
local cmds = {"stats"}
print("addling")
/*--------------------------------------------------------------------------
 NAME - ACConVar
 FUNCTION - AutoCompletes ConVar
--------------------------------------------------------------------------*/
local function AutoComplete( cmd, args )
	print( cmd, args )

	args = string.Trim( args )
	args = string.lower( args )

	local tbl = {}

	for k, v in pairs( player.GetAll() ) do
		local nick
		if string.find( string.lower( v:Nick() ), args ) then
			nick = "\"" .. v:Nick() .. "\""
			nick = "wl_stats " .. nick

			table.insert( tbl, nick )
		end
	end

	return tbl
end

local function GetStats( ply, cmd, args )
  local target
	for k, v in pairs( player.GetAll() ) do
		if string.find( string.lower( v:Nick() ), string.lower( args[1] ) ) then
		  target = v
		end
	end

  if target:IsPlayer() or target:IsBot() then
    net.Start("SendCache")
      net.WriteEntity(ply)
      net.WriteEntity(target)
    net.SendToServer()
  end
end

concommand.Add( "wl_stats", GetStats, AutoComplete )

net.Receive("SendCache", function()
  local tbl = net.ReadTable()
  local keyThresh = net.ReadFloat()
  local timeThresh = net.ReadFloat()
  MsgC( Color( 255, 255, 255 ), "-- Macro Watchlist -- \n" )
  MsgC( Color( 255, 255, 255 ), "     ".."-- " .. tbl.player:Nick() .. " --\n" )
  MsgC( Color( 255, 255, 255 ), "          ".."Suspected: " )
  if tbl.suspect then
    MsgC( Color( 255, 0, 0 ), tostring(tbl.suspect) .. "\n" )
  else
    MsgC( Color( 50, 255, 50 ), tostring(tbl.suspect) .. "\n" )
  end

  print("\n")
  MsgC( Color( 255, 255, 255 ), "          ".."JPS/Jumps per second: \n" )
  MsgC( Color( 255, 255, 255 ), "               ".."JPS Average: " )
  MsgC( Color( 255, 255, 255 ), tbl.JPS.avg )
  MsgC( Color( 255, 255, 255 ), "     ".."JPS Seconds: " )
  MsgC( Color( 255, 255, 255 ), #tbl.cache.JPS .. "\n")
  print("\n")

  MsgC( Color( 255, 255, 255 ), "          ".. "Time in " .. keyThresh .. " jumps:\n" )
  MsgC( Color( 255, 255, 255 ), "               ".."Lowest time: " )
  if tbl.bestTime > timeThresh then
    MsgC( Color( 50, 255, 50 ), tbl.bestTime)
  else
    MsgC( Color( 255, 0, 0 ), tbl.bestTime)
  end
  MsgC( Color( 255, 255, 255 ), "     ".."Time diff: " )
  if tbl.TthresholdDist > 0 then
    MsgC( Color( 50, 255, 50 ), tbl.TthresholdDist .. "\n")
  else
    MsgC( Color( 255, 0, 0 ), tbl.TthresholdDist .. "\n")
  end
  print("\n")

  local lastJumps = 20

  MsgC( Color( 255, 255, 255 ), "          ".."Last ".. lastJumps .." Hops : \n" )
  --MsgC( Color( 255, 255, 255 ), "               ")
  local jumps = 0
  local time = 0
  local amount = {}
  local strs =0
  for _, v in pairs(tbl.cache.jumps) do
    if type(v) == "string" then
      table.insert(amount, _)
    end
  end

  --#tbl.cache.jumps
	if !amount[#amount] then return end
  for i= amount[#amount], 1, -1 do

		if strs >= lastJumps then return end
		--print(i-amount[math.Clamp(#amount-lastJumps, 1, #amount)])
    local curJump = tbl.cache.jumps[#tbl.cache.jumps - i]

    if !curJump then
      curJump = 0
    end
    local color = Color(255,255,255)
    local prevJump = tbl.cache.jumps[#tbl.cache.jumps - i -1]
    if type(prevJump) == "string" then
      local function findNextFloat(tbl, num)
        if type(tbl[num]) != "number" then
          return findNextFloat(tbl, num-1)
        end
        return tbl[num]
      end
      prevJump = findNextFloat(tbl.cache.jumps, #tbl.cache.jumps - i -1)
    end
    if !prevJump then
      prevJump = 0
    end
    if type(curJump) == "string" then
      MsgC( color, "J: " .. jumps )
      MsgC( color, " T: " .. math.Round(time,3) )
      MsgC( color, " AT: " .. math.Round(time/jumps,3) )
      MsgC( Color(255,255,255), " | " )
      color = Color(255,255,0)
      MsgC( color, curJump )
      jumps = 0
			strs = math.Clamp(strs+1, 0, lastJumps)
      time = 0
      MsgC( Color(255,255,255), " | " )
    else
      jumps = jumps+1
      time = time + (curJump-prevJump)

    end

  end
  --PrintTable(tbl.cache.jumps)
  --PrintTable(tbl)
end)
