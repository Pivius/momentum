/*
  Avoid making duplicates
*/
if Watchlist then
  print("yes")
  Watchlist:Remove()
end

Watchlist = {}
Watchlist.players = {}                -- Player list
Watchlist.func = function() end       -- Run Function
Watchlist.event = "Think" -- Event

math.randomseed(os.clock()+os.time())       -- Avoid getting the same id as previously created watchlists
Watchlist.uniqueID = math.random( 1, 5000 )

/*
  Find key in table
*/
local function HasKey(table, key)
  for _, value in pairs(table) do
    if _ == key then
      return true
    end
  end
  return false
end

/*
  Init
*/
function Watchlist:Create()
  hook.Call("ReAdd")
end

/*
  Remove
*/
function Watchlist:Remove()
  hook.Remove( self.event, self.uniqueID.." Watchlist" )
  self = nil
end

/*
  Create new function
*/
function Watchlist:Function(func)
  self.func = func
  hook.Call("ReAdd")
end

/*
  Watchlist event
*/
function Watchlist:Event(event)
  hook.Remove( self.event, self.uniqueID.." Watchlist" )
  self.event = event
  hook.Call("ReAdd")
end

/*
  Called functions when player is added or removed
*/
function Watchlist:PlayerAdded(ply)

end
function Watchlist:PlayerRemoved(ply)

end

/*
  Add player to watchlist
*/
function Watchlist:AddPlayer(ply)
  Watchlist.players[ply:SteamID()] = {}
  Watchlist:PlayerAdded(ply)
end

/*
  Remove player from watchlist
*/
function Watchlist:RemovePlayer(ply)
  Watchlist:PlayerRemoved(ply)
  Watchlist.players[ply:SteamID()] = nil
end

/*
  Create/Recreate event hook
*/
function Watchlist:ReAdd()

  if hook.GetTable()[Watchlist.uniqueID.." Watchlist"] then
    hook.Remove( self.event, self.uniqueID.." Watchlist" )
  end
  hook.Add(Watchlist.event, Watchlist.uniqueID.." Watchlist", Watchlist.func)
end
hook.Add("ReAdd", "Watchlist hook", Watchlist.ReAdd)

Watchlist:Create() -- init

local meta = FindMetaTable( "Player" )

function meta:IsWatchlisted()
  if Watchlist.players[self:SteamID()] then
    return true
  end
  return false
end

return Watchlist
