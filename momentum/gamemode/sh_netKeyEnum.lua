keyEnum = {}
// Relevant keys
keyEnum.keys = {
  ["W"] = IN_FORWARD,
  ["A"] = IN_MOVELEFT,
  ["S"] = IN_BACK,
  ["D"] = IN_MOVERIGHT,
  ["J"] = IN_JUMP,
  ["DUCK"] = IN_DUCK,
  ["M1"] = IN_ATTACK,
  ["M2"] = IN_ATTACK2,
  ["JUMP"] = IN_JUMP
}

if SERVER then
  util.AddNetworkString( "KeyEnum_Net" )
  util.AddNetworkString( "KeyEnum_KeyDown" )
end

// Normal KeyDown
function keyEnum:KeyDown(ply, enum)
  if CLIENT then
    if ply == LocalPlayer() then
      return ply:KeyDown(enum)
    else
      return GetGlobalBool( tostring(ply).."_KeyDown_"..enum, false )
    end
  else
    return ply:KeyDown(enum)
  end

end

function keyEnum.KD(ply, mv)
  if ply:IsValid() then
    for k, v in pairs(keyEnum.keys) do
      if mv:KeyDown(v) then
        SetGlobalBool( tostring(ply).."_KeyDown_"..v, true )
      else
        SetGlobalBool( tostring(ply).."_KeyDown_"..v, false )
      end
    end
  end
end
hook.Add("SetupMove", "KeyDown", keyEnum.KD)
