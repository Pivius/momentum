function LongJump(ply, mv, cmd)
  --local sound = ReadSound( "physics/body/body_medium_scrape_smooth_loop1.wav", nil, ply)
	local vForward = mv:GetAngles():Right():GetNormalized()
  vForward:Rotate(Angle(0,90,0))
	vForward.z = 0
	local vRight = mv:GetAngles():Right():GetNormalized()
	vRight.z = 0

  local td = {}
	td.start = mv:GetOrigin()
	td.endpos = mv:GetOrigin()-(Vector(0,0,2))
	td.filter = player.GetAll()

	local tr = util.TraceLine(td)
  if !ply.ljTime then
    ply.ljTime = CurTime()
  end
  if tr.HitWorld then
    ply.ljTime = CurTime()
  elseif !tr.HitWorld and ply:OnGround() and ply.ljTime+0.2 > CurTime() then
    if mv:KeyPressed(IN_JUMP) then
			local mul = 1
			if mv:KeyDown(IN_DUCK) then
				mul = 2.5
			end
      Combo:AddTo(ply, "Edge Jump")
      mv:SetVelocity(mv:GetVelocity()+Vector(0,0,(ply:GetJumpPower()/1.5)*mul))
      mv:SetVelocity(mv:GetVelocity()+(mv:GetVelocity():GetNormalized() *Vector(100,100,1)))
    end
  end
end
hook.Add("SetupMove", "LongJumping", LongJump)
