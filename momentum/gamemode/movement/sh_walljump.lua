
function Runup(ply, mv, tr, cmd)
  local cur_time = CurTime()

  if
    (tr.HitWorld &&
    ply:KeyPressed(IN_JUMP) &&
    mv:KeyDown(IN_ATTACK2) &&
    !ply:RunningUp() &&
    ply:RunUps() < ply:MaxRunUps()) ||
    (CLIENT and ply.sync_module.GetLast() and ply.sync_module.GetLastTime() == cur_time)
  then

    if !ply.sync_module.GetLast() then
      ply.sync_module.Insert(true, cur_time)
    end

    if IsFirstTimePredicted() then
      ply:RunUps(ply:RunUps()+1)
    end

    ply:RunningUp(true)
    if -(mv:GetVelocity().z/50) > 10 and SERVER then
      local dmg = DamageInfo()
    	dmg:SetDamage( math.abs(mv:GetVelocity().z/50) )
    	dmg:SetAttacker( game.GetWorld() )
    	dmg:SetDamageType( DMG_FALL )

    	ply:TakeDamageInfo( dmg )
      ply:EmitSound( "player/pl_fallpain3.wav", 75, 200, 0.5, CHAN_AUTO )
    end

    mv:SetVelocity(mv:GetVelocity()*Vector(0.5,0.5,0))
    if !ply:OnGround() then
      mv:SetVelocity(mv:GetVelocity()+Vector(0,0,220*2))
    else
      mv:SetVelocity(mv:GetVelocity()+Vector(0,0,220))
    end
    Combo:AddTo(ply, "WallRun")
  end
  --print((mv:GetVelocity().z < 0 or ply:OnGround()))



end

function WallJump(ply, mv, cmd)
  if !ply.sync_module then
    ply.sync_module = include( 'momentum/gamemode/modules/sync.lua' )
  end

  local sync_time = ply.sync_module.GetLastTime()

	local vForward = mv:GetAngles():Right():GetNormalized()
  vForward:Rotate(Angle(0,90,0))
	vForward.z = 0

  if ply:OnGround() and IsFirstTimePredicted() then
    ply:RunUps(0)
  end

  local td = {}
	td.start = ply:EyePos()
	td.endpos = ply:EyePos()+(vForward*35)
	td.filter = player.GetAll()

	local trFwd = util.TraceLine(td)

  td = {}
	td.start = ply:EyePos()
	td.endpos = ply:EyePos()-(vForward*35)
	td.filter = player.GetAll()

	local trBk = util.TraceLine(td)


  if !ply.sync_module.GetLast() or CLIENT then
    Runup(ply, mv, trFwd, cmd)
  end

  if ((mv:GetVelocity().z < 0 && ply.sync_module.GetLast() && ply.sync_module.GetLastTime() < CurTime()) || ply:OnGround() ) && IsFirstTimePredicted()  then // Finished running up

    ply:RunningUp(false)
    ply.sync_module.Insert(false, CurTime() )
    ply:WallJump(false)
  end

  if trBk.HitWorld and ply:RunningUp() and ply:KeyPressed(IN_JUMP) and mv:KeyDown(IN_ATTACK2) and !ply:WallJump() then
    ply:WallJump(true)
    --mv:SetVelocity(mv:GetVelocity()*Vector(1,1,3))
  end
end
hook.Add("SetupMove", "WallJumping", WallJump)
