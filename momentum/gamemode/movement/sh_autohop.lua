local meta = FindMetaTable( "Player" )
function DetectLedge(ply, cmd, cud)
	local trace = {}
		trace.start = ply:GetPos()
		trace.endpos = (trace.start - Vector(0,0,2) + Vector(-ply:GetAimVector():Angle():Right().y, ply:GetAimVector():Angle():Right().x,0)*35 )

		local trLow = util.TraceHull( trace )

		local trace2 = {}
		trace2.start = ply:GetPos()
		trace2.endpos = (trace2.start + Vector(0,0,15) + Vector(-ply:GetAimVector():Angle():Right().y, ply:GetAimVector():Angle():Right().x,0)*35 )


			local trHigh = util.TraceHull( trace2 )
			if (!trHigh.HitWorld and trLow.HitWorld) and !ply:IsOnGround() then
				cmd:SetVelocity(cmd:GetVelocity() + Vector(-ply:GetAimVector():Angle():Right().y, ply:GetAimVector():Angle():Right().x,2)*200)

			end
end
--hook.Add("SetupMove", "Detect Ledge", DetectLedge)
/*
function meta:LeftGround(mv)
	local onground = self:OnGround()
	local jumpkey  = mv:KeyPressed(IN_JUMP)
	local jumping  = false

	local trace 	 = {}
	trace.start 	 = self:GetPos()
	trace.endpos 	 = (trace.start - Vector(0,0,26)  )
  trace.filter = player.GetAll()
	local tr 			 = util.TraceHull( trace )

	if onground and jumpkey then

    return true
  end
  return false

end
*/
function meta:LandGround(vel)
	local onground = self:IsOnGround()
	local jumpkey  = self:KeyDown(IN_JUMP)
	local jumping  = false

	local trace 	 = {}
	trace.start 	 = self:GetPos()
	trace.endpos 	 = (trace.start - Vector(0,0,15)  )
  trace.filter = player.GetAll()
	local tr 			 = util.TraceHull( trace )

	if tr.HitWorld and vel.z < 0  then
    return true
  end
  return false

end
function meta:ChargeHop()
  local time = CurTime()
  local chargeStages = 3
  local chargeTime = 0.35//PerStage
  local startPower = 220
  local endPower = 500
  local incr = (endPower-startPower)/chargeStages
	if !self:OnGround() then
		self:ChargeTime(time+chargeTime)
	end
  if self:ChargeTime() <= time and self:Charge() < chargeStages then
    self:ChargeTime(time+chargeTime)
    self:Charge(math.Clamp(self:Charge()+1, -1, chargeStages))
    if self:Charge() > 0 then
      if SERVER then
        	self:EmitSound("player/suit_sprint.wav", 500, 200)
				if self:Charge() == chargeStages then
					self:EmitSound("player/suit_denydevice.wav", 500, 200)
				end
      end
	  end
		self:SetJumpPower(math.Clamp(startPower+(incr*self:Charge()), 220, 9999))
  end
end
/*
function meta:DoubleJump()
	if not IsFirstTimePredicted() then return end

	if !self:GetHops() then
		self:SetHops(1)
	end
	local jumps = self:GetHops()
	local mul = (250 * 2)+10
	if jumps == 1 then
		self:SetHopTimer(CurTime() + 0.4)
	elseif jumps > 1 then
		self:SetHopTimer(CurTime() + 0.66)
	end
		self:SetHops( self:GetHops() + 1)

	if jumps >= 2  then
		self:SetJumpPower(mul)

  else
		self:SetJumpPower(300)
	end
end*/

function Bhop(ply,mv, cmd)
	--local ply = LocalPlayer()
	--if !IsFirstTimePredicted() then return end
	if !ply:Alive() or ply:WaterLevel() >= 1 then return end
  local vel = mv:GetVelocity()
	local sound = "plats/hall_elev_door.wav"
  local time = CurTime()
	if !ply:OnGround() and !ply:LeftGround() then
		hook.Call("PlayerLeftGround", nil, ply)
	end

  if ply:JumpHeld() == true and ply:DoHop() == false then
    ply:DoHop(true)

  end

  if (ply:DoHop() and bit.band( mv:GetButtons(),  IN_JUMP ) == 2) then
    ply:CanHop(true)

  end
  if ply:IsOnGround() then

    --ply.pm_time = CurTime()+0.45
  end

	if (!ply:OnGround() and ply:CanHop()  ) then
    if IsFirstTimePredicted() then
      ply.plyTickJump = cmd:TickCount()

    end
  	mv:SetButtons( bit.band( mv:GetButtons(), bit.bnot( IN_JUMP ) ) )

  end

  if ply:OnGround() then

      ply:DoHop(false)
    timer.Create(ply:SteamID().."Hop", 0.1, 1, function()
      if !ply:DoHop() then
        ply:CanHop(false)
      end
    end)
  end

  if (ply.plyTickJump == cmd:TickCount() and CLIENT and ply:DoHop()) then
    ply:CanHop(true)
  end
end
--hook.Add( 'SetupMove', 'Queue Hop', Bhop)
