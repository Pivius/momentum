STEP_TIME = 200
STEPSIZE = 18
MAX_STEP_CHANGE = 32
EV_STEP_4 = 12
EV_STEP_8 = 13
EV_STEP_12 = 14
EV_STEP_16 = 15

local function PM_ClipVelocity( vecin, normal, vecout, overbounce )
	local	backoff;
	local	change;
	local		i;

	backoff = vecin:Dot(normal)

	if ( backoff < 0 ) then
		backoff = backoff*overbounce;
	else
		backoff = backoff/overbounce;
	end

	for i=1, 3 do
		change = normal[i]*backoff;
		vecout[i] = vecin[i] - change;
	end
end

local function CrossProduct( v1, v2, cross )
	cross[1] = v1[2]*v2[3] - v1[3]*v2[2]
	cross[2] = v1[3]*v2[1] - v1[1]*v2[3]
	cross[3] = v1[1]*v2[2] - v1[2]*v2[1]
end


/*
==================
PM_SlideMove
Returns qtrue if the velocity was clipped in some way
==================
*/
local	MAX_CLIP_PLANES =	5
function PM_SlideMove( ply, mv, cmd, gravity )
	local	bumpcount, numbumps;
  local bcount = 1
	local	dir = Vector()
	local	d;
	local	numplanes;
	local	planes = {Vector(0,0,0), Vector(0,0,0), Vector(0,0,0), Vector(0,0,0), Vector(0,0,0)}
	local	primal_velocity = Vector()
	local	clipVelocity= Vector()
	local	i, j, k;
	local	trace;
	local	endPos;
	local	time_left;
	local	into;
	local	endVelocity= Vector()
	local	endClipVelocity= Vector()
  local velocity = mv:GetVelocity()
  local cTime = FrameTime()*cmd:TickCount()
  local groundplane
	numbumps = 4;

	local Pos = mv:GetOrigin()
  local eP = Pos *1
	local moveDir = mv:GetVelocity():GetNormalized()
  if !ply.impactSpeed then
    ply.impactSpeed = 0
  end
  if !ply.pm_time then
    ply.pm_time = include( 'momentum/gamemode/modules/sync.lua' )
		ply.pm_time.Insert(false, 0)
  end

  if !ply.primal then
    ply.primal = velocity
  end

  eP.z = (eP.z - 0.25) -- -2- (math.Clamp(vel.z*FrameTime(), -vel.z*FrameTime(), 0)) // trace a bit further than feet
  local tr = {
    start = Pos,
    endpos = eP,
    mins = ply:OBBMins(),
    maxs = ply:OBBMaxs(),
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }


  local tL = util.TraceHull( tr)

  groundplane = tL.Hit
  primal_velocity:Set(velocity)
	if ( gravity ) then
    endVelocity = velocity
    endVelocity.z = endVelocity.z - (gravity * FrameTime())
    velocity.z = (velocity.z+endVelocity.z) * 0.5
		primal_velocity.z = endVelocity.z

		if ( groundPlane ) then
      // slide along the ground plane
      PM_ClipVelocity( velocity, tL.HitNormal, velocity, 1.001 )
      mv:SetVelocity(velocity)
		end
	end

	time_left = FrameTime()

	// never turn against the ground plane
	if ( groundPlane ) then
		numplanes = 1;
    planes = tL.HitNormal
	else
		numplanes = 0;
	end

	// never turn against original velocity
  planes[numplanes] = velocity:GetNormalized()
	numplanes = numplanes+1

	for bumpcount=1, numbumps do

		// calculate position we are trying to move to
    endPos = Pos + (time_left*velocity)
		// see if we can make it there
    trace = {
      start = Pos,
      endpos = endPos,
      mins = ply:OBBMins(),
      maxs = ply:OBBMaxs(),
      mask = MASK_PLAYERSOLID_BRUSHONLY,
      filter = function(e1, e2)
        return not e1:IsPlayer()
      end
    }
    trace = util.TraceHull( trace)

		if (trace.AllSolid) then
			// entity is completely trapped in another solid
			velocity.z = 0;	// don't build up falling damage, but allow sideways acceleration
      mv:SetVelocity(velocity)
			return true
		end

		if (trace.Fraction > 0) then
      Pos = trace.HitPos
		end

		if (trace.fraction == 1) then
			 break
		end

		// save entity for contact
		--PM_AddTouchEnt( trace.entityNum );

		time_left = time_left - (time_left * trace.Fraction)

		if (numplanes >= MAX_CLIP_PLANES) then
			// this shouldn't really happen
			velocity = Vector(velocity.x,velocity.y,0)
      mv:SetVelocity(velocity)
			return true
		end

		//
		// if this is the same plane we hit before, nudge velocity
		// out along it, which fixes some epsilon issues with
		// non-axial planes
		//
		for i = 1, numplanes do

			if ( trace.HitNormal:Dot(planes[i] ) > 0.99) then
        velocity = trace.HitNormal+velocity

        --mv:SetVelocity(velocity)
				break
			end
      if ( i < numplanes ) then
  			continue
  		end
		end

    planes[numplanes] = trace.HitNormal
		numplanes = numplanes+1

		//
		// modify velocity so it parallels all of the clip planes
		//

		// find a plane that it enters
		for i = 1, numplanes do
			into = velocity:Dot(planes[i])
			if ( into >= 0.1 ) then
				continue;		// move doesn't interact with the plane
			end

			// see how hard we are hitting things
			if ( -into > ply.impactSpeed ) then
				ply.impactSpeed = -into;
			end

			// slide along the plane
			PM_ClipVelocity(velocity, planes[i], clipVelocity, 1.001 );

			// slide along the plane
			PM_ClipVelocity(endVelocity, planes[i], endClipVelocity, 1.001 );

			// see if there is a second plane that the new move enters
			for j = 1, numplanes do
				if ( j == i ) then
					continue;
				end
				if ( clipVelocity:Dot( planes[j] ) >= 0.1 ) then
					continue;		// move doesn't interact with the plane
				end

				// try clipping the move to the plane
			  PM_ClipVelocity( clipVelocity, planes[j], clipVelocity, 1.001 );
				PM_ClipVelocity( endClipVelocity, planes[j], endClipVelocity, 1.001 );

				// see if it goes back into the first clip plane
				if ( clipVelocity:Dot( planes[i] ) >= 0 ) then
					continue;
				end

				// slide the original velocity along the crease
				CrossProduct(planes[i], planes[j], dir)
				dir:Normalize()
				d = dir:Dot( velocity )
        clipVelocity = dir*d
				CrossProduct(planes[i], planes[j], dir);
				dir:Normalize()
				d = dir:Dot( endVelocity )
        endClipVelocity = dir*d

				// see if there is a third plane the the new move enters
				for k = 1, numplanes do
					if ( k == i || k == j ) then
						continue;
					end
					if ( clipVelocity:Dot(planes[k] ) >= 0.1 ) then
						continue;		// move doesn't interact with the plane
					end

					// stop dead at a tripple plane interaction
					--velocity = Vector(velocity.x,velocity.y,0)
          --mv:SetVelocity(velocity)
					return true;
				end
			end

			// if we have fixed all interactions, try another move
      velocity = clipVelocity
      endVelocity = endClipVelocity
      --mv:SetVelocity(velocity)

			break
		end

	end

	if ( gravity ) then
    velocity = endVelocity

    --mv:SetVelocity(velocity)

    local fLateralStoppingAmount = primal_velocity:Length2D() - velocity:Length2D()

  	if fLateralStoppingAmount > 1 and mv:KeyDown(IN_ATTACK2) and !ply:OnGround() and IsFirstTimePredicted() and !ply.HitWall and mv:GetVelocity():Length2D() > 32 then

      ply.primal = primal_velocity
    	ply.pm_time.Insert(true, CurTime()+0.25)
      ply.HitWall = true
    end
				--print(ply.pm_time.GetLastTime())

    if ( ply.pm_time.GetLastTime() >= CurTime() ) then
      local addVelocity = ply.primal - primal_velocity
      local tr = {
        start = mv:GetOrigin(),
      endpos = mv:GetOrigin(),
        mins = ply:OBBMins()-Vector(2,2,0),
        maxs = ply:OBBMaxs()+Vector(2,2,0),
        mask = MASK_PLAYERSOLID_BRUSHONLY,
        filter = function(e1, e2)
          return not e1:IsPlayer()
        end
      }
      tr = util.TraceHull(tr)
      --if !tr.Hit  then

        mv:SetVelocity(Vector(velocity.x + addVelocity.x,velocity.y + addVelocity.y, mv:GetVelocity().z))
      --end
    elseif ply.pm_time.GetLastTime() < CurTime() then

      ply.HitWall = false
    end
  end
  local addVelocity = ply.primal - primal_velocity
  local wishDir = Vector(addVelocity.x,addVelocity.y,0):GetNormalized()

  if (primal_velocity*Vector(1,1,0)):Dot(wishDir) < -5 then
		--if ply.pm_time.GetLastTime() then
			ply.pm_time.Insert(false, 0)
    --ply.pm_time =0
  end

	return ( bcount != 1 );
end

/*
==================
PM_StepSlideMove
==================
*/

function PM_StepSlideMove( ply, mv, cmd, gravity )
	local	start_o = Vector()
  local start_v = Vector()
	local down_o  = Vector()
  local down_v  = Vector()
  local	up      = Vector()
  local down    = Vector()
	local	trace, stepSize
  local origin = mv:GetOrigin()

  if ply:Sliding() then

    return
  end

	if ( PM_SlideMove( ply, mv, cmd, gravity )  == 1 ) then
		return;		// we got exactly where we wanted to go first try
	end
  local velocity = mv:GetVelocity()
  start_o:Set( origin )
  start_v:Set( velocity )

  down:Set(start_o)
	down.z = down.z - STEPSIZE;
  trace = {
    start = start_o,
    endpos = down,
    mins = ply:OBBMins(),
    maxs = ply:OBBMaxs(),
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }
  trace = util.TraceHull( trace)

  up = Vector(0,0,1)
	// never step up when you still have up velocity
	if ( velocity.z > 0 && (trace.Fraction == 1.0 || trace.HitNormal:Dot(up) < 0.7)) then

	return;
	end

  down_o:Set( origin )
  down_v:Set( velocity )

  up:Set(start_o)
	up.z = up.z + STEPSIZE;

	// test the player position if they were a stepheight higher
  trace = {
    start = start_o,
    endpos = up,
    mins = ply:OBBMins(),
    maxs = ply:OBBMaxs(),
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }
  trace = util.TraceHull( trace)

	if ( trace.AllSolid ) then
		--if ( pm->debugLevel ) then
			--Com_Printf("%i:bend can't step\n", c_pmove);
		--}
		return;		// can't step up
	end

	stepSize = trace.HitPos.z - start_o.z

  origin:Set(trace.HitPos)
  velocity:Set( start_v )
  mv:SetOrigin(trace.HitPos)
  mv:SetVelocity(velocity)
  // try slidemove from this position
	PM_SlideMove( ply, mv, cmd, gravity );
  velocity = mv:GetVelocity()
  --origin = mv:GetOrigin()
	// push down the final amount
  down:Set(origin)
	down.z = down.z - stepSize;

  trace = {
    start = origin,
    endpos = down,
    mins = ply:OBBMins(),
    maxs = ply:OBBMaxs(),
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }
  trace = util.TraceHull( trace)

	if ( !trace.AllSolid ) then
    origin = trace.HitPos
    mv:SetOrigin(origin)
	end
	if ( trace.Fraction < 1.0 ) then
		PM_ClipVelocity( velocity, trace.HitNormal, velocity, 1.001 );
    mv:SetVelocity(velocity)
	end



  if true then
  	// if the down trace can trace back to the original position directly, don't step
    trace = {
      start = origin,
      endpos = start_o,
      mins = ply:OBBMins(),
      maxs = ply:OBBMaxs(),
      mask = MASK_PLAYERSOLID_BRUSHONLY,
      filter = function(e1, e2)
        return not e1:IsPlayer()
      end
    }
    trace = util.TraceHull(trace)

  	if ( trace.Fraction == 1.0 ) then
  		// use the original move
      origin:Set(down_o)
      velocity:Set(down_v)
      --mv:SetOrigin(origin)
      mv:SetVelocity(velocity)
  	end

  	// use the step move
  	local	delta;

  	delta =origin.z - start_o.z;

  	if ( delta > 2 ) then
  		if ( delta < 7 ) then
  			ply.step = EV_STEP_4
  		elseif ( delta < 11 ) then
  			ply.step = EV_STEP_8
  		elseif ( delta < 15 ) then
  			ply.step = EV_STEP_12
  		else
  			ply.step = EV_STEP_16
  		end
    end
	end
end

function Step(ply)
  local	oldStep;
	local		delta;
	local		step;

		// check for stepping up before a previous step is completed
    if !ply.stepTime then ply.stepTime = 0 end
    if !ply.step then ply.step = EV_STEP_4 end
    if !ply.stepChange then ply.stepChange = 1 end

		delta = CurTime() - ply.stepTime;
		if (delta < STEP_TIME) then
			oldStep = ply.stepChange * (STEP_TIME - delta) / STEP_TIME;
		else
			oldStep = 0;
		end

		// add this amount
		step = 4 * (ply.step - EV_STEP_4 + 1 );
		ply.stepChange = oldStep + step;
		if ( ply.stepChange > MAX_STEP_CHANGE ) then
			ply.stepChange = MAX_STEP_CHANGE;
		end
    if ply:GetStepSize() != ply.stepChange then
      ply:SetStepSize( ply.stepChange )
    end

		ply.stepTime = CurTime()
end
