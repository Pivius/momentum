function Noclip(ply, mv)
  if CLIENT then
    if ply:GetNWBool("Noclip") ~= ply:Noclip() then

      ply:Noclip(ply:GetNWBool("Noclip"))
      if ply:Noclip() then
        ply:SetGroundEntity(NULL)
      end
    end
  else
    if ply:GetNWBool("Noclip") ~= ply:Noclip() then
      if ply:Noclip() == false then
        mv:SetVelocity(Vector(0,0,0))
        mv:SendToGround()
      end
      ply:SetNWBool("Noclip", ply:Noclip())
      if ply:Noclip() then
        ply:SetGroundEntity(NULL)
      end
    end
  end
  -- Disable noclip
  if !ply:Noclip() then

  return end

  local forward = mv:GetForwardSpeed()
  local side = mv:GetSideSpeed()
  local up = mv:GetUpSpeed()
  local ang = mv:GetMoveAngles()
  local speed = 150
  local pos = mv:GetOrigin()

  if ( mv:KeyDown( IN_DUCK ) ) then speed = 25 end
  if ( mv:KeyDown( IN_SPEED ) ) then speed = speed * 2 end
  local acceleration = ( ang:Forward() * mv:GetForwardSpeed() ) + ( ang:Right() * mv:GetSideSpeed() ) + ( ang:Up() * mv:GetUpSpeed() )

  local accelSpeed = math.min( acceleration:Length(), ply:GetMaxSpeed() ) --ply:GetMaxSpeed()
  local accelDir = acceleration:GetNormal()
  acceleration = (accelDir * accelSpeed)*(speed/3.6)

  local newVelocity = mv:GetVelocity() + acceleration * FrameTime()
  newVelocity = newVelocity * (0.871059- FrameTime());

  // set velocity
  mv:SetVelocity( newVelocity );

  // move the player
  local newOrigin = mv:GetOrigin() + newVelocity * FrameTime()
  mv:SetOrigin( newOrigin )
	return true

end
hook.Add( "Move", "Noclip move", Noclip )
