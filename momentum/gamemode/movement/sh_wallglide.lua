local PLAYER = FindMetaTable( "Player" )
local moveKeys = {
	[1] = IN_FORWARD,
	[2] = IN_BACK,
	[3] = IN_MOVERIGHT,
	[4] = IN_MOVELEFT
}

-- Trace to wall
function PLAYER:checkWall(mv, dir)
	local td = {}
	td.start = mv:GetOrigin()+Vector(0,0,50)
	td.endpos = mv:GetOrigin()-(dir*20)
	td.filter = player.GetAll()
	local tr = util.TraceLine(td)
	if (tr.Fraction < 1) and not tr.HitSky then
		return true
	end
	return false
end

function WallGliding(ply, mv, cmd)
  local sound = ReadSound( "physics/body/body_medium_scrape_smooth_loop1.wav", nil, ply)
  if !ply:OnGround() and mv:KeyDown(IN_ATTACK2) then
		local vForward = mv:GetAngles():Right():GetNormalized()
    vForward:Rotate(Angle(0,90,0))
		vForward.z = 0
		local vRight = mv:GetAngles():Right():GetNormalized()
		vRight.z = 0

		--Directions
		local dirs = {
			[1] = vForward,
			[2] = -vForward,
			[3] = vRight,
			[4] = -vRight
		}
    local grav = 2
    for k, v in pairs(dirs) do
      if ply:checkWall(mv, v) then
        if mv:GetVelocity().z > 10 and mv:GetVelocity().z < 50 then
          grav = 5
          ply:CanFire(true)
        end
        sound:Play()
        mv:SetVelocity(mv:GetVelocity()+Vector(0,0,grav))
      end
    end
  else
  sound:FadeOut( 0.25 )
  end
end
--hook.Add("SetupMove", "Wall Gliding", WallGliding)
