

include( 'sql/database.lua' )
include( 'sql/core.lua' )
include( 'sh_hook.lua')
include( 'shared.lua' )

AddCSLuaFile( 'sh_hook.lua')
AddCSLuaFile( 'modules/cl_hudmodule.lua' )
AddCSLuaFile( 'modules/cl_ease.lua' )
AddCSLuaFile( 'modules/sync.lua' )
AddCSLuaFile( 'shared.lua' )
AddCSLuaFile( "player_class/player_sandbox.lua" )
AddCSLuaFile( 'vgui/topics.lua' )


DEFINE_BASECLASS( 'gamemode_base' )
GM.PlayerSpawnTime = {}

hook.Add('Initialize', 'console', function()
	game.ConsoleCommand('sv_maxvelocity 9999\n')
	game.ConsoleCommand( 'sv_friction 8\n' )
	game.ConsoleCommand( 'sv_gravity 400\n' )
	game.ConsoleCommand('sv_sticktoground 1\n')
	game.ConsoleCommand('sv_airaccelerate 10\n')
	game.ConsoleCommand('sv_accelerate 10\n')
	game.ConsoleCommand('mp_falldamage 0\n')

end)
stats:Init()
Admin:Init()

--[[---------------------------------------------------------
   Name: gamemode:PlayerSpawn( )
   Desc: Called when a player spawns
-----------------------------------------------------------]]
function GM:PlayerSpawn( pl )


	player_manager.SetPlayerClass( pl, 'player_sandbox' )

	BaseClass.PlayerSpawn( self, pl )

	pl:SetAvoidPlayers( false )
	pl:SetCollisionGroup( COLLISION_GROUP_WEAPON )

end

-- Set the ServerName every 30 seconds in case it changes..
-- This is for backwards compatibility only - client can now use GetHostName()
local function HostnameThink()

	SetGlobalString( "ServerName", GetHostName() )

end
timer.Create( "HostnameThink", 30, 0, HostnameThink )

function GM:GetFallDamage( ply, speed )
	 return ( speed / 50)
end

/*
  NAME - Update on Join
  FUNCTION - Duh!
*/

hook.Add("PlayerInitialSpawn", "Send_CMD", function(ply)
  if ply:IsBot() then return end
	timer.Simple(0.5, function()
		local rank = Admin.Ranks[ply:GetRank()]
	  local tbl = {}
		print(ply:GetRank())
	  for cmd, v in pairs(Admin.Commands.cmds) do
	  	if Admin.Commands.cmds[cmd] and (UT:TblContain(rank["cmds"], "*") or UT:TblContain(rank["cmds"], cmd)) then
	  		tbl[cmd] = v
		  end
	  end
	  net.Start("VGAM_sendCMD")
	    net.WriteTable(table.GetKeys( tbl))
	  net.Send(ply)
	end)
end)
