customHook = {}
customHook.event = {}


function hookAdd(event, identifier, func)
  if !event or !identifier or !func then return end
  if !customHook.event[event] then
		customHook.event[event] = {}
	end
  customHook.event[event][identifier] = func
end

function hookCall(event, ...)
  if !event then return end
  local vararg = {...}
  if !vararg then
    vararg = {}
  end

  if customHook.event[event] then
    for ident,func in pairs(customHook.event[event]) do
      return func(unpack(vararg))
    end

  end
end
