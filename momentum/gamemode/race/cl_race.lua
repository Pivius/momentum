Race = {} || Race
Race.Open = false
Race.Locked = false
Race.Grid = 8
Race.GridRadius = 10
Race.Rotation = 22.5
Race.Selected = false
Race.CurCheckpoint = false

// 0 = Non-Restrict, 1 = Horizontal, 2 = Vertical
Race.ZoneMode = 0
Race.HitPos = Vector(0,0,0)

Race.StartFinished = false || Race.StartFinished
Race.EndFinished = false

Race.ZonePrepare = {}
Race.StartZone = {} || Race.StartZone
Race.StartZone.Ang = Angle(0,0,0)
Race.Checkpoints = {}
Race.EndZone = {}
Race.EndZone.Ang = Angle(0,0,0)
local render = render
local math = math
local sqrt = math.sqrt

local function Ray(pos, ang, scale, x, y, w, h)
  local trace = util.IntersectRayWithPlane(LocalPlayer():EyePos(), LocalPlayer():GetAimVector(), pos, ang:Up())
  if trace then
    local pos = WorldToLocal(trace, Angle(), pos, ang)
    local xpos = pos.x / scale
    local ypos = (-pos.y) / scale
    local px, py, sx, sy = x, y, w, h
    local hover = xpos >= px && ypos >= py && xpos <= px + sx && ypos <= py + sy
    return hover
  else
    return false
  end
end

/*
  NAME - Race.SetZone
  FUNCTION -
	VARIABLEs -
							tbl - Table to insert data into
							...  - Arguments that are inserted into the table
*/
function Race.SetZone(tbl, ...)
  local args = {...}
  for k, v in pairs(args) do
    tbl[#tbl+k] = Vector(v.x, v.y, v.z)

  end
end
/*
  pos - origin
  grid - grid size
  w - length of the grid
  h - height of the grid
  lw - line width
  col - color
*/
function drawGrid(pos, grid, w, h, lw, col, hitnorm)
  local endcol = Color(col.r,col.g,col.b,0)

  local function renderLine(dir, start, gr)
    if !start then
      start = pos
    end
    if !gr then
      gr = 1
    end
    for i=gr, math.Round(w/grid) do
      local pos = start -(dir*grid)
      local Col = Color(col.r, col.g, col.b, Lerp(i/math.Round(w/grid)/1.1,col.a, 0))
      local eCol = Color(col.r, col.g, col.b, Lerp((i+1)/math.Round(w/grid)/1.1,col.a, 0))
      local startPos = pos+((dir*grid)*(i-(gr-1)))
      local endPos = startPos + ((dir*grid))
      render.StartBeam( 2 )
        render.AddBeam( startPos, lw, 1, Col)
        render.AddBeam( endPos, lw, 1, eCol)
      render.EndBeam()
    end
  end

  local function Branch(dir, brDir, origin, grPos)
    if !origin then
      origin = pos
    end
    if !grPos then
      grPos = 2
    end
    for i=grPos, math.Round(w/grid) do
      local wide = Vector(-lw/2-0.1,0,0)
      if dir.x < 0 then
        wide = Vector(lw/2,0,0)
      else
        wide = Vector(-lw/2,0,0)
      end
      local brS = origin+((brDir*grid)*(i-1))-wide
      renderLine( dir, brS, i )
      renderLine( -dir, brS, i )
      local brU = origin+((Vector(0,0,math.Clamp(brS.x+brS.y, -1, 1))*grid)*(i-1))-wide
      --renderLine( Vector(0,0,math.Clamp(dir.x+dir.y,-1,1)), brS, i )
      --renderLine( -Vector(0,0,math.Clamp(dir.x+dir.y,-1,1)), brS, i )
    end
  end
  if hitnorm:Length() == 0 then
    local plyAng = LocalPlayer():GetAngles():Forward()
    plyAng:Normalize()
    hitnorm = Vector(math.Round(plyAng.x/1),math.Round(plyAng.y/1),math.Round(plyAng.z/1))*1
  end
  //Right
  renderLine(Vector(-hitnorm.y, math.Clamp(hitnorm.x+hitnorm.z,-1,1), 0))
  Branch(Vector(0,0,math.Clamp(hitnorm.y+hitnorm.x,-1,1)), Vector(-hitnorm.y, math.Clamp(hitnorm.x+hitnorm.z,-1,1), 0))
  //Left
  renderLine(-Vector(-hitnorm.y, math.Clamp(hitnorm.x+hitnorm.z,-1,1), 0))
  Branch(Vector(0,0,math.Clamp(hitnorm.y+hitnorm.x,-1,1)), -Vector(-hitnorm.y, math.Clamp(hitnorm.x+hitnorm.z,-1,1), 0))
  //Forward
  renderLine(Vector(-hitnorm.z, 0, math.Clamp(hitnorm.y+hitnorm.x,-1,1)))
  --Branch(Vector(0,1,0), Vector(1,0,0))
  //Back
  renderLine(-Vector(-hitnorm.z, 0, math.Clamp(hitnorm.y+hitnorm.x,-1,1)))
  --Branch(Vector(0,1,0), Vector(-1,0,0))
  //Up
  --renderLine(Vector(0,0,1))
  //Down
  --renderLine(Vector(0,0,-1))

  /*
  for i=1, math.Round(w/grid) do
    Branch(Vector(0,0,1), Vector(1,0,0), pos+Vector(-grid*i,grid*i,0), i+2)
  end
  for i=1, math.Round(w/grid) do
    Branch(Vector(0,0,1), Vector(1,0,0), pos+Vector(-grid*i,grid*i,0), i+2)
  end
  for i=1, math.Round(w/grid) do
    Branch(Vector(0,0,1), Vector(-1,0,0), pos+Vector(grid*i,grid*i,0), i+2)
  end*/
  /*
  //Right
  render.StartBeam( 2 )
    render.AddBeam( pos, 2, 1, col)
    render.AddBeam( pos-Vector(w,0,0), 2, 1, endcol)
  render.EndBeam()

  //Forward
  render.StartBeam( 2 )
    render.AddBeam( pos, 2, 1, col)
    render.AddBeam( pos+Vector(0,w,0), 2, 1, endcol)
  render.EndBeam()

  //Back
  render.StartBeam( 2 )
    render.AddBeam( pos, 2, 1, col)
    render.AddBeam( pos-Vector(0,w,0), 2, 1, endcol)
  render.EndBeam()*/
end
local function GetClosestVector(to, tbl)
  local closest = 9999999
  local key = 1
  for _, v in ipairs(tbl) do
    local compare = to:DistToSqr(v)
    if compare <= closest then
      closest = compare
      key = _
    end
    if _ == #tbl then
      return tbl[key]
    end
  end

end

function ZoneStencil(bot, top, dist, width, color, alpha, ref)
  if ! ref then
    ref = 3
  end
  local minDist = Vector(-(width) +dist.x,-(width)+dist.y,-(width)+dist.z)
  --local dist = Vector(width, width, width)
  render.ClearStencil()

  render.SetStencilEnable(true)

  render.SetStencilPassOperation( STENCILOPERATION_KEEP )
	render.SetStencilFailOperation( STENCILOPERATION_KEEP )
  render.SetColorMaterial()
  render.SetStencilCompareFunction(STENCIL_ALWAYS)
  render.SetStencilZFailOperation( STENCILOPERATION_REPLACE )
    render.DrawBox(Vector(),Angle(), top+dist, bot-dist, Color(0,0,0,0)) // Inside Out
    --render.DrawBox(Vector(),Angle(), bot-dist2, top-dist2, Color(0,0,0,200)) // Inside Out
    --render.DrawSphere(B1, -100, 50, 50, Color(0,0,0,0))
    render.SetStencilCompareFunction(STENCIL_ALWAYS)
    render.SetStencilZFailOperation( STENCILOPERATION_INCR )
    render.SetStencilReferenceValue(ref)
    --render.DrawBox(Vector(),Angle(), bot+dist2, top+dist2, Color(color.r,color.g,color.b,alpha))
    render.DrawBox(Vector(),Angle(), bot-dist, top+dist, Color(color.r,color.g,color.b,alpha))
    /*
    render.SetStencilCompareFunction(STENCIL_ALWAYS)
    render.SetStencilZFailOperation( STENCILOPERATION_INCR )
    render.SetStencilReferenceValue(ref)
    render.DrawBox(Vector(),Angle(), bot-dist2, top-dist2, Color(0,0,0,200))

    render.SetStencilCompareFunction(STENCIL_ALWAYS)
    render.SetStencilZFailOperation( STENCILOPERATION_INCR )
    render.SetStencilReferenceValue(ref)
    render.DrawBox(Vector(),Angle(), bot+dist2, top+dist2, Color(0,0,0,200))*/

    render.SetStencilCompareFunction(STENCIL_ALWAYS)
    render.SetStencilZFailOperation( STENCILOPERATION_INCR )
    render.SetStencilReferenceValue(ref)

    render.DrawBox(Vector(),Angle(), top+minDist, bot-minDist, Color(0,0,0,0)) // Inside Out
    --render.DrawSphere(B1, -90, 50, 50, Color(0,0,0,0))
    render.SetStencilCompareFunction(STENCIL_ALWAYS)
    render.SetStencilZFailOperation( STENCILOPERATION_DECR )
    render.SetStencilReferenceValue(ref)
    render.DrawBox(Vector(),Angle(), bot-minDist, top+minDist, Color(0,0,0,0))
    --render.DrawSphere(B1, 90, 50, 50, Color(0,0,0,0))
    render.SetStencilCompareFunction(STENCIL_EQUAL)
    render.SetStencilReferenceValue(ref)
    render.SetColorMaterialIgnoreZ()
    render.DrawBox(Vector(),Angle(), top+dist, bot-dist, Color( color.r, color.g, color.b, 255 ))

  render.SetStencilEnable(false)
end

local function DrawZone(Min, Max, Col)

  local DrawMaterial = Material( "trick/trails/beam_generic_white.vmt" )
  --render.SetMaterial( DrawMaterial )
  local width = 2
  local RealMin = Vector(math.min(Min.x,Max.x), math.min(Min.y,Max.y), math.min(Min.z,Max.z))
  local RealMax = Vector(math.max(Min.x,Max.x), math.max(Min.y,Max.y), math.max(Min.z,Max.z))
  Min = RealMin-Vector(width*1.5,width*1.5,width*1.5)
  Max = RealMax+Vector(width*1.5,width*1.5,width*1.5)
	local B1, B2, B3, B4 = Vector(Min.x, Min.y, Min.z), Vector(Min.x, Max.y, Min.z), Vector(Max.x, Max.y, Min.z), Vector(Max.x, Min.y, Min.z)
	local T1, T2, T3, T4 = Vector(Min.x, Min.y, Max.z), Vector(Min.x, Max.y, Max.z), Vector(Max.x, Max.y, Max.z), Vector(Max.x, Min.y, Max.z)

  local local_min = WorldToLocal( Min, Angle(), B1, Angle() )
  local local_max = WorldToLocal( Max, Angle(), B1, Angle() )
  local center = Vector(Min.x+local_max.x/2, Min.y+local_max.y/2, Min.z+local_max.z/2)
  local closest_vector = GetClosestVector(LocalPlayer():GetPos(), {B1,B2,B3,B4,T1,T2,T3,T4})
  local distance = math.sqrt(LocalPlayer():GetPos():DistToSqr(closest_vector))
  local player_distance = distance--LocalPlayer():GetPos():Distance(closest)
  local dist = Vector(-(width*2)-(player_distance/1000),-(width*2)-(player_distance/1000),(width*2)-(player_distance/1000))

  // Make the whole thing the color to make it more visible at distance
  // Start fading it in at 1000 units away from it, then 150 alpha at 7500 units away
  local full_alpha = math.min((math.max(distance-(1000),0)*(100))/(3200), 255)
  --ZoneStencil(B1, T3, dist, minDist, Col, full_alpha)
  local test_dist = Vector(2,2,2)
  local test_mindist = test_dist - Vector(2,2,2)
  ZoneStencil(B1, T3, dist, width, Col, full_alpha, 3)
  --ZoneStencil(B2, T3, dist, width, Col, full_alpha, 3)
  --ZoneStencil(B4, T3, dist, width, Col, full_alpha, 3)
  --ZoneStencil(B1, T4, dist, width, Col, full_alpha, 3)

  render.SetColorMaterial()
		render.DrawBeam( T1-Vector(dist.x-1, dist.y-1, -dist.z), T2-Vector(dist.x-1, -dist.y+1, -dist.z), width, 0, 1, Col )
		render.DrawBeam( T2-Vector(dist.x-1, -dist.y+1, -dist.z), T3-Vector(-dist.x+1, -dist.y+1, -dist.z), width, 0, 1, Col )
		render.DrawBeam( T3-Vector(-dist.x+1, -dist.y+1, -dist.z), T4-Vector(-dist.x+1, dist.y-1, -dist.z), width, 0, 1, Col )
		render.DrawBeam( T4-Vector(-dist.x+1, dist.y-1, -dist.z), T1-Vector(dist.x-1, dist.y-1, -dist.z), width, 0, 1, Col )

		render.DrawBeam( B1-Vector(dist.x-1, dist.y-1, dist.z), B2-Vector(dist.x-1, -dist.y+1, dist.z), width, 0, 1, Col )
		render.DrawBeam( B2-Vector(dist.x-1, -dist.y+1, dist.z), B3-Vector(-dist.x+1, -dist.y+1, dist.z), width, 0, 1, Col )
		render.DrawBeam( B3-Vector(-dist.x+1, -dist.y+1, dist.z), B4-Vector(-dist.x+1, dist.y-1, dist.z), width, 0, 1, Col )
		render.DrawBeam( B4-Vector(-dist.x+1, dist.y-1, dist.z), B1-Vector(dist.x-1, dist.y-1, dist.z), width, 0, 1, Col )

  --render.DrawSphere(B2, -100, 50, 50, Color( Col.r, Col.g, Col.b, 255 ))
		render.DrawBeam( B1-Vector(dist.x-1, dist.y-1, dist.z), T1-Vector(dist.x-1, dist.y-1, -dist.z), width, 0, 1, Col )
		render.DrawBeam( B2-Vector(dist.x-1, -dist.y+1, dist.z), T2-Vector(dist.x-1, -dist.y+1, -dist.z), width, 0, 1, Col )
		render.DrawBeam( B3-Vector(-dist.x+1, -dist.y+1, dist.z), T3-Vector(-dist.x+1, -dist.y+1, -dist.z), width, 0, 1, Col )
		render.DrawBeam( B4-Vector(-dist.x+1, dist.y-1, dist.z), T4-Vector(-dist.x+1, dist.y-1, -dist.z), width, 0, 1, Col )

end

function RenderZones()
  local ply = LocalPlayer()
  /*
  if lobby:getPlayerLobby(ply) == "None" then
    return
  end
  local plyr = lobby:getPlayerLobby(ply).minigamePlayers[ply]
  local team = lobby:getPlayerLobby(ply).minigameTeams[plyr]
  if team != "Zoner" then return end*/
  local tracedata = {}
  tracedata.start = ply:EyePos()
  tracedata.endpos = ply:EyePos()+(ply:GetAimVector())*250
  tracedata.filter = player.GetAll()
  local trace = util.TraceLine(tracedata)
  local hit = trace.HitPos
  local snap = Race.Grid
  local hitMod = hit
  if hitMod.x % snap < snap/2 then
    hitMod.x = hitMod.x - hitMod.x % snap
  else
    hitMod.x = hitMod.x + (snap - hitMod.x % snap)
  end
  if hitMod.y % snap < snap/2 then
    hitMod.y = hitMod.y - hitMod.y % snap
  else
    hitMod.y = hitMod.y + (snap - hitMod.y % snap)
  end
  if hitMod.z % snap < snap/2 then
    hitMod.z = hitMod.z - hitMod.z % snap
  else
    hitMod.z = hitMod.z + (snap - hitMod.z % snap)
  end

  hitMod = hitMod+(trace.HitNormal*1)
  if snap >= 8 then
    local w = snap*15
    local h = snap*15
    ---drawGrid(hitMod, snap,  w, h, 0.2, Color(155,255,255,255), trace.HitNormal)
  end

	--local Min = hitMod
  --local Max = hitMod

  if (Race.Open || Race.Locked) && !Race.Selected then
    Race.HitPos = hitMod
    Race.CursorPos = (Race.CursorPos && Lerp(0.05, Race.CursorPos, hitMod)) || hitMod
    Race.CursorAlpha = (Race.CursorAlpha && Lerp(0.05, Race.CursorAlpha, 255)) || 0
  else
    if !Race.Selected then
      Race.CursorAlpha = (Race.CursorAlpha && Lerp(0.1, Race.CursorAlpha, 0)) || 0
    end
  end

  if Race.Selected then
  	local Min = Race.Selected[1]
    local Max = hitMod

    if Race.ZoneMode == 1 then
      Max.z = Min.z
    elseif Race.ZoneMode == 2 then
      Max.x = Race.Selected[2].x
      Max.y = Race.Selected[2].y
    elseif #Race.Selected >= 2 && (Race.ZoneMode < 1 || Race.ZoneMode > 2) then
       Max = Race.Selected[2]
    end

    Race.HitPos = Max
    Race.CursorPos = (Race.CursorPos && Lerp(0.1, Race.CursorPos, Max)) || Max
    DrawZone(Min, Max, Color(255,255,255,200))
  end
  if Race.StartFinished then
    DrawZone(Race.StartZone[1], Race.StartZone[2], Color(25, 241, 13))
  end

  if Race.EndFinished then
    DrawZone(Race.EndZone[1], Race.EndZone[2], Color(241, 13, 13))
  end

  for key, cps in pairs(Race.Checkpoints) do
    if #cps == 2 then
      DrawZone(cps[1], cps[2], Color(241, 239, 13))
    end
  end
  if Race.CursorPos && Race.CursorAlpha then
    render.SetColorMaterial()
    render.StartBeam( 2 )
      render.AddBeam( Race.CursorPos+Vector(0,0,0), 1, 1, Color(255,255,255, Race.CursorAlpha))
      render.AddBeam( Race.CursorPos+Vector(10,0,0), 1, 1, Color(255,255,255, 0))
    render.EndBeam()
    render.StartBeam( 2 )
      render.AddBeam( Race.CursorPos-Vector(0,0,0), 1, 1, Color(255,255,255, Race.CursorAlpha))
      render.AddBeam( Race.CursorPos-Vector(10,0,0), 1, 1, Color(255,255,255, 0))
    render.EndBeam()

    render.StartBeam( 2 )
      render.AddBeam( Race.CursorPos+Vector(0,0,0), 1, 1, Color(255,255,255, Race.CursorAlpha))
      render.AddBeam( Race.CursorPos+Vector(0,10,0), 1, 1, Color(255,255,255, 0))
    render.EndBeam()
    render.StartBeam( 2 )
      render.AddBeam( Race.CursorPos-Vector(0,0,0), 1, 1, Color(255,255,255, Race.CursorAlpha))
      render.AddBeam( Race.CursorPos-Vector(0,10,0), 1, 1, Color(255,255,255, 0))
    render.EndBeam()

    render.StartBeam( 2 )
      render.AddBeam( Race.CursorPos+Vector(0,0,0), 1, 1, Color(255,255,255, Race.CursorAlpha))
      render.AddBeam( Race.CursorPos+Vector(0,0,10), 1, 1, Color(255,255,255, 0))
    render.EndBeam()
    render.StartBeam( 2 )
      render.AddBeam( Race.CursorPos-Vector(0,0,0), 1, 1, Color(255,255,255, Race.CursorAlpha))
      render.AddBeam( Race.CursorPos-Vector(0,0,10), 1, 1, Color(255,255,255, 0))
    render.EndBeam()
  end
end
hook.Add("PreDrawOpaqueRenderables", "Zones", RenderZones)

local function CreateFrame(parent, x, y, w, h)
  local frame = parent:Add( "DFrame" )
  frame:SetPos( x, y )
  frame:SetTitle("")
  frame:SetSize( w, h  )
  frame:SetDraggable(false)
  frame:ShowCloseButton( false )
  frame.Paint = function(self, w, h )
  end

  return frame
end

local function CreateLabel(parent, txt, x, y, font, col, mode)
  local label = parent:Add( "DLabel" )
  surface.SetFont(GetFont(font))
  local txt_w, txt_h = surface.GetTextSize(txt)
  if mode == "center" then
    label:SetPos( x-(txt_w/2), y )
  elseif mode == "right" then
    label:SetPos( x-(txt_w), y )
  elseif !mode then
    label:SetPos( x, y )
  end
  label.original = {["x"] = x, ["y"] = y}
  label:SetSize( txt_w, txt_h  )
	label:SetFont( GetFont(font))
  label:SetText(txt)
	label:SetTextColor(col)

  label.UpdatePos = function()
    surface.SetFont(GetFont(font))
    local txt_w, txt_h = surface.GetTextSize(label:GetText())
    if mode == "center" then
      label:SetPos( label.original["x"]-(txt_w/2), label.original["y"] )
    elseif mode == "right" then
      label:SetPos( label.original["x"]-(txt_w), label.original["y"] )
    elseif !mode then
      label:SetPos( label.original["x"], label.original["y"] )
    end
  end
  return label
end

local function CreateBox(parent, x, y, w, h, col, mode)
  local box = parent:Add( "DLabel" )
  surface.SetFont(GetFont(font))

  if mode == "center" then
    box:SetPos( x-(w/2), y )
  elseif mode == "right" then
    box:SetPos( x-(w), y )
  elseif !mode then
    box:SetPos( x, y )
  end
  box.mode = mode
  box.original = {["x"] = x, ["y"] = y, ["w"] = w, ["h"] = h}
  box:SetSize( w, h  )
	box:SetTextColor(col)
  box.Paint = function(self, w, h )
    surface.SetDrawColor( col )
    surface.DrawRect(0, 0, w, h)
  end

  box.UpdatePos = function()
    if mode == "center" then
      box:SetPos( box.original["x"]-(box:GetWide()  /2), box.original["y"] )
    elseif mode == "right" then
      box:SetPos( box.original["x"]-(box:GetWide()), box.original["y"] )
    elseif !mode then
      box:SetPos( box.original["x"], box.original["y"] )
    end
  end
  return box
end

local function CreateButton(parent, txt, x, y, w, h, font, col, txtcol, mode)
  local button = parent:Add( "DButton" )

  if mode == "center" then
    button:SetPos( x-(w/2), y )
  elseif mode == "right" then
    button:SetPos( x-(w), y )
  elseif !mode then
    button:SetPos( x, y )
  end
  button:SetSize( w, h )
  button:SetText("")
  button.Text = txt
  button.isHovered = function() end
  button.notHovered = function() end
  button.SecondThink = function()
  end
  button.Think = function()
    button.SecondThink()
    if button:IsHovered() then
      button.isHovered()
    else
      button.notHovered()
    end
  end
  button.Paint = function(self, w, h )
    surface.SetDrawColor( col )
    surface.DrawRect(0, 0, w, h)
    surface.SetFont(GetFont(font))
    surface.SetTextColor( txtcol )
    local txt_w, txt_h = surface.GetTextSize(self.Text)
    surface.SetTextPos( (w/2) - (txt_w/2), (h/2) - (txt_h/2) )
    surface.DrawText( self.Text )
  end
  return button
end

local function CreateInfoButton(par, infopar, t, infot, x, y, w, h, font, col, txtcol, mode)
  local button = CreateButton(par, t, x, y, w, h, font, col, txtcol, mode)
  local buttondiv = CreateBox(button, x, h-3, 0, 2, Color(255,255,255), "center")
  local info = CreateFrame(infopar, 0, y, w, 300)
  local wrap = wrapText( "TE_Text_Small", infot, w )
  if 1 == #wrap then
    info[1] = CreateLabel(info, infot, x, (y+h), "TE_Text_Small", Color(255,255,255), "center")
  else
    for _, txts in pairs(wrap) do
      local str = table.concat( txts, " " )
      SetFont("TE_Text_Small")
      local txt_w, txt_h = surface.GetTextSize(str)

      info[_] = CreateLabel(info, str, x, (txt_h*_), "TE_Text_Small", Color(255,255,255), "center")

    end
  end
  info:SetAlpha(0)
  info:Hide()
  SetFont("TE_Text_Small")
  local txt_w, txt_h = surface.GetTextSize(infot)
  local infodiv = CreateBox(infopar, 0, (y+h)-3, 0, 2, Color(255,255,255))

  button.Think = function()
    if button:IsHovered() then
      local div = buttondiv
      div:SetWide(UT:Hermite(0.35, div:GetWide(), par:GetWide()+10))

      if math.Round(div:GetWide()) >= par:GetWide() then
        infodiv:SetWide(UT:Hermite(0.25, infodiv:GetWide(), infopar:GetWide()))
        infodiv.UpdatePos()
        if info:GetAlpha() < 5 || !info:IsVisible() then
          info:AlphaTo(255, 0.15)
          info:Show()
        end
      end
      div.UpdatePos()
    else
      if info:GetAlpha() > 254 then
        info:AlphaTo(0, 0.15)
      end
      timer.Simple(0.16, function() info:Hide() end)

      infodiv:SetWide(UT:Hermite(0.35, infodiv:GetWide(), 0))

      if math.Round(infodiv:GetWide()) <= 0 then
        local div = buttondiv
        div:SetWide(UT:Hermite(0.35, div:GetWide(), 0))
        div.UpdatePos()
      end
    end
  end
  return button
end

//CreateFrame(parent, x, y, w, h)
//CreateLabel(parent, txt, x, y, font, col, mode)
//CreateBox(parent, x, y, w, h, col, mode)
//CreateButton(parent, txt, x, y, w, h, font, col, txtcol, mode)
//CreateInfoButton(par, infopar, t, infot, x, y, w, h, font, col, txtcol, mode)

local MENU =
{
	Init = function( self )
    self.ply = LocalPlayer()

    self.Main = CreateFrame(self, ScrW()-270, 100, 250, 550)
    self.Main.Paint = function(self, w, h)
      surface.SetDrawColor(Color(0,0,0, 175))
      surface.DrawRect(0,0, w, h)
    end

    self.MainMenu = CreateFrame(self.Main, 0, 0, 250, 550)
    self.MainMenu.Paint = function(self, w, h)
    end

    self.SettingsPanel = CreateFrame(self.Main, 0, 0, 250, 550)
    self.SettingsPanel.Paint = function(self, w, h)
    end
    self.SettingsPanel:Hide()

    self.ZonePanel = CreateFrame(self.Main, 0, 0, 250, 550)
    self.ZonePanel.Paint = function(self, w, h)
    end
    self.ZonePanel:Hide()
    local bWide = self.Main:GetWide()
    SetFont("Zoner_Text", {
      font = "SIMPLIFICA",
      weight = 700,
      size = 50,
      antialias = true
    })
    SetFont("Zoner_TextSmall", {
      font = "SIMPLIFICA",
      weight = 700,
      size = 20,
      antialias = true
    })
    local function newBut(parent, txt, x, y, w ,h, col)
      if !col then
        col = Color(255,255,255)
      end
      local Button = CreateButton(parent, txt, x, y, w ,h, "Zoner_Text", Color(col.r,col.g,col.b, 0), col, "center")
      Button.Divide = CreateBox(Button, x, h-2, 0, 2, col, "center")
      Button.isHovered = function()
        local div = Button.Divide
        div:SetWide(UT:Hermite(0.25, div:GetWide(), Button:GetWide()+10))
        div.UpdatePos()
      end
      Button.notHovered = function()
        local div = Button.Divide
        div:SetWide(UT:Hermite(0.25, div:GetWide(), 0))
        div.UpdatePos()
      end
      return Button
    end

    //Main Menu
    self.Zoning = newBut(self.MainMenu, "Zoning", bWide/2, 50+10, bWide, 50)
    self.Zoning.DoClick = function()
      self.ZonePanel:AlphaTo(255, 0.15)
      self.ZonePanel:Show()
      if self.MainMenu:IsVisible() then
        self.MainMenu:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.MainMenu:Hide() end)
      end
    end

    self.Settings = newBut(self.MainMenu, "Settings", bWide/2, (50*2)+(10*2), bWide, 50)
    self.Settings.DoClick = function()
      self.SettingsPanel:AlphaTo(255, 0.15)
      self.SettingsPanel:Show()
      if self.MainMenu:IsVisible() then
        self.MainMenu:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.MainMenu:Hide() end)
      end
    end

    self.ClearT = newBut(self.MainMenu, "Clear Times", bWide/2, (50*3)+(10*3), bWide, 50)
    self.ClearT.DoClick = function()
      print("Clear")
    end

    self.Lock = newBut(self.MainMenu, "Lock UI", bWide/2, (50*4)+(10*4), bWide, 50)
    self.Lock.DoClick = function()
      if Race.Locked then
        Race.Locked = false
      else
        Race.Locked = true
      end
    end

    self.ZonePanel.Grid = newBut(self.ZonePanel, "Grid: " .. Race.Grid, bWide/2, 0, bWide, 50, Color(200, 204, 4))
    self.ZonePanel.Grid.DoClick = function(self)
      local max = 128
      local min = 1
      Race.Grid = Race.Grid*2
      if Race.Grid > max then
        Race.Grid = min
      end
      self.Text = "Grid: " .. Race.Grid
    end

    self.ZonePanel.Grid.DoRightClick = function(self)
      local max = 128
      local min = 1
      Race.Grid = Race.Grid/2
      if Race.Grid < min then
        Race.Grid = max
      end
      self.Text = "Grid: " .. Race.Grid
    end

    //CheckPoint
    self.ZonePanel.CP = newBut(self.ZonePanel, "Set Checkpoint", bWide/2, 50+10, bWide, 50, Color(200, 204, 4))
    self.ZonePanel.CP.DoClick = function()
      if Race.CurCheckpoint then
        if Race.Selected && Race.Selected != Race.Checkpoints[Race.CurCheckpoint] then
          chat.AddText("[", Color(41, 228, 25), "Race", Color(255,255,255), "]", Color(203, 33, 16), " Please finish the current zone.")
          return
        end
      else
        Race.CurCheckpoint = 1
      end
      if Race.ZoneMode <= 1 then // Do Horizontal movement only
        if !Race.Checkpoints[Race.CurCheckpoint] then
          Race.Checkpoints[Race.CurCheckpoint] = {}
        end
        Race.SetZone(Race.Checkpoints[Race.CurCheckpoint], Race.HitPos)
        Race.ZoneMode = Race.ZoneMode+1
        Race.Selected = Race.Checkpoints[Race.CurCheckpoint]
      elseif Race.ZoneMode == 2 then // Then do vertical movement only
        Race.Checkpoints[Race.CurCheckpoint][2] = Race.HitPos
        Race.ZoneMode = 0
        Race.Selected = false
        Race.CurCheckpoint = Race.CurCheckpoint+1
      end
    end

    //Zoning
    //Clear start zone
    self.ZonePanel.CSZ = newBut(self.ZonePanel, "Clear Start Zone", bWide/2, (50*3)+(10*3), bWide, 50, Color(4, 204, 6))
    self.ZonePanel.CSZ.DoClick = function()
      Race.StartFinished = false
      Race.StartZone = {}
      self.ZonePanel.SZ:AlphaTo(255, 0.15)
      self.ZonePanel.SZ:Show()
      if self.ZonePanel.CSZ:IsVisible() then
        self.ZonePanel.CSZ:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.ZonePanel.CSZ:Hide() end)
      end
    end
    self.ZonePanel.CSZ:Hide()
    self.ZonePanel.CSZ:SetAlpha(0)
    // Start Zone
    self.ZonePanel.SZ = newBut(self.ZonePanel, "Set Start Zone", bWide/2, (50*3)+(10*3), bWide, 50, Color(4, 204, 6))
    self.ZonePanel.SZ.DoClick = function()
      if Race.Selected && Race.Selected != Race.StartZone then
        chat.AddText("[", Color(41, 228, 25), "Race", Color(255,255,255), "]", Color(203, 33, 16), " Please finish the current zone.")
        return
      end
      if Race.ZoneMode <= 1 then // Do Horizontal movement only
        Race.SetZone(Race.StartZone, Race.HitPos)
        Race.ZoneMode = Race.ZoneMode+1
        Race.Selected = Race.StartZone
      elseif Race.ZoneMode == 2 then // Then do vertical movement only
        Race.StartZone[2] = Race.HitPos
        Race.ZoneMode = 0
        Race.Selected = false
        Race.StartFinished = true
        self.ZonePanel.CSZ:AlphaTo(255, 0.15)
        self.ZonePanel.CSZ:Show()
        if self.ZonePanel.SZ:IsVisible() then
          self.ZonePanel.SZ:AlphaTo(0, 0.15)
          timer.Simple(0.16, function() self.ZonePanel.SZ:Hide() end)
        end
      end
    end

    self.ZonePanel.CEZ = newBut(self.ZonePanel, "Clear End Zone", bWide/2, (50*4)+(10*4), bWide, 50, Color(199, 119, 119))
    self.ZonePanel.CEZ:Hide()
    self.ZonePanel.CEZ.DoClick = function()
      Race.EndFinished = false
      Race.EndZone = {}
      self.ZonePanel.EZ:AlphaTo(255, 0.15)
      self.ZonePanel.EZ:Show()
      if self.ZonePanel.CEZ:IsVisible() then
        self.ZonePanel.CEZ:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.ZonePanel.CEZ:Hide() end)
      end
    end

    self.ZonePanel.EZ = newBut(self.ZonePanel, "Set End Zone", bWide/2, (50*4)+(10*4), bWide, 50, Color(204, 4, 4))
    self.ZonePanel.EZ.DoClick = function()
      if Race.Selected && Race.Selected != Race.EndZone then
        chat.AddText("[", Color(41, 228, 25), "Race", Color(255,255,255), "]", Color(203, 33, 16), " Please finish the current zone.")
        return
      end

      if Race.ZoneMode <= 1 then // Do Horizontal movement only
        Race.SetZone(Race.EndZone, Race.HitPos)
        Race.ZoneMode = Race.ZoneMode+1
        Race.Selected = Race.EndZone
      elseif Race.ZoneMode == 2 then // Then do vertical movement only
        Race.EndZone[2] = Race.HitPos
        Race.ZoneMode = 0
        Race.Selected = false
        Race.EndFinished = true
        self.ZonePanel.CEZ:AlphaTo(255, 0.15)
        self.ZonePanel.CEZ:Show()
        if self.ZonePanel.EZ:IsVisible() then
          self.ZonePanel.EZ:AlphaTo(0, 0.15)
          timer.Simple(0.16, function() self.ZonePanel.EZ:Hide() end)
        end
      end
    end

    self.ZonePanel.Help = newBut(self.ZonePanel, "Help", bWide/2, (50*5)+(10*5), bWide, 50, Color(0, 56, 255))
    self.ZonePanel.Help.DoClick = function()
      local teamCol = {
        [1] = Color(41, 228, 25),
        [2] = Color(228, 177, 25)
      }
      if lobby:getPlayerLobby(LocalPlayer()) != "None" then
        teamCol = lobby:getPlayerLobby(LocalPlayer()).minigameColors[LocalPlayer()]
      end
      chat.AddText("[", teamCol[1], "Race", Color(255,255,255), "] Help: ")
      chat.AddText("[", teamCol[1], "Creating a Zone", Color(255,255,255), "]", " Press one of the Set ___ Zone buttons.")
      chat.AddText(Color(255,255,255), "Once you know how wide and tall the zone is going to be press the same button.")
      chat.AddText("[", teamCol[1], "Removing a Zone", Color(255,255,255), "]", " If you want to remove a zone just press the Clear ___ Zone button.")
      chat.AddText("[", teamCol[1], "Grid", Color(255,255,255), "]", " Your cursor locks onto a grid, you can choose the grid size by left clicking or right clicking the Grid button.")

    end

    self.ZonePanel.Back = newBut(self.ZonePanel, "Back", bWide/2, (50*6)+(10*6), bWide, 50, Color(255, 255, 255))
    self.ZonePanel.Back.DoClick = function()
      self.MainMenu:AlphaTo(255, 0.15)
      self.MainMenu:Show()
      if self.ZonePanel:IsVisible() then
        self.ZonePanel:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.ZonePanel:Hide() end)
      end
    end

  end,

	PerformLayout = function( self )

		self:SetSize( ScrW(), ScrH() )
		self:SetPos( 0, 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )

	end
}

MENU = vgui.RegisterTable( MENU, "EditablePanel" )

if ( IsValid( ZoneMenu ) ) then
	ZoneMenu:Remove()
end

function Race.ZonerOpen()
  Race.Open = true
  if ( !IsValid( ZoneMenu ) ) then
		ZoneMenu = vgui.CreateFromTable( MENU )
    ZoneMenu:SetAlpha(0)
	end

	if ( IsValid( ZoneMenu ) ) then
    ZoneMenu:AlphaTo(255, 0.15)
		ZoneMenu:Show()
		--ZoneMenu:MakePopup()
		ZoneMenu:SetKeyboardInputEnabled( false )
	end

end
hookAdd("MinigameOpen", "RaceZoner", Race.ZonerOpen)

function Race.ZonerClose()
	Race.Open = false

	if ( IsValid( ZoneMenu ) ) && !Race.Locked then
		timer.Simple(0.2, function()
			if not Race.Open then
				ZoneMenu:Hide()
			end
		end)

		ZoneMenu:AlphaTo(0, 0.15)
	end


end
hookAdd("MinigameClose", "RaceZoner", Race.ZonerClose)
