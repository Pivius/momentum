if SERVER then
  util.AddNetworkString( "PBSync" )
end
Playback = Playback or {}
Playback.Players = Playback.Players or {}
Playback.MaxPlayers = 16
Playback.MaxLength = 60*3 //3 min
Playback.MaxBots = 1
Playback.Bots = Playback.Bots or {}



function Playback:CreateBot(type)
  if type == nil then return end
  local spawn = false
  // Quick look to see if we should spawn a bot
  if #player.GetBots() == 0 then
    spawn = true
  end
  for _,bot in pairs( player.GetBots() ) do
    if bot.Type == type then
      return
    elseif _ == #player.GetBots() and bot.Type != type then
      spawn = true
    end
  end
  timer.Create("MakeBot".. type, 0.5, 1, function()
    if spawn then
      RunConsoleCommand( "bot" )
    end
  end)
  timer.Create("CreateBot".. type, 1.5, 1, function()
    if spawn then
      for _,bot in pairs( player.GetBots() ) do
        if !bot.Exist then
    			bot:SetMoveType( MOVETYPE_NONE )
    			bot:SetCollisionGroup( COLLISION_GROUP_DEBRIS )
    			bot.Player = "None"
          bot.Type = type
          bot:SetNWString( "Nick", bot.Player )
          bot:SetNWString( "Type", bot.Type )
    			bot:SetFOV( 100, 0 )
    			bot:SetGravity( 0 )
          bot.Exist = true
          local t = {
            [bot] = {
              --["Replaying"] = false,
              ["PlayFrame"] = 0,
              ["Occupied"] = false,
              ["Data"] = {
                [1] = {},
                [2] = {},
                [3] = {},
                [4] = {},
                [5] = {},
                [6] = {}
              },
              ["Frames"] = 0,
              ["Player"] = bot.Player
            }
          }
          table.Merge(Playback.Bots, t)
        end
      end
    end
  end)
end

/*
function SpawnBots( )

	for _,bot in pairs( player.GetBots() ) do
    if !bot.Exist then
			bot:SetMoveType( MOVETYPE_NONE )
			bot:SetCollisionGroup( COLLISION_GROUP_DEBRIS )
			bot.Player = "None"

			bot:SetFOV( 100, 0 )
			bot:SetGravity( 0 )
      bot.Exist = true
      local t = {
        [bot] = {
          --["Replaying"] = false,
          ["PlayFrame"] = 0,
          ["Occupied"] = false,
          ["Data"] = {
            [1] = {},
            [2] = {},
            [3] = {},
            [4] = {},
            [5] = {},
            [6] = {}
          },
          ["Frames"] = 0
        }
      }
      table.Merge(Playback.Bots, t)
    end
	end

	if #player.GetBots() < Playback.MaxBots then

		RunConsoleCommand( "bot" )
		timer.Simple( 1, function()
			SpawnBots( )
		end )
	end
end

SpawnBots( )*/

function Playback:AddPlayer(ply, type)
  local startTime = CurTime()
  if !Playback.Players[ply] then
    Playback.Players[ply] = {}
  end
  Playback.Players[ply][type] = {
      ["Frames"] = 0,
      ["Data"] = {
        [1] = {},
        [2] = {},
        [3] = {},
        [4] = {},
        [5] = {},
        [6] = {}
      },
      ["StartTime"] = startTime,
      ["Recording"] = false,
      ["Replaying"] = false,
      ["PlayFrame"] = 0
    }

  --table.Merge(Playback.Players, t)

end

function Playback:Record(ply, type)
  if !Playback.Players[ply] or !Playback.Players[ply][type] then
    Playback:AddPlayer(ply, type)
  end
  Playback.Players[ply][type]["StartTime"] = CurTime()
  Playback.Players[ply][type]["Recording"] = true
  print( "Recording" )
end

function Playback:RemovePlayer(ply, type)
  if Playback.Players[ply] and Playback.Players[ply][type] then
    if UT:HasKey( Playback.Players[ply], type ) then
      if Playback.Players[ply][type]["Recording"] then
          Playback.Players[ply][type] = {
            ["Frames"] = 0,
            ["Data"] = {
              [1] = {},
              [2] = {},
              [3] = {},
              [4] = {},
              [5] = {},
              [6] = {}
            },
            ["StartTime"] = 0,
            ["Recording"] = false,
            ["Replaying"] = false,
            ["PlayFrame"] = 0
          }
        print( "Recording ended" )
      end
    end
  end
end

function Playback:Play(ply, type, pre)
  --if Playback.Players[ply][type]["Recording"] then return end

  local ReplayBot = nil
  for bot, val in pairs(Playback.Bots) do
    if bot["Type"] == type then
      if !pre then
        Playback.Bots[bot]["Occupied"] = true
        Playback.Bots[bot]["Data"] = Playback.Players[ply][type]["Data"]
        Playback.Bots[bot]["Frames"] = Playback.Players[ply][type]["Frames"]
        Playback.Bots[bot]["PlayFrame"] = 0
        Playback.Bots[bot]["Player"] = ply:Nick()
        bot.Player = ply:Nick()
        bot:SetNWString( "Nick", bot.Player )
        ReplayBot = bot
        break
      elseif pre then
        Playback.Bots[bot]["Occupied"] = true
        Playback.Bots[bot]["Data"] = pre["Data"]
        Playback.Bots[bot]["Frames"] = pre["Frames"]
        Playback.Bots[bot]["PlayFrame"] = 0
        Playback.Bots[bot]["Player"] = ply:Nick()
        bot.Player = ply:Nick()
        bot:SetNWString( "Nick", bot.Player )
        ReplayBot = bot
      end
    end
  end
  print( "Playback" )
  return ReplayBot

end

function Playback:Stop(ply, type)
  for bot, val in pairs(Playback.Bots) do
    if !bot["Occupied"] and bot["Type"] == type then
      Playback.Bots[bot] = {
          ["PlayFrame"] = 0,
          ["Occupied"] = false,
          ["Data"] = {
            [1] = {},
            [2] = {},
            [3] = {},
            [4] = {},
            [5] = {},
            [6] = {}
          },
          ["Frames"] = 0,
          ["Player"] = "None"
        }
      break
    end
  end
  print( "Playback ended" )
end

local function Record(ply, mv)
  if ply:IsValid() and UT:HasKey( Playback.Players, ply ) then
    for t, v in pairs(Playback.Players[ply]) do

      if v["Recording"] then
        if v["StartTime"]+Playback.MaxLength < CurTime() then

           Playback:RemovePlayer(ply)
          return
        end
        local origin = mv:GetOrigin()
    		local angles = mv:GetAngles()
    		local frame = v["Frames"]

        v["Data"][1][frame] = origin.x
        v["Data"][2][frame] = origin.y
        v["Data"][3][frame] = origin.z
        v["Data"][4][frame] = angles.p
        v["Data"][5][frame] = angles.y


        v["Frames"] = frame+1
      end
    end
  end

end
hook.Add( "SetupMove", "PlaybackRecord", Record )

local function ButtonRecord( ply, cmd )
	 if ply:IsValid() and UT:HasKey( Playback.Players, ply ) then
     for t, v in pairs(Playback.Players[ply]) do
       if v["Recording"] then
         local frame = v["Frames"]
         v["Data"][6][frame] = cmd:GetButtons()
      end
    end
  end
end
hook.Add( "StartCommand", "ButtonRecord", ButtonRecord )

local function Replay(ply, mv)

  if ply:IsBot() and UT:HasKey( Playback.Bots, ply ) and Playback.Bots[ply]["Frames"] > 0 then

    local frame = Playback.Bots[ply]["PlayFrame"]
    if frame >= Playback.Bots[ply]["Frames"] then

      local data = Playback.Bots[ply]["Data"]
      Playback.Bots[ply]["Occupied"] = false
      Playback.Bots[ply]["PlayFrame"] = 0
      mv:SetOrigin( Vector( data[ 1 ][ 0 ], data[ 2 ][ 0 ], data[ 3 ][ 0 ] ) )
      ply:SetEyeAngles( Angle( data[ 4 ][ 0 ], data[ 5 ][ 0 ], 0 ) )
      return
    else

      local data = Playback.Bots[ply]["Data"]
      mv:SetOrigin( Vector( data[ 1 ][ frame ], data[ 2 ][ frame ], data[ 3 ][ frame ] ) )
      ply:SetEyeAngles( Angle( data[ 4 ][ frame ], data[ 5 ][ frame ], 0 ) )
      Playback.Bots[ply]["PlayFrame"] = frame + 1
    end
  end
end
hook.Add( "SetupMove", "Replay", Replay )

local function ButtonReplay( ply, cmd )

  if ply:IsBot() and UT:HasKey( Playback.Bots, ply ) and Playback.Bots[ply]["Frames"] > 0 then
		cmd:ClearButtons()
		cmd:ClearMovement()
    local data = Playback.Bots[ply]["Data"]
    local frame = Playback.Bots[ply]["PlayFrame"]
		if data[6][ frame ] and ply:GetMoveType() == 0 then
      ply:SetEyeAngles( Angle( data[ 4 ][ frame ], data[ 5 ][ frame ], 0 ) )
			cmd:SetButtons( tonumber( data[ 6 ][ frame ] ) )
		end
	end
end
hook.Add( "StartCommand", "ButtonReplay", ButtonReplay )

concommand.Add( "SR", function( ply, cmd, args )
	--Playback:AddPlayer(ply)
  net.Start( "PBSync" )
    net.WriteString("AddPlayer")
    net.WriteEntity(ply)
    net.WriteString("FreeRoam")
  net.SendToServer()

end )

concommand.Add( "ER", function( ply, cmd, args )
	--Playback:RemovePlayer(ply)
  net.Start( "PBSync" )
    net.WriteString("RemovePlayer")
    net.WriteEntity(ply)
    net.WriteString("FreeRoam")
  net.SendToServer()

end )

concommand.Add( "Playback", function( ply, cmd, args )
	--Playback:Play(ply)
  net.Start( "PBSync" )
    net.WriteString("Play")
    net.WriteEntity(ply)
    net.WriteString("FreeRoam")
  net.SendToServer()

end )

concommand.Add( "EndPlayback", function( ply, cmd, args )
	--Playback:Stop(ply)
  net.Start( "PBSync" )
    net.WriteString("End")
    net.WriteEntity(ply)
    net.WriteString("FreeRoam")
  net.SendToServer()

end )

net.Receive( "PBSync", function( len, ply )
	 local str = net.ReadString()
   if str == "AddPlayer" then
     Playback:AddPlayer(net.ReadEntity(), net.ReadString())
   elseif str == "RemovePlayer" then
     Playback:RemovePlayer(net.ReadEntity(), net.ReadString())
   elseif str == "Play" then
     Playback:Play(net.ReadEntity(), net.ReadString())
   elseif str == "End" then
     Playback:Stop(net.ReadEntity(), net.ReadString())
   end
end )
--Playback:CreateBot("FreeRoam")
