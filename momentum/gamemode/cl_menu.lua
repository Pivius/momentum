lobby = lobby or {}
lobby.lobbies = {}
lobby.keepopen = false

--include("modules/cl_hudedit.lua")
include("vgui/topics.lua")
function syncLobbies()
  net.Start("lobbyNetworking")
    net.WriteString("sync")
    net.WriteTable({LocalPlayer()})
  net.SendToServer()
end

local BUTTON =
{

  Init = function( self )
    self.Font = "HUD Menu"
    self.Text = ""

    self.Color = Color(255,255,255, 50)
    self.Highlight = Color(0,0,0)
    local alpha = 0

    self.ButtonColor = self:Add( "DModelPanel" )

    self.ButtonColor:SetAlpha(255)
    self.ButtonColor:SetColor(self.Color)
    self.ButtonColor:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Button.Font = self.Font
    self.Alpha = vgui.Create("DButton")
    self.Alpha:SetSize(0,0)
    self.Alpha:SetAlpha(self.Color.a)

    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, 255) )
      surface.DrawRect(0, 0, w, h)
    end

    self.Button:SetText("")
    local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button:SetSize(self:GetWide(),self:GetTall())
    --self.Button:Dock(TOP)
    self.Button.OnClick = function()
    end
    self.Button.DoClick = function()
      self.Button.OnClick()
      alphabut:SetAlpha(250)
      alphabut:AlphaTo( alpha, 0.2, 0)
      colorbut:SetColor(self.Highlight)
      colorbut:ColorTo(self.Color, 0.2)
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      alpha = self:GetParent().Color.a+50
      alphabut:AlphaTo( alpha, 0.3 , 0)
    end

    self.Button.OnCursorExited = function(self)
      alpha = self:GetParent().Color.a
      alphabut:AlphaTo( alpha, 0.3 , 0)
    end

    self.Button.Paint = function(self, w, h)

      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( self:GetParent().Font )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Text)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Text )
    end

	end,
  Update = function(self)
    if !self.Button then return end
    self.ButtonColor:SetColor(self.Color)

    self.Button.Font = self.Font
    self.Alpha:SetAlpha(self.Color.a)
    self.Button:SetText("")
    self.Button:SetSize(self:GetWide(),self:GetTall())
  end,
  SetFont = function(self, font)
    self.Font = font
    self:Update()
  end,

  SetText = function(self, txt)
    self.Text = txt
    self:Update()
  end,

  SetColor = function(self, col)
    self.Color = col
    self:Update()
  end,

  SetHighlight = function(self, h)
    self.Highlight = h
    self:Update()
  end,
	Paint = function( self, w, h )
	end,

  Think = function( self, w, h )
  end
}

BUTTON = vgui.RegisterTable( BUTTON, "EditablePanel" )

local TEXTEDIT =
{

  Init = function( self )
    self.Font = "HUD Menu"
    self.Text = ""
    self.DefText = ""
    self.Numeric = false
    self.ClickText = ""
    self.OnType = function() end
    self.OnEnter = function() end
    self.Color = Color(255,255,255, 50)
    self.Highlight = Color(0,0,0)
    local alpha = 0

    self.ButtonColor = self:Add( "DModelPanel" )

    self.ButtonColor:SetAlpha(255)
    self.ButtonColor:SetColor(self.Color)
    self.ButtonColor:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Button.Font = self.Font
    self.Alpha = vgui.Create("DButton")
    self.Alpha:SetSize(0,0)
    self.Alpha:SetAlpha(self.Color.a)

    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, 255) )
      surface.DrawRect(0, 0, w, h)
    end

    self.Button:SetText("")
    local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button:SetSize(self:GetWide(),self:GetTall())
    --self.Button:Dock(TOP)
    self.Button.OnClick = function()
    end
    self.Button.DoClick = function()
      self.Button.OnClick()
      alphabut:SetAlpha(250)
      alphabut:AlphaTo( alpha, 0.2, 0)
      colorbut:SetColor(self.Highlight)
      colorbut:ColorTo(self.Color, 0.2)

      if not self.editing then
        self.editing = true
        self.txt = self.txt or {}
        self.txt = vgui.Create( "DTextEntry" )

        self.txt:SetSize( 0, 0 )
        self.txt:SetPos(0, 0)
        self.txt:SetFont(self.Font)
        self.txt:SetText("")
        self.txt:SetTextColor(Color(255,255,255))
        self.txt:SetAlpha(0)
        self.txt:SetEditable(true)
        self.txt:SetNumeric( self.Numeric )
        self.txt:SetDrawBackground(false)
        self.txt:SetEnterAllowed( true )
        self.txt:SetUpdateOnType( true )
        self.Text = self.ClickText
        self.txt:MakePopup()
        lobby.keepopen = true
      end

      self.txt.OnValueChange = function(s, string)
        self.Text = self.txt:GetValue()
        self.OnType(self, string)
        self.txt:SetCaretPos( string:len() )
      end

      self.txt.OnEnter = function()
        self.editing = false
        self.OnEnter(self)
        self.txt:Remove()
        lobby.keepopen = false
      end
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      alpha = self:GetParent().Color.a+50
      alphabut:AlphaTo( alpha, 0.3 , 0)
    end

    self.Button.OnCursorExited = function(self)
      alpha = self:GetParent().Color.a
      alphabut:AlphaTo( alpha, 0.3 , 0)
    end

    self.Button.Paint = function(self, w, h)

      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( self:GetParent().Font )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Text)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Text )
    end

	end,
  Update = function(self)
    if !self.Button then return end
    self.ButtonColor:SetColor(self.Color)

    self.Button.Font = self.Font
    self.Alpha:SetAlpha(self.Color.a)
    self.Button:SetText("")
    self.Button:SetSize(self:GetWide(),self:GetTall())
    /*
      local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button.Paint = function(self, w, h)

      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( self:GetParent().Font )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Text)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Text )
    end
    */
  end,
  SetFont = function(self, font)
    self.Font = font
    self:Update()
  end,

  SetText = function(self, txt)
    self.Text = txt
    self.DefText = txt
    self:Update()
  end,

  SetColor = function(self, col)
    self.Color = col
    self:Update()
  end,

  SetHighlight = function(self, h)
    self.Highlight = h
    self:Update()
  end,



	Paint = function( self, w, h )
	end,

  Think = function( self, w, h )
  end
}

TEXTEDIT = vgui.RegisterTable( TEXTEDIT, "EditablePanel" )

local LOBBY_BUTTON =
{
	Init = function( self )
    self.Host = ""
    self.Mode = ""
    self.Name = ""
    self.Players = ""
    self.Settings = {}

  end,

  Setup = function( self, lb, lst, id )

		self.Host = lb["lobbyHost"]
    self.Mode = lb["lobbyMode"]
    self.Name = lb["lobbyName"]
    self.Players = lb["lobbyPlayers"]
    self.Settings = lb["lobbySettings"]
    self.List = lst
    self.LobbyID = id
		self:Think( self )
    local alpha = 0

    self.ButtonColor = self:Add( "DModelPanel" )

    self.ButtonColor:SetAlpha(255)
    self.ButtonColor:SetColor(lobby.minigames[self.Mode])
    self.ButtonColor:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Alpha = vgui.Create("DButton")
    self.Alpha:SetSize(0,0)
    self.Button.Name = self.Name
    self.Button.Host = self.Host
    self.Alpha:SetAlpha(10)

    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, 255) )
      surface.DrawRect(0, 0, w, h)
    end
    self.Button:SetText("")
    local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button:SetSize(self:GetWide(),self:GetTall())
    --self.Button:Dock(TOP)
    self.Button.DoClick = function()
      alphabut:SetAlpha(250)
      alphabut:AlphaTo( alpha, 0.2, 0)
      colorbut:SetColor(Color(255,255,255))

      colorbut:ColorTo(lobby.minigames[lb["lobbyMode"]], 0.2)
      if !table.HasValue(self.Players, LocalPlayer()) then
        net.Start("lobbyNetworking")
          net.WriteString("join")

          net.WriteTable( {[1] = LocalPlayer(), [2] = self.LobbyID, [3] = false} ) --replace host with lobbyid
        net.SendToServer()
      else
        net.Start("lobbyNetworking")
          net.WriteString("leave")
          net.WriteTable( {[1] = LocalPlayer()} )
        net.SendToServer()
      end
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      alphabut:AlphaTo( 120, 0.3 , 0)
      alpha = 120

    end

    self.Button.OnCursorExited = function(self)
      alphabut:AlphaTo( 10, 0.3 , 0)
      alpha = 10
    end

    self.Button.Paint = function(self, w, h)
      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "HUD Menu" )
      self.txtw, self.txth = surface.GetTextSize(self.Name)
      surface.SetTextPos( (w/2)- self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self.Name )

    end

	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    if !lobby.lobbies[self.LobbyID] then
      self.Button.Paint = function() end
      self.ButtonColor:Remove()
      self.Button:Remove()
      self:Remove()
    return end

    if lobby.lobbies[self.LobbyID]["lobbyHost"] != self.Host or lobby.lobbies[self.LobbyID]["lobbyMode"] != self.Mode or lobby.lobbies[self.LobbyID]["lobbySettings"] != self.Settings then
      self.Button.Paint = function() end
      self.ButtonColor:Remove()
      self.Button:Remove()
      self:Remove()
			return
		end
  end
}

LOBBY_BUTTON = vgui.RegisterTable( LOBBY_BUTTON, "EditablePanel" )

local MINIGAME_BUTTON =
{
	Init = function( self )
    self.Minigame = "None"
    self.func = function() end
    local alpha = 0
    self.Alpha = self:Add( "DButton" )
    self.Alpha:SetAlpha(0)
    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
    end

    self.TextAlpha = self:Add( "DModelPanel" )

    self.TextAlpha:SetAlpha(255)
    self.TextAlpha:SetColor(lobby.minigames[self.Minigame])
    self.TextAlpha:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Button:SetPos( 0, 0 )
    self.Button:SetSize( (850+200 - 210 - 30)/5, 100 )
    self.Button:SetText("")
    self.Button.DoClick = function()
      if self.Minigame == "None" then return end
      self.Alpha:SetAlpha(250)
      self.Alpha:AlphaTo( alpha, 0.2, 0)
      self.funct = self.func(self)
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      self:GetParent().Alpha:AlphaTo( 200, 0.3 , 0)
      self:GetParent().TextAlpha:ColorTo(Color(255,255,255), 0.3)
      alpha = 200
    end

    self.Button.OnCursorExited = function(self)
      self:GetParent().Alpha:AlphaTo( 0, 0.3 , 0)
      self:GetParent().TextAlpha:ColorTo(lobby.minigames[self:GetParent().Minigame], 0.3)
      alpha = 0
    end

    self.Button.Paint = function(self, w, h)
      surface.SetDrawColor( Color(25,25,25, self:GetParent().Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(self:GetParent().TextAlpha:GetColor())
      surface.SetFont( "HUD Menu2" )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Minigame)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Minigame )
    end
  end,
  ReColor = function(self)
    self.TextAlpha:SetColor(lobby.minigames[self.Minigame])
  end,
	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
  end
}

MINIGAME_BUTTON = vgui.RegisterTable( MINIGAME_BUTTON, "EditablePanel" )

/*
███    ███ ███████ ███    ██ ██    ██
████  ████ ██      ████   ██ ██    ██
██ ████ ██ █████   ██ ██  ██ ██    ██
██  ██  ██ ██      ██  ██ ██ ██    ██
██      ██ ███████ ██   ████  ██████
*/


local MENU =
{
	Init = function( self )
    local lP = LocalPlayer()
    // Main Menu
    self.Main= self:Add( "DFrame" )

    self.Main:SetPos( 0, 50 )
    self.Main:SetSize(400, ScrH()/1.015)
    self.Main:SetTitle("")
    self.Main:SetDraggable(false)
    self.Main:ShowCloseButton( false )
    self.Main.Paint = function(self, w, h )
    end
    local ButtonCol = Color(15,15,15,170)
    local ButtonHighCol = Color(255,255,255)

    /*
    ██       ██████  ██████  ██████  ██ ███████ ███████
    ██      ██    ██ ██   ██ ██   ██ ██ ██      ██
    ██      ██    ██ ██████  ██████  ██ █████   ███████
    ██      ██    ██ ██   ██ ██   ██ ██ ██           ██
    ███████  ██████  ██████  ██████  ██ ███████ ███████
    */
    self.Lobbies= self:Add( "EditablePanel" )
    self.Lobbies:SetPos( 0, 25 )
    self.Lobbies:SetSize(ScrW(), ScrH()-120)
    self.Lobbies:SetAlpha(0)
    self.Lobbies:Hide()
    self.Lobbies.Paint = function(self, w, h )
    end

    self.Lobbies.Minigames= self.Lobbies:Add( "EditablePanel" )
    self.Lobbies.Minigames:SetPos( 0, 25 )
    self.Lobbies.Minigames:SetSize((850+200), 50)
    self.Lobbies.Minigames:SetAlpha(255)
    self.Lobbies.Minigames.Paint = function(self, w, h )
      --surface.SetDrawColor( Color(15,15,15,170) )
      --surface.DrawRect(0, 0, w, h)
    end
    for minigame, col in pairs(lobby.minigames) do

      if minigame != "None" then

        self.Lobbies.Minigames[minigame] = vgui.CreateFromTable( BUTTON, self.Lobbies.Minigames )
        self.Lobbies.Minigames[minigame]:SetPos( 0, 0 )
        self.Lobbies.Minigames[minigame]:SetSize((850+200)/(table.Count(lobby.minigames)-1), 50)
        self.Lobbies.Minigames[minigame]:SetText(minigame)
        self.Lobbies.Minigames[minigame]:Dock(RIGHT)
        self.Lobbies.Minigames[minigame]:SetColor(ButtonCol)
        self.Lobbies.Minigames[minigame]:SetHighlight(col)
        self.Lobbies.Minigames[minigame].Button.OnClick = function()
        local par = self:GetParent():GetParent()
        local Settings = nil
        net.Start("lobbyNetworking")
          net.WriteString("create")
          net.WriteTable({LocalPlayer(), minigame, Settings})
        net.SendToServer()
        end
      end
    end



    self.Lobbies.Frame= self.Lobbies:Add( "EditablePanel" )
    self.Lobbies.Frame:SetPos( 0, 75 )
    self.Lobbies.Frame:SetSize(ScrW(), ScrH()-(120-50))
    self.Lobbies.Frame:SetAlpha(255)
    self.Lobbies.Frame.Paint = function(self, w, h )
      --surface.SetDrawColor( Color(15,15,15,170) )
      --surface.DrawRect(0, 0, w, h)
    end
    /*
    self.Lobbies.Info = self.Lobbies.Frame:Add( "DFrame" )
    self.Lobbies.Info:SetPos( 0, 0 )
    self.Lobbies.Info:SetSize(ScrW(), 1)
    self.Lobbies.Info:SetTitle("")
    self.Lobbies.Info:SetDraggable(false)
    self.Lobbies.Info:SetAlpha(255)
    self.Lobbies.Info:ShowCloseButton( false )
    self.Lobbies.Info.Paint = function(self, w, h )
      surface.SetDrawColor( Color(15,15,15,170) )
      surface.DrawRect(0, 0, w, h)
    end*/
    self.Lobbies.List = vgui.Create("DScrollPanel", self.Lobbies.Frame)
    --self.Lobbies.List:SetSize(200, ScrH()/1.015-50)
    --self.Lobbies.List:SetPos(20, 5+50)
    self.Lobbies.List:Dock( FILL )
    self.Lobbies.List:SetVerticalScrollbarEnabled(false)

    self.Lobbies.List.VBar:SetAlpha(0)

    self.Lobbies.List.offset = 0

    self.Lobbies.List.Paint = function(self, w, h )

    end

    self.Lobbies.List.OnVScroll = function(self, iOffset )
      self.offset = iOffset
    end

    self.Lobbies.List.Think = function(self )
      self.offsetsmooth = self.offsetsmooth and UT:Lerp(0.03, self.offsetsmooth , self.offset) or self.offset

      self.pnlCanvas:SetPos( 0, self.offsetsmooth )
    end

    self.Lobbies.List.PerformLayout = function(self )
      local wide = self:GetWide()

      self:Rebuild()

      self.VBar:SetUp( self:GetTall(), self.pnlCanvas:GetTall() )
      self.VBar:SetSize(0,0)
      if self.VBar.Enabled then wide = wide end-- self.VBar:GetWide() end

      self.pnlCanvas:SetWide(wide)

      self:Rebuild()
    end

    self.Lobbies.Frame.Topline = self.Lobbies.Frame:Add( "DFrame" )
    self.Lobbies.Frame.Topline:SetPos( 0, 0 )
    self.Lobbies.Frame.Topline:SetSize(ScrW(), 1)
    self.Lobbies.Frame.Topline:SetTitle("")
    self.Lobbies.Frame.Topline:SetDraggable(false)
    self.Lobbies.Frame.Topline:SetAlpha(255)
    self.Lobbies.Frame.Topline:ShowCloseButton( false )
    self.Lobbies.Frame.Topline.Paint = function(self, w, h )
      surface.SetDrawColor( Color(255,255,255,255) )
      surface.DrawRect(0, 0, w, h)
    end

    self.Lobbies.Back = vgui.CreateFromTable( BUTTON, self.Lobbies )
    self.Lobbies.Back:SetPos( 2, 0 )
    self.Lobbies.Back:SetSize(100, 25)
    self.Lobbies.Back:SetText("Back")
    self.Lobbies.Back:SetColor(ButtonCol)
    self.Lobbies.Back:SetHighlight(ButtonHighCol)
    self.Lobbies.Back.Button.OnClick = function()
      self.Main:AlphaTo(255, 0.15, 0)
      self.Lobbies:AlphaTo(0, 0.15, 0)
      timer.Simple(0.16, function() self.Lobbies:Hide() end)
    end

    self.LobbiesBut = vgui.CreateFromTable( BUTTON, self.Main )
    self.LobbiesBut:SetPos( 0, 0 )
    self.LobbiesBut:SetSize(300, 40)
    self.LobbiesBut:SetText("Lobbies")
    self.LobbiesBut:SetColor(ButtonCol)
    self.LobbiesBut:SetHighlight(ButtonHighCol)
    self.LobbiesBut.Button.OnClick = function()
      self.Lobbies:Show()
      self.Main:AlphaTo(0, 0.15, 0)
      self.Lobbies:AlphaTo(255, 0.15, 0)
    end

    /*
    ███████ ███████ ████████ ████████ ██ ███    ██  ██████  ███████
    ██      ██         ██       ██    ██ ████   ██ ██       ██
    ███████ █████      ██       ██    ██ ██ ██  ██ ██   ███ ███████
         ██ ██         ██       ██    ██ ██  ██ ██ ██    ██      ██
    ███████ ███████    ██       ██    ██ ██   ████  ██████  ███████
    */


    self.Settings= self:Add( "DFrame" )

    self.Settings:SetPos( 0, 50 )
    self.Settings:SetSize(400, ScrH()/1.015)
    self.Settings:SetTitle("")
    self.Settings:SetDraggable(false)
    self.Settings:SetAlpha(0)
    self.Settings:Hide()
    self.Settings:ShowCloseButton( false )
    self.Settings.Paint = function(self, w, h )
    end

    // HUD
    self.Settings.HUD = self:Add( "DFrame" )

    self.Settings.HUD:SetPos( 0, 50 )
    self.Settings.HUD:SetSize(400, ScrH()/1.015)
    self.Settings.HUD:SetTitle("")
    self.Settings.HUD:SetDraggable(false)
    self.Settings.HUD:SetAlpha(0)
    self.Settings.HUD:Hide()
    self.Settings.HUD:ShowCloseButton( false )
    self.Settings.HUD.Paint = function(self, w, h )
    end

    self.Settings.HUD.Toggle = vgui.CreateFromTable( BUTTON, self.Settings.HUD )
    self.Settings.HUD.Toggle:SetPos( 0, (40+5)*0 )
    self.Settings.HUD.Toggle:SetSize(300, 40)
    self.Settings.HUD.Toggle:SetText("HUD")
    self.Settings.HUD.Toggle:SetColor(ButtonCol)
    self.Settings.HUD.Toggle:SetHighlight(ButtonHighCol)
    self.Settings.HUD.Toggle.Button.OnClick = function()
      local convar = GetConVar("grp_hud")
      local float = convar:GetInt()+1
      if float > 1 then
        float = 0
      end
      convar:SetInt( float )
      console.RunCmd("hud_enabled", lP, {float})
    end

    self.Settings.HUD.Lag = vgui.CreateFromTable( TEXTEDIT, self.Settings.HUD )
    self.Settings.HUD.Lag:SetPos( 0, (40+5)*3 )
    self.Settings.HUD.Lag:SetSize(300, 40)
    self.Settings.HUD.Lag:SetText("Hud Sensitivity: " .. GetConVar("grp_hud_lag_sensitivity"):GetInt())
    self.Settings.HUD.Lag:SetColor(ButtonCol)
    self.Settings.HUD.Lag:SetHighlight(ButtonHighCol)
    self.Settings.HUD.Lag.Numeric = true
    self.Settings.HUD.Lag.ClickText = "Hud Sensitivity: "
    self.Settings.HUD.Lag.Min = 1
    self.Settings.HUD.Lag.Max = 30
    self.Settings.HUD.Lag.Button.OnClick = function()
    end

    self.Settings.HUD.Lag.OnEnter = function(self)
      if tonumber(self.txt:GetValue()) == nil or tonumber(self.txt:GetValue()) < self.Min then
        local string = tostring(self.Min)
        self.Text = "Hud Sensitivity: " .. string
        self.txt:SetText(string)
      end
      GetConVar("grp_hud_lag_sensitivity"):SetInt(self.txt:GetText())
    end

    self.Settings.HUD.Lag.OnType = function(self, string)

      self.Text = "Hud Sensitivity: " .. string

      if tonumber(string) and tonumber(string) > self.Max then
        string = tostring(self.Max)
        self.Text = "Hud Sensitivity: " .. string
        self.txt:SetText(string)
      end
      --self.txt:SetText(string)
    end

    self.Settings.HUD.Crosshair = vgui.CreateFromTable( BUTTON, self.Settings.HUD )
    self.Settings.HUD.Crosshair:SetPos( 0, (40+5)*4 )
    self.Settings.HUD.Crosshair:SetSize(300, 40)
    self.Settings.HUD.Crosshair:SetText("Crosshair")
    self.Settings.HUD.Crosshair:SetColor(ButtonCol)
    self.Settings.HUD.Crosshair:SetHighlight(ButtonHighCol)
    self.Settings.HUD.Crosshair.Button.OnClick = function()
      local convar = GetConVar("grp_crosshair")
      local float = convar:GetInt()+1
      if float > 2 then
        float = 0
      end
      convar:SetInt( float )
    end

    self.Settings.HUD.Back = vgui.CreateFromTable( BUTTON, self.Settings.HUD )
    self.Settings.HUD.Back:SetPos( 0, (40+5)*7 )
    self.Settings.HUD.Back:SetSize(300, 40)
    self.Settings.HUD.Back:SetText("Back")
    self.Settings.HUD.Back:SetColor(ButtonCol)
    self.Settings.HUD.Back:SetHighlight(ButtonHighCol)
    self.Settings.HUD.Back.Button.OnClick = function()
      self.Settings:AlphaTo(255, 0.15, 0)
      self.Settings.HUD:AlphaTo(0, 0.15, 0)
      timer.Simple(0.16, function() self.Settings.HUD:Hide() end)
    end

    self.Settings.HUDBut = vgui.CreateFromTable( BUTTON, self.Settings )
    self.Settings.HUDBut:SetPos( 0, 0 )
    self.Settings.HUDBut:SetSize(300, 40)
    self.Settings.HUDBut:SetText("HUD")
    self.Settings.HUDBut:SetColor(ButtonCol)
    self.Settings.HUDBut:SetHighlight(ButtonHighCol)
    self.Settings.HUDBut.Button.OnClick = function()
      self.Settings.HUD:Show()
      self.Settings.HUD:AlphaTo(255, 0.15, 0)
      self.Settings:AlphaTo(0, 0.15, 0)

    end

    self.Settings.Back = vgui.CreateFromTable( BUTTON, self.Settings )
    self.Settings.Back:SetPos( 0, (40+5) )
    self.Settings.Back:SetSize(300, 40)
    self.Settings.Back:SetText("Back")
    self.Settings.Back:SetColor(ButtonCol)
    self.Settings.Back:SetHighlight(ButtonHighCol)
    self.Settings.Back.Button.OnClick = function()
      self.Main:AlphaTo(255, 0.15, 0)
      self.Settings:AlphaTo(0, 0.15, 0)
      timer.Simple(0.16, function() self.Settings:Hide() end)
    end

    self.SettingsBut = vgui.CreateFromTable( BUTTON, self.Main )
    self.SettingsBut:SetPos( 0, 40+5 )
    self.SettingsBut:SetSize(300, 40)
    self.SettingsBut:SetText("Settings")
    self.SettingsBut:SetColor(ButtonCol)
    self.SettingsBut:SetHighlight(ButtonHighCol)
    self.SettingsBut.Button.OnClick = function()
      self.Settings:Show()
      self.Main:AlphaTo(0, 0.15, 0)
      self.Settings:AlphaTo(255, 0.15, 0)
    end

    /*
    ██      ███████  █████  ██████  ███████ ██████  ██████   ██████   █████  ██████  ██████
    ██      ██      ██   ██ ██   ██ ██      ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ██   ██
    ██      █████   ███████ ██   ██ █████   ██████  ██████  ██    ██ ███████ ██████  ██   ██
    ██      ██      ██   ██ ██   ██ ██      ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ██   ██
    ███████ ███████ ██   ██ ██████  ███████ ██   ██ ██████   ██████  ██   ██ ██   ██ ██████
    */


    self.LeaderBut = vgui.CreateFromTable( BUTTON, self.Main )
    self.LeaderBut:SetPos( 0, (40+5)*2 )
    self.LeaderBut:SetSize(300, 40)
    self.LeaderBut:SetText("Leaderboards")
    self.LeaderBut:SetColor(ButtonCol)
    self.LeaderBut:SetHighlight(ButtonHighCol)
    self.LeaderBut.Button.OnClick = function()
      self.Main:AlphaTo(0, 0.3, 0)
    end

    /*
    ███████ ████████  █████  ████████ ███████
    ██         ██    ██   ██    ██    ██
    ███████    ██    ███████    ██    ███████
         ██    ██    ██   ██    ██         ██
    ███████    ██    ██   ██    ██    ███████
    */

    self.Stats= self:Add( "DFrame" )
    self.Stats:SetPos( 0, 50 )
    self.Stats:SetSize(ScrW(), ScrH())
    self.Stats:SetTitle("")
    self.Stats:SetDraggable(false)
    self.Stats:SetAlpha(0)
    self.Stats:Hide()
    self.Stats:ShowCloseButton( false )
    self.Stats.Paint = function(self, w, h )
    end
    self.Stats.Avg = lP:GetAvg()
    self.Stats.TSA = lP:TSA()
    self.Stats.TSS = lP:TSS()

    self.Stats.Text= self.Stats:Add( "TopicPanel" )
    self.Stats.Text:SetPos( 0, 0 )
    self.Stats.Text:SetSize(300, (40+5)*9)

    self.Stats.Back = vgui.CreateFromTable( BUTTON, self.Stats )
    self.Stats.Back:SetPos( 0, (40+5)*9+1 )
    self.Stats.Back:SetSize(300, 40)
    self.Stats.Back:SetText("Back")
    self.Stats.Back:SetColor(ButtonCol)
    self.Stats.Back:SetHighlight(ButtonHighCol)
    self.Stats.Back.Button.OnClick = function()
      self.Main:AlphaTo(255, 0.15, 0)
      self.Stats:AlphaTo(0, 0.15, 0)
      timer.Simple(0.16, function() self.Stats:Hide() end)
    end

    self.Statsbut = vgui.CreateFromTable( BUTTON, self.Main )
    self.Statsbut:SetPos( 0, (40+5)*3 )
    self.Statsbut:SetSize(300, 40)
    self.Statsbut:SetText("Stats")
    self.Statsbut:SetColor(ButtonCol)
    self.Statsbut:SetHighlight(ButtonHighCol)
    self.Statsbut.Button.OnClick = function()

      self.Stats:Show()
      self.Main:AlphaTo(0, 0.15, 0)
      self.Stats:AlphaTo(255, 0.15, 0)
    end
    local stat = self.Stats.Text
    stat:NewPage("Stats")
    stat:AddTopic("Speed")
    stat:AddColor(Color(255, 255, 255, 255))
    stat:AddFont(GetFont("Help_Text"))

    stat:AddText("Average: ") //6
    stat:AddText("0", "Avg") //7
    stat:AddText("// All-Time Topspeed: ") //8
    stat:AddText("0", "TSA") //9
    stat:AddText("// Session Topspeed: " ) //10
    stat:AddText("0", "TSS") //11

    stat:AddTopic("Points") //12
    stat:AddColor(Color(255, 255, 255)) //13
    stat:AddFont(GetFont("Help_Text")) //14, 15, 16
    stat:AddText("Rank: ") //17
    stat:AddColor(Color(255,255,255), "Rank Col") //18
    stat:AddText("Starter", "Rank") //19
    stat:AddColor(Color(255, 255, 255)) //20
    stat:AddText("// Points: ") //21
    stat:AddText("0", "Pts") //22
    stat:AddText("// Points from strafing: ") //23
    stat:AddText("0", "SP") //24
    stat:AddText("// Time spent strafing: ") //25
    stat:AddText("0", "ST") //26
    stat.Think = function()
      if LocalPlayer():GetAvg() > 0 then
        self.Stats.Avg = lP:GetAvg()
      end

      stat:Replace("Avg", tostring(LocalPlayer():GetAvg()) .. " (" .. tostring(self.Stats.Avg) .. ")")
      stat:Replace("TSA", tostring(math.Round(LocalPlayer():TSA())))
      stat:Replace("TSS", tostring(math.Round(LocalPlayer():TSS())))
      stat:Replace("Rank Col", rankColor(calcRank(stats.session[lP:SteamID()].pts)))
      stat:Replace("Rank", calcRank(stats.session[lP:SteamID()].pts))
      stat:Replace("Pts", tostring(stats.session[lP:SteamID()].pts))
      stat:Replace("SP", tostring(math.Round(stats.session[lP:SteamID()].spts, 2)))
      stat:Replace("ST", tostring(lP:StrafeTime()))
    end
    /*
    ██   ██ ███████ ██      ██████
    ██   ██ ██      ██      ██   ██
    ███████ █████   ██      ██████
    ██   ██ ██      ██      ██
    ██   ██ ███████ ███████ ██
    */



    self.Help= self:Add( "DFrame" )
    self.Help:SetPos( 0, 50 )
    self.Help:SetSize(600, ScrH()/1.015)
    self.Help:SetTitle("")
    self.Help:SetDraggable(false)
    self.Help:SetAlpha(0)
    self.Help:Hide()
    self.Help:ShowCloseButton( false )
    self.Help.Paint = function(self, w, h )
    end

    self.Help.Text= self.Help:Add( "TopicPanel" )
    self.Help.Text:SetPos( 0, 0 )
    self.Help.Text:SetSize(600, (40+5)*9)


    self.Help.Back = vgui.CreateFromTable( BUTTON, self.Help )
    self.Help.Back:SetPos( 0, (40+5)*9+1 )
    self.Help.Back:SetSize(300, 40)
    self.Help.Back:SetText("Back")
    self.Help.Back:SetColor(ButtonCol)
    self.Help.Back:SetHighlight(ButtonHighCol)
    self.Help.Back.Button.OnClick = function()
      self.Main:AlphaTo(255, 0.15, 0)
      self.Help:AlphaTo(0, 0.15, 0)
      timer.Simple(0.16, function() self.Help:Hide() end)
    end


    self.HelpBut = vgui.CreateFromTable( BUTTON, self.Main )
    self.HelpBut:SetPos( 0, (40+5)*4 )
    self.HelpBut:SetSize(300, 40)
    self.HelpBut:SetText("Help")
    self.HelpBut:SetColor(ButtonCol)
    self.HelpBut:SetHighlight(ButtonHighCol)
    self.HelpBut.Button.OnClick = function()
      self.Help:Show()
      self.Main:AlphaTo(0, 0.15, 0)
      self.Help:AlphaTo(255, 0.15, 0)
    end

  local text = self.Help.Text
  text:NewPage("General")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Stuff everyone should know before moving on.")

    text:AddTopic("Controls")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("WASD - Movement keys.")
    text:AddText("Space - Jump.")
    text:AddText("CTRL - Duck/Buttslide.")
    text:AddText("LMB - Shoot.")
    text:AddText("RMB - Specials.")

    text:AddTopic("UI")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("At the topleft of your screen you'll see your rank on top.")
    text:AddText("Underneath your rank comes your combos, pts and time left.")
    text:AddText("//")
    text:AddText("Bottom left is you health, it will disappear when your health reaches 100")
    text:AddText("//")
    text:AddText("To the right of the healthbar is your speed. The limit of the bar follows your session topspeed")

    text:AddColor(Color(119, 246, 124))
    text:AddFont(GetFont("Help_SubTopic"))
    text:AddText("// CGaz")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("CGaz is what's being showed in the middle of your screen when you're trying to strafe.")
    text:AddText("It shows you the optimal angles for strafing, the colors represent different things.")
    text:AddColor(Color(38, 60, 255))
    text:AddText("Best possible angle for speed gain.")
    text:AddColor(Color(43, 255, 38))
    text:AddText("Shows an alright angle in general")
    text:AddColor(Color(255, 207, 38))
    text:AddText("How far you can turn without losing speed.")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddText("You want to stay in the green/blue area as much as possible.")

    text:AddTopic("Bunnyhop/Bhop")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText( "Bunnyhopping is a way of maintaining speed without losing it to friction." )
    text:AddText( "There's semi autohopping which allows you to queue bhops by releasing then holding space after jumping" )
    text:AddColor(Color(255, 255, 50, 255))
    text:AddText( "Example: Press space, once you leave the ground release then hold space." )

    text:AddTopic("Air strafing")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText( "Air strafing allows you to maneuver and gain speed in the air." )
    text:AddText( "You can do so by holding any of the movement keys and moving your mouse." )
    text:AddColor(Color(255, 255, 50, 255))
    text:AddText( "Example: Hold W+D and turn your mouse right." )

    text:AddTopic("Buttsliding/BS")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Buttsliding lets you glide along the ground with low friction, the friction does")
    text:AddText("increase the longer you're buttsliding")
    text:AddText("You can also turn faster buttsliding when strafing doesn't cut it.")
  text:NewPage("Basics")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("All known special moves and other basic movement stuff.")

    text:AddTopic("Edge Jump")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Edge jumping lets you get some extra speed and height when you're")
    text:AddText("jumping right at the edge of something.")
    text:AddText("It's important that you touch the ground without jumping right before Edge Jumping for it to work")
    text:AddColor(Color(255, 255, 50, 255))
    text:AddText("Rule of thumb: When you look down you should be looking at something you're not standing on like the roads from a building.")

    text:AddTopic("Rocket Jump")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Rocket Jumping is an efficient way of getting momentum.")
    text:AddText("You can shoot up to 3 rockets at a time.")
    text:AddText("The firing rate is one rocket per 800ms.")
    text:AddText("The rockets travel at a speed of 1000ups.")
    text:AddText("To fire a rocket you just press M1.")


    text:AddTopic("Charged Jump")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Charged jumps allows you to charge a jump and jump higher, it comes in three stages.")
    text:AddText("You can Charge Jump by performing buttsliding while holding M2 then jump.")
    text:AddText("It takes 350ms to charge it once.")
    text:AddColor(Color(25, 200, 25, 255))
    text:AddText("Stage 1: You jump slightly higher, good for jumping between buildings or obstacles at eye level.")
    text:AddColor(Color(75, 150, 255, 255))
    text:AddText("Stage 2: Good for getting ontop of buildings taller than you.")
    text:AddColor(Color(252, 174, 0))
    text:AddText("Stage 3: You can get over or ontop of most obstacles.")
    text:AddColor(Color(255, 255, 50, 255))
    text:AddText( "// Example: Hold CTRL and M2, when you hear a sound you've charged a jump." )

    text:AddTopic("Double Charged Jump")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Double Charged Jump lets you do a Charge Jump twice in a row, the second one is free of charge.")
    text:AddText("All you got to do is land within: ")
    text:AddColor(Color(25, 200, 25, 255))
    text:AddText("1 Second for stage 1")
    text:AddColor(Color(75, 150, 255, 255))
    text:AddText("1.5 Seconds for stage 2")
    text:AddColor(Color(252, 174, 0))
    text:AddText("2 Seconds for stage 3")

    text:AddTopic("Ramp Sliding")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Ramp Sliding lets you slide down any ramp as after falling from a high place.")
    text:AddText("It will transition your vertical speed to diagonal speed.")
    text:AddText("You have to hold Duck before and after you hit the ramp.")
    text:AddColor(Color(255, 255, 50, 255))


    text:AddTopic("Skimming")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Skimming is feature that lets you hit corners or ledges, but still maintain your speed when nothing is blocking you anymore.")
    text:AddText("To do this you have to hold M2 before hitting a surface.")
    text:AddText("It'll only last for 250ms after hitting a wall.")
    text:AddColor(Color(255, 255, 50, 255))
    text:AddText( "Example: Hold M2 then bump into a wall or ledge, preferably at an angle. Once nothing is blocking you you'll go as fast as when you crashed." )

    text:AddTopic("WallRunning")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("WallRunning lets you gain vertical momentum at the cost of half your horizontal momentum.")
    text:AddText("You can nullify the speed loss by Skimming as you WallRun.")
    text:AddText("You can WallRun max two times before touching the ground.")
    text:AddText("If you fall too far you lose health when WallRunning.")
    text:AddText("You can WallRun by holding M2 and pressing Space at a wall.")
    text:AddColor(Color(255, 255, 50, 255))
    text:AddText( "Example: Approach a wall while holding M2, once you reach the wall press Space." )
  text:NewPage("Techniques")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Different techniques and combination of moves.")

    text:AddTopic("Circle Jump")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Circle Jumping is a good way of getting as much speed as possible when you're")
    text:AddText("moving slowly.")
    text:AddText("To circle jump you hold W+D/A and move your mouse right/left.")
    text:AddText("When you're at your peak speed you jump.")
    text:AddText("You gain much more speed circle jumping while buttsliding.")
    text:AddColor(Color(255, 255, 50, 255))
    text:AddText( "Example: Hold W+D+(Duck) and turn your mouse right." )
    text:AddText( "            At your peak speed you press space and keep turning." )

    text:AddTopic("Charged Edge Jump")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Charged Edge Jump is a combination of Charged Jump and Edge Jump.")
    text:AddText("You have to do a Charged Jump at a ledge to manage this.")
    text:AddColor(Color(255, 255, 50, 255))
    text:AddText( "Example: Hold Duck and M2, once you can Edge Jump you press space." )

    text:AddTopic("Double Charged Edge Jump")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("A hard trick that which doesn't work everywhere.")
    text:AddText("It's a Double Charged Jump, but both jumps happens in combination with Edge Jumping")

    text:AddTopic("Charged Rocket Jump")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("A Charged Jump combined with Rocket Jumping.")
    text:AddText("Gives you two times more height than normal Rocket Jumping")
  text:NewPage("Strafing")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Strafing is the most important part of this, you'll have to know how to strafe to manage your speed.")

    text:AddTopic("Fullbeat")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Fullbeat strafes is accomplished by holding W+A and moving your mouse to the left or W+D and moving your mouse to the right.")
    text:AddColor(Color(109, 196, 188))
    text:AddText("Key points:")
    text:AddText("- Always move your mouse, otherwise you won't gain speed")
    text:AddText("- Strafe angle gets smaller, but also further away from the opposite angle the faster you go.")
    text:AddText("- When switching from left to right side you want it to happen in an instant.")
    text:AddText("- Optimally you want to switch side everytime you land")

    text:AddTopic("Halfbeat")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Halfbeat strafes is accomplished by either:")
    text:AddText("Holding W+A when moving your mouse to the left and D when moving your mouse to the right.")
    text:AddText("or")
    text:AddText("Holding W+D when moving your mouse to the right and A when moving your mouse to the left.")
    text:AddColor(Color(109, 196, 188))
    text:AddText("Key points:")
    text:AddText("- Keypoints from Fullbeat still applies here")

    text:AddTopic("AirControl")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("AirControl is a feature that allows you to maintain 100% of your velocity when turning by only holding W.")
    text:AddText("If you turn too far it won't activate and you'll actually lose some speed.")
    text:AddText("When trying to turn going fast you'll kind of move in a circle towards where you aim.")
    text:AddColor(Color(255, 255, 50, 255))
    text:AddText( "Example: Hold W+D+(Duck) and turn your mouse right." )
    text:AddText( "            At your peak speed you press space and keep turning." )
  text:NewPage("Combos")
    local function subTopic(txt)
      text:AddColor(Color(119, 246, 124))
      text:AddFont(GetFont("Help_SubTopic"))
      text:AddText(txt)
    end
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("All you need to know about combos")

    text:AddTopic("Lines")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("A line is a bunch of combos and techniques strung together.")
    text:AddText("There is no multiplier, you gain x amount of points doing a combo. The amount of points depletes the more you repeat it in a row.")
    text:AddText("A line generally lasts for at least 5 seconds, everytime you do a combo the timer resets.")
    text:AddText("Strafing slows down the timer, also give you a bit of points.")

    text:AddTopic("Passive Combos")

    subTopic("AirTime")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Spend " .. LocalPlayer():ComboReset()-1 .. " seconds in the air" )
    text:AddColor(Color(247, 237, 0))
    text:AddText("Points: " .. Combos["AirTime"]["pts"] )

    subTopic("// Strafing")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Everytime you strafe you gain up to 0.01 pts per tick or roughly 0.5 pts per second.")
    text:AddText("You'll also add a enough time to the timer to almost freeze it.")
    text:AddText("The more speed you gain from strafing the more points you get.")
    text:AddColor(Color(247, 237, 0))
    text:AddText("Points: 0.1 - 0.0001" )

    text:AddTopic("Single Combos")
    subTopic("Charged Jumps")
    text:AddColor(Color(255, 255, 255, 255))
    text:AddFont(GetFont("Help_Text"))
    text:AddText("Do Charged Jumps" )
    text:AddColor(Color(25, 200, 25))
    text:AddText("Stage 1 Points: " .. Combos["Charged Jump S1"]["pts"] )
    text:AddColor(Color(75, 150, 255))
    text:AddText("Stage 2 Points: " .. Combos["Charged Jump S2"]["pts"] )
    text:AddColor(Color(252, 174, 0))
    text:AddText("Stage 3 Points: " .. Combos["Charged Jump S3"]["pts"] )

    for k, v in pairs(Combos) do
      if v["desc"] != nil then
        subTopic("// " .. k)
        text:AddColor(Color(255, 255, 255, 255))
        text:AddFont(GetFont("Help_Text"))
        text:AddText(v["desc"] )
        text:AddColor(Color(247, 237, 0))
        text:AddText("Points: " .. v["pts"] )
      end
    end

    text:AddTopic("Branched Combos")

    for k, v in pairs(Combos.Branch) do
      if v["desc"] != nil then
        subTopic(k)
        text:AddColor(Color(255, 255, 255, 255))
        text:AddFont(GetFont("Help_Text"))
        text:AddText(v["desc"] )
        text:AddColor(Color(247, 237, 0))
        text:AddText("Points: " .. v["pts"] .. " // " )
      end
    end
  end,

	PerformLayout = function( self )

		self:SetSize( 850+200, ScrH() )
		self:SetPos( ScrW()/2 - ((850+200)/2), 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    for id, lb in pairs( lobby.lobbies ) do

			if ( IsValid( lb.Entry ) ) then continue end
      if ( IsValid( lb.Entry2 ) ) then continue end

			lb.Entry = vgui.CreateFromTable( LOBBY_BUTTON, lb.Entry )
      lb.Entry:SetSize((850+200), 30)
      lb.Entry:Dock(TOP)
      lb.Entry:DockMargin(0,0,0,5)
			lb.Entry:Setup( lb, self.Lobbies.List, id )

      self.Lobbies.List:AddItem( lb.Entry  )


		end
	end
}

MENU = vgui.RegisterTable( MENU, "EditablePanel" )

if ( IsValid( ContextMenu ) ) then
	ContextMenu:Remove()
end
function GM:OnContextMenuOpen()
  gui.EnableScreenClicker(true)
  if LocalPlayer():Editing() then

    return
  end
	open = true

	if ( !IsValid( ContextMenu ) ) then
		ContextMenu = vgui.CreateFromTable( MENU )
    ContextMenu:SetAlpha(0)
	end

	if ( IsValid( ContextMenu ) ) then
    ContextMenu:AlphaTo(255, 0.15)
		ContextMenu:Show()
		ContextMenu:MakePopup()
		ContextMenu:SetKeyboardInputEnabled( false )
	end
end

function GM:OnContextMenuClose()
  if LocalPlayer():Editing() then
    return
  end
  if lobby.keepopen then return end
	open = false

	if ( IsValid( ContextMenu ) ) then
		timer.Simple(0.2, function()
			if not open then
				ContextMenu:Hide()
			end
		end)

		ContextMenu:AlphaTo(0, 0.15)
	end

  gui.EnableScreenClicker(false)
end
/*
concommand.Add("hudeditor", function(ply, command, args)

  hEdit.Editor(true)
end)*/
