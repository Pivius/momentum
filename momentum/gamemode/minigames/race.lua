local race = {}

race.desc = "Just a lobby"
race.teams = {
  [1] = "Racer",
  [2] = "Zoner"
}

race.colors = {
  [1] = Color(41, 228, 25),
  [2] = Color(228, 177, 25)
}

race.players = {}
race.settings = {}
race.timeLimit = 0
race.RocketJumping = true
race.Zoner = ""

function race.ReInit()
end

function race.addPlayer(ply)
  race.players[ply] = 1
  lobby:SetTeam(ply, race.colors[1])
end

function race.removePlayer(ply)
  UT:RemoveByKey(race.players, ply)
  lobby:SetTeam(ply, Color(255,255,255))
end

function race.setZoner(ply)
  if race.Zoner == ply then
    return false
  end
  race.Zoner = ply
  race.players[ply] = 2
  chat.Text(ply, "[", race.colors[1], "Race", Color(255,255,255), "] You've been selected as the ", race.colors[2], "zoner", Color(255,255,255),".")
  chat.Text(ply, "[", race.colors[1], "Race", Color(255,255,255), "] You can open up the editor by holding Q or using the console command +menu.")

  lobby:SetTeam(ply, race.colors[2])
end

function race.Think()

  if race.Zoner == "" then
    local val, random =table.Random(race.players)

    race.setZoner(random)
  end
end


return race
