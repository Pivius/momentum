local hunted = {}

hunted.desc = "Hunters chase the Hunted."

hunted.teams = {
  [1] = "Hunters",
  [2] = "Hunted"
}

hunted.colors = {
  [1] = Color(0,150,200,255),
  [2] = Color(200,50,0,255)

}
hunted.switching = false
hunted.state = "inactive"

hunted.hunted = "none"
hunted.hunters = {}

hunted.hitBox = 50

hunted.players = {}
hunted.settings = {}
hunted.timeLimit = 300
hunted.gravity = 1
hunted.airAccelerate = 10

function hunted.Sync()
  net.Start("lobbyNetworking")
    net.WriteString("sync")
    net.WriteTable({lobby:getLobbies()})
  net.Broadcast()
end

function hunted.ReInit()
  hunted.timeLimit = hunted.settings["time"]
  hunted.gravity = hunted.settings["gravity"]/400
  hunted.airAccelerate = hunted.settings["airaccelerate"]
end

function hunted.addPlayer(ply)
  hunted.players[ply] = 1 --table.insert(hunted.players, ply)
  lobby:SetTeam(ply, hunted.colors[1], hunted.colors[2])
  if hunted.state != "inactive" then
    table.insert(hunted.hunters, ply)
    notifyPly(ply, "Hunters", "Hunted You're a hunter // Catch the hunted", "normal", 3, {
      [1] = hunted.colors[1],
      [4] = hunted.colors[1],
      [7] = hunted.colors[2]
    })
    ply:Spawn() -- respawn all to prevent camping
  end
end

function hunted.removePlayer(ply)
  if ply == hunted.hunted then
    hunted.tagged(ply, "leave") -- Just use this function to select a random hunted
  end
  UT:RemoveByKey(hunted.players, ply) --table.RemoveByValue(hunted.players, ply)
  if table.HasValue(hunted.hunters, ply) then
    table.RemoveByValue(hunted.hunters, ply)
  end
  lobby:SetTeam(ply, Color(255,255,255))
end

function hunted.startGame()
  if hunted.state != "inactive" then return end -- prevent accidental restarts

  if table.Count(hunted.players) <= 1 then return end -- make sure there is more than 1 players
  local randhunted = hunted.generateHunted() -- generates a new hunted
  table.Empty(hunted.hunters)
  hunted.hunted = randhunted
  hunted.players[randhunted] = 2
  lobby:SetTeam(hunted.hunted, hunted.colors[2], hunted.colors[1])
  --hunted.hunted:TrailColor(hunted.colors[2]) -- gives him a new trail color
  for k, v in pairs(hunted.players) do
    if k != hunted.hunted then -- only get the players that was not selected as hunted
      table.insert(hunted.hunters, k)
      hunted.players[k] = 1
      lobby:SetTeam(k, hunted.colors[1], hunted.colors[2])
      --k:TrailColor(hunted.colors[1])
    end
    k:UpdateTrail()
    k:Spawn() -- respawn all to prevent camping
  end
  hunted.state = "active" -- set the state to avoid accidental restarts
  notifyPly(randhunted, "Hunted You're now the hunted // Your objective is to outrun the hunters", "normal", 3, {
    [1] = hunted.colors[2],
    [5] = hunted.colors[2],
    [12] = hunted.colors[1]
  })
  notifyPly(hunted.hunters, "Hunted " .. randhunted:Nick() .. " is now the hunted // You objective is to catch the hunted", "normal", 3, {
    [1] = hunted.colors[1],
    [6] = hunted.colors[2],
    [13] = hunted.colors[2]
  })
  hunted.Sync()
end

function hunted.generateHunted(ignore)
  local val, randhunted = table.Random(hunted.players)
    if randhunted == ignore then

      return hunted.generateHunted(ignore)
    end
  return randhunted
end

function hunted.isHunted(ply)
  if hunted.hunted == ply then return true else return false end
end

function hunted.died()
  if not hunted.hunted:Alive() then -- make sure the hunted is dead
    local randhunted = hunted.generateHunted(hunted.hunted)
    local oldhunted  = hunted.hunted
    hunted.players[oldhunted] = 1
    --oldhunted:TrailColor(hunted.colors[1])
    lobby:SetTeam(oldhunted, hunted.colors[1], hunted.colors[2])
    table.RemoveByValue(hunted.hunters, randhunted) -- removes the new hunted from the hunters

    hunted.hunted = randhunted
    hunted.players[hunted.hunted] = 2
    lobby:SetTeam(randhunted, hunted.colors[2], hunted.colors[1])
    --randhunted:TrailColor(hunted.colors[2])


    notifyPly(hunted.hunters, "Hunted " .. randhunted:Nick() .. " has been randomly selected as the hunted", "normal", 3, {
      [1] = hunted.colors[1],
      [9] = hunted.colors[2]
    })
    table.insert(hunted.hunters, oldhunted) -- adds the old hunted to hunters

    notifyPly(oldhunted, "Hunted You died and " .. randhunted:Nick() .. " has been selected as hunted", "normal", 3, {
      [1] = hunted.colors[1],
      [10] = hunted.colors[2]
    })

    notifyPly(randhunted, "Hunted You have been randomly selected as the hunted", "normal", 3, {
      [1] = hunted.colors[2],
      [9] = hunted.colors[2]
    })
    hunted.Sync()
  end
end

function hunted.tagged(ply, tagger)
  if hunted.isHunted(ply) and tagger != "leave" then
    hunted.switching = true
    hunted.players[ply] = 1
    --ply:TrailColor(hunted.colors[1])
    lobby:SetTeam(ply, hunted.colors[1], hunted.colors[2])
    ply:UpdateTrail()
    ply:Spawn() -- respawn the tagged player
    table.RemoveByValue(hunted.hunters, tagger)

    hunted.hunted = tagger
    hunted.players[hunted.hunted] = 2
    --tagger:TrailColor(hunted.colors[2])
    lobby:SetTeam(tagger, hunted.colors[2], hunted.colors[1])
    notifyPly(ply, "Hunted You have been tagged by " .. tagger:Nick(), "normal", 3, {
      [1] = hunted.colors[1],
      [7] = hunted.colors[2]
    })
    notifyPly(tagger, "Hunted You're now the hunted", "normal", 3, {
      [1] = hunted.colors[2],
      [5] = hunted.colors[2]
    })
    notifyPly(hunted.hunters, "Hunted " .. tagger:Nick() .. " is now the hunted", "normal", 3, {
      [1] = hunted.colors[1],
      [6] = hunted.colors[2]
    })
    table.insert(hunted.hunters, ply)
    hunted.Sync()
    timer.Simple(0.25, function() hunted.switching = false end)
  elseif hunted.isHunted(ply) and tagger == "leave" then
    hunted.switching = true

    hunted.hunted = hunted.generateHunted(ply)
    hunted.players[hunted.hunted] = 2
    lobby:SetTeam(hunted.hunted, hunted.colors[2], hunted.colors[1])
    --hunted.hunted:TrailColor(hunted.colors[2])

    table.RemoveByValue(hunted.hunters, hunted.hunted)
    hunted.hunted:Spawn()
    notifyPly(hunted.hunted, "Hunted You have been randomly selected as the hunted", "normal", 3, {
      [1] = hunted.colors[2],
      [9] = hunted.colors[2]
    })
    notifyPly(hunted.hunters, "Hunted " .. hunted.hunted:Nick() .. " has been randomly selected as the hunted", "normal", 3, {
      [1] = hunted.colors[1],
      [9] = hunted.colors[2]
    })
    hunted.Sync()
    timer.Simple(0.25, function() hunted.switching = false end)
  end
end

function hunted.Think()

  if table.Count(hunted.players) >= 2 and hunted.state == "inactive" then
    hunted.startGame()
  elseif table.Count(hunted.players) < 2 then
    hunted.state = "inactive"
  end

  if hunted.state == "inactive" then return end
	if hunted.hunted == "none" then return end

  if table.Count(hunted.players) <= 1 and hunted.state != "inactive" then
    hunted.state = "inactive"
    hunted.hunted = "none"
    hunted.hunters = {}
    notifyPly(hunted.players, "Hunted Waiting for more players", "normal", 3, {
      [1] = hunted.colors[2],
      [9] = hunted.colors[2]
    })
  end
  hunted.died()

  hunted.huntedHitBox = ents.FindInSphere(hunted.hunted:GetPos(), hunted.hitBox)
	for _, ent in pairs(hunted.huntedHitBox) do
		if (ent:IsPlayer() ) and ent:Alive() and ent != hunted.hunted and table.HasValue(hunted.hunters, ent) and hunted.switching == false then

      hunted.tagged(hunted.hunted, ent)
		end
	end
end



return hunted
