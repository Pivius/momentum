local elimination = {}

elimination.desc = "Hunters chase the elimination."

elimination.teams = {
  [1] = "Hunters",
  [2] = "Runners"

}

elimination.colors = {
  [1] = Color(200,50,0,255),
  [2] = Color(0,150,200,255)


}

elimination.state = "inactive"

elimination.runners = {}
elimination.hunters = {}

elimination.hitBox = 50

elimination.players = {}
elimination.settings = {}
elimination.timeLimit = 300
elimination.gravity = 1
elimination.airAccelerate = 10

function elimination.ReInit()
  elimination.timeLimit = elimination.settings["time"]
  elimination.gravity = elimination.settings["gravity"]/400
  elimination.airAccelerate = elimination.settings["airaccelerate"]
end
function elimination.addPlayer(ply)
  --ply:TrailColor(elimination.colors[1])
  elimination.players[ply] = 1 --table.insert(elimination.players, ply)
  lobby:SetTeam(ply, elimination.colors[1], elimination.colors[2])
  if elimination.state != "inactive" then
    table.insert(elimination.hunters, ply)
    ply:Spawn()
  end
end

function elimination.removePlayer(ply)
  --ply:TrailColor(Color(255,255,255))
  UT:RemoveByKey(elimination.players, ply) --table.RemoveByValue(elimination.players, ply)
  lobby:SetTeam(ply, Color(255,255,255))
  if !elimination.isRunner(ply) then
    table.RemoveByValue(elimination.hunters, ply)
  else
    table.RemoveByValue(elimination.runners, ply)
    elimination.tagged(ply, "leave")
  end

end

function elimination.isRunner(ply)
  if table.HasValue(elimination.runners, ply) then
    return true
  else
    return false
  end
end

function elimination.startGame()
  if elimination.state != "inactive" then return end -- prevent accidental restarts

  if table.Count(elimination.players) <= 2 then return end -- make sure there is more than 2 players
  table.Empty(elimination.hunters)
  table.Empty(elimination.runners)
  if table.Count(elimination.players) < 10 then
    local hunter = elimination.generateHunter() -- generates a hunter
    elimination.players[hunter] = 1
    --hunter:TrailColor(elimination.colors[1]) -- gives him a new trail color
    lobby:SetTeam(hunter, elimination.colors[1], elimination.colors[2])
    table.insert(elimination.hunters, hunter)
    hunter:UpdateTrail()
    hunter:Spawn() -- respawn all to prevent camping
  else
    for i=1, 3 do
      local hunter = elimination.generateHunter() -- generates a hunter
      elimination.players[hunter] = 1
      --hunter:TrailColor(elimination.colors[1]) -- gives him a new trail color
      lobby:SetTeam(hunter, elimination.colors[1], elimination.colors[2])
      table.insert(elimination.hunters, hunter)
      hunter:UpdateTrail()
      hunter:Spawn() -- respawn all to prevent camping
    end
  end

  for k, v in pairs(elimination.players) do
    if !table.HasValue(elimination.hunters, k) then
      table.insert(elimination.runners, k)
      elimination.players[k] = 2
      --k:TrailColor(elimination.colors[2])
      lobby:SetTeam(k, elimination.colors[2], elimination.colors[1])
      k:UpdateTrail()
      k:Spawn() -- respawn all to prevent camping
    end

  end
  notifyPly(elimination.runners, "Elimination You're now a runner // Your objective is to avoid getting caught by the hunters", "normal", 3, {
    [1] = elimination.colors[1],
    [5] = elimination.colors[2],
    [15] = elimination.colors[1]
  })
  notifyPly(elimination.hunters, "Elimination You're now a hunter // Your objective is to catch all the runners", "normal", 3, {
    [1] = elimination.colors[1],
    [5] = elimination.colors[1],
    [13] = elimination.colors[2]
  })
  elimination.state = "active" -- set the state to avoid accidental restarts
end

function elimination.restartGame()
  table.Empty(elimination.hunters)
  table.Empty(elimination.runners)
  elimination.state = "inactive"
end

function elimination.generateHunter()
  local val, hunters = table.Random(elimination.players)
    if table.HasValue(elimination.hunters, hunters) then

      return elimination.generateHunter()
    end
  return hunters
end

function elimination.died()
  for _, ply in pairs(elimination.runners) do
    if !ply:Alive() then
      elimination.players[ply] = 1
      --ply:TrailColor(elimination.colors[1])
      lobby:SetTeam(ply, elimination.colors[1], elimination.colors[2])
      table.RemoveByValue(elimination.runners, ply)
      table.insert(elimination.hunters, ply)
      notifyPly(ply, "Elimination You died and is now a hunter", "normal", 1.5, {
        [1] = elimination.colors[1],
        [8] = elimination.colors[1]
      })
    end
  end
end

function elimination.tagged(ply, tagger)
  if elimination.isRunner(ply) and !elimination.isRunner(tagger) then
    elimination.players[ply] = 1
    --ply:TrailColor(elimination.colors[1])
    lobby:SetTeam(ply, elimination.colors[1], elimination.colors[2])
    table.RemoveByValue(elimination.runners, ply)
    table.insert(elimination.hunters, ply)
    notifyPly(elimination.players, "Elimination " .. ply:Nick() .." was caught by ".. tagger:Nick()..".", "normal", 1.5, {
      [1] = elimination.colors[1],
      [2] = elimination.colors[1],
      [6] = elimination.colors[1]
    })
  elseif elimination.isRunner(ply) and tagger == "leave" then
    elimination.players[ply] = 1
    --ply:TrailColor(elimination.colors[1])
    lobby:SetTeam(ply, elimination.colors[1], elimination.colors[2])
    table.RemoveByValue(elimination.runners, ply)
    notifyPly(elimination.players, "Elimination " .. ply:Nick() .." left.", "normal", 1.5, {
      [1] = elimination.colors[1],
      [2] = elimination.colors[1]
    })
  end
end

function elimination.Think()

  if table.Count(elimination.players) >= 3 and elimination.state == "inactive" then
    elimination.startGame()
  elseif table.Count(elimination.players) < 3 then
    elimination.state = "inactive"
  end

  if table.Count(elimination.players) == #elimination.hunters then

    elimination.state = "inactive"
  end

  if elimination.state == "inactive" then return end

  if table.Count(elimination.players) <= 2 then
    elimination.state = "inactive"
    elimination.runners = {}
    elimination.hunters = {}
  end
  elimination.died()

  for _, ply in pairs(elimination.runners) do
    local runnerHitBox = ents.FindInSphere(ply:GetPos(), elimination.hitBox)
  	for _, ent in pairs(runnerHitBox) do
  		if (ent:IsPlayer() ) and ent:Alive() and ent != ply and table.HasValue(elimination.hunters, ent) then

        elimination.tagged(ply, ent)
  		end
  	end
  end
end

return elimination
