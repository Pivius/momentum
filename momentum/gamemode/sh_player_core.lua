local PLAYER = FindMetaTable( "Player" )

if SERVER then
  util.AddNetworkString( "Settings_Grab" )
end

if CLIENT then
  function grabVar(string, ply)

    net.Start("Settings_Grab")
      net.WriteString(string)
      net.WriteEntity(ply)
      net.WriteEntity(LocalPlayer())
    net.SendToServer()
  end
  net.Receive("Settings_Grab", function()
    local func = net.ReadString()
    local result = net.ReadType()
    local target = net.ReadEntity()
    getmetatable(target)[func](target, result)
    --getmetatable(target)[func](result)
  end)
else
  net.Receive("Settings_Grab", function()
    local str = net.ReadString()
    local target = net.ReadEntity()
    local ply = net.ReadEntity()

    net.Start("Settings_Grab")
      net.WriteString(str)
      net.WriteType( target.Settings[str] )
      net.WriteEntity(target)
    net.Send(ply)
  end)
end

function GetDate()
  local Timestamp = os.time()
  local TimeString = os.date( "%d/%m/%Y" , Timestamp )
  return TimeString
end

/*
  NAME - AddSettings
  FUNCTION - Get or set values
    GET: player:name()
    SET: player:name(var)
*/
function PLAYER:AddSettings(name, default, ignoreError)
  if UT:TblgetKey(self.Settings, name) then

    if !ignoreError then
      UT:ErrorWarning('Setting "' .. name ..  '" already exists')
    end
  else
    self.Settings[name] = default
    self.Default[name] = default

    local copy = getmetatable(name)
    local createFunc = {}
    createFunc  = {
    	[name] = function( self, var )
        if !var and !isbool(var) then

          return self.Settings[name]
        elseif var or isbool(var) then

          self.Settings[name] = var
        end
    	end
    }
    table.Merge(copy, createFunc)

  	createFunc.__index = copy

    setmetatable( getmetatable(self), createFunc )
  end
end

function PLAYER:createSettings()
  //Movement
  self:AddSettings("Accel", 10)         -- accelerate
  self:AddSettings("LeftGround", false)

  self:AddSettings("AirAccel", 10)          -- Air accelerate
  self:AddSettings("AirStopAccel", 1)       -- Air Stop accelerate
  self:AddSettings("AirControl", 0)         -- Air Control
  self:AddSettings("StrafeAccelerate", 5)   -- A/D Strafe Accelerate
  self:AddSettings("WishSpeed", 400)        -- Wish Speed

  self:AddSettings("AutoHop", false)        -- AutoBhop
  self:AddSettings("Gain", 15)              -- Air Gain
  self:AddSettings("Friction", 6)           -- Friction
  self:AddSettings("Sliding", false)        -- Is sliding on ramp
  self:AddSettings("GroundSlanted", false)  -- Checks for slanted ground
  self:AddSettings("GroundTicks", 0)        -- Ticks spent on ground
  self:AddSettings("SlideNormal", 0.75)     -- How steep a ramp can be before you're able to surf on it
  self:AddSettings("SlideTime", 0)

  //Hops
  --self:AddSettings("Hops", 0)
  --self:AddSettings("HopTimer", 0)
  self:AddSettings("CanHop", false)
  self:AddSettings("DoHop", false)
  self:AddSettings("JumpHeld", false)
  self:AddSettings("ChargeTime", 0)
  self:AddSettings("DisCharge", 0)
  self:AddSettings("Hops", 0)
  self:AddSettings("SuperJumps", 0)
  self:AddSettings("Charge", 0)
  //Spectate
  self:AddSettings("CanSpec", true)     -- Determines if the player can spectate or not.
  //Lobby
  self:AddSettings("InLobby", false)

  //RocketJump
  self:AddSettings("CanFire", true)
  self:AddSettings("Rockets", {})
  self:AddSettings("RocketDelay", 0)
  self:AddSettings("FiredRockets", 0)
  self:AddSettings("RocketSpeed", 1000)
  self:AddSettings("BlastRadius", 120)

  //WallJumping
  self:AddSettings("RunningUp", false)
  self:AddSettings("WallJump", false)
  self:AddSettings("RunUps", 0)
  self:AddSettings("MaxRunUps", 2)

  //Grapple
  self:AddSettings("GrapplePos", Vector(0,0,0)) -- The Position of the grappling hook
  self:AddSettings("TracePos", Vector(0,0,0))   -- The grapple trace. Mostly required for my syncing
  self:AddSettings("AllowGrapple", true)        -- Decides if the player can grapple
  self:AddSettings("Grappling", false)          -- Is the player grappling or not
  self:AddSettings("Retracting", false)         -- Is the player retracting or not
  self:AddSettings("RetractSpeed", 150)         -- The speed the player retracts at
  self:AddSettings("RopeTension", 0)            -- Rope tension
  self:AddSettings("RopeLength", 0)             -- Rope length
  self:AddSettings("GrappleSync", {})           -- Sync table
  self:AddSettings("Intersections", {})        -- Intersecting rope

  //Stats
  self:AddSettings("TSS", 0) --Topspeed Session
  self:AddSettings("TSA", 0) --Topspeed AllTime
  self:AddSettings("Combo", {})
  self:AddSettings("Points", 0)
  self:AddSettings("ComboTimer", 0)
  self:AddSettings("ComboReset", 5)
  self:AddSettings("Combo_AirTime", 0)
  self:AddSettings("Combo_Pogo", false)
  self:AddSettings("StrafePts", 0)
  self:AddSettings("StrafePtsTot", 0)
  self:AddSettings("AirTime", 0)
  self:AddSettings("StrafeTime", 0)
  //Trick
  self:AddSettings("Editing", false) -- Is editing

  //Utility
  self:AddSettings("Noclip", false)         -- Noclip
end

function PLAYER:Speed()
  return self:GetVelocity():Length()/10
end

function PLAYER:Speed2D()
  return self:GetVelocity():Length2D()--/10
end

function PLAYER:SpeedZ()
  return math.abs(self:GetVelocity().z/10)
end

function PLAYER:Topspeed()
  if self:Speed2D() <= 100 then return end
  if self:Speed2D() > self:TSS() then
    self:TSS(self:Speed2D())

    if self:Speed2D() > self:TSA() then
      self:TSA(self:Speed2D())
      timer.Create("Top Delay " .. self:SteamID(), 5, 1, function()
        if SERVER then
          updateRank( self )
        end
      end)

    end
  end
end

//
//  Hooks
//

hook.Add('PlayerTick', 'UpdateVel', function(ply, mv)

  if ply:Alive() and !(ply:GetMoveType() == 8 or ply:GetMoveType() == 4 or ply:Noclip()) and !ply:IsBot() then
    ply:Topspeed()
  end

end)

hook.Add('PlayerDeathThink', 'Player_Death_Think', function(ply)
  if ( ply.NextSpawnTime && ply.NextSpawnTime > CurTime() ) then return end
  if ( ply:IsBot() || ply:KeyPressed( IN_ATTACK ) || ply:KeyPressed( IN_ATTACK2 ) || ply:KeyPressed( IN_JUMP ) ) then

  	ply:Spawn()

  end
end)
hook.Add("OnEntityCreated","InitPlayerSpawn",function(ply)
  local plMeta = getmetatable(ply)
	if plMeta!=FindMetaTable("Player") then
    return
  end
  ply.Default = {}
  ply.Settings = {}
  ply:createSettings()

  if SERVER then
    stats:updatePlayers()
    ply.MakeSettings = true
    ply.lockBot = false
    ply:UnLock()

  	ply:TrailLife(125)
  	ply:TrailAdditive(true)
  	ply:TrailSize(4, 0)
  	ply:TrailRes(0.01)
    stats:loadStats()
  	ply:StartFade(2)
  	ply:TrailMat('trick/trails/beam_generic_white.vmt')--trick/test.vmt
  	--ply:TrailMat('trick/test.vmt')--trick/test.vmt
	   ply:SetCanWalk( false )
     if !ply.rope then
       ply.rope = ents.Create("rope")
       ply.rope:SetParent(ply)
       ply.rope:SetPos(ply:GetPos())
       ply.rope:Spawn()
     end
   end
end)

//Y NO WORK
/*
hook.Add('PlayerInitialSpawn', 'Player_Init_Spawn', function(ply)
  ply.MakeSettings = true

  ply:UnLock()
  ply:TrailColor(Color(255,255,255))
	ply:TrailLife(275)
	ply:TrailAdditive(true)
	ply:TrailSize(4, 0)
	ply:TrailRes(0.01)

	ply:StartFade(2)
	ply:TrailMat('trick/trails/beam_generic_white.vmt')--trick/test.vmt
	--ply:TrailMat('trick/test.vmt')--trick/test.vmt
	ply:SetCanWalk( false )

end)
*/
hook.Add('PlayerSpawn', 'Player_Spawn', function( ply )
  ply:CreateTrail()
end)

hook.Add('PlayerDeath', 'Player_Death', function ( ply )

	ply:RemoveTrail()
end)

hook.Add('PlayerDisconnected', 'Player_Disc', function( ply )
	ply:RemoveTrail()
end)

hook.Add('OnPlayerHitGround', 'TouchedGround', function( ply, water, floater, speed )

  ply:LeftGround(false)
  ply:Combo_Pogo(false)
end)

hook.Add('PlayerLeftGround', 'LeftGround', function( ply )
  ply:LeftGround(true)

  ply:Combo_Pogo(true)

end)

hook.Add("SetupMove", "Combos", function(ply)
  if !IsFirstTimePredicted() then return end
  if !ply:OnGround() then
    hookCall("Combo_AirTime", ply)
  else

    ply:Combo_AirTime(CurTime()+ply:ComboReset()-1)
  end
end)

hook.Add("SetupMove", "Movement", function(ply, mv, cmd)
  if ply:Noclip() || ply:GetMoveType() == MOVETYPE_NOCLIP then return end
  if ply:KeyReleased(IN_JUMP) then
    ply:JumpHeld(true)
  elseif ply:KeyPressed(IN_JUMP) then
    ply:JumpHeld(false)
  end

  ply:AirAccel(1)
  ply:AirStopAccel(1)
	ply:AirControl(150) -- 150
	ply:StrafeAccelerate(1)
	ply:WishSpeed(320)


  //Buttsliding
  if mv:KeyDown(IN_DUCK) and ply:OnGround() and mv:GetVelocity():Length2D() > 1 then
    ply:Friction(0.25)


    if mv:KeyDown(IN_ATTACK2) then

      ply:Accel(2)
      ply:ChargeHop()
      ply:FrictionDecay(false)
      if mv:KeyPressed(IN_JUMP ) and ply:Charge() >= 1 then
        print(Lerp(ply:Charge()/3, 0.5, 2))
        ply:DisCharge(CurTime()+Lerp(ply:Charge()/3, 0.5, 2))
        ply:SuperJumps(ply:SuperJumps()+1)
        Combo:AddTo(ply, "Charged Jump S" .. ply:Charge())
      end
    else
      if (ply:DisCharge() <= CurTime() and ply:Charge() >= 1 ) or ply:SuperJumps() >= 2 then
        if ply:Charge() != 0 and SERVER then
          ply:EmitSound("player/suit_denydevice.wav", 500, 200)
        end
        ply:Charge(0)
        ply:SetJumpPower(220)
        ply:SuperJumps(0)
      end
      ply:Friction(0.5)
      ply:FrictionDecay(true)
      ply:Accel(10)
      ply:SetJumpPower(220)
    end

  else
    if (ply:DisCharge() <= CurTime() and ply:Charge() >= 1 ) or ply:SuperJumps() >= 2 then
      if ply:Charge() != 0 and SERVER then
        ply:EmitSound("player/suit_denydevice.wav", 500, 200)
      end
      ply:Charge(0)
      ply:SetJumpPower(220)
      ply:SuperJumps(0)
    end
    ply:FrictionDecay(false)
    ply:ChargeTime(CurTime()+0.35)
    ply:Friction(ply.Default["Friction"])
    ply:Accel(ply.Default["Accel"])
  end
  Bhop(ply,mv, cmd)


  if ply:GetGroundEntity() != NULL then
    if mv:KeyPressed(IN_JUMP) and !mv:KeyWasDown( IN_JUMP ) then -- Allow bhop
      return
    end
    Friction( ply, mv, cmd, ply:Friction() )
    Accelerate( ply, mv, cmd, ply:Accel() )

    PM_StepSlideMove( ply, mv, cmd, false ) // Divide by 800 due to that being default
  else
    PM_AirMove( ply, mv, cmd )
    PM_StepSlideMove( ply, mv, cmd, GetConVar( "sv_gravity" ):GetFloat()/800 ) // Divide by 800 due to that being default
  end
  Step(ply)
  --ply:SetStepSize( 64 )
  --ply.impactSpeed = mv:GetVelocity()
  RampSlide.Slide(ply, mv, cmd)
end)

--Player Regeneration
function Regen()
  local delay = 1
  for _,pl in pairs (player.GetAll()) do
    if pl:Crouching() and pl:OnGround() and !pl:KeyDown(IN_FORWARD) and !pl:KeyDown(IN_BACK) and !pl:KeyDown(IN_MOVELEFT) and !pl:KeyDown(IN_MOVERIGHT) then
      delay = 0.3
    end
    if CurTime() > (Lastthink or 0) + delay then
      Lastthink = CurTime()
      local hp = pl:Health()
      if hp < 100 then
        if (pl.LastHit or 0) + 5 < CurTime() and pl:Alive() then
          pl:SetHealth(hp+1)
        end
      end
      if hp < 0 then
        pl:SetHealth(0)
      end
    end
  end
end
hook.Add("Think", "Regeneration", Regen)
