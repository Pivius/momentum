/*
  NAME - createrank
  FUNCTION - Creates a rank using cmd
*/
Admin.Commands:createCMD("createrank", "caller ; rank ; inheritance ; cmds ; power ", function(caller, rank, inherit, cmds, power)
	-- Check if player has chosen a rank name
	if !rank then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You forgot to name the rank!")
		return
	end
	-- Checks if rank already exists
	if Admin:GetRank(rank) then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "The rank " .. rank .. " already exists!")
		return
	end
	-- Check inheritance, cmds and power
	if !inherit then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "It should probably inherit from somewhere!")
		inherit = "user"
	end

	if !cmds then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You should probably add some commands!")
		cmds = ""
	end

	if !power then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You should probably give it a power!")
		power = 1
	end
	-- Display the rank
	chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "Created " .. rank)
	chat.Text(caller, Admin.Colors["VGAM"], "[".. rank .."] ", Admin.Colors["normal"], "Inheritance: " .. inherit)
	chat.Text(caller, Admin.Colors["normal"],"Commands: " .. cmds)
	chat.Text(caller, Admin.Colors["normal"],"Power: " .. power)
	cmds = string.Explode( ",", cmds )
	-- Add the rank
	Admin:AddRank(rank, inherit, cmds, power)

end)

/*
  NAME - removerank
  FUNCTION - Removes rank using cmd
*/
Admin.Commands:createCMD("removerank", "caller ; rank ; move", function(caller, rank, move)
	-- Check if player has chosen a rank
	if !rank then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You forgot the rank!")
		return
	end
	-- Check if the player want to move the players
	if !move then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You didn't choose where to move the players, being moved to user.")
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "Removed " .. rank)
		Admin:RemoveRank(rank, "user")
		return
	end
	-- Check if misspelled rank
	if !Admin:GetRank(move) then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You can't move the players to " .. move .. ", being moved to user.")
		Admin:RemoveRank(rank, "user")
		return
	end
	-- Display removed rank
	chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "Removed " .. rank)
	--Remove rank
	Admin:RemoveRank(rank, move)
end)

/*
  NAME - editrank
  FUNCTION - Edits rank using cmd
*/
Admin.Commands:createCMD("editrank", "caller ; rank ; newname ; inheritance ; cmds ; power", function(caller, rank, name, inherit, cmds, power)
	-- Check if player has chosen a rank
	if !rank then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You forgot to select a rank!")
		return
	end
	-- Check if player has chosen a new name
	if !name then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You forgot to choose a new name!")
		return
	end
	-- Check if the rank exists
	if !Admin:GetRank(rank) then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "The rank " .. rank .. " does not exist!")
		return
	end
	-- Check inheritance, cmds and power
	if !inherit then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "It should probably inherit from somewhere!")
		inherit = name
	end

	if !cmds then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You should probably add some commands!")
		cmds = ""
	end

	if !power then
		chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You should probably give it a power!")
		power = 1
	end
	-- Display edited rank
	chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "Edited " .. rank)
	chat.Text(caller, Admin.Colors["VGAM"], "[".. name .."] ", Admin.Colors["normal"], "Inheritance: " .. inherit)
	chat.Text(caller, Admin.Colors["normal"],"Commands: " .. cmds)
	chat.Text(caller, Admin.Colors["normal"],"Power: " .. power)
	cmds = string.Explode( ",", cmds )
	-- Edit rank
	Admin:EditRank(rank, name, inherit, cmds, power)
end)

/*
  NAME - getranks
  FUNCTION - Get all ranks
*/
Admin.Commands:createCMD("getranks", "caller", function(caller)
	chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "Ranks:")

	for k ,v in pairs(Admin.Ranks) do
		chat.Text(caller, Admin.Colors["VGAM"], "[".. k .."] ", Admin.Colors["normal"], "Inheritance: " .. v["inheritance"])
		chat.Text(caller, Admin.Colors["normal"],"Commands: " .. table.concat( v["cmds"], ", " ))
		chat.Text(caller, Admin.Colors["normal"],"Power: " .. v["power"])
	end
end)
