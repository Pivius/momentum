
Admin.Commands:createCMD("resettop", "caller ; user", function(caller, user)
  if !user or user == "" then
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "Please enter a steamID or nick.")
    return
  end
  local nick = Admin.Commands:autoComplete( user )
  if nick == nil then
    if string.Split( string.lower(user), ":" )[1] == user then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "This is not a target!")
      return
    end
    if !Admin:IsHigherThan( caller, Admin.Player:GetRank(user) ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You can't target this rank!")
      return
    end
    local sid = string.Split( string.lower(user), ":" )
    if string.match(sid[1], "steam_") and isnumber(util.StringToType( sid[2], "Float" )) and isnumber(util.StringToType( sid[2], "Float" )) then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], 'You reset ', Admin.Colors["VGAM"], user, Admin.Colors["normal"], "'s topspeed.")
      stats:Remove(user)
    else
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You entered an invalid steamID or Nick.")
    end
  else
    nick = string.gsub(nick, '"', "")
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], 'You reset ', Admin.Colors["VGAM"], nick, Admin.Colors["normal"], "'s topspeed.")
    plyFromNick(nick):resetTop()
  end

end)

Admin.Commands:createCMD("noclip", "caller ; target", function(caller, target)

	if target then
    if !Admin:IsHigherThan( caller, Admin.Player:GetRank(target:SteamID()) ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You can't target this user!")
      return
    end

    if target:Noclip() then
      target:Noclip(false)
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You have disabled ", Admin.Colors["VGAM"], target:Nick(), Admin.Colors["normal"], "'s noclip.")
      chat.Text(target, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Noclip off.")
    else
      target:Noclip(true)
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You have enabled ", Admin.Colors["VGAM"], target:Nick(), Admin.Colors["normal"], "'s noclip.")
			chat.Text(target, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Noclip on.")
    end

  else
    if caller:Noclip() then
      caller:Noclip(false)
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Noclip off.")
    else
      caller:Noclip(true)
			chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Noclip on.")
    end
	end
end)


Admin.Commands:createCMD("trick", "caller ; target", function(caller, target)
	if target then
    if !Admin:IsHigherThan( caller, Admin.Player:GetRank(target:SteamID()) ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You can't target this user!")
      return
    end

    if target:Editing() then
      target:Editing(false)
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You have disabled editing for ", Admin.Colors["VGAM"], target:Nick())
      chat.Text(target, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Editing off.")
    else
      target:Editing(true)
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You have enabled editing for ", Admin.Colors["VGAM"], target:Nick())
			chat.Text(target, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Editing on.")
    end

  else
    if caller:Editing() then
      caller:Editing(false)
      caller:SetNWBool("Editing", false)
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Editing off.")
    else
      caller:Editing(true)
      caller:SetNWBool("Editing", true)
			chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], "Editing on.")
    end
	end
end)
