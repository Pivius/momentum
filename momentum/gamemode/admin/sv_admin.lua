local meta = FindMetaTable( "Player" )
Admin = {} or Admin
Admin.Player = {} or Admin.Player
Admin.Ranks = {}
Admin.players = {}

--- GLOBAL

---
/*
  NAME - Initialize
  FUNCTION - Initalizes the Admin Module
*/
function Admin:Init()
  Admin:LoadRanks()
  Admin.Commands:LoadCMD()
end

--TODO Add flags
/*
 - Most used colors of the admin mod
*/
Admin.Colors = {["error"] = Color(255,75,0, 255),
["normal"] = Color(255,255,255, 255),
["VGAM"]   = Color(0,75,255,255)
}

/*
  NAME - Insert
  FUNCTION - Inserts values to a mysql table
*/
function Admin:Insert(sID, name, rank, flags)
	local query = mysql:Insert(SQLDetails.tables.users)
  query:Insert("sID", sID)
  query:Insert("name", name)
  query:Insert("rank", rank)
  query:Insert("flags", flags)
	query:Execute()
end

/*
  NAME - Update
  FUNCTION - Updates a mysql table
*/
function Admin:Update(sID, name, rank, flags)
	local query = mysql:Update(SQLDetails.tables.users)

  query:Where("sID", sID)
  query:Update("name", name)
  query:Update("rank", rank)
  query:Update("flags", flags)
	query:Execute()
end

/*
  NAME - Remove
  FUNCTION - Removes a mysql table
*/
function Admin:Remove(sID)
	local query = mysql:Delete(SQLDetails.tables.users)
		query:Where("sID", sID)
	query:Execute()
end

/*
  NAME - AddRank
  FUNCTION - Creates a rank
*/
function Admin:AddRank(name, inheritance, cmds, power)
  cmds = table.concat( cmds, ", " )
  Admin.Ranks[name] = {inheritance = inheritance, cmds = cmds, power = power}

  local sql = mysql:Insert(SQLDetails.tables.ranks)
		sql:Insert("name", name)
		sql:Insert("inheritance", inheritance)
		sql:Insert("cmds", cmds)
    sql:Insert("power", power)
	  sql:Execute()
end

/*
  NAME - EditRank
  FUNCTION - Edits an already existing rank
*/
function Admin:EditRank(rank, name, inheritance, cmds, power)
  if name == "nil" then
    name = rank
  end
  local oldinheritance = Admin.Ranks[rank]["inheritance"]
  local oldcmds = Admin.Ranks[rank]["cmds"]
  local oldpower = Admin.Ranks[rank]["power"]
  table.RemoveByValue( Admin.Ranks, rank )
  if inheritance == "keep" or !inheritance then
    inheritance = oldinheritance
  elseif cmds == {"keep"} or !cmds then
    cmds = oldcmds
  elseif power == "keep" or !power then
    power = oldpower
  end
  cmds = table.concat( cmds, ", " )
  Admin.Ranks[name] = {inheritance = inheritance, cmds = cmds, power = power}

  local sql = mysql:Update(SQLDetails.tables.ranks)
		sql:Where("name", rank)
    sql:Update("name", name)
		sql:Update("inheritance", inheritance)
		sql:Update("cmds", cmds)
    sql:Update("power", power)
	  sql:Execute()
end

/*
  NAME - AddCmd
  FUNCTION - Edits an already existing rank
*/
function Admin:AddCmd(rank, cmd, retry)

  if Admin:GetRank(rank) == false then
    print("Rank ".. rank .." doesn't exist!")
    if !retry then
      print("Retrying")
      timer.Simple(1, function()
        Admin:AddCmd(rank, cmd, true)
      end )
    end
    return
  end
  local cmds = {}
  table.CopyFromTo( Admin.Ranks[rank]["cmds"], cmds )
  table.insert(cmds, cmd)
  Admin.Ranks[rank]["cmds"] = cmds
  --table.RemoveByValue( Admin.Ranks, rank )
  cmds = table.concat( cmds, ", " )

  local sql = mysql:Update(SQLDetails.tables.ranks)
		sql:Where("name", rank)
		sql:Update("cmds", cmds)
	  sql:Execute()
end

/*
  NAME - RemoveRank
  FUNCTION - Duh!
*/
function Admin:RemoveRank(rank, move)
  local query = mysql:Select(SQLDetails.tables.users)
    query:Callback(function(result, status, lastID)
      if (type(result) == "table" and #result > 0) then
        for k, v in pairs(result) do
          if v["rank"] == rank then
            local name = v["name"]
            local sID  = v["sID"]
            if move then
              Admin.Player:SetRank(sID, move)
            else
              Admin.Player:SetRank(sID, "user")
            end
          end
        end
			end
		end)
  query:Execute()

  local query2 = mysql:Delete(SQLDetails.tables.ranks)
		query2:Where("name", rank)
	query2:Execute()
  UT:RemoveFromTable(Admin.Ranks,rank)

end

/*
  NAME - LoadRanks
  FUNCTION - Loads ranks from a mysql table
*/
function Admin:LoadRanks()
  local query = mysql:Select(SQLDetails.tables.ranks)
    query:Callback(function(result, status, lastID)
      if (type(result) == "table" and #result > 0) then
        for k, v in pairs(result) do
          local name = v["name"]
          local inheritance = v["inheritance"]
          local cmds = v["cmds"]
          local power = v["power"]
          print(cmds)
          cmds = string.gsub( cmds, ",", "" )
          cmds = string.Explode( " ", cmds )

          local rnktbl = {
            [name] = {
              ["inheritance"] = inheritance,
              ["cmds"] = cmds,
              ["power"] = power
            }
          }
          UT:MergeTableTo(Admin.Ranks, rnktbl)
        end

			end
		end)

  query:Execute()

end

/*
  NAME - meta:GetRank
  FUNCTION - Gets the rank of chosen player
*/
function meta:GetRank()
  local sID = self:SteamID()
  if !Admin.players[sID] then return end
  local rnk = Admin.players[sID]["rank"]
  return rnk

end

/*
  NAME - GetRank
  FUNCTION - Returns the rank if the rank exists
*/
function Admin:GetRank(name)
  if !Admin.Ranks[name] then return false end
  return Admin.Ranks[name]
end

/*
  NAME - GetRank
  FUNCTION - Gets the player rank
*/

function Admin.Player:GetRank(sID)
  if !sID then return end

  local query = mysql:Select(SQLDetails.tables.users)
    query:Where("sID", sID)
    query:Callback(function(res, status, id)
    if type(res) == "table" and #res > 0 then
      gotrank = res[1]["rank"]
    end
  end)
  query:Execute()
  return gotrank
end

/*
  NAME - isHigherThan
  FUNCTION - Check to see if player power is higher than required
*/
function Admin:IsHigherThan( ply, required )
  if ply == "root" then
    return true
  end
  local plyrank = ply:GetRank()
  if !Admin.Ranks[required] or !Admin.Ranks[plyrank] then return end
  local power   = Admin.Ranks[plyrank]["power"]
  required      = Admin.Ranks[required]["power"]
  return (tonumber(power) >= tonumber(required))

end

/*
  NAME - GetOnlineAdmins
  FUNCTION - Gets all the online admins
*/
function Admin:GetOnlineAdmins()
	local tab = {}

	for _,ply in pairs( player.GetAll() ) do
		if Admin:IsHigherThan( ply, 2 ) then
			table.insert( tab, ply )
		end
	end

	return tab
end

/*
  NAME - GetPlayerFrom
  FUNCTION - Gets all players from a rank
*/
function Admin:GetPlayerFrom(power)
	local tab = {}

	for _,ply in pairs( player.GetAll() ) do
		if Admin:IsHigherThan( ply, power ) then
			table.insert( tab, ply )
		end
	end

	return tab
end

/*
  NAME - GetPlayerFrom
  FUNCTION - Gets all players from a rank
*/
function Admin.Player:GetTarget(caller, target)
  local ply = nil
  if !target then
    return caller
  elseif !caller || type( target ) != "string" || tonumber(target) then
    return nil
  end

	if Admin.Player:GetBySteamID( target ) then
    ply =  Admin.Player:GetBySteamID( target )
  elseif type( target ) == "string" then
    ply = plyFromNick(string.gsub(Admin.Commands:autoComplete( target ), '"', ""))
  end

  if ply != caller then
    if !Admin:IsHigherThan( caller, ply:GetRank() ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You can't target this rank!")
      ply = nil
    end
  end

  return ply
end

/*
  NAME - GetBySteamID
  FUNCTION - Gets the player by the steam id
*/
function Admin.Player:GetBySteamID(sID)
  for _,ply in ipairs( player.GetAll() ) do
    if ply:SteamID() == sID then
      return ply
    end
  end
  return nil
end

/*
  NAME - SetRank
  FUNCTION - Sets the player rank
*/
function Admin.Player:SetRank(sID, rank)
  if !Admin:GetRank(rank) or !rank then return end
  local query = mysql:Update(SQLDetails.tables.users)
    query:Where("sID", sID)
    query:Update("rank", rank)
  query:Execute()
end

-- META

/*
  NAME - SteamIDTo64
  FUNCTION - Transforms a SteamID to a steam64id
*/
function meta:SteamIDTo64()
  return util.SteamIDTo64(self:SteamID())
end

/*
  NAME - AddFlags
  FUNCTION - Adds flags to a player
*/
function meta:AddFlags(flags)
  local sID = self:SteamID()
  local name = self:Nick()
  Admin.players[self:SteamID()][flags] = flags
end

/*
  NAME - GetFlags
  FUNCTION - Gets the flags of a player
*/
function meta:GetFlags()
  if Admin.players[self:SteamID()][flags] == nil then
    return {}
  else
    return Admin.players[self:SteamID()][flags]
  end
end

/*
  NAME - meta:SetRank
  FUNCTION - Sets the rank of chosen player
*/
function meta:SetRank(rank)
  if !Admin:GetRank(rank) then return end
  local steamID = self:SteamID()
  local name = self:Nick()
  Admin.players[steamID]["rank"] = rank
  Admin:Update(steamID, name, rank, {})
end

/*
  NAME - sRemove
  FUNCTION - Removes a table by steamid
*/
function sIDRemove(sID)
  local flags = {}
  local users
  local query = mysql:Select(SQLDetails.tables.users)
    query:Where("sID", sID)
    query:Callback(function(res, status, id)

    if type(res) == "table" and #res > 0 then
      Admin:Remove(sID)
      UT:RemoveByKey(Admin.players, sID)
    end
  end)
  query:Execute()
end

/*
  NAME - plyRemove
  FUNCTION - Removes a table by user nick
*/
function plyRemove(nick)
  local flags = {}
  local users
  local query = mysql:Select(SQLDetails.tables.users)
    query:Where("name", nick)
    query:Callback(function(res, status, id)

    if type(res) == "table" and #res > 0 then
      local query = mysql:Delete(SQLDetails.tables.users)
        query:Where("name", nick)
      query:Execute()
      UT:RemoveByKey(Admin.players, res[1]["sID"])
    end
  end)
  query:Execute()

end

/*
  NAME - plyUpdate
  FUNCTION - Update table with given arguments
*/
function plyUpdate( sID, rank )
  local flags = {}
  local users
  local query = mysql:Select(SQLDetails.tables.users)
    query:Where("sID", sID)
    query:Callback(function(res, status, id)

    if type(res) == "table" and #res > 0 then
      Admin:Update(sID, res[1]["name"], rank, flags)
      users = {
        [sID] = {
          ["name"] = res[1]["name"],
          ["flags"] = flags,
          ["rank"] = rank
        }
      }
    else
      Admin:Insert(sID, "unknown", rank, {})
      users = {
        [sID] = {
          ["name"] = "unknown",
          ["flags"] = flags,
          ["rank"] = rank
        }
      }
    end
    UT:MergeTableTo(Admin.players, users)


  end)
  query:Execute()

end

/*
  NAME - UpdateSQL
  FUNCTION - Updates the mysql table when player joins or changes name
*/
function UpdateSQL( ply )
  if !ply:IsPlayer() or ply:IsBot() then return end
  local name = ply:Nick()
  local sID  = ply:SteamID()
  local flags = {}

  local query = mysql:Select(SQLDetails.tables.users)
    query:Where("sID", sID)
    query:Callback(function(res, status, id)
    local rank
    if type(res) == "table" and #res > 0 then
      rank = res[1]["rank"]
      Admin:Update(sID, name, rank, flags)
    else
      Admin:Insert(sID, name, "user", {})
    end
    if rank == "nil" then
      Admin:Update(sID, name, "user", {})
      local users = {
        [sID] = {
          ["name"] = name,
          ["flags"] = flags,
          ["rank"] = "user"
        }
      }
      UT:MergeTableTo(Admin.players, users)
    else
      local users = {
        [sID] = {
          ["name"] = name,
          ["flags"] = flags,
          ["rank"] = rank
        }
      }
      UT:MergeTableTo(Admin.players, users)

    end

  end)
  query:Execute()
end

/*
  NAME - Update on Join
  FUNCTION - Duh!
*/
hook.Add("PlayerInitialSpawn", "Update On Join", function(ply)
  if !ply:IsPlayer() then return end
  UpdateSQL( ply )
end)

/*
  NAME - Update on change
  FUNCTION - Duh!
*/
hook.Add("nameChange", "Updt on change", function(ply)
  if !ply:IsPlayer() then return end
  UpdateSQL( ply )
end)
