util.AddNetworkString( "lobbyNetworking" )
lobby = lobby or {}

lobby.lobbies = lobby.lobbies or {}
lobby.ranked = {}
lobby.defaultSettings = {
		["shift"] = false,
		["time"] = 300,
		["gravity"] = 400,
		["airaccelerate"] = 10,
		["autohop"] = false
}


hook.Add("PlayerInitialSpawn", "Checkranks", function(ply)
	--GetSet("inLobby", "Player")
  net.Start("lobbyNetworking")
    net.WriteString("sync")
    net.WriteTable({lobby:getLobbies()})
  net.Broadcast()
end)

hook.Add("PlayerDisconnected", "Remove player from lobby", function(ply)
	lobby:removePlayerLobby(ply:InLobby(), ply)
end)

function lobby:lobbyName( host, mg )
	if host == "ranked" then
		return "ranked " .. mg .. " lobby"
	end
	if host:IsValid() then
		return host:Nick() .. "'s " .. mg .. " lobby"
	end
end

function lobby:openLobby(host, mg, vars)
		local lbName = self:lobbyName( host, mg )
		local lbID = #lobby.lobbies+1
		local settings
		if !vars then
			settings = lobby.defaultSettings
		else
			settings = vars
		end
    if host:InLobby() then
      lobby:removePlayerLobby(host:InLobby(), host)
    end
    host:InLobby(lbID)
		local tbl = {
			[lbID] = {
				["lobbyName"] = lbName,
				["lobbyHost"] = host,
				["lobbyMode"] = mg,
				["lobbySettings"] = settings,
        ["lobbyPlayers"] = {[1] = host},
			}
		}
		UT:MergeTableTo(lobby.lobbies, tbl)

    hook.Call("createLobby", nil, tbl)

end


function lobby.Think()
  for k, v in pairs(lobby.lobbies) do
   if v["Minigame"] and UT:HasKey(v["Minigame"], "Think") then
     v["Minigame"].Think()
   end
   if #v["lobbyPlayers"] <= 0 then
     lobby:closeLobby(k)
   end

  end
end
hook.Add("Think", "lobby think", lobby.Think)

function lobby:getLobbies()
  local t = {}
  for k, v in pairs( lobby.lobbies ) do

    local tbl = { [k] = {
      ["lobbyName"] = v["lobbyName"],
      ["lobbyHost"] = v["lobbyHost"],
      ["lobbyMode"] = v["lobbyMode"],
      ["lobbySettings"] = v["lobbySettings"],
      ["lobbyPlayers"] = v["lobbyPlayers"],
			["minigamePlayers"] = v["Minigame"].players,
			["minigameColors"] = v["Minigame"].colors,
			["minigameTeams"] = v["Minigame"].teams
    }
    }
    UT:MergeTableTo(t, tbl)

  end
	return t
end

function lobby:prepareLobby(lbID)
	local lb = lobby.lobbies[lbID]
	local tbl = {
	  ["Minigame"] = include("momentum/gamemode/minigames/" .. string.lower(lb["lobbyMode"] .. ".lua"))
	}

	UT:MergeTableTo(lb, tbl)
	lb["Minigame"].settings = lb["lobbySettings"]
	if UT:HasKey(lb["Minigame"], "ReInit") then
 		lb["Minigame"].ReInit()
	end
 	lb["Minigame"].addPlayer(lb["lobbyHost"])
end

hook.Add("createLobby", "Creates the lobbies", function(lb)

  for k, v in pairs(lb) do
    lobby:prepareLobby(k)
    chat.Text(nil, Color(255,255,255), v["lobbyHost"]:Nick() .. " created a ", lobby.minigames[v["lobbyMode"]], tostring(v["lobbyMode"]), Color(255,255,255), " lobby.")
  end

  net.Start("lobbyNetworking")
    net.WriteString("sync")
    net.WriteTable({lobby:getLobbies()})
  net.Broadcast()
  --PrintTable(lobby.lobbies)
end)

function lobby:addPlayerLobby(lobbyID, ply)
	if ply:IsValid() then
		if table.HasValue(lobby.lobbies[lobbyID]["lobbyPlayers"], ply) then return end
		if ply:InLobby() then
			lobby:removePlayerLobby(ply:InLobby(), ply)
		end
	  ply:InLobby(lobbyID)
	  table.insert(lobby.lobbies[lobbyID]["lobbyPlayers"], ply)
	  lobby.lobbies[lobbyID]["Minigame"].addPlayer(ply)
	  chat.Text(nil, Color(255,255,255), ply:Nick() .. " has joined " .. lobby.lobbies[lobbyID]["lobbyHost"]:Nick() .. "'s ", lobby.minigames[lobby.lobbies[lobbyID]["lobbyMode"]], lobby.lobbies[lobbyID]["lobbyMode"], Color(255,255,255), " lobby." )

	  net.Start("lobbyNetworking")
	    net.WriteString("sync")
	    net.WriteTable({lobby:getLobbies()})
	  net.Broadcast()
	end
end

function lobby:removePlayerLobby(lobbyID, ply)
  if ply:InLobby() != false then

		if !lobby.lobbies[lobbyID] then
			ply:InLobby(false)
			return
		end
    lobby.lobbies[lobbyID]["Minigame"].removePlayer(ply)

		table.RemoveByValue(lobby.lobbies[lobbyID]["lobbyPlayers"], ply)

		if lobby.lobbies[lobbyID]["lobbyHost"] == ply and #lobby.lobbies[lobbyID]["lobbyPlayers"] >= 1 then
			lobby.lobbies[lobbyID]["lobbyHost"] = lobby.lobbies[lobbyID]["lobbyPlayers"][1]
			lobby.lobbies[lobbyID]["lobbyName"] = self:lobbyName( lobby.lobbies[lobbyID]["lobbyHost"], lobby.lobbies[lobbyID]["lobbyMode"] )
		end
    net.Start("lobbyNetworking")
      net.WriteString("sync")
      net.WriteTable({lobby:getLobbies()})
    net.Broadcast()
		ply:InLobby(false)
  end
end

function lobby:closeLobby(lobbyID)
	local lbID = lobbyID
  local tbl  = {}
  table.remove(lobby.lobbies, lbID)
  for k, v in pairs(lobby.lobbies) do
    table.insert(tbl, v)
    for _, ply in pairs(v["lobbyPlayers"]) do
      ply:InLobby(k)
    end
  end
  table.CopyFromTo( tbl, lobby.lobbies )

  net.Start("lobbyNetworking")
    net.WriteString("sync")
    net.WriteTable({lobby:getLobbies()})
  net.Broadcast()
end

function lobby:runLobby(lobby)
end

function lobby:pauseLobby(lobby)
end

function lobby:rankedLobby(lobby, ranked)
end

function lobby:timeLimitLobby(lobby, time)
end

function lobby:settingsLobby(lobby, settings)
	local shift = settings["shift"]
	local time = settings["time"]
	local gravity = settings["gravity"]
	local airaccel = settings["airaccelerate"]
end

function lobby:getByID( lbID )
	if lobby.lobbies[ lbID ] then
		return lobby.lobbies[ lbID ]
	else
		return false
	end
end

function lobby:isValidLobby( lbID )
	if lobby.lobbies[ lbID ] then
		return true
	else
		return false
	end
end
