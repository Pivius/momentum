console = {}
console.cooldown = {}
console.cmds = {}
util.AddNetworkString( "Con_Append" )
util.AddNetworkString( "Con_ToConsole" )

function console.CreateCmd(cmd, desc, func)
  console.cmds[cmd] = {["Description"] = desc, ["Function"] = func}
end

function console.GetCmd(cmd)
  if console.cmds[string.lower(cmd)] then
    return true
  end
  return false
end
function console.RunCmd(cmd, ply)
  local func = console.cmds[string.lower(cmd)]["Function"]
  func(ply)
end

function console.Append()
  local ply = net.ReadEntity()
  local str = net.ReadString()
  if IsValid(ply) then
    if str:sub(1, 1) == "/" then
      if console.GetCmd(str:sub(2)) then
        console.RunCmd(str:sub(2), ply)
      else
        console.SendToConsole( ply, {"Could not find the command ", str:sub(2)} )
      end
    else

      if !console.cooldown[ply] then
        
        ply:Say( str, false )
        console.cooldown[ply] = CurTime()+0.5
      end
      if console.cooldown[ply] <= CurTime() then
        ply:Say( str, false )
        console.cooldown[ply] = CurTime()+0.5
      end
    end
  end
end
net.Receive("Con_Append", console.Append)

function console.SendToConsole( ply, tbl )
  net.Start("Con_ToConsole")
    net.WriteTable(tbl)
  net.Send(ply)
end

hook.Add( "OnMsgSent", "Append to console", function(rf, varargs)
  console.SendToConsole( rf, varargs )
end)


// SERVERSIDE COMMANDS
