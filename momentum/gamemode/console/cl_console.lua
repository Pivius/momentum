console = {}
console.open = false
console.cmds = {}
console.restored = {}
console.boldness = 450
console.font = "MavenPro"
console.colors = {
  ["Background"]    = Color(15,15,15,220),
  ["Text"]          = Color(255,255,255,255),
  ["Command"]      = Color(27, 156, 249),
  ["Error"]         = Color(241, 90, 5),
  ["Mention"]       = Color(255,255,255,255),
  ["Notification"]  = Color(202, 235, 133),
}
// Shortcuts
local CC = console.colors
local CC_BG, CC_TXT, CC_CMD, CC_ERR, CC_MENTION, CC_NOTE = CC.Background, CC.Text, CC.Command, CC.Error, CC.Mention, CC.Notification

function console.UpdateColors()
  CC_BG, CC_TXT, CC_CMD, CC_ERR, CC_MENTION, CC_NOTE = CC.Background, CC.Text, CC.Command, CC.Error, CC.Mention, CC.Notification
end

//Store in file
function console.Store()
  if !file.Exists( "gconsole", "DATA" ) then // If not dir exists
    file.CreateDir( "gconsole" )
  end

  file.Write( "gconsole/cvars.txt", "stored console commands // " )

  local commands = console.cmds
  local cvars = {}
  table.sort(commands) //Sorts alphabetically
  for cmds, args in pairs(commands) do
    // Uses "//" to separate convars
    if args["Variable"][1] == nil then
      file.Append( "gconsole/cvars.txt", cmds .. " // ")
    else
      file.Append( "gconsole/cvars.txt", cmds .. " - " .. table.concat(args["Variable"], "/") .. " // ")
    end
  end

end

//Grab stored convars
function console.Grab()
  if !file.Exists( "gconsole", "DATA" ) then
    return
  end

  local cvars = file.Read("gconsole/cvars.txt", "DATA")
  local cmds = {}
  table.Merge(cmds, console.cmds)
  cvars = string.Explode(" // ", cvars)

  table.remove(cvars, 1)
  table.remove(cvars, #cvars)

  for _, convars in pairs(cvars) do

    local cmd, vars = string.Explode(" - ", convars)[1], string.Explode(" - ", convars)[2]
    if vars != nil then

      vars = string.Explode("/", vars)

      UT:RemoveByKey(cmds, cmd)
      if console.CmdExists(cmd) then
        console.RunCmd(cmd, LocalPlayer(), vars)
      end
    else
      UT:RemoveByKey(cmds, cmd)
      if console.CmdExists(cmd) then
        console.RunCmd(cmd, LocalPlayer(), {nil})
      end
    end
  end

  for cmd, convars in pairs(cmds) do
    console.RunCmd(cmd, LocalPlayer(), convars["Default"])
  end
end

function console.CreateCmd(cmd, desc, func, ...)
  args = {...} or {nil}
  console.cmds[cmd] = {["Description"] = desc, ["Function"] = func, ["Variable"] = args, ["Default"] = args}
end

function console.CmdExists(cmd)
  if console.cmds[string.lower(cmd)] then
    return true
  end
  return false
end

function console.CmdVar(cmd)
  if console.cmds[string.lower(cmd)] then
    return unpack(console.cmds[string.lower(cmd)]["Variable"])
  end
  return 0
end

function console.RunCmd(cmd, ply, args)
  local func = console.cmds[string.lower(cmd)]["Function"]
  local retVal = nil
  for _, v in pairs(args) do
    if tonumber(v) then
      args[_] = tonumber(v)
    end
  end

  retVal = func(ply, unpack(args))
  /*
  if tonumber(val) then
    retVal = func(ply, tonumber(val))
  elseif type(val) == "string" then
    retVal = func(ply, tostring(val))
  elseif type(val) == "table" then
    PrintTable(val)
    retVal = func(ply, unpack(val))
  end
  if retVal == nil then
    retVal = func(ply)
  end*/

  console.cmds[string.lower(cmd)]["Variable"] = {retVal}
  if unpack(args) then
    Console:InsertEntry(CC_NOTE, '" ', cmd, '"', " = ", tostring(retVal))
  end
  timer.Simple(0.1, function() console.Store() end)
end

function console.FindCmds(str)
  local tbl = {}
  if str != nil then
    local strng = string.Explode(" ", str)
    for cmd, _ in pairs( console.cmds ) do
      if string.match(cmd, "^"..str) then
        table.insert(tbl, cmd)
      end

    end
    return tbl
  else
    for cmd, _ in pairs( console.cmds ) do
        table.insert(tbl, cmd)
    end
    return tbl
  end
end

function console.AutoComp(cmds, str)
  local tbl = {}
  local match = 99
  local pCmd = ""
  for _, cmd in pairs(cmds) do

    local cmdMatch = 0
    if pCmd == "" then
      pCmd = cmd
    end

    for i=1, #cmd do
      if cmd:sub(i, i) == pCmd:sub(i, i) then
        cmdMatch = cmdMatch+1
      elseif cmd:sub(i, i) != pCmd:sub(i, i) or i == #cmd then
        pCmd = cmd
        match = math.min(cmdMatch, match)
      end
    end
  end
  return pCmd:sub(1, match)
end

local CONSOLE = {}

function CONSOLE:Init()
  self.Console = self:Add( "DFrame" )
  self.Console:SetPos( 0, 0 )
  self.Console:SetSize(ScrW(), ScrH()/2)
  self.Console:SetTitle("")
  self.Console:SetDraggable(false)
  self.Console:ShowCloseButton( false )
  self.Console.Paint = function(self, w, h )
    surface.SetDrawColor( CC_BG )
    surface.DrawRect(0, 0, w, h)
  end

  --TextEntry
  self.TE = self:Add( "DTextEntry" )
  self.TE:SetPos( 0, self.Console:GetTall() +1 )
  self.TE:SetSize( ScrW(), 25 )
  self.TE:SetUpdateOnType( true )
  self.TE.History = {}
  self.TE.HistoryPos = 0
  self.TE.CaretRate  = 0.5
  self.TE.LastCaret  = CurTime()
  self.TE.Caret      = false
  self.TE.Paint = function(self, w, h)
    surface.SetDrawColor( CC_BG )
    surface.DrawRect(0, 0, w, h)
    SetFont("Console", {
      font = console.font,
      weight = console.boldness,
      size = console.CmdVar("console_font_size"),
      antialias = true
    })
    surface.SetTextColor( CC_TXT )
    local tw, th = surface.GetTextSize("|")
    surface.SetTextPos( 5, h/2 - th/2 )
    surface.DrawText( "-" )
  end
  self:UpdateText()
  self.TE.OnValueChange = function(entry, str)
    self:UpdateText()
  end

  self.TE.OnKeyCodeTyped = function(entry, key)
    if key == KEY_ESCAPE then
      console.Hide()
      gui.HideGameUI()
    elseif key == KEY_TAB then
      --print("a")
      if entry:GetValue():sub(1, 1) == "/" then
        --self:InsertEntry(Color(150,150,255,255), '" ', "Command", '"', tostring(var), " Default: ", tostring(console.cmds[cmd]["Default"]), 400*2, " | ", console.cmds[cmd]["Description"]  )
        local commands = console.FindCmds(entry:GetValue():sub(2))
        table.sort(commands)

        local autoComp = console.AutoComp(commands, entry:GetValue():sub(2))
        entry:SetText("/" .. autoComp)
        self:UpdateText()
        SetFont("Console", {
          font = console.font,
          weight = console.boldness,
          size = console.CmdVar("console_font_size"),
          antialias = true
        })
        for _, cmd in pairs(commands) do
          local var = unpack(console.cmds[cmd]["Variable"])

          if console.cmds[cmd]["Variable"][1] != nil then
            if tonumber(var) then

              self:InsertEntry(CC_NOTE, '" ', cmd, ' "', 200, tostring(var), 300, " | ", " Default: ", tostring(unpack(console.cmds[cmd]["Default"])), 300*2, " | ", console.cmds[cmd]["Description"]  )
            elseif type(var) == "string" && !tonumber(var) then
              self:InsertEntry(CC_NOTE, '" ', cmd, ' "', 200, tostring(var), 300, " | ", " Default: ", tostring(unpack(console.cmds[cmd]["Default"])), 300*2, " | ", console.cmds[cmd]["Description"]  )
            end
          else
            self:InsertEntry(CC_NOTE, '" ', cmd, ' "', 300*2, " | ", console.cmds[cmd]["Description"]  )
          end
        end
      end
      timer.Simple(0.001, function()
        entry:RequestFocus()
        entry:SetCaretPos( #entry:GetValue() )
      end)
      -- Auto scroll

        --self.Console.Scroll.offset = (self.Console.Scroll:GetTall())- (#self.Console.Entries*20)
        --self:Scroll((self.Console.Scroll:GetTall())- (#self.Console.Entries*20))

    elseif key == KEY_DOWN then
      if #entry.History <= 0 then return end
      entry.HistoryPos = entry.HistoryPos+1
      if entry.HistoryPos > #entry.History then
        entry.HistoryPos = 1
      end
      entry:SetText(entry.History[entry.HistoryPos])
      entry:SetCaretPos( #entry.History[entry.HistoryPos] )
      self:UpdateText()
    elseif key == KEY_UP then
      if #entry.History <= 0 then return end
      entry.HistoryPos = entry.HistoryPos-1
      if entry.HistoryPos < 1 then
        entry.HistoryPos = #entry.History
      end
      entry:SetText(entry.History[entry.HistoryPos])
      entry:SetCaretPos( #entry.History[entry.HistoryPos] )
      self:UpdateText()
    elseif key == KEY_ENTER then
      local cmd = string.Explode( " ", entry:GetValue() )[1]
      local val = string.Explode( " ", entry:GetValue() )
      if table.HasValue(entry.History, entry:GetValue()) then
        local key = table.KeyFromValue( entry.History, entry:GetValue() )
        table.remove(entry.History,key)
        for i=key, #entry.History do
          entry.History[i] = entry.History[i+1]
        end
      end
      table.insert(entry.History, entry:GetValue())
      if cmd:sub(1, 1) == "/" then
        table.remove(val, 1)
        self:Append(cmd, val)
      else
        self:Append(entry:GetValue())
        console.Hide()
      end
      entry.HistoryPos = 0
    end
  end
  self.TE:RequestFocus()

  self.Console.Scroll = self.Console:Add("DScrollPanel")
  self.Console.Scroll:SetSize( ScrW()-10, (self.Console:GetTall()) - 10  )
  self.Console.Scroll:SetPos(5, 5)

  self.Console.Scroll:SetVerticalScrollbarEnabled(false)

 self.Console.Scroll.VBar:SetAlpha(0)
 self.Console.Scroll.offset = 0

 self.Console.Scroll.OnVScroll = function(self, iOffset)
   self.offset = iOffset


 end
 self.Console.Scroll.SecondThink = function()end
 self.Console.Scroll.ThirdThink = function()end
 self.Console.Scroll.FourthThink = function()end
 self.Console.Scroll.Think = function(self)
   self.SecondThink()
   self.ThirdThink()
   self.FourthThink()
   self.offsetsmooth = self.offsetsmooth and UT:Lerp(0.15, self.offsetsmooth , self.offset) or self.offset

   self.pnlCanvas:SetPos( 0, self.offsetsmooth )
 end

 self.Console.Scroll.PerformLayout = function(self)
   local wide = self:GetWide()

   self:Rebuild()

   self.VBar:SetUp( self:GetTall(), self.pnlCanvas:GetTall() )

   if self.VBar.Enabled then wide = wide - self.VBar:GetWide() end

   self.pnlCanvas:SetWide(wide)

   self:Rebuild()
 end

 self.Console.Entries = {}
 timer.Simple(0.15, function()
    Console:Clear()
    Console:InsertEntry(CC_NOTE, "Do /help for more information.")
    Console:Scroll((Console.Console.Scroll:GetTall()))
  end)
end

function CONSOLE:Append(str, ...)
  local args = ...
  if str:sub(1, 1) == "/" then
    if console.CmdExists(str:sub(2)) then
      local cmd = string.lower(str:sub(2))
      local default = type(console.cmds[cmd]["Default"])
      local var = console.cmds[cmd]["Variable"]

      if args[1] == nil and var[1] != nil then
        Console:InsertEntry(CC_NOTE, '" ', cmd, '"', " = ", tostring(unpack(var)))
        return
      elseif args[1] == "" or args[1] == " " then
        self:InsertEntry(CC_ERR, "You failed to insert a value.")
        return
      end

      console.RunCmd(cmd, ply, args)

    else
      net.Start("Con_Append")
        net.WriteEntity(LocalPlayer())
        net.WriteString(str)
      net.SendToServer()
    end
  else
    net.Start("Con_Append")
      net.WriteEntity(LocalPlayer())
      net.WriteString(str)
    net.SendToServer()
  end

  self.TE:SetText("")

end

function CONSOLE:InsertEntry(...)
  local args = {...}
  if IsValid(self.Console.Scroll) then
    local entry = table.Count(self.Console.Entries)+1
    self.Console.Entries[entry] = self.Console:Add( "DFrame" )
    self.Console.Entries[entry]:Dock( TOP ) -- top
    self.Console.Entries[entry]:DockMargin( 0,0,0,0 )
    self.Console.Entries[entry]:SetSize(ScrW(), console.CmdVar("console_font_size")+2)
    self.Console.Entries[entry]:SetTitle("")
    self.Console.Entries[entry]:SetDraggable(false)
    self.Console.Entries[entry]:ShowCloseButton( false )
    self.Console.Entries[entry].Paint = function(self, w, h )
      SetFont("Console", {
        font = console.font,
        weight = console.boldness,
        size = console.CmdVar("console_font_size"),
        antialias = true
      })

      local text = {}
      local position = 0
      surface.SetTextColor( CC_TXT )
      surface.SetTextPos( 0, 0 )
      for _, ntype in pairs( args ) do

        if type(ntype) == "table" and ntype.r and ntype.b and ntype.g then
          surface.SetTextColor( ntype )
        elseif type(ntype) == "string" then
          surface.SetTextPos( position, 0 )
          if text != {} then

            local tw, th = surface.GetTextSize(table.concat( text ) )
            surface.SetTextPos( tw+position, 0 )
          end
          surface.DrawText( ntype )
          table.insert( text, ntype )

        elseif type(ntype) == "number" then

          position =  ntype
          text = {}
        end
      end
    end
    self.Console.Scroll:AddItem( self.Console.Entries[entry] )
    self.Console.Entries[entry]:SetZPos( -2000 )

    self:Scroll((self.Console.Scroll:GetTall())- (#self.Console.Entries*(console.CmdVar("console_font_size")+2)))
  end
end

function CONSOLE:UpdateText()
  self.TE.Paint = function(self, w, h)
    surface.SetDrawColor( CC_BG )
    surface.DrawRect(0, 0, w, h)
    SetFont("Console", {
      font = console.font,
      weight = console.boldness,
      size = console.CmdVar("console_font_size"),
      antialias = true
    })
    surface.SetTextColor( CC_TXT )
    local tw, th = surface.GetTextSize("-")
    surface.SetTextPos( 5, h/2 - th/2 )
    surface.DrawText( "-" )
    if CurTime() >= self.LastCaret then
      self.LastCaret  =CurTime()+self.CaretRate
      if !self.Caret then
        self.Caret = true
      else
        self.Caret = false
      end
    end
    local lSide = string.Left( self:GetValue(), self:GetCaretPos() )
    local valW, valH = surface.GetTextSize(lSide)

    if self.Caret then
      surface.SetTextPos( 7+valW+(tw/2), h/2 - th/2 )
      surface.DrawText( "|" )
    end
    surface.SetTextPos( 7+tw, h/2 - th/2 )
    surface.DrawText( self:GetValue() )
  end
end

function CONSOLE:RemoveEntry(entry)
  if IsValid(self.Console.Entries[entry]) then
    self.Console.Entries[entry]:Remove()
    table.remove(self.Console.Entries, entry)
  end
end

function CONSOLE:Clear()
  for k, v in pairs(self.Console.Entries) do
    self:RemoveEntry(k)
  end
  self:Scroll(self.Console.Scroll:GetTall())

end

function CONSOLE:Scroll(dlta)
  dlta = math.Clamp(dlta, (Console.Console.Scroll:GetTall())- (#Console.Console.Entries*(console.CmdVar("console_font_size")+2)), 0)
  self.Console.Scroll.offset = dlta
  self.Console.Scroll.VBar.Scroll = -dlta
end

function CONSOLE:PerformLayout()
	self:SetSize( ScrW()-20, ScrH() )
	self:SetPos( 10,0 )
end


vgui.Register( "HudEditor", CONSOLE, "EditablePanel" )

net.Receive("Con_ToConsole", function()
  local tbl = net.ReadTable()
  Console:InsertEntry(unpack(tbl))
  Console:Scroll((Console.Console.Scroll:GetTall())- (#Console.Console.Entries*(console.CmdVar("console_font_size")+2)))
end)

hook.Add( "OnPlayerChat", "ChatToConsole", function( player, strText, bTeamOnly, bPlayerIsDead )
  if ( !IsValid( Console ) ) then
  	Console = vgui.CreateFromTable( CONSOLE )
    Console:SetAlpha(0)
  end
  Console:InsertEntry(Color(100,100,255), player:Nick() , Color(255,255,255), ": ".. strText)
  Console:Scroll((Console.Console.Scroll:GetTall())- (#Console.Console.Entries*(console.CmdVar("console_font_size")+2)))
end )

if ( IsValid( Console ) ) then
  Console:Clear()
  timer.Simple(0.16, function()
	   Console:Remove()
     Console = vgui.CreateFromTable( CONSOLE )
     Console:SetAlpha(0)
   end)
end

if ( !IsValid( Console ) ) then
  Console = vgui.CreateFromTable( CONSOLE )
  Console:SetAlpha(0)

end

function console.Show()
  if ( !IsValid( Console ) ) then
  	Console = vgui.CreateFromTable( CONSOLE )
    Console:SetAlpha(0)
  end

	if ( IsValid( Console ) ) then
    Console:AlphaTo(255, 0.15)
  	Console:Show()
  	Console:MakePopup()
  end
  console.open = true
end

function console.Hide()
  console.open = false
	if ( IsValid( Console ) ) then
    Console:AlphaTo(0, 0.15, 0)
    timer.Simple(0.16, function()
      if !console.open then
        Console:Hide()
      end
    end)
  end
end

concommand.Add("console", function(ply, command, args)
  if !console.open then
    console.Show()
  else
    console.Hide()
  end
end)

// CLIENTSIDE Commands

// INSTANT COMMANDS
console.CreateCmd("help", "Shows basic info.", function(ply)
  Console:InsertEntry(CC_CMD, "Write / then press tab to get available commands." )
  Console:InsertEntry(CC_CMD, "You can also chat using the console leaving out the /." )
end)

console.CreateCmd("clear", "Clears the console.", function(ply)
  Console:Clear()
end)

//CONSOLE SPECIFIC COMMANDS
local oldPress = hook.GetTable()["PlayerBindPress"]
console.CreateCmd("console_cm", "Switches between old chat and console", function(ply, val)
  hook.Remove("PlayerBindPress", "pChatBind")
  if math.Clamp(val, 0, 1) == 1 then
    hook.Add("PlayerBindPress", "pChatBind", function(ply, bind, pressed)
    	if string.sub( bind, 1, 11 ) == "messagemode" then
        console.Show()
    		return true
    	end
    end)

    Console:InsertEntry(CC_CMD, "Console will now open when you press the chat bind." )
  else
    hook.Add("PlayerBindPress", "pChatBind", oldPress)
    Console:InsertEntry(CC_CMD, "Console will not be opened when you press the chat bind." )
  end
  return math.Clamp(val, 0, 1)
end, 1)

console.CreateCmd("console_font_size", "Size of the font", function(ply, val)
  for k, v in pairs(Console.Console.Entries) do
    Console.Console.Entries[k]:SetSize(ScrW(), math.Clamp(val, 3, 27)+2)
  end
  Console:Scroll((Console.Console.Scroll:GetTall())- (#Console.Console.Entries*(console.CmdVar("console_font_size")+2)))
  return math.Clamp(val, 3, 27)
end, 18)

console.CreateCmd("console_font_bold", "Bold font", function(ply, val)
  val = math.Clamp(val, 0, 1)
  if val == 0 then
    console.boldness = 450
  else
    console.boldness = 800
  end
  return val
end, 0)

console.CreateCmd("console_font", "Console font", function(ply, ...)
  local str = table.concat({...}, " ")
  local setFont = "MavenPro"
  for _, fonts in pairs(font.clientList) do
    if string.lower(fonts) == string.lower(str) then
      setFont = fonts
    end
  end
  console.font = setFont
  return setFont
end, "MavenPro")

console.CreateCmd("fonts", "Console font", function(ply)
  Console:InsertEntry(CC_NOTE, "Fonts: " )
  for _, fonts in pairs(font.clientList) do
    Console:InsertEntry( fonts )
  end
end)

// UI COMMANDS
/*
console.CreateCmd("ui_style", "1:Detailed, 2:Minimalistic", function(ply, val)

end, 1)

console.CreateCmd("hud_speed", "0:None 1:Graph, 2:Normal", function(ply, val)
end, 1)

console.CreateCmd("hud_health", "0:None 1:Normal", function(ply, val)
end, 1)*/

console.CreateCmd("hud_enabled", "0:Off 1:On", function(ply, val)
  print("a")
  return math.Clamp(val, 0, 1)
end, 1)

console.CreateCmd("hud_indicators", "0:None 1:Always 2:Lobbies", function(ply, val)
  return math.Clamp(val, 0, 2)
end, 1)

console.CreateCmd("hud_keyecho", "0:None 1:Normal 2:Minimalistic", function(ply, val)
  return math.Clamp(val, 0, 2)
end, 0)

console.CreateCmd("hud_keyecho_press", "0:Fade 1:Outlined", function(ply, val)
  return math.Clamp(val, 0, 1)
end, 0)

console.CreateCmd("hud_crosshair", "0:Disabled 1:Enabled 2:No Speed", function(ply, val)
  return math.Clamp(val, 0, 2)
end, 1)

console.CreateCmd("hud_crosshair_hradius", "Crosshair hover radius", function(ply, val)
  return math.Clamp(val, 0, 300)
end, 110)

console.CreateCmd("hud_crosshair_gradius", "Crosshair grapple radius", function(ply, val)
  return math.Clamp(val, 0, 300)
end, 70)

console.CreateCmd("hud_crosshair_opacity", "0:Disabled 1:Enabled 2:No Speed", function(ply, val)

  return math.Clamp(val, 0, 255)
end, 255)

console.CreateCmd("topspeed", "topspeed", function(ply, val)
  local i = 1
  if table.Count(stats.session) == 0 then
    Console:InsertEntry(CC_ERR, "There is no top speeds!")
    return
  end
  Console:InsertEntry(CC_NOTE, "Top 15", 150, "SteamID", 400, "Nick", 600, "Speed", 725, "Date"  )

  SetFont("Console", {
    font = console.font,
    weight = console.boldness,
    size = console.CmdVar("console_font_size"),
    antialias = true
  })

  local lb = {}
  for sID, stats in pairs(stats.session) do
    local count = #lb+1
    stats["sID"] = sID
    lb[count] = stats

  end

  table.sort( lb, function( a, b ) return a["speed"] > b["speed"] end )
  for k, v in pairs(lb) do
    if k <= 15 then
      PrintTable(v)
      local idw = surface.GetTextSize(v["sID"])
      local nw = surface.GetTextSize(v["name"])
      local sw = surface.GetTextSize(math.Round(v["speed"], 1))
      local dw = surface.GetTextSize(v["date"])
      Console:InsertEntry(CC_TXT, (surface.GetTextSize("Top 15")/2),  tostring(k), 150- (idw/2) +(surface.GetTextSize("SteamID")/2), v["sID"], 400- (nw/2) +(surface.GetTextSize("Nick")/2), v["name"], 600- (sw/2) +(surface.GetTextSize("Speed")/2), tostring(math.Round(v["speed"], 1)), 725 - (dw/2) +(surface.GetTextSize("Date")/2), v["date"]  )
    end
  end
end)

console.Grab()
