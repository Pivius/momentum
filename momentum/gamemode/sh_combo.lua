Combo = {}
Combos = {} // Table of all possible combos
Combos.Branch = {} // Combos that branch out of other combos
Restrictions = {} // Combos that are restricted to once at a time

// Initialize combos
function Combo:Init()
  Combo:Add("Edge Jump", "Do a Edge Jump", 5)
  Combo:Add("Rocket Jump", "Do a Rocket Jump", 5)
  --Combo:Add("WallJump", "Do a WallJump", 5)
  Combo:Add("WallRun", "Do a WallRun", 5)
  Combo:Add("Charged Jump S1", nil, 2) // Stage 1 charged jump
  Combo:Add("Charged Jump S2", nil, 4) // Stage 2 charged jump
  Combo:Add("Charged Jump S3", nil, 6) // Stage 3 charged jump
  Combo:Add("Ramp Slide", "Do a Ramp Slide", 10, true) // Sliding down ramps
  Combo:Add("Surf", "Surf on a ramp", 5, true) // Surfing
  Combo:Add("Strafing", nil, 0.01, true) // Surfing

  // Player has been in the air for x amount of time
  Combo:Add("AirTime", nil, 15, true, function(ply, this, combo)
    if ply:Combo_AirTime() <= CurTime() then
      // Don't repeat this combo anymore
      ply:Combo_AirTime(99999)
      return true
    end
    return false
  end, "Combo_AirTime")

  // Rocket jump 10 times to get pogo combo
  Combo:AddBranch("Rocket Jump", "Rocket Jump 10 times without touching the ground", "Pogo", 20, true, function(ply, this, combo, mC)
    // Player has left ground and can do the combo
    if ply:Combo_Pogo() then
      if type(ply:Combo_Pogo()) == "boolean" then
        ply:Combo_Pogo( 1 )
      elseif type(ply:Combo_Pogo()) == "number" then
        // Increment the number of times pogoing
        ply:Combo_Pogo(ply:Combo_Pogo()+1)
      end
    end

    if isnumber(ply:Combo_Pogo()) then
      // Pogoed 10 times
      if ply:Combo_Pogo() >= 10 then
        // Reset Pogos
        ply:Combo_Pogo( 1 )
        //Pass it!
        return true
      end
    end
    return false
  end)
end

/*
  NAME      - Combo:Add
  FUNCTION  - Adds a combo
  VARIABLES -
							combo                   - The name of the combo
							pts                     - Amount of points given for doing the combo
              (ALTERNATIVE) restrict  - Restricts it so it doesn't activate more than once in a row
              (ALTERNATIVE) funct     - Function that "Combo:AddTo" checks and see if it can pass
              (ALTERNATIVE) hook      - Checks the function in a hook. Note that I use custom hooksystem so not all normal ones work
*/
function Combo:Add(combo, desc, pts, restrict, funct, hook)
  if !Combos[combo] then
    if !funct then funct = false end
    Combos[combo] = {["pts"] = pts, ["function"] = funct, ["desc"] = desc}
    if hook then
      // Checks the combo per player in the hook
      hookAdd(hook, combo, function(ply)
        Combo:AddTo(ply, combo)
      end)
    end
    if restrict then //Quick fix for combos that repeat in hooks
      Restrictions[combo] = restrict
    end
  end
end

/*
  NAME      - Combo:AddBranch
  FUNCTION  - Branches a completely new combo out of a combo
  VARIABLES -
							from                   - The combo to branch out from
							to                     - Name of this combo|
              pts                    - Amount of points given for doing the combo
              (ALTERNATIVE) restrict - Restricts it so it doesn't activate more than once in a row
              (ALTERNATIVE) funct    - Function that "Combo:AddTo" checks and see if it can pass
              (ALTERNATIVE) hook     - Checks the function in a hook. Note that I use custom hooksystem so not all normal ones work

*/
function Combo:AddBranch(from, desc, to, pts, restrict, funct, hook)
  if !Combos[from] then

  else
    if !Combos.Branch[to] then
      Combos.Branch[to] = {["function"] = funct, ["pts"] = pts, ["main"] = from, ["desc"] = desc}
      Combos[to] = {["pts"] = pts}
      if hook then
        // Checks the combo per player in the hook
        hookAdd(hook, combo, function(ply)
          Combo:AddTo(ply, combo)
        end)
      end
    end
  end
end

/*
  NAME      - Combo:AddTo
  FUNCTION  - Checks and adds the combo to a player
  VARIABLES -
							ply   - Player entity
							combo - Name of this combo
*/
function Combo:AddTo(ply, combo)
  if !IsFirstTimePredicted() then return end
  assert( type(Combos[combo]) == 'table', "Combo does not exist!")
  local funct = function() return true end
  local mainCombo = combo
  local plyCombo = ply:Combo()

  // Replace combo with the branching combo if it's found
  for k, v in pairs(Combos.Branch) do
    if v["main"] == combo then

      funct = v["function"]
      combo = k
    end
  end

  local condition = funct(ply, combo, ply:Combo(), mainCombo)
  if !condition then
    combo = mainCombo
  end
  if #plyCombo > 0 then
    if Restrictions[combo] and table.GetKeys( plyCombo[#plyCombo] )[1] == combo then
      return
    end
  end

  if type(Combos[combo]["function"]) == "function"  then
    funct = Combos[combo]["function"]
    condition = funct(ply, combo, ply:Combo())
    if !condition then
      return
    end
  end

  local mult = 1
  if #plyCombo > 0 then
    for i=math.max(#plyCombo-5, 1), #plyCombo do

      if plyCombo[i][combo] then
        mult = math.max(0.1, mult-0.25)
      end
    end
  end

  local pts = Combos[combo]["pts"]*mult
  local Line = {[combo] = pts}

  table.insert(plyCombo,Line)
  ply:Combo(plyCombo)

  ply:ComboTimer(CurTime()+ply:ComboReset())
end

/*
  NAME      - Combo:Calc
  FUNCTION  - Calculates amount of points to per combo line
  VARIABLES -
							combo - A player's combo line (Table)
*/
function Combo:Calc(combo)
  assert( type(combo) == 'table', "Can't find anything to calculate." )
  if #combo < 0 then return end
  local pts = 0
  for _, line in pairs(combo) do
    for _, points in pairs(line) do
      pts = pts+points
    end
  end
  return pts
end

/*
  NAME      - ComboCooldown
  FUNCTION  - Resets the combo when needed
  HOOK      - Think
*/
function ComboCooldown()
  for _, ply in pairs(player.GetAll()) do
    if ply:ComboTimer() <= CurTime() and ply:ComboTimer() != 0 then
      local points = math.Round(ply:Points()+Combo:Calc(ply:Combo())+ply:StrafePts(), 2)
      if ply:Points() < points then
        ply:Points(points)
        if SERVER then
          givePts(ply, points)
        end
      end
      ply:Combo({})
      ply:StrafePts(0)
      ply:ComboTimer(0)
    end
  end
end
hook.Add("Think", "Combo cooldown", ComboCooldown)

/*
  NAME      - ComboAirStrafe
  FUNCTION  - Adds points according to strafe
  HOOK      - AirStrafe
*/
function ComboAirStrafe(ply, speed, wish)
  if !IsFirstTimePredicted() then return end
  if speed > 0 then
    local PPS = math.Round((speed/wish)/100,4) //Points per strafe
    if #ply:Combo() == 0 then
      Combo:AddTo(ply, "Strafing")
    end
    ply:ComboTimer(ply:ComboTimer()+(PPS*1.5))

    ply:StrafePts(ply:StrafePts()+PPS)
    ply:StrafePtsTot(ply:StrafePtsTot() + PPS)
    ply:StrafeTime(math.Round(ply:StrafeTime()+FrameTime(),3))
    --ply:Points(math.Round(ply:Points()+PPS, 2 ))
  end
end
hookAdd("AirStrafe", "Combo airstrafe", ComboAirStrafe)
Combo:Init()
