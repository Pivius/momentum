stats = stats or {}
stats.average = stats.average or {}
stats.session = stats.session or {}
local meta = FindMetaTable( "Player" )

net.Receive("Stats_Update", function(len, ply)
  local ent = net.ReadEntity()
  local ply = (ent:IsPlayer() and ent) or LocalPlayer()
  local tbl = net.ReadTable()

  if !IsValid(ply) then return end
  if tbl[ply:SteamID()] then
    ply:TSA(tbl[ply:SteamID()]["speed"])
    ply:Points(tbl[ply:SteamID()]["pts"])
  else
    ply:TSA(0)
  end
  stats.session = tbl

end)

net.Receive("Stats_ResetTop", function(len, ply)
  local ent = net.ReadEntity()
  local ply = (ent:IsPlayer() and ent) or LocalPlayer()
  ply:TSS(0)
end)

function initAvg(ply)
  if CLIENT then
    stats.average[ply:SteamID()] = {
      ["totalVel"] = 0,
      ["Samples"] = 0,
      ["averageVel"] = 0
    }
  else
    local tbl = {
      [ply:SteamID()] = {
        ["totalVel"] = 0,
        ["Samples"] = 0 ,
        ["averageVel"] = 0
      }
    }
    UT:MergeTableTo(stats.average, tbl)
  end
end


function meta:resetAvg()
  if stats.average[self] == nil then
    initAvg(self)
    return
  end
  stats.average[self:SteamID()]["totalVel"] = 0
  stats.average[self:SteamID()]["Samples"] = 0
  stats.average[self:SteamID()]["averageVel"] = 0
end

function averageSpeed()
  for k, ply in pairs(player.GetAll()) do
    if !ply:Alive() then ply:resetAvg() return end
    local Vel = ply:Speed2D()
    if stats.average[ply:SteamID()] == nil then
      initAvg(ply)
    end
    local totalVel = stats.average[ply:SteamID()]["totalVel"]
    local Samples = stats.average[ply:SteamID()]["Samples"]
    local averageVel = stats.average[ply:SteamID()]["averageVel"]
    if Vel > 400 then
      stats.average[ply:SteamID()]["totalVel"] = totalVel + Vel
      stats.average[ply:SteamID()]["Samples"] = Samples + 1
      stats.average[ply:SteamID()]["averageVel"] = math.Round(totalVel / Samples)
    end
    if Vel == 0 and ply:OnGround() then
      ply:resetAvg()
    end
  end
end
hook.Add( "Think", "Averagespeed", averageSpeed )

function meta:GetAvg()
  if !stats.average[self:SteamID()] then return 0 end
  return stats.average[self:SteamID()]["averageVel"]
end
