stats = stats or {}
stats.session = {}
stats.server = {}
stats.server.TopSpeed = 0
local meta = FindMetaTable( "Player" )

util.AddNetworkString( "rankNetworking" )
util.AddNetworkString( "Stats_Update" )
util.AddNetworkString( "Stats_ResetTop" )

function stats:Init()
  stats:loadStats()
end
/*
  NAME - Insert
  FUNCTION - Inserts values to a mysql table
*/
function stats:Insert(sID, name, speed, pts, strafepts, strafetime, airtime, rank, place, date)
  local query = mysql:Insert(SQLDetails.tables.stats)
  query:Insert("sID", sID)
  query:Insert("name", name)
  query:Insert("speed", speed)
  query:Insert("pts", pts)
  query:Insert("strafepts", strafepts)
  query:Insert("strafetime", strafetime)
  query:Insert("airtime", airtime)
  query:Insert("rank", rank)
  query:Insert("placement", place)
  query:Insert("date", date)
  query:Execute()
end

/*
  NAME - Update
  FUNCTION - Updates a mysql table
*/
function stats:Update(sID, name, speed, pts, strafepts, strafetime, airtime, rank, place, date)
  local query = mysql:Update(SQLDetails.tables.stats)

  query:Where("sID", sID)
  query:Update("name", name)
  query:Update("speed", speed)
  query:Update("pts", pts)
  query:Update("strafepts", strafepts)
  query:Update("strafetime", strafetime)
  query:Update("airtime", airtime)
  query:Update("rank", rank)
  query:Update("placement", place)
  query:Update("date", date)
  query:Execute()
end

/*
  NAME - Remove
  FUNCTION - Removes a mysql table
*/
function stats:Remove(sID)
  local query = mysql:Delete(SQLDetails.tables.stats)
    query:Where("sID", sID)
  query:Execute()
  stats:loadStats()
end

function stats:updatePlayers()
  for _, ply in pairs(player.GetAll()) do
    if stats.session[ply:SteamID()] then
      local plyStats = stats.session[ply:SteamID()]
      ply:TSA(plyStats["speed"])
      ply:Points(plyStats["pts"])
      ply:StrafePtsTot(plyStats["spts"])
      ply:StrafeTime(plyStats["stime"])
    end
  end
  net.Start("Stats_Update")
    net.WriteEntity(NULL)
    net.WriteTable(stats.session)
  net.Broadcast()
end

/*
  NAME - loadStats
  FUNCTION - Loads stats from a mysql table
*/
function stats:loadStats()
  stats.session = {}
  local query = mysql:Select(SQLDetails.tables.stats)
    query:Callback(function(result, status, lastID)
      if (type(result) == "table" and #result > 0) then
        for k, v in pairs(result) do
          local sID = v["sID"]
          local name = v["name"]
          local speed = v["speed"]
          local pts = v["pts"]
          local rank = v["rank"]
          local placement = v["placement"]
          local date = v["date"]
          local spts = v["strafepts"]
          local stime = v["strafetime"]
          local atime = v["airtime"]

          local statstbl = {
            [sID] = {
              ["name"] = name,
              ["speed"] = speed,
              ["pts"] = pts,
              ["rank"] = rank,
              ["placement"] = placement,
              ["date"] = date,
              ["spts"] = spts,
              ["stime"] = stime,
              ["atime"] = atime
            }
          }
          UT:MergeTableTo(stats.session, statstbl)

        end
      end
    end)
  query:Execute()
  timer.Simple(1, function()
    stats:updatePlayers()
  end)
end

/*
  NAME - updateTop
  FUNCTION - Updates when player gets new alltime top
*/
function updateRank( ply )
  if !ply:IsPlayer() or ply:IsBot() then return end
  local name = ply:Nick()
  local sID  = ply:SteamID()

  local query = mysql:Select(SQLDetails.tables.stats)
    query:Where("sID", sID)
    query:Callback(function(res, status, id)
    if type(res) == "table" and #res > 0 then

      stats:Update(sID, name, ply:TSA(), ply:Points(), ply:StrafePtsTot(), 0, 0, calcRank(ply:Points()), 0, GetDate())
    else
      stats:Insert(sID, name, ply:TSA(), ply:Points(), ply:StrafePtsTot(), 0, 0, "Starter", 0, GetDate())
    end
    local users = {
      [sID] = {
        ["name"] = name,
        ["speed"] = ply:TSA(),
        ["pts"] = ply:Points(),
        ["rank"] = calcRank(ply:Points()),
        ["placement"] = 0,
        ["date"] = GetDate(),
        ["spts"] = ply:StrafePtsTot(),
        ["stime"] = ply:StrafeTime(),
        ["atime"] = 0
      }
    }
    /*
    if ply:TSA() > stats.server.TopSpeed then
      stats.server.TopSpeed = ply:TSA()
    end*/
    UT:MergeTableTo(stats.session, users)
    netRanks("updatePoints", {ranksCache}, ply)
    net.Start("Stats_Update")
      net.WriteEntity(ply)
      net.WriteTable(stats.session)
    net.Send(ply)
  end)
  query:Execute()
end

function givePts(ply, pts)
  ply:Points(pts)
  
  updateRank(ply)
end

function getRank(ply)
  return calcRank(ply:Points())
end

function meta:resetTop()
  self:TSS(0)
  self:TSA(0)
  stats:Update(self:SteamID(), self:Nick(), 0,  self:Points(), ply:StrafePtsTot(), 0, 0, calcRank(self:Points()), 0, GetDate())
  --stats:Remove(self:SteamID())
  stats.server.TopSpeed = 0
  net.Start("Stats_ResetTop")
    net.WriteEntity(self)
  net.Send(self)
end
