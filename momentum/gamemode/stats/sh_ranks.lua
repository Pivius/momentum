ranks = {
	{ "Bouncer", Color( 200, 200, 200 ) },
	{ "New", Color(94, 94, 94) },
  { "Amateur", Color( 200, 129, 129 ) },
	{ "Slow", Color(37, 59, 80) },
  { "Casual", Color( 4, 100, 0 ) },
	{ "91% IQ", Color(236, 244, 94) },
  { "Lower mid tier", Color( 15, 15, 255 ) },
	{ "Addicted", Color(116, 215, 16) },
	{ "Decent-ish", Color( 0, 213, 213 ) },
	{ "Rocket Jumper", Color(16, 57, 180) },
  { "Pro Hopper", Color(255, 252, 0) },
	{ "Autist", Color(36, 61, 152) },
  { "Mid tier", Color( 155, 100, 255 ) },
  { "Doing it sideways", Color( 128, 0, 128 ) },
	{ "Supercalifragilisticexpialidocious", Color(250, 0, 255) },
  { "No effort", Color( 98, 0, 0 ) },
  { "Simply amazing", Color(189, 60, 60) },
	{ "B O N E L E S S", Color(255, 145, 0) },
	{ "Skillful", Color(137, 242, 206) },
  { "God why?", Color( 230, 255, 255) },
  { "Ligma", Color( 215, 0, 6 ) },
  { "Time Waster", Color( 10, 255, 35 ) },
	{ "ez", Color(13, 13, 13)},
	{ "b0nker", Color(51, 0, 255)},
}
--23
ranksCache = {}
toppts = 25000

function rankColor(rank)
  for k, v in pairs(ranks) do
    if v[1] == rank then
      return v[2]
    end
  end
end

function getRanks()
  return ranks
end

function calcRank(pts)
  local reqpts = 1000
  local totalrnks = #ranks
	if !pts then
		return getRanks()[1][1]
	end
  for i=2, #ranks-1 do
    local mult =  (1/#ranks) *i
    reqpts = calcRankPts(ranks[i][1])

    if pts >= reqpts and pts < calcRankPts(ranks[i+1][1]) then
      return ranks[i][1]
    elseif pts >= toppts then
      return ranks[#ranks][1]
    elseif pts < calcRankPts(ranks[2][1]) then
      return ranks[1][1]
    end
  end
end

function calcRankPts(rank)
  local reqpts
  local mult
  local totalrnks = #ranks
  for i=1, #ranks do
    if ranks[i][1] == rank then
			local num = i
			num = math.Clamp(num-1, 1, num)
      mult = (1/(#ranks-1)) *num
      if i == 1 then
        reqpts = 0
      elseif i == totalrnks then
        reqpts = toppts
      else
        reqpts = toppts * mult --* ((i*0.075))
      end
      return math.Round(reqpts)
    end
  end
end

function netRanks(type, vargs, ply)
  if SERVER then
    net.Start("rankNetworking")
      net.WriteString(type)
      net.WriteTable(vargs)
    net.Send(ply)
  else
    net.Start("rankNetworking")
      net.WriteString(type)
      net.WriteTable(vargs)
    net.SendToServer()
  end
end

function netBroadcast(type, vargs)
  if SERVER then
    net.Start("rankNetworking")
      net.WriteString(type)
      net.WriteTable(vargs)
    net.Broadcast()
  end
end

net.Receive( "rankNetworking", function()
 local type = net.ReadString()
 local varargs = net.ReadTable()
 if type == "getRank" then
	 if SERVER then
     local ply = varargs[1]

     net.Start("rankNetworking")
       net.WriteString("getRank")
       net.WriteTable( {ply:SteamID(), ranksCache[ply:SteamID()]} )
     net.Send(ply)
   else
     local cache = {
       [varargs[1]] = varargs[2]
   }
     UT:MergeTableTo(ranksCache, cache)

   end
 elseif type == "updatePoints" then
   if CLIENT then
     cache = varargs[1]
     table.Empty(ranksCache)
     UT:MergeTableTo(ranksCache, cache)
   else
     stats:loadStats()
     netBroadcast("updatePoints", {ranksCache})
   end
 end
end )
