-------------------------------------
-- Draw Player behind walls.
-------------------------------------
function drawPlayer(ent)

    local ang = LocalPlayer():EyeAngles()
    local pos = LocalPlayer():EyePos()+ang:Forward()*10

            render.ClearStencil()
            render.SetStencilEnable(true)
            render.SetStencilReferenceValue( 1 )
            render.SetStencilWriteMask(1)
            render.SetStencilTestMask(1)
            render.SetStencilZFailOperation(STENCILOPERATION_REPLACE)
            render.SetStencilPassOperation(STENCILOPERATION_KEEP)
            render.SetStencilFailOperation(STENCILOPERATION_KEEP)
            render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_ALWAYS)


            render.SetBlend( 0 )
            /*
              local scale = Vector(1,1,0.9)
              local mat = Matrix()
              mat:Scale(Vector(scale))

              ent:EnableMatrix("RenderMultiply", mat)


              */
              local tran = Vector(0,0,0)
              local mat = Matrix()
              mat:Scale(Vector(1,1,1))
              mat:Translate( tran )
              ent:EnableMatrix("RenderMultiply", mat)

              ent:DrawModel()
              mat:Translate( Vector(0,0,0) )
              mat:Scale(Vector(1,1,1))
              ent:EnableMatrix("RenderMultiply", mat)
            render.SetBlend( 1 )
            render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
            cam.Start3D2D(Vector(pos.x, pos.y, pos.z),Angle(ang.p+90,ang.y,0),1)
              cam.IgnoreZ(false)
                surface.SetDrawColor(hud.customColor)
                surface.DrawRect(-ScrW(),-ScrH(),ScrW()*2,ScrH()*2)
                cam.IgnoreZ(false)
            cam.End3D2D()

              ent:DrawModel()
            render.SetStencilEnable(false)

end

-------------------------------------
-- Render player.
-------------------------------------
hook.Add("PreDrawOpaqueRenderables","PlayerStencil",function()

end)

-------------------------------------
-- Calculate the distance between two elements. http://www.purplemath.com/modules/distform.htm
-------------------------------------
function GetDistanceSqr(pp ,cx, cy)

    return (pp.x - cx) ^ 2 + (pp.y - cy) ^ 2
end

-------------------------------------
-- Find the  root.
-------------------------------------
function GetDistance(pp, cx, cy)
    return math.sqrt(GetDistanceSqr(pp, cx ,cy))
end

-------------------------------------
-- Indicator UI.
-------------------------------------


function hud.indicator_info()

    for k, v in pairs (player.GetAll()) do
      if v != LocalPlayer() then

        local    NameTag = (v:LocalToWorld(Vector(0,0,77))):ToScreen()
        local    Middle = (v:LocalToWorld(Vector(0,0,77/2))):ToScreen()
        local    bot = (v:LocalToWorld(Vector(0,0,0))):ToScreen()
        local    speedTag = (v:LocalToWorld(Vector(0,0,10))):ToScreen()
        local    speed   = v:Speed()
        local playername = ""
        local      alpha = 255
        local     cx, cy = input.GetCursorPos()
        playername       = v:Name()

        local dist = LocalPlayer():GetPos():Distance(v:GetPos())
        local size = 15/(math.Clamp(dist/1200, 1, 1.5))

        alpha = Lerp(GetDistance(Middle, cx, cy)/(math.Clamp(alpha, 0, 255)/3) , 255, 0)
        local color = ColorAlpha(hud.color, alpha)
        if lobby:getPlayerLobby(v) == lobby:getPlayerLobby(LocalPlayer()) and lobby:getPlayerLobby(v) != "None" then
          surface.SetDrawColor(hud.customColor )
          surface.SetMaterial(Material("materials/trick/triangle.png", "noclamp"))
          surface.DrawTexturedRectRotated( bot.x, bot.y, size, size, 0 )
          color = ColorAlpha(hud.customColor, alpha)
        end

        if  v:Alive() then
          local color = ColorAlpha(hud.customColor, alpha)
          draw.DrawText(playername, "HUD Player", NameTag.x, NameTag.y-10 , color, TEXT_ALIGN_CENTER)
          draw.DrawText(math.Round(speed/10)*10, "HUD Player", speedTag.x+20, speedTag.y+10, color, TEXT_ALIGN_CENTER)
        end
      end
    end
end
hook.Add("HUDPaint", "Indicator info", hud.indicator_info)


function hud.indicator_offscreen()
  /*
  for k, v in pairs (player.GetAll()) do
    if v != LocalPlayer() then
      local       plypos = (v:LocalToWorld(Vector(0,0,73/2))):ToScreen()
      local screenCenter = Vector(ScrW(), ScrH(), 0)/2

      plypos.x = plypos.x-screenCenter.x
      plypos.y = plypos.y-screenCenter.y

      local angle = math.deg(math.atan2(plypos.y, plypos.x))
      angle =  (math.rad(angle-90))
      local cos = math.cos(angle)
      local sin = -math.sin(angle)
      local radius = 10
      local m = cos/sin
      local screenBounds = screenCenter*0.8
      --screenBounds.x =  radius * math.cos(-angle*math.pi/180) * 1
      --screenBounds.y =  radius * math.sin(-angle*math.pi/180) * 1
      plypos.x= screenCenter.x+ (sin*150)
      plypos.y= screenCenter.y+ (cos*150)


      --if then

      --end

      --if plypos.x >screenBounds.x or plypos.x <-screenBounds.x or plypos.y >screenBounds.y or plypos.y <-screenBounds.y then


        if cos >0 then
          plypos.x = screenBounds.y/m
          plypos.y = screenBounds.y
        else
          plypos.x = -screenBounds.y/m
          plypos.y = -screenBounds.y
        end

        if plypos.x >screenBounds.x then
          plypos.x = screenBounds.x
          plypos.y = screenBounds.x*m
        elseif plypos.x <-screenBounds.x then
          plypos.x = -screenBounds.x
          plypos.y = -screenBounds.x*m
        end

        plypos.x = plypos.x+screenCenter.x
        plypos.y = plypos.y+screenCenter.y

        --surface.DrawLine( plypos.x, plypos.y, screenCenter.x, screenCenter.y )
        surface.SetMaterial(Material("materials/trick/triangle.png", "noclamp"))
        angle = math.deg(angle)

        surface.DrawTexturedRectRotated( plypos.x, plypos.y, 15, 15, -angle )

      --end
    end
  end*/
end
hook.Add("HUDPaint", "Indicator offscreen", hud.indicator_offscreen)
