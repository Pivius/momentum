
-------------------------------------
-- Idea SECTION
-------------------------------------
-- SPEED UI
-------------------------------------
-- IDEA The limit should be all time top -- Idea BY THEO

-------------------------------------
-- LOBBIES
-------------------------------------
-- IDEA Hard mode, worse acceleration (higher grav?), no shift

-------------------------------------
-- HEALTH UI
-------------------------------------
--IDEA Light/White UI style
--IDEA Fill up health heart and speed guy idea by metaloexdxdxdxd
-------------------------------------
-- UI.
-------------------------------------
hud = hud or {}
hud.module = include( 'momentum/gamemode/modules/cl_hudmodule.lua' )
hud.ease_module = include( 'momentum/gamemode/modules/cl_ease.lua' )
hud.HUD_CACHE = {}
Topspeed     = 0

topstartlimit= 5
toplimit     = topstartlimit
-------------------------------------
-- Commands.
-- Different console commands for the UI.
-------------------------------------
local hudtoggle   = CreateClientConVar("grp_hud", "1", true, true, "Hides/shows hud")
local indicators   = CreateClientConVar("grp_indicators", "1", true, true, "Hides/shows player indicators")
local crosshairtoggle = CreateClientConVar( "grp_crosshair", "1", true, true, "Hides/shows crosshair")
local LagSens = CreateClientConVar( "grp_hud_lag_sensitivity", "10", true, true, "Hud lag sensitivity. Default: 10")

cvars.AddChangeCallback( "grp_hud_lag_sensitivity", function( convar_name, value_old, value_new )
  if tonumber(value_new) > 30 then
    LagSens:SetInt(30)
    return
  elseif tonumber(value_new) < 1 then
    LagSens:SetInt(1)
    return
  end
  local ls = math.Clamp(LagSens:GetInt(),1,30)
  for k, v in pairs(hud.HUD_CACHE) do
    v.tiltRate = math.Clamp(ls,1,30)/4
    v.resetRate = 40/((ls/4)/2)
  end
end )
-------------------------------------
-- The main UI colors.
-------------------------------------
hud.color = Color(255, 255, 255, 220)
hud.customColor = Color(255,255,255)
hud.customColor2 = Color(200,200,200)

hud.Team = "None"
-------------------------------------
-- Easing
-------------------------------------
hud.ease = {}
hud.ease.HP = {}
hud.ease.HP.FadeTime = 0

hud.ease.HP.Alpha = 0
hud.ease.HP.W = 0
hud.ease.HP.Ease = hud.ease_module.new(1, {alpha = 0, W = 0}, {alpha = hud.ease.HP.Alpha, W = hud.ease.HP.W}, "inOutCubic")

hud.ease.vel = {}
hud.ease.vel.FadeTime = 0

hud.ease.vel.Alpha = 0
hud.ease.vel.W = 0
hud.ease.vel.Ease = hud.ease_module.new(1, {alpha = 0, W = 0}, {alpha = hud.ease.vel.Alpha, W = hud.ease.vel.W}, "inOutCubic")

hud.ease.cgaz = {}
hud.ease.cgaz.FadeTime = 0
hud.ease.cgaz.Alpha = 0
hud.ease.cgaz.W = 0
hud.ease.cgaz.Pos = 0
hud.ease.cgaz.Ease = hud.ease_module.new(1, {alpha = 0, W = 0, Pos = 0}, {alpha = hud.ease.cgaz.Alpha, W = hud.ease.cgaz.W, Pos = hud.ease.cgaz.Pos}, "inOutCubic")

hud.keys = {
  ["W"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_FORWARD},
  ["A"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_MOVELEFT},
  ["S"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_BACK},
  ["D"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_MOVERIGHT},
  ["JUMP"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_JUMP},
  ["DUCK"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_DUCK},
  ["M1"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_ATTACK},
  ["M2"] = {
    ["color"] = Color(0, 0, 0, 150),
    ["enum"] = IN_ATTACK2}
}

-------------------------------------
-- INIT UI.
-------------------------------------
function hud.Init()
  local ls = math.Clamp(LagSens:GetFloat(),1,30)
  --Speedometer

  if HUD_HP then
    HUD_HP:Remove()
    hud.HUD_CACHE["HUD_HP"] = nil
  end
  HUD_HP = hud.module.CreatePanel( nil, ls / 4, 40 / ( ( ls / 4 ) / 2 ) )
  hud.HUD_CACHE["HUD_HP"] = HUD_HP

  if HUD_CGAZ then
    HUD_CGAZ:Remove()
    hud.HUD_CACHE["HUD_CGAZ"] = nil
  end
  HUD_CGAZ = hud.module.CreatePanel( nil, ls / 4, 40 / ( ( ls / 4 ) / 2 ) )
  hud.HUD_CACHE["HUD_CGAZ"] = HUD_CGAZ

  if HUD_VEL then
    HUD_VEL:Remove()
    hud.HUD_CACHE["HUD_CGAZ"] = nil
  end
  HUD_VEL = hud.module.CreatePanel( nil, ls / 4, 40 / ( ( ls / 4 ) / 2 ) )
  hud.HUD_CACHE["HUD_VEL"] = HUD_VEL

  if HUD_TOP then
    HUD_TOP:Remove()
    hud.HUD_CACHE["HUD_TOP"] = nil
  end

  HUD_TOP = hud.module.CreatePanel( nil, ls / 4, 40 / ( ( ls / 4 ) / 2 ) )
  hud.HUD_CACHE["HUD_TOP"] = HUD_TOP
end
hud.Init()
-------------------------------------
-- Removes standard UI.
-------------------------------------
function GM:HUDDrawTargetID()
end
function GM:HUDShouldDraw(name)
    local draw = true
    if(name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudSecondaryAmmo" or name == "CHudCrosshair" ) then
    draw = false;
    end
return draw;
end

function hud.crosshair()
  if !console.CmdVar("hud_enabled") then
    return
  end
  local ply      = LocalPlayer()
  local speed   = ply:Speed()

  surface.SetDrawColor( 255, 255, 255, 255 )
  surface.SetMaterial(Material("materials/trick/hud/Dot.png", "noclamp"))
  surface.DrawTexturedRect( (ScrW()/2)-1, (ScrH()/2)-1, 2, 2 )
  surface.SetDrawColor( 255, 1, 1, 255 )
  --surface.DrawCircle( ScrW()/2, ScrH()/2, 4, 255, 255, 255, 255 )
  --draw.DrawText(math.Round(speed/10)*10, "HUD Speed", (ScrW()/2)*1.005, (ScrH()/2), Color(255,255,255, 255), TEXT_ALIGN_CENTER)

end

hook.Add("HUDPaint", "ch", hud.crosshair)
-------------------------------------
-- Spectators UI.
-------------------------------------
function hud.spectators()
  local ply = LocalPlayer():GetObserverTarget() or LocalPlayer()
  if !Spectate.Players[ply] then return end
  if #Spectate.Players[ply]["Spectators"] > 0 then
    surface.SetFont( "HUD Spectators" )
    surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )

    local tx, ty = surface.GetTextSize( "Spectators")

    surface.SetTextPos( ScrW()-tx, 0)
    surface.DrawText( "Spectators" )

    for k, v in pairs(Spectate.Players[ply]["Spectators"]) do
      surface.SetFont( "HUD Spectators" )
      surface.SetTextColor( Color(hud.customColor.r, hud.customColor.g, hud.customColor.b, 255) )

      local tx, ty = surface.GetTextSize( v:Nick() )

      surface.SetTextPos( ScrW()-tx, (k*(ty+1)))
      surface.DrawText( v:Nick() )
    end
  end
end
hook.Add("HUDPaint", "spectators", hud.spectators)


function surface.DrawTexturedRectRotatedPoint( x, y, w, h, rot, x0, y0 )

	local c = math.cos( math.rad( rot ) )
	local s = math.sin( math.rad( rot ) )

	local newx = y0 * s - x0 * c
	local newy = y0 * c + x0 * s

	surface.DrawTexturedRectRotated( x + newx, y + newy, w, h, rot )

end


-------------------------------------
-- Draws a outlined box.
-------------------------------------
function hud.OutlinedBox( x, y, w, h, thickness, color)
    surface.SetDrawColor( color )

    for i=0, thickness  do

        surface.DrawRect(x, y, w, h-h+thickness)
        surface.DrawRect(x, y+h-thickness, w, h-h+thickness)
        surface.DrawRect(x, y, w-w+thickness, h)
        surface.DrawRect(x+w-thickness+.5, y, w-w+thickness, h)
    end

end

-------------------------------------
-- Draws a box.
-------------------------------------
function hud.drawBox(corner, x, y, w, h, color, key, font, txtcolor, enum, keoutline, thickness, outlinecolor)
  local ply = Spectate.IsSpectating(LocalPlayer()) or LocalPlayer()
  surface.SetFont( font )
  local width, height = surface.GetTextSize( key )
  centerW, centerH = x + ( w/2 ), (y + (h/2))-(height/2)

  if  ( keyEnum:KeyDown(ply, enum ) and keoutline == true and outlineecho:GetBool()) then
    --surface.DrawRect(x, y, w, h) maybe
    hud.OutlinedBox(x, y, w, h, thickness, outlinecolor)
  end
  if keyEnum:KeyDown(ply, enum ) and !outlineecho:GetBool() then
    color.a = color.a and Lerp(0.1, color.a, 230) or 230
  else
    color.a = color.a and Lerp(0.1, color.a, hud.color.a) or hud.color.a
  end

  draw.RoundedBox(corner, x, y, w, h, color)
  draw.DrawText( key, font, centerW, centerH, txtcolor, TEXT_ALIGN_CENTER )
end


-------------------------------------
-- Left Side UI.
-------------------------------------

function HUD_HP:Draw3D(ply, scrw, scrh)
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  local localPly = Spectate.IsSpectating(ply) or ply
  local x, y     = 0, scrh-200      // X and Y position
  local w, h     = -scrw, 5            // Width and Height
  local hp       = math.Clamp(ply:Health(), 0, 100)
  local hpPos    = x
  local hpWidth  = hud.ease.HP.Ease:get().W
  local hpHeight = 20
  local alpha = hud.ease.HP.Ease:get().alpha
  local color = ColorAlpha(hud.customColor, alpha)
  // BackDraw
  --PrintTable(hud.ease.HP.Ease)

  local a = hud.ease.HP.Ease:update(1*FrameTime())

  if hp <= 99 then
    hud.ease.HP.FadeTime = CurTime()+5
  end
  if hud.ease.HP.FadeTime <= CurTime() and hp == 100 then
    hud.ease.HP.W = 0
    hud.ease.HP.Alpha = 0
    --if a and hud.ease.HP.Ease.target.alpha != 0 then
      --hud.ease.HP.Ease:reset()
    --end
  elseif hp < 100 or hud.ease.HP.FadeTime > CurTime() then
    hud.ease.HP.W = 500
    hud.ease.HP.Alpha = 255

  end
  hud.ease.HP.Ease:Target({alpha = hud.ease.HP.Alpha, W = hud.ease.HP.W})


  surface.SetDrawColor( Color(0,0,0,math.Clamp(alpha, 0, 100) ))
  surface.DrawRect( x, y+(h/2)-(hpHeight/2), -hpWidth, hpHeight )
  hpHeight    = 10
  local healthWidth    = hp*(hpWidth/100)-5
  hpBar = hpBar and UT:Hermite(0.075, hpBar, healthWidth) or healthWidth
  surface.SetDrawColor( color )
  surface.DrawRect( hpPos, y+(h/2)-(hpHeight/2), -hpBar, hpHeight )

  local delaycol = Color(color.r, color.g, color.b, math.Clamp(color.a - 105, 0 ,255))
  surface.SetDrawColor( delaycol )
  delayedHpBar = delayedHpBar and UT:Hermite(0.06, delayedHpBar, healthWidth) or healthWidth
  surface.DrawRect( hpPos, y+(h/2)-(hpHeight/2), -delayedHpBar, hpHeight )

  SetFont("HUD Health", {
    font = "Ostrich Sans",
    weight = 600,
    size = 75,
    antialias = true
  })
  surface.SetTextColor( color )
  local hpX, hpY = surface.GetTextSize( math.Round( hp ) )
  surface.SetTextPos( math.Clamp(hpPos-(hpWidth/2)-(hpX/2), -9999, -hpX),  y-hpY-10)
  surface.DrawText( math.Round( hp ) )

  surface.SetDrawColor( hud.color )


end

local cGazR = hud.ease_module.new(1, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, "outInCubic")
local cGazL = hud.ease_module.new(1, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, "outInCubic")
local cGazB = hud.ease_module.new(1, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, "outInCubic")
ticks_on_ground = 0
function HUD_CGAZ:Draw3D(ply, scrw, scrh)
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  local localPly = Spectate.IsSpectating(ply) or ply
  local x, y     = -500, scrh-200      // X and Y position
  local w, h     = -scrw, 10            // Width and Height
  local bar_width     = 1000
  local bar_height    = 20
  local alpha = 255
  local color = ColorAlpha(hud.customColor, alpha)
  local draw_line = true
  local a = hud.ease.cgaz.Ease:update(1*FrameTime())


  local maxWidth, maxPos, optWidth, optPos, rWidth, rPos = GetCGaz(localPly, 100, 5, false, draw_line)
  if ply:OnGround() then
    if ticks_on_ground > 5 then

      if hud.ease.cgaz.FadeTime <= CurTime() then

        hud.ease.cgaz.W = 0
        hud.ease.cgaz.Alpha = 0
        hud.ease.cgaz.Pos = 0
      end
    else
      ticks_on_ground = ticks_on_ground + 1
    end
  else
    hud.ease.cgaz.FadeTime = CurTime()+2.5
    if hud.ease.cgaz.FadeTime > CurTime() then
      hud.ease.cgaz.W = bar_width
      hud.ease.cgaz.Alpha = alpha
      hud.ease.cgaz.Pos = x
    end
    ticks_on_ground = 0
  end
  hud.ease.cgaz.Ease:Target({alpha = hud.ease.cgaz.Alpha, W = hud.ease.cgaz.W, Pos = hud.ease.cgaz.Pos})

  bar_width = hud.ease.cgaz.Ease:get().W
  x = hud.ease.cgaz.Ease:get().Pos
  alpha = hud.ease.cgaz.Ease:get().alpha
  color = ColorAlpha(hud.customColor, alpha)

  surface.SetDrawColor( Color(0,0,0,math.Clamp(alpha, 0, 100)) )
  surface.DrawRect( x, y+(h/2)+(bar_height/2), bar_width, bar_height )

  if draw_line && !(ply:KeyDown(IN_FORWARD) && ply:KeyDown(IN_MOVERIGHT) && ply:KeyDown(IN_MOVELEFT) ) && ticks_on_ground <= 5 then
    local cGazRUpdate = cGazR:update(0.1)
    local cGazLUpdate = cGazL:update(0.1)
    local cGazBUpdate = cGazB:update(0.1)
    rPos = math.Clamp(rPos, x, x+bar_width)
    rWidth = math.Clamp(rWidth, 0, rWidth - ((rPos+  rWidth) - (bar_width/2)))

    optPos = math.Clamp(optPos, x, x+bar_width)
    optWidth = math.Clamp(optWidth, 0, optWidth - ((optPos+  optWidth) - (bar_width/2)))

    maxPos = math.Clamp(maxPos, x, x+bar_width)
    maxWidth = math.Clamp(maxWidth, 0, maxWidth - ((maxPos+  maxWidth) - (bar_width/2)))

    local cgaz_maxWidth = maxWidth
    local cgaz_maxPos = maxPos

    local cgaz_optWidth = optWidth
    local cgaz_optPos = optPos

    local cgaz_rWidth = rWidth
    local cgaz_rPos = rPos

    if ( ply:KeyDown(IN_MOVERIGHT) && !backwards) || ( ply:KeyDown(IN_MOVELEFT) && backwards )  then
      local cgaz_maxWidth = cGazR:get().maxWidth
      local cgaz_maxPos = cGazR:get().maxPos

      local cgaz_optWidth = cGazR:get().optWidth
      local cgaz_optPos = cGazR:get().optPos

      local cgaz_rWidth = cGazR:get().rWidth
      local cgaz_rPos = cGazR:get().rPos
      cGazR:Target({optWidth = optWidth, optPos = optPos, maxWidth = maxWidth, maxPos = maxPos, rWidth = rWidth, rPos = rPos})
    elseif ( ply:KeyDown(IN_MOVELEFT) && !backwards ) || ( ply:KeyDown(IN_MOVERIGHT) && backwards ) then
      local cgaz_maxWidth = cGazL:get().maxWidth
      local cgaz_maxPos = cGazL:get().maxPos

      local cgaz_optWidth = cGazL:get().optWidth
      local cgaz_optPos = cGazL:get().optPos

      local cgaz_rWidth = cGazL:get().rWidth
      local cgaz_rPos = cGazL:get().rPos
      cGazL:Target({optWidth = optWidth, optPos = optPos, maxWidth = maxWidth, maxPos = maxPos, rWidth = rWidth, rPos = rPos})
    end

    // Accel Range
    surface.SetDrawColor(Color(240, 211, 20, alpha) )
    surface.DrawRect( x+( bar_width / 2 ) + cgaz_rPos, y+(h)+(bar_height/2), cgaz_rWidth, bar_height-10)
    // Optimal accel
    surface.SetDrawColor(Color(40, 231, 9, alpha) )
    surface.DrawRect( x+( bar_width / 2 ) + cgaz_optPos, y+(h)+(bar_height/2), cgaz_optWidth, bar_height-10)
    // Max accel
    surface.SetDrawColor( Color(32, 95, 201, alpha) )
    surface.DrawRect( x+( bar_width / 2 ) + cgaz_maxPos, y+(h)+(bar_height/2), cgaz_maxWidth, bar_height-10)
  end

end

-------------------------------------
-- Right Side UI.
-------------------------------------
function HUD_VEL:Draw3D(ply, scrw, scrh)
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  local localPly = Spectate.IsSpectating(ply) or ply
  local x, y        = 0, scrh-200      // X and Y position
  local w, h        = scrw, 5            // Width and Height
  local vel         = ply:Speed2D()
  local velPos      = x
  local vBWidth     = 500
  local vBHeight    = 20
  local alpha = 255
  local color = ColorAlpha(hud.customColor, alpha)
  local kEPos       = x+w-(95*3)-(h*4)
  local kE_w, kE_h = 95, 95

  local a = hud.ease.vel.Ease:update(1*FrameTime())

  if vel >= 1 then
    hud.ease.vel.FadeTime = CurTime()+2.5
  end
  if hud.ease.vel.FadeTime <= CurTime() and vel == 0 then
    hud.ease.vel.W = 0
    hud.ease.vel.Alpha = 0
  elseif vel > 0 or hud.ease.vel.FadeTime > CurTime() then
    hud.ease.vel.W = vBWidth
    hud.ease.vel.Alpha = alpha

  end
  hud.ease.vel.Ease:Target({alpha = hud.ease.vel.Alpha, W = hud.ease.vel.W})

  vBWidth = hud.ease.vel.Ease:get().W
  alpha = hud.ease.vel.Ease:get().alpha
  color = ColorAlpha(hud.customColor, alpha)

  // Default line
  surface.SetDrawColor( Color(0,0,0,100) )
  surface.DrawRect( x, y+(h/2)-(vBHeight/2), vBWidth, vBHeight )
  --surface.DrawRect( x, y, w, h )

  vBWidth     = vBWidth-5
  vBHeight    = vBHeight-10
  local TSS = math.Clamp(localPly:TSS(), 1000, localPly:TSS())
  local maxVelWidth = (vBWidth)/TSS
  local velWidth    = math.Clamp(vel,0, TSS)*maxVelWidth
  local hundVel = math.floor((math.floor( vel/50 )*50)/100)
  velBar = velWidth or UT:Hermite(0.25, velBar, velWidth) and velBar
  surface.SetDrawColor( color )
  surface.DrawRect( velPos, y+(h/2)-(vBHeight/2), velBar, vBHeight )

  local delaycol = Color(color.r, color.g, color.b, math.Clamp(color.a - 105, 0 ,255))
  surface.SetDrawColor( delaycol )
  delayedvelBar = delayedvelBar and math.Clamp(UT:Hermite(0.15, delayedvelBar, velWidth), velBar, 999) or velWidth
  surface.DrawRect( velPos, y+(h/2)-(vBHeight/2), delayedvelBar, vBHeight )

  SetFont("HUD Speed", {
    font = "Ostrich Sans",
    weight = 600,
    size = 75,
    antialias = true
  })
  surface.SetTextColor( color )
  local velX, velY = surface.GetTextSize( math.Round( vel ) )
  surface.SetTextPos( velPos+(vBWidth/2)-(velX/2),  y-velY-10)
  surface.DrawText( math.Round( vel ) )

  // Aestethics

  surface.SetDrawColor( Color(0,0,0,200) )
  surface.DrawRect( x-5, y-15-4, 10, 60+8 )
  surface.SetDrawColor( hud.color )
  surface.DrawRect( x-2, y-15, 4, 60 )

end

-------------------------------------
-- Top UI.
-------------------------------------
function HUD_TOP:Draw3D(ply, scrw, scrh)
  if console.CmdVar("hud_enabled") == 0 then
    return
  end
  local x, y      = -scrw+200, -scrh+100      // X and Y position
  local w, h      = ((scrw+100)*2), 5            // Width and Height
  local localPly = Spectate.IsSpectating(LocalPlayer()) or ply
  local combo = localPly:Combo()
  local pts = Combo:Calc(combo)
  local rank = calcRank(localPly:Points())
  // Default line
  surface.SetDrawColor( hud.customColor )
  --surface.DrawRect( x, y, w, h )
  if lobby:getPlayerLobby(localPly) != "None" then


    local player = lobby:getPlayerLobby(localPly).minigamePlayers[localPly]
    local teams = lobby:getPlayerLobby(localPly).minigameTeams[player]
    local teamCol = lobby:getPlayerLobby(localPly).minigameColors[player]
    SetFont("HUD Team", {
      font = "Ostrich Sans",
      weight = 600,
      size = 75,
      antialias = true
    })
    //PrintTable(lobby:getPlayerLobby(localPly).minigamePlayers)
    local txtx, txty = surface.GetTextSize( teams )
    surface.SetTextColor( teamCol )
    surface.SetTextPos( (x+-600+scrw*2)-(txtx/2),  y)
    surface.DrawText( teams )
  end

  SetFont("HUD Rank", {
    font = "SIMPLIFICA",
    weight = 1000,
    size = 60,
    antialias = true
  })
  local txtx, txty = surface.GetTextSize( rank )
  surface.SetTextColor( rankColor(rank) )
  surface.SetTextPos( x,  y-txty)
  surface.DrawText( rank )
  SetFont("HUD Combo", {
    font = "SIMPLIFICA",
    weight = 1000,
    size = 55,
    antialias = true
  })
  local combos = {}
  for i=#combo, 1, -1 do

    if #combo > 0 then

      local line = table.GetKeys( combo[i] )[1]
      local prev = ""
      if #combos > 0 then

        prev = table.GetKeys( combos[#combos] )[1]
      end
      if prev != line then
        combos[ #combos+1] = {[line] = 1}
      else

        combos[#combos] = {[line] = combos[#combos][line]+1}
      end
      if table.Count(combos) >= 5 then
        break
      end
    end
  end
  --PrintTable(combos)
  for k, line in pairs(combos) do
    local comboName = table.GetKeys( line )[1]
    local repeats = line[comboName]
    local txtx, txty = surface.GetTextSize( comboName )
    surface.SetTextColor( hud.customColor )
    surface.SetTextPos( x,  y+((10+txty)*k)-txty)
    surface.DrawText( repeats .. "X " .. comboName )
  end
  if #combo > 0 then
    local txtx, txty = surface.GetTextSize( pts )
    surface.SetTextColor( hud.customColor )
    surface.SetTextPos( x,  y+((10+txty)*math.min(#combos+1, 6))-txty)
    surface.DrawText( "PTS: " .. math.Round(pts+ply:StrafePts(),2) )

    local timer = ply:ComboTimer()
    local txtx, txty = surface.GetTextSize( timer )
    surface.SetTextColor( hud.customColor )
    surface.SetTextPos( x,  y+((10+(txty))*math.min(#combos+1, 6)))
    surface.DrawText( "Time: " .. math.Round(math.max(timer-CurTime(), 0 ),2) )
  end
end
