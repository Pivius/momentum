local open = false

PLAYER_LINE =
{
	Init = function( self )
    SetFont("Score_P_Info", {
      font = "SIMPLIFICA",
      weight = 600,
      size = 22,
    --  antialias = true
    })
    self:Dock( TOP )
		--self:DockPadding( 3, 3, 3, 3 )
		self:SetHeight( 32 )
		--self:DockMargin( 2, 0, 2, 2 )

    self.infoOpened = false
    local w, h = self:GetSize()
    self.LineButton = self:Add( "DButton", self )
    self.LineButton:SetSize( 700, h )
    self.LineButton:SetText("")
    self.LineButton.DoClick = function()
      if self.infoOpened then
        self.infoOpened = false
      else
        self.infoOpened = true
      end
    end
    self.LineButton:SetAlpha(150)

    self.LineButton.Paint = function() end

		self.AvatarButton = self:Add( "DButton" )
		self.AvatarButton:SetSize( 28, 28 )
		self.AvatarButton.DoClick = function()
      --self.Player:ShowProfile()
      gui.OpenURL( "https://steamcommunity.com/profiles/"..self.PURL )
    end

		function self.AvatarButton:Paint(w, h)
			return true
		end

		self.AvatarButton:Dock( LEFT )
		self.AvatarButton:DockMargin( 28-7, 2, 0, 0 )

		self.Avatar		= vgui.Create( "AvatarImage", self.AvatarButton )
		self.Avatar:SetSize( 28, 28  )
		self.Avatar:SetMouseInputEnabled( false )

    self.Rank= self:Add( "DLabel" )
		self.Rank:SetFont( GetFont("Score_P_Info") )
		self.Rank:SetTextColor(Color(255, 255, 255))
		self.Rank:Dock( LEFT )
		self.Rank:DockMargin( 105, 0, -25, 0 )
		self.Rank:SetContentAlignment( 5 )

		self.Name		= self:Add( "DLabel" )
		self.Name:SetFont( GetFont("Score_P_Info") )
		self.Name:SetTextColor(Color(255, 255, 255))
		self.Name:Dock( LEFT )
		self.Name:DockMargin( 0, 0, 0, 0 )
    self.Name:SetContentAlignment( 5 )

		self.Speed= self:Add( "DLabel" )
		self.Speed:SetFont( GetFont("Score_P_Info"))
		self.Speed:SetTextColor(Color(255, 255, 255))
		self.Speed:SetContentAlignment( 5 )
		self.Speed:Dock( LEFT )
		self.Speed:SetWidth( 75 )
		self.Speed:DockMargin( 0, 0, 0, 0 )
    self.FinishInit = true

		self.TSpeed= self:Add( "DLabel" )
		self.TSpeed:SetWidth( 75 )
		self.TSpeed:SetFont( GetFont("Score_P_Info"))
		self.TSpeed:SetTextColor(Color(255, 255, 255))
		self.TSpeed:Dock( LEFT )
		self.TSpeed:SetContentAlignment( 5 )

		self.Points= self:Add( "DLabel" )
    self.Points:SetWidth( 50 )
    self.Points:SetFont( GetFont("Score_P_Info") )
    self.Points:SetTextColor(Color(255, 255, 255))
		self.Points:Dock( LEFT )
		self.Points:DockMargin( 15, 0, 0, 0 )
    self.Points:SetContentAlignment( 5 )

		self.Latency= self:Add( "DLabel" )
    self.Latency:SetWidth( 50 )
    self.Latency:SetFont( GetFont("Score_P_Info") )
    self.Latency:SetTextColor(Color(255, 255, 255))
		self.Latency:Dock( LEFT )
		self.Latency:DockMargin( 15, 0, 0, 0 )
    self.Latency:SetContentAlignment( 5 )

	end,

	Setup = function( self, pl )

		self.Player = pl

		self.Avatar:SetPlayer( pl )

		self:Think( self )

	end,

	Think = function( self )
    local ply = self.Player
		if ( !IsValid( ply ) ) then
      self:SetZPos( 9999 ) -- Causes a rebuild
			self:Remove()
			return
		end
    local nick = ply:Nick()
    local steamid = ply:SteamID()
		local topspeed = 0
		local rank = getRanks()[1][1]
		local pts = 0
		if !stats.session[ply:SteamID()] then
			rank = getRanks()[1][1]
			pts = 0
		else
			rank = calcRank(stats.session[ply:SteamID()]["pts"])
			pts = stats.session[ply:SteamID()]["pts"]
		end
		--PrintTable(stats.session)
		if stats.session[ply:SteamID()] then
			topspeed = stats.session[ply:SteamID()].speed
		end

		if self.Name:GetText() != nick then
    	SetFont("Score_P_Info")
			local txtx, txty = surface.GetTextSize( nick )
			local titlex, titley = surface.GetTextSize( "NAME" )
			self.Name:Dock( LEFT )
			self.Name:DockMargin( 105+(titlex)-(txtx/2), 0, -(titlex)-(txtx/2)+150, 0 )

			self.Name:SetWide(txtx)
		end

		self.Name:SetText(nick)
    self.PURL = util.SteamIDTo64( steamid )
    self.TSpeed:SetText(math.Round(tonumber(topspeed)))
    self.Speed:SetText(math.Round(ply:Speed2D()/ 10)*10)
    self.Latency:SetText(ply:Ping())
		self.Points:SetText(math.Round(pts,0))

		if self.Rank:GetText() != rank then
    	SetFont("Score_P_Info")
			local txtx, txty = surface.GetTextSize( rank )
			local titlex, titley = surface.GetTextSize( "RANK" )
			self.Rank:Dock( LEFT )
			self.Rank:DockMargin( 75+(titlex)-(txtx/2), 0, -(titlex)-(txtx/2)+83, 0 )
			self.Rank:SetColor(rankColor(rank))
			self.Rank:SetWide(txtx)
		end

		self.Rank:SetText(rank)

		--
		-- Connecting players go at the very bottom
		--
		if ( ply:Team() == TEAM_CONNECTING ) then
			self:SetZPos( 2000 )
		end

	end,
	Paint = function( self, w, h )
    if self.LineButton:IsHovered() then
			self.LineButton:SetAlpha(Lerp(0.05, self.LineButton:GetAlpha(), 225))
    else
      self.LineButton:SetAlpha(Lerp(0.05, self.LineButton:GetAlpha(), 150))
    end
		if ( !IsValid( self.Player ) ) then
			return
		end
		surface.SetDrawColor(Color(0, 0, 0, self.LineButton:GetAlpha()))
		surface.DrawRect(0, 0, w, h)
	end
}

PLAYER_LINE = vgui.RegisterTable( PLAYER_LINE, "DPanel" )

local SCORE_BOARD =
{
	Init = function( self )
    SetFont("Score_Info", {
      font = "Ostrich Sans",
      weight = 660,
      size = 15,
      antialias = true
    })
    self.Bar= self:Add( "EditablePanel" )

    self.Bar:SetPos( 0, 41 )
    self.Bar:SetSize(ScrW() - (250*2), 30)
    self.Bar.Paint = function(self, w, h )
      surface.SetDrawColor( Color(15,15,15,170) )
      surface.DrawRect(0, 0, w, h)
    end

    self.Profile= self.Bar:Add( "DLabel" )
		self.Profile:SetFont( GetFont("Score_Info") )
		self.Profile:SetTextColor(Color(255, 255, 255))
    self.Profile:SetText("Profile")
		self.Profile:Dock( LEFT )
		self.Profile:DockMargin( 5, 0, -20, 0 )
		self.Profile:SetContentAlignment( 5 )

    self.Rank= self.Bar:Add( "DLabel" )
    --self.Profile:SetPos( 10, 30+(30/2)+2 )
		self.Rank:SetFont( GetFont("Score_Info") )
		self.Rank:SetTextColor(Color(255, 255, 255))
    self.Rank:SetText("Rank")
		self.Rank:Dock( LEFT )
		self.Rank:DockMargin( 72, 0, -30, 0 )
		self.Rank:SetContentAlignment( 5 )

    self.Name= self.Bar:Add( "DLabel" )
    --self.Name:SetPos( 156, 30+(30/2)+2 )
    self.Name:SetFont( GetFont("Score_Info") )
    self.Name:SetTextColor(Color(255, 255, 255))
    self.Name:SetText("Name")
		self.Name:Dock( LEFT )
		self.Name:DockMargin( 157	, 0, -25, 0 )
		self.Name:SetContentAlignment( 5 )

		self.Speed= self.Bar:Add( "DLabel" )
    self.Speed:SetFont( GetFont("Score_Info") )
    self.Speed:SetTextColor(Color(255, 255, 255))
    self.Speed:SetText("Speed")
		self.Speed:Dock( LEFT )
		self.Speed:DockMargin( 118, 0, -10, 0 )
		self.Speed:SetContentAlignment( 5 )

		self.Topspeed = self.Bar:Add( "DLabel" )
    self.Topspeed:SetPos( 588, 30+(30/2)+2 )
    self.Topspeed:SetFont( GetFont("Score_Info") )
    self.Topspeed:SetTextColor(Color(255, 255, 255))
    self.Topspeed:SetText("Topspeed")
		self.Topspeed:Dock( LEFT )
		self.Topspeed:DockMargin( 21, 0, -10, 0 )
		--self.Topspeed:SetWidth( 100 )
		self.Topspeed:SetContentAlignment( 5 )

		self.Points = self.Bar:Add( "DLabel" )
    self.Points:SetPos( 588, 30+(30/2)+2 )
    self.Points:SetFont( GetFont("Score_Info") )
    self.Points:SetTextColor(Color(255, 255, 255))
    self.Points:SetText("Points")
		self.Points:Dock( LEFT )
		self.Points:DockMargin( 25, 0, -25, 0 )
		--self.Points:SetWidth( 100 )
		self.Points:SetContentAlignment( 5 )

		self.Latency= self.Bar:Add( "DLabel" )
    self.Latency:SetPos( 588, 30+(30/2)+2 )
    self.Latency:SetFont( GetFont("Score_Info") )
    self.Latency:SetTextColor(Color(255, 255, 255))
    self.Latency:SetText("Ping")
	  self.Latency:Dock( LEFT )
		self.Latency:DockMargin( 25, 0, -10, 0 )
    --self.Latency:SetWidth( 100 )
		self.Latency:SetContentAlignment( 5 )

		self.Header = self:Add( "Panel" )
		self.Header:Dock( TOP )
		self.Header:SetHeight( 72 )

		self.List = self:Add( "DScrollPanel" )
		self.List:Dock( FILL )
		self.List:SetVerticalScrollbarEnabled(false)
    /*
    self.List.Paint = function(self, w, h )
      surface.SetDrawColor( Color(15,15,15,170) )
      surface.DrawRect(0, 0, w, h)
    end*/
		self.List.VBar:SetAlpha(0)

		self.List.offset = 0


		function self.List:OnVScroll(iOffset)
			self.offset = iOffset
		end

		function self.List:Think()
      self.offsetsmooth = self.offsetsmooth and UT:Lerp(0.03, self.offsetsmooth , self.offset) or self.offset

      self.pnlCanvas:SetPos( 0, self.offsetsmooth )
		end


		function self.List:PerformLayout()
      local wide = self:GetWide()

      self:Rebuild()

      self.VBar:SetUp( self:GetTall(), self.pnlCanvas:GetTall() )

      if self.VBar.Enabled then wide = wide - self.VBar:GetWide() end

      self.pnlCanvas:SetWide(wide)

      self:Rebuild()
		end
	end,

	PerformLayout = function( self )

		self:SetSize( ScrW() - (250*2), ScrH()-100 )
		self:SetPos( 250, 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )

		-- table.sort(plyrs, function(a, b) return string.lower(a:Nick()) < string.lower(b:Nick()) end)
		for id, pl in pairs( player.GetAll() ) do

			if ( IsValid( pl.ScoreEntry ) ) then continue end
      --if ( IsValid( pl.ScoreInfo ) ) then continue end

			pl.ScoreEntry = vgui.CreateFromTable( PLAYER_LINE, pl.ScoreEntry )
			pl.ScoreEntry:Setup( pl )

      --pl.ScoreInfo = vgui.CreateFromTable( PLAYER_LINE_INFO, pl.ScoreInfo )
      --pl.ScoreInfo:Setup( pl )

			self.List:AddItem( pl.ScoreEntry )
      --self.List:AddItem( pl.ScoreInfo  )

		end

	end
}

SCORE_BOARD = vgui.RegisterTable( SCORE_BOARD, "EditablePanel" );

if ( IsValid( Scoreboard ) ) then
	Scoreboard:Remove()
end

--[[---------------------------------------------------------
   Name: gamemode:ScoreboardShow( )
   Desc: Sets the scoreboard to visible
-----------------------------------------------------------]]
function GM:ScoreboardShow()
	open = true

	if ( !IsValid( Scoreboard ) ) then
		Scoreboard = vgui.CreateFromTable( SCORE_BOARD )
	end

	if ( IsValid( Scoreboard ) ) then
    Scoreboard:AlphaTo(255, 0.15)
		Scoreboard:Show()
		Scoreboard:MakePopup()
		Scoreboard:SetKeyboardInputEnabled( false )
	end

end

--[[---------------------------------------------------------
   Name: gamemode:ScoreboardHide( )
   Desc: Hides the scoreboard
-----------------------------------------------------------]]
function GM:ScoreboardHide()
	open = false

	if ( IsValid( Scoreboard ) ) then
		timer.Simple(0.2, function()
			if not open then
				Scoreboard:Hide()
			end
		end)

		Scoreboard:AlphaTo(0, 0.15)
	end


end


--[[---------------------------------------------------------
   Name: gamemode:HUDDrawScoreBoard( )
   Desc: If you prefer to draw your scoreboard the stupid way (without vgui)
-----------------------------------------------------------]]
function GM:HUDDrawScoreBoard()

end
