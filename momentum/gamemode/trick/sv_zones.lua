
Zones = {}
Zones.Ents = Zones.Ents or {}
Zone = {}
Zones.ZoneTable = {}
Zones.Cache = {}
bRetry = 0

if Zones.Ents then
	for k,v in pairs( Zones.Ents ) do

		if IsValid(v) then

			v:Remove()
			v = nil
		end
	end

end

--NET
util.AddNetworkString( "Create Zone" )
util.AddNetworkString( "Update Zone" )
util.AddNetworkString( "Update" )
util.AddNetworkString( "Remove Zone" )
-------------------------------------
-- SQL.
-------------------------------------
function Zones:QInsert(name, id, p1, p2, mode)
	local point1 = util.TypeToString( p1 )
	local point2 = util.TypeToString( p2 )
	local query = mysql:Insert(SQLDetails.tables.zones)
		query:Insert("map", game.GetMap())
		query:Insert("id", id)
		query:Insert("name", name)
		query:Insert("p1", point1)
		query:Insert("p2", point2)
		query:Insert("mode", mode)
	query:Execute()
end

function Zones:QUpdate(name, id, p1, p2, mode)
	local point1 = util.TypeToString( p1 )
	local point2 = util.TypeToString( p2 )
	local query = mysql:Update(SQLDetails.tables.zones)
		query:Where("map", game.GetMap())
		query:Where("id", id)
		query:Update("name", name)
		query:Update("p1", point1)
		query:Update("p2", point2)
		query:Update("mode", mode)
	query:Execute()
end

function Zones:QRemove(id)
	local query = mysql:Delete(SQLDetails.tables.zones)
		query:Where("map", game.GetMap())
		query:Where("id", id)
	query:Execute()
end

-------------------------------------
-- Creates physical zone.
-------------------------------------
function Zones:Add(v)
	if v then
		table.insert(Zones.Cache, tab)
		Zones:QInsert(v["Name"], v["ID"], v["Point1"], v["Point2"], v["Mode"])
	end
end

function Zones:Delete(id, zone)
	table.remove(Zones.Cache, id)
	Zones:QRemove(id)
	Zones:Reload()
	for k, v in pairs(Zones.Cache) do

		if Zones.Cache[k].ID > id then

			local newid = Zones.Cache[k].ID - 1
			local point1 = util.TypeToString( Zones.Cache[k].Point1 )
			local point2 = util.TypeToString( Zones.Cache[k].Point2 )
			local query = mysql:Update(SQLDetails.tables.zones)
				query:Where("map", game.GetMap())
				query:Where("id", Zones.Cache[k].ID)
				query:Update("id", newid)
				query:Update("name", Zones.Cache[k].Name)
				query:Update("p1", point1)
				query:Update("p2", point2)
				query:Update("mode", Zones.Cache[k].Mode)
			query:Execute()

		end
	end
	timer.Create("Refresh", 1,1, function() table.Empty(Zones.Cache) Zones:Load() end)
	PrintTable(Zones.Cache)
end

function Zones:Setup()
	for k,v in pairs( Zones.Cache ) do

		local ent = ents.Create( "zone" )
		ent:SetPos( v.Point1 )
		ent.min = v.Point1
		ent.max = v.Point2
		ent.name = v.Name
		ent.mode = v.Mode
		ent:Spawn()
		UT:AddToTable(Zones.Ents, ent)
		Zones:QUpdate(ent.name, v.ID, ent.min, ent.max, ent.mode)
	end
end

function Zones:Reload()
	Zones:Setup()
	for k,v in pairs( Zones.Ents ) do

		if IsValid(v) then

			v:Remove()
			v = nil
		end
	end

	Zones.Ents = {}

	Zones:Setup()

end

net.Receive("Create Zone", function(len, ply)
	tab = net.ReadTable()
	Zones:Add(tab)

	Zones:Reload()
end)

net.Receive("Update", function(len, ply)
	tab = net.ReadTable()
	table.SortByMember( Zones.Cache, "ID", true )

	Zones.Cache[tab.ID] = tab
	PrintTable(Zones.Cache[tab.ID])
	--print(Zones.Cache[tab.ID]["Name"])
	Zones:QUpdate(tab.Name, tab.ID, tab.Point1, tab.Point2, tab.Mode)
	Zones:Reload()
	--Zones:Reload()
end)

net.Receive("Remove Zone", function(len, ply)
	tab = net.ReadTable()
	Zones:Delete(tab.ID, tab.Name)
end)
-------------------------------------
-- Reset Zones.
-------------------------------------
function Zones:Reset()
	local query = mysql:Delete(SQLDetails.tables.zones)
	query:Execute()
end


-------------------------------------
-- Load Zones.
-------------------------------------
function Zones:Load()

	local query = mysql:Select(SQLDetails.tables.zones)
	query:Callback(function(result)
		if (type(result) == "table" and #result > 0) then

			for k, v in pairs(result) do
				local map = v["map"]
				local id  = v["id"]
				local name = v["name"]
				local p1 = v["p1"]
				local p2 = v["p2"]
				local mode = v["mode"]
				if map == game.GetMap() then
					local tbl = {
						[id] = {
							["Map"] = map,
							["ID"]  = id,
							["Name"] = name,
							["Point1"] = util.StringToType( tostring(p1), "Vector" ),
							["Point2"] = util.StringToType( tostring(p2), "Vector" ),
							["Mode"] = mode
						}
					}
					UT:MergeTableTo(Zones.Cache, tbl)

				end
			end
		end
	end)
	query:Execute()

end


timer.Create("Testing123", 1,1, function()Zones:Reload() end)
function Zones:AwaitLoad( Retry )

	if not Retry then end
	if #table.GetKeys( Zones.Cache ) > 0 then
		--Zones:Setup()
		bRetry = 0
	else
		if bRetry < 100 then
			bRetry = bRetry + 1
			Zones:Load()
			if #table.GetKeys( Zones.Cache ) == 0 then
				print( "Couldn't load data. Retrying (Try " .. bRetry .. ")" )
			end

			timer.Simple( 5, function() Zones:AwaitLoad( true ) end )
		end
	end
end

-------------------------------------
-- Lists Zones.
-------------------------------------

function Zones:Find(zone)
	local pattern = zone:find( "[%a%c%p%s]" )
	if pattern then
		for k, v in pairs(Zones.Cache) do
			if v.Name == zone then
				return v
			end
		end
	else
		return Zones.Cache[tonumber(zone)]
	end
	return false
end



-------------------------------------
-- Update Zone to player.
-------------------------------------

function UpdateZones()
	local znch = Zones.Cache

	--PrintTable(znch)
	net.Start("Update Zone")

		net.WriteTable(znch)

	net.Send(Admin:GetPlayerFrom("owner"))
end
timer.Create( "UpdateZones", 1, 0, UpdateZones )
