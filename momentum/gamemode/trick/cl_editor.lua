editor = editor or {}
editor.open = false
editor.verts = editor.verts or {}
editor.verts.table = editor.verts.table or {}
editor.verts.selected = 0
editor.keys =  {["U"] = 0, ["L"] = 0, ["R"] = 0, ["D"] = 0}
editor.mode = "enter"
editor.addmode = "Trace"

local function CreatePoint()
  local tracedata = {}
  local ply = LocalPlayer()
  if editor.addmode == "Trace" then
    tracedata.start = ply:EyePos()
    tracedata.endpos = ply:EyePos()+(ply:GetAimVector())*250
    tracedata.filter = player.GetAll()
    local trace = util.TraceLine(tracedata)
  elseif editor.addmode == "Feet" then
    tracedata.start = ply:GetPos()
    tracedata.endpos = ply:GetPos()
    tracedata.filter = player.GetAll()
  end
  local trace = util.TraceLine(tracedata)
  editor.verts.selected = math.Clamp(editor.verts.selected+1, 1, 99)
  table.insert(editor.verts.table, editor.verts.selected, trace.HitPos+(trace.HitNormal*4))
end

local function RemovePoint(n)
  for k, v in pairs(editor.verts.table ) do
    if k >= n then
      if k == #editor.verts.table then
        table.remove(editor.verts.table, k)
      else
        editor.verts.table[k] = editor.verts.table[k+1]
      end
      if editor.verts.selected >= n then
          editor.verts.selected = math.Clamp(n-1, 1, 99)
      end
    end
  end
end

local function Ray(pos, ang, scale, x, y, w, h)
  local trace = util.IntersectRayWithPlane(LocalPlayer():EyePos(), LocalPlayer():GetAimVector(), pos, ang:Up())
  if trace then
    local pos = WorldToLocal(trace, Angle(), pos, ang)
    local xpos = pos.x / scale
    local ypos = (-pos.y) / scale
    local px, py, sx, sy = x, y, w, h
    local hover = xpos >= px and ypos >= py and xpos <= px + sx and ypos <= py + sy
    return hover
  else
    return false
  end
end

function PointEdit()
  local ply = LocalPlayer()
  if !ply:Editing() then return end

  local ang =  Angle(0,ply:EyeAngles().y-90,-ply:EyeAngles().p+90)
  local plyAng = ply:GetAngles():Right()
  plyAng:Normalize()
  local side =  Vector(math.Round(plyAng.x), math.Round(plyAng.y), math.Round(plyAng.z))
  if editor.verts.selected > 0 then
    if input.IsKeyDown( KEY_UP ) and editor.keys["U"] == 0 then
      editor.keys["U"] = 1
      editor.verts.table[editor.verts.selected]:Add( Vector( 0, 0, 1 ) )
    elseif !input.IsKeyDown( KEY_UP ) and editor.keys["U"] == 1 then
      editor.keys["U"] = 0
    end
    if input.IsKeyDown( KEY_LEFT ) and editor.keys["L"] == 0 then
      editor.keys["L"] = 1
      editor.verts.table[editor.verts.selected]:Add( Vector( -side.x, -side.y, 0 ) )
    elseif !input.IsKeyDown( KEY_LEFT ) and editor.keys["L"] == 1 then
      editor.keys["L"] = 0
    end
    if input.IsKeyDown( KEY_DOWN ) and editor.keys["D"] == 0 then
      editor.keys["D"] = 1
      editor.verts.table[editor.verts.selected]:Add( Vector( 0, 0, -1 ) )
    elseif !input.IsKeyDown( KEY_DOWN ) and editor.keys["D"] == 1 then
      editor.keys["D"] = 0
    end
    if input.IsKeyDown( KEY_RIGHT ) and editor.keys["R"] == 0 then
      editor.keys["R"] = 1
      editor.verts.table[editor.verts.selected]:Add( Vector( side.x, side.y  , 0 ) )
    elseif !input.IsKeyDown( KEY_RIGHT ) and editor.keys["R"] == 1 then
      editor.keys["R"] = 0
    end
  end
  for _, v in pairs(editor.verts.table) do
    cam.Start3D2D(v, ang, 0.2)
      /*surface.SetDrawColor(0, 0, 0, 100)
    	surface.DrawRect(-350/1.9, -250, 350, 300)*/
      local px, py, sx, sy = -350/1.9, -250, 350, 300
      //All credits to handsomematt's 3d2dvgui for this part
      if Ray(v, ang, 0.2, px, py, sx, sy) then
        SetFont("TE_Node_Name", {
        font = "SIMPLIFICA",
        weight = 700,
        size = 70,
        antialias = true
        })
        local txt_w, txt_h = surface.GetTextSize(_)
        surface.SetDrawColor(0, 0, 0, 100)
      	surface.DrawRect(-((txt_w/2)+10), py, txt_w+10, 70)
        surface.SetTextColor(Color(255,255,255))
        surface.SetTextPos(-((txt_w/2)+5/2), py+(70-txt_h))
        surface.DrawText(_)

        //SELECT
        local txt_w, txt_h = surface.GetTextSize("Select")
        if Ray(v, ang, 0.2, -((txt_w/2)+10), py+80, txt_w+10, 70) then
          surface.SetDrawColor(0, 0, 0, 200)
          if ply:KeyDown( IN_USE ) then
            editor.verts.selected = _
          end
        else
          surface.SetDrawColor(0, 0, 0, 100)
        end
      	surface.DrawRect(-((txt_w/2)+10), py+80, txt_w+10, 70)
        surface.SetTextColor(Color(255,255,255))
        surface.SetTextPos(-((txt_w/2)+5/2), (py+80)+(70-txt_h))
        surface.DrawText("Select")

        //REMOVE
        local txt_w, txt_h = surface.GetTextSize("Remove")
        if Ray(v, ang, 0.2, -((txt_w/2)+10), py+(80*2), txt_w+10, 70) then
          surface.SetDrawColor(0, 0, 0, 200)

          if ply:KeyDown( IN_USE ) then
            RemovePoint(_)
          end
        else
          surface.SetDrawColor(0, 0, 0, 100)
        end
      	surface.DrawRect(-((txt_w/2)+10), py+(80*2), txt_w+10, 70)
        surface.SetTextColor(Color(255,255,255))
        surface.SetTextPos(-((txt_w/2)+5/2), (py+(80*2))+(70-txt_h))
        surface.DrawText("Remove")
      end
    cam.End3D2D()
  end
end
hook.Add("PostDrawTranslucentRenderables", "PointEdit", PointEdit)
/*
function traceGrid(pos, w, h)
  local grid = {}
  table.insert(grid, pos)
  local function testTraceUp(pos, dir, acc)
    local tracedataUp = {}
    tracedataUp.start = pos
    tracedataUp.endpos = pos+(pos:Up()*acc)
    tracedataUp.filter = player.GetAll()
    local traceUp = util.TraceLine(tracedataUp)
    if !traceUp.HitWorld then
      local tracedata = {}
      tracedata.start = traceUp.HitPos
      tracedata.endpos = traceUp.HitPos+(dir*w)
      tracedata.filter = player.GetAll()
      local trace = util.TraceLine(tracedata)
      if trace.HitWorld then
        testTraceUp(trace.HitPos, dir, acc)
      else
        return trace.HitPos
      end
    end
  end
  local trDRight = {}
  trDRight.start = pos
  trDRight.endpos = pos+(pos:Right()*w)
  trDRight.filter = player.GetAll()
  local trR = util.TraceLine(trDRight)

  local trDLeft = {}
  trDLeft.start = pos
  trDLeft.endpos = pos-(pos:Right()*w)
  trDLeft.filter = player.GetAll()
  local trL = util.TraceLine(trDLeft)

  local trDForward = {}
  trDForward.start = pos
  trDForward.endpos = pos+(pos:Forward()*w)
  trDForward.filter = player.GetAll()
  local trF = util.TraceLine(trDForward)

  local trDBack = {}
  trDBack.start = pos
  trDBack.endpos = pos-(pos:Forward()*w)
  trDBack.filter = player.GetAll()
  local trB = util.TraceLine(trDBack)

  local trDUp = {}
  trDUp.start = pos
  trDUp.endpos = pos+(pos:Up()*h)
  trDUp.filter = player.GetAll()
  local trU = util.TraceLine(trDUp)

  local trDDown = {}
  trDDown.start = pos
  trDDown.endpos = pos-(pos:Up()*h)
  trDDown.filter = player.GetAll()
  local trD = util.TraceLine(trDDown)
end
*/
/*
  pos - origin
  grid - grid size
  w - length of the grid
  h - height of the grid
  lw - line width
  col - color
*/
function drawGrid(pos, grid, w, h, lw, col, hitnorm)
  local endcol = Color(col.r,col.g,col.b,0)

  local function renderLine(dir, start, gr)
    if !start then
      start = pos
    end
    if !gr then
      gr = 1
    end
    for i=gr, math.Round(w/grid) do
      local pos = start -(dir*grid)
      local Col = Color(col.r, col.g, col.b, Lerp(i/math.Round(w/grid)/1.1,col.a, 0))
      local eCol = Color(col.r, col.g, col.b, Lerp((i+1)/math.Round(w/grid)/1.1,col.a, 0))
      local startPos = pos+((dir*grid)*(i-(gr-1)))
      local endPos = startPos + ((dir*grid))
      render.StartBeam( 2 )
        render.AddBeam( startPos, lw, 1, Col)
        render.AddBeam( endPos, lw, 1, eCol)
      render.EndBeam()
    end
  end

  local function Branch(dir, brDir, origin, grPos)
    if !origin then
      origin = pos
    end
    if !grPos then
      grPos = 2
    end
    for i=grPos, math.Round(w/grid) do
      local wide = Vector(-lw/2-0.1,0,0)
      if dir.x < 0 then
        wide = Vector(lw/2,0,0)
      else
        wide = Vector(-lw/2,0,0)
      end
      local brS = origin+((brDir*grid)*(i-1))-wide
      renderLine( dir, brS, i )
      renderLine( -dir, brS, i )
      local brU = origin+((Vector(0,0,math.Clamp(brS.x+brS.y, -1, 1))*grid)*(i-1))-wide
      --renderLine( Vector(0,0,math.Clamp(dir.x+dir.y,-1,1)), brS, i )
      --renderLine( -Vector(0,0,math.Clamp(dir.x+dir.y,-1,1)), brS, i )
    end
  end
  if hitnorm:Length() == 0 then
    local plyAng = LocalPlayer():GetAngles():Forward()
    plyAng:Normalize()
    hitnorm = Vector(math.Round(plyAng.x/1),math.Round(plyAng.y/1),math.Round(plyAng.z/1))*1
  end
  //Right
  renderLine(Vector(-hitnorm.y, math.Clamp(hitnorm.x+hitnorm.z,-1,1), 0))
  Branch(Vector(0,0,math.Clamp(hitnorm.y+hitnorm.x,-1,1)), Vector(-hitnorm.y, math.Clamp(hitnorm.x+hitnorm.z,-1,1), 0))
  //Left
  renderLine(-Vector(-hitnorm.y, math.Clamp(hitnorm.x+hitnorm.z,-1,1), 0))
  Branch(Vector(0,0,math.Clamp(hitnorm.y+hitnorm.x,-1,1)), -Vector(-hitnorm.y, math.Clamp(hitnorm.x+hitnorm.z,-1,1), 0))
  //Forward
  renderLine(Vector(-hitnorm.z, 0, math.Clamp(hitnorm.y+hitnorm.x,-1,1)))
  --Branch(Vector(0,1,0), Vector(1,0,0))
  //Back
  renderLine(-Vector(-hitnorm.z, 0, math.Clamp(hitnorm.y+hitnorm.x,-1,1)))
  --Branch(Vector(0,1,0), Vector(-1,0,0))
  //Up
  --renderLine(Vector(0,0,1))
  //Down
  --renderLine(Vector(0,0,-1))

  /*
  for i=1, math.Round(w/grid) do
    Branch(Vector(0,0,1), Vector(1,0,0), pos+Vector(-grid*i,grid*i,0), i+2)
  end
  for i=1, math.Round(w/grid) do
    Branch(Vector(0,0,1), Vector(1,0,0), pos+Vector(-grid*i,grid*i,0), i+2)
  end
  for i=1, math.Round(w/grid) do
    Branch(Vector(0,0,1), Vector(-1,0,0), pos+Vector(grid*i,grid*i,0), i+2)
  end*/
  /*
  //Right
  render.StartBeam( 2 )
    render.AddBeam( pos, 2, 1, col)
    render.AddBeam( pos-Vector(w,0,0), 2, 1, endcol)
  render.EndBeam()

  //Forward
  render.StartBeam( 2 )
    render.AddBeam( pos, 2, 1, col)
    render.AddBeam( pos+Vector(0,w,0), 2, 1, endcol)
  render.EndBeam()

  //Back
  render.StartBeam( 2 )
    render.AddBeam( pos, 2, 1, col)
    render.AddBeam( pos-Vector(0,w,0), 2, 1, endcol)
  render.EndBeam()*/
end
function Visualize()
  local ply = LocalPlayer()
  if !ply:Editing() then return end
  if editor.addmode == "Trace" then
    local tracedata = {}
    tracedata.start = ply:EyePos()
    tracedata.endpos = ply:EyePos()+(ply:GetAimVector())*250
    tracedata.filter = player.GetAll()
    local trace = util.TraceLine(tracedata)
    local hit = trace.HitPos
    local snap = 16
    local hitMod = hit
    if hitMod.x % snap < snap/2 then
      hitMod.x = hitMod.x - hitMod.x % snap
    else
      hitMod.x = hitMod.x + (snap - hitMod.x % snap)
    end
    if hitMod.y % snap < snap/2 then
      hitMod.y = hitMod.y - hitMod.y % snap
    else
      hitMod.y = hitMod.y + (snap - hitMod.y % snap)
    end
    if hitMod.z % snap < snap/2 then
      hitMod.z = hitMod.z - hitMod.z % snap
    else
      hitMod.z = hitMod.z + (snap - hitMod.z % snap)
    end
    hitMod = hitMod+(trace.HitNormal*1)
    drawGrid(hitMod, snap, 150, 150, 1, Color(155,255,255,255), trace.HitNormal)
    render.SetColorMaterial()
    render.DrawSphere( hitMod, 4, 30, 30, Color( 255, 255, 255, 255 ) )
  elseif editor.addmode == "Feet" then
    local tracedata = {}
    tracedata.start = ply:GetPos()
    tracedata.endpos = ply:GetPos()
    tracedata.filter = player.GetAll()
    local trace = util.TraceLine(tracedata)
    render.SetColorMaterial()
    render.DrawSphere( trace.HitPos+(trace.HitNormal*4), 4, 30, 30, Color( 255, 255, 255, 255 ) )
  end
end
hook.Add("PostDrawTranslucentRenderables", "Visualize Editor", Visualize)

function DrawPoint()
  local ply = LocalPlayer()
  if !ply:Editing() then return end

  for k, v in pairs(editor.verts.table) do
    render.SetColorMaterial()
    if k == editor.verts.selected then
      render.DrawSphere( v, 4, 30, 30, Color( 100, 100, 255, 240 ) )
    else
      render.DrawSphere( v, 4, 30, 30, Color( 255, 255, 255, 150 ) )
    end
      /*

    if k < #editor.verts.table and #editor.verts.table > 2 then
      render.DrawBeam( v, editor.verts.table[k+1], 2, 1, 2, Color(255,255,255 ) )
    elseif k == #editor.verts.table and #editor.verts.table > 2 then
      render.DrawBeam( v, editor.verts.table[1], 2, 1, 2, Color(255,255,255 ) )
    end*/
  end

  --render.SetMaterial( mat ) -- Apply the material
  local verts = {}
  local obj = Mesh()
  table.Merge(verts, editor.verts.table)
  table.insert(verts, verts[1])
  render.CullMode( 0 )
  render.SetColorMaterial()
	mesh.Begin( 6, #verts) -- Begin writing to the dynamic mesh


	for i = 1, #verts do

  		mesh.Position( verts[i] ) -- Set the position
  		mesh.TexCoord( 0, 0, 0 ) -- Set the texture UV coordinates
  		mesh.AdvanceVertex() -- Write the vertex

	end

	mesh.End() -- Finish writing the mesh and draw i
  render.CullMode( 0 )
  /*
  	mesh.Begin( MATERIAL_POLYGON, #editor.verts.table+1) -- Begin writing to the dynamic mesh
	for i = 1, #editor.verts.table do

  		mesh.Position( editor.verts.table[i] ) -- Set the position
  		mesh.TexCoord( 0, 0, 0 ) -- Set the texture UV coordinates
  		mesh.AdvanceVertex() -- Write the vertex

	end
	mesh.End() -- Finish writing the mesh and draw i
*/
end
hook.Add("PostDrawTranslucentRenderables", "Draw Point", DrawPoint)

local function CreateFrame(parent, x, y, w, h)
  local frame = parent:Add( "DFrame" )
  frame:SetPos( x, y )
  frame:SetTitle("")
  frame:SetSize( w, h  )
  frame:SetDraggable(false)
  frame:ShowCloseButton( false )
  frame.Paint = function(self, w, h )
  end

  return frame
end

local function CreateLabel(parent, txt, x, y, font, col, mode)
  local label = parent:Add( "DLabel" )
  surface.SetFont(GetFont(font))
  local txt_w, txt_h = surface.GetTextSize(txt)
  if mode == "center" then
    label:SetPos( x-(txt_w/2), y )
  elseif mode == "right" then
    label:SetPos( x-(txt_w), y )
  elseif !mode then
    label:SetPos( x, y )
  end
  label.original = {["x"] = x, ["y"] = y}
  label:SetSize( txt_w, txt_h  )
	label:SetFont( GetFont(font))
  label:SetText(txt)
	label:SetTextColor(col)

  label.UpdatePos = function()
    surface.SetFont(GetFont(font))
    local txt_w, txt_h = surface.GetTextSize(label:GetText())
    if mode == "center" then
      label:SetPos( label.original["x"]-(txt_w/2), label.original["y"] )
    elseif mode == "right" then
      label:SetPos( label.original["x"]-(txt_w), label.original["y"] )
    elseif !mode then
      label:SetPos( label.original["x"], label.original["y"] )
    end
  end
  return label
end

local function CreateBox(parent, x, y, w, h, col, mode)
  local box = parent:Add( "DLabel" )
  surface.SetFont(GetFont(font))

  if mode == "center" then
    box:SetPos( x-(w/2), y )
  elseif mode == "right" then
    box:SetPos( x-(w), y )
  elseif !mode then
    box:SetPos( x, y )
  end
  box.mode = mode
  box.original = {["x"] = x, ["y"] = y, ["w"] = w, ["h"] = h}
  box:SetSize( w, h  )
	box:SetTextColor(col)
  box.Paint = function(self, w, h )
    surface.SetDrawColor( col )
    surface.DrawRect(0, 0, w, h)
  end

  box.UpdatePos = function()
    if mode == "center" then
      box:SetPos( box.original["x"]-(box:GetWide()  /2), box.original["y"] )
    elseif mode == "right" then
      box:SetPos( box.original["x"]-(box:GetWide()), box.original["y"] )
    elseif !mode then
      box:SetPos( box.original["x"], box.original["y"] )
    end
  end
  return box
end

local function CreateButton(parent, txt, x, y, w, h, font, col, txtcol, mode)
  local button = parent:Add( "DButton" )

  if mode == "center" then
    button:SetPos( x-(w/2), y )
  elseif mode == "right" then
    button:SetPos( x-(w), y )
  elseif !mode then
    button:SetPos( x, y )
  end
  button:SetSize( w, h )
  button:SetText("")

  button.Paint = function(self, w, h )
    surface.SetDrawColor( col )
    surface.DrawRect(0, 0, w, h)
    surface.SetFont(GetFont(font))
    surface.SetTextColor( txtcol )
    local txt_w, txt_h = surface.GetTextSize(txt)
    surface.SetTextPos( (w/2) - (txt_w/2), (h/2) - (txt_h/2) )
    surface.DrawText( txt )
  end
  return button
end

local function CreateInfoButton(par, infopar, t, infot, x, y, w, h, font, col, txtcol, mode)
  local button = CreateButton(par, t, x, y, w, h, font, col, txtcol, mode)
  local buttondiv = CreateBox(button, x, h-3, 0, 2, Color(255,255,255), "center")
  local info = CreateFrame(infopar, 0, y, w, 300)
  local wrap = wrapText( "TE_Text_Small", infot, w )
  if 1 == #wrap then
    info[1] = CreateLabel(info, infot, x, (y+h), "TE_Text_Small", Color(255,255,255), "center")
  else
    for _, txts in pairs(wrap) do
      local str = table.concat( txts, " " )
      SetFont("TE_Text_Small")
      local txt_w, txt_h = surface.GetTextSize(str)

      info[_] = CreateLabel(info, str, x, (txt_h*_), "TE_Text_Small", Color(255,255,255), "center")

    end
  end
  info:SetAlpha(0)
  info:Hide()
  SetFont("TE_Text_Small")
  local txt_w, txt_h = surface.GetTextSize(infot)
  local infodiv = CreateBox(infopar, 0, (y+h)-3, 0, 2, Color(255,255,255))

  button.Think = function()
    if button:IsHovered() then
      local div = buttondiv
      div:SetWide(UT:Hermite(0.35, div:GetWide(), par:GetWide()+10))

      if math.Round(div:GetWide()) >= par:GetWide() then
        infodiv:SetWide(UT:Hermite(0.25, infodiv:GetWide(), infopar:GetWide()))
        infodiv.UpdatePos()
        if info:GetAlpha() < 5 or !info:IsVisible() then
          info:AlphaTo(255, 0.15)
          info:Show()
        end
      end
      div.UpdatePos()
    else
      if info:GetAlpha() > 254 then
        info:AlphaTo(0, 0.15)
      end
      timer.Simple(0.16, function() info:Hide() end)

      infodiv:SetWide(UT:Hermite(0.35, infodiv:GetWide(), 0))

      if math.Round(infodiv:GetWide()) <= 0 then
        local div = buttondiv
        div:SetWide(UT:Hermite(0.35, div:GetWide(), 0))
        div.UpdatePos()
      end
    end
  end
  return button
end

local ZONECREATE =
{
	Init = function( self )
    self.ply = LocalPlayer()
    // Main Menu
    self.Main = CreateFrame(self, 65, 41, 175, 585)
    self.Main.Paint = function(self, w, h)
      surface.SetDrawColor(Color(0,0,0, 100))
      surface.DrawRect(0,0, w, h)
    end
    self.Info = CreateFrame(self, 65+175-2, 41, 175, 585)
    self.Info.Paint = function(self, w, h)
      surface.SetDrawColor(Color(0,0,0, 100))
      surface.DrawRect(0,0, w, h)
    end
    local bWide = self.Main:GetWide()
    SetFont("TE_Text", {
      font = "SIMPLIFICA",
      weight = 700,
      size = 50,
      antialias = true
    })
    SetFont("TE_Text_Small", {
      font = "SIMPLIFICA",
      weight = 700,
      size = 20,
      antialias = true
    })
    self.Title = CreateLabel(self.Main, "Zone Creator", bWide/2, 2, "TE_Text", Color(255,255,255), "center")
    self.Divide1 = CreateBox(self.Main, bWide/2, 50, 170, 2, Color(255,255,255), "center")
    self.Add_BUTTON = CreateInfoButton(self.Main, self.Info, "Add", "Adds a point at the marked location", bWide/2, (50+10)+(60*1), bWide, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center", 50-2)


    self.AddMode_BUTTON = CreateButton(self.Main, "Add Mode", bWide/2, (50+10)+(60*2), bWide, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.AddMode_BUTTON_DIVIDE = CreateBox(self.AddMode_BUTTON, bWide/2, 50-2, 0, 2, Color(255,255,255), "center")
    self.AddMode_INFO = CreateFrame(self.Info, 0, 147, bWide, 300)
    self.AddMode_INFO[1] = CreateLabel(self.AddMode_INFO, editor.addmode, bWide/2, 0, "TE_Text_Small", Color(255,255,255), "center")
    self.AddMode_INFO:SetAlpha(0)
    self.AddMode_INFO:Hide()
    self.AddMode_INFO_DIVIDE = CreateBox(self.Info, 0, 168, 0, 2, Color(255,255,255))

    self.EnterMode_BUTTON = CreateButton(self.Main, "Enter Mode", bWide/2, (50+10)+(60*3), bWide, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.EnterMode_BUTTON_DIVIDE = CreateBox(self.EnterMode_BUTTON, bWide/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Reset_BUTTON = CreateButton(self.Main, "Reset", bWide/2, (50+10)+(60*4), bWide, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Reset_BUTTON_DIVIDE = CreateBox(self.Reset_BUTTON, bWide/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Save_BUTTON = CreateInfoButton(self.Main, self.Info, "Save", "Saves current points", bWide/2, (50+10)+(60*5), bWide, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center", 50-2)

    self.Back_BUTTON = CreateButton(self.Main, "Back", bWide/2, (50+10)+(60*6), bWide, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Back_BUTTON_DIVIDE = CreateBox(self.Back_BUTTON, bWide/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Add_BUTTON.DoClick = function()
      CreatePoint()
    end

    self.AddMode_BUTTON.DoClick = function()
      if editor.addmode == "Trace" then
        editor.addmode = "Feet"
        self.AddMode_INFO[1]:SetText("Feet")
        self.AddMode_INFO[1].UpdatePos()
      elseif editor.addmode == "Feet" then
        editor.addmode = "Trace"
        self.AddMode_INFO[1]:SetText("Trace")
        self.AddMode_INFO[1].UpdatePos()
      end
    end

    self.EnterMode_BUTTON.DoClick = function()
      if editor.mode == "enter" then
        editor.mode = "surf"
      elseif editor.mode == "surf" then
        editor.mode = "onground"
      elseif editor.mode == "onground" then
        editor.mode = "enter"
      end
    end

    self.Reset_BUTTON.DoClick = function()
      editor.verts.selected = 0
      editor.verts.table = {}
    end

    self.Back_BUTTON.DoClick = function()
      self:GetParent().ShowCreate = false
    end
  end,

	PerformLayout = function( self )

		self:SetSize( ScrW(), ScrH() )
		self:SetPos( 0, 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    // Hover

    if self.AddMode_BUTTON:IsHovered() then
      local div = self.AddMode_BUTTON_DIVIDE
      div:SetWide(UT:Hermite(0.35, div:GetWide(), self.Main:GetWide()+10))

      if math.Round(div:GetWide()) >= self.Main:GetWide() then
        self.AddMode_INFO_DIVIDE:SetWide(UT:Hermite(0.25, self.AddMode_INFO_DIVIDE:GetWide(), self.Info:GetWide()))
        self.AddMode_INFO_DIVIDE.UpdatePos()
        if self.AddMode_INFO:GetAlpha() < 5 or !self.AddMode_INFO:IsVisible() then
          self.AddMode_INFO:AlphaTo(255, 0.15)
          self.AddMode_INFO:Show()
        end
      end
      div.UpdatePos()
    else
      if self.AddMode_INFO:GetAlpha() > 254 then
        self.AddMode_INFO:AlphaTo(0, 0.15)
      end
      timer.Simple(0.16, function() self.AddMode_INFO:Hide() end)

      self.AddMode_INFO_DIVIDE:SetWide(UT:Hermite(0.35, self.AddMode_INFO_DIVIDE:GetWide(), 0))

      if math.Round(self.AddMode_INFO_DIVIDE:GetWide()) <= 0 then
        local div = self.AddMode_BUTTON_DIVIDE
        div:SetWide(UT:Hermite(0.35, div:GetWide(), 0))
        div.UpdatePos()
      end
    end

    if self.EnterMode_BUTTON:IsHovered() then
      local div = self.EnterMode_BUTTON_DIVIDE
      div:SetWide(UT:Hermite(0.35, div:GetWide(), self.Main:GetWide()))
      div.UpdatePos()
    else
      local div = self.EnterMode_BUTTON_DIVIDE
      div:SetWide(UT:Hermite(0.35, div:GetWide(), 0))
      div.UpdatePos()
    end

    if self.Reset_BUTTON:IsHovered() then
      local div = self.Reset_BUTTON_DIVIDE
      div:SetWide(UT:Hermite(0.35, div:GetWide(), self.Main:GetWide()))
      div.UpdatePos()
    else
      local div = self.Reset_BUTTON_DIVIDE
      div:SetWide(UT:Hermite(0.35, div:GetWide(), 0))
      div.UpdatePos()
    end

    if self.Back_BUTTON:IsHovered() then
      local div = self.Back_BUTTON_DIVIDE
      div:SetWide(UT:Hermite(0.35, div:GetWide(), self.Main:GetWide()))
      div.UpdatePos()
    else
      local div = self.Back_BUTTON_DIVIDE
      div:SetWide(UT:Hermite(0.35, div:GetWide(), 0))
      div.UpdatePos()
    end

	end
}

ZONECREATE = vgui.RegisterTable( ZONECREATE, "EditablePanel" )

local ZONEE =
{
	Init = function( self )
    self.ply = LocalPlayer()
    // Main Menu
    self.Main = CreateFrame(self, 65, 41, ScrW()-130, ScrH()-135)

    self.VertList = CreateFrame(self.Main, (ScrW()-130)-250, 0, 250, 400)
    self.VertList.verts = {}
    self.VertList:Hide()
    self.VertList:SetAlpha(0)
    self.VertList.Paint = function(self, w, h )
      surface.SetDrawColor( Color(0,0,0, 150) )
      surface.DrawRect(0, 0, w, h)
    end

    if ( !IsValid( self.Create ) ) then
  		self.Create = vgui.CreateFromTable( ZONECREATE, self )
      self.Create:SetAlpha(0)
      self.Create:Hide()
  	end

    self.Edit = CreateFrame(self.Main, 0, 0, 175, 300)
    self.Edit:Hide(0)
    self.Edit:SetAlpha(0)
    self.Edit.Paint = function(self, w, h )
      surface.SetDrawColor( Color(0,0,0, 150) )
      surface.DrawRect(0, 0, w, h)
    end
    self.Buttons = CreateFrame(self.Main, 0, 0, 175, 300)
    local bWide = self.Buttons:GetWide()
    SetFont("TE_Text", {
      font = "SIMPLIFICA",
      weight = 700,
      size = 50,
      antialias = true
    })
    self.Title = CreateLabel(self.Buttons, "Zone Editor", bWide/2, 2, "TE_Text", Color(255,255,255), "center")
    self.Divide1 = CreateBox(self.Buttons, bWide/2, 50, 170, 2, Color(255,255,255), "center")

    self.Create_BUTTON = CreateButton(self.Buttons, "Create", bWide/2, 50+10, 150, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Create_BUTTON_DIVIDE = CreateBox(self.Create_BUTTON, 150/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Edit_BUTTON = CreateButton(self.Buttons, "Edit", bWide/2, (50+10)+60, 150, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Edit_BUTTON_DIVIDE = CreateBox(self.Edit_BUTTON, 150/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Back_BUTTON = CreateButton(self.Buttons, "Back", bWide/2, (50+10)+(60*2), 150, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Back_BUTTON_DIVIDE = CreateBox(self.Back_BUTTON, 150/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Create_BUTTON.DoClick = function()
      self.ShowCreate = true
    end

    self.Edit_BUTTON.DoClick = function()
      self.ShowEdit = true
    end
    self.ShowCreate = false
    self.ShowEdit = false

    self.Back_BUTTON.DoClick = function()
      self:GetParent().ShowZoneE = false
    end
  end,

	PerformLayout = function( self )

		self:SetSize( ScrW(), ScrH() )
		self:SetPos( 0, 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    // Hover
    if self.Create_BUTTON:IsHovered() then
      local div = self.Create_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 150))
      div.UpdatePos()
    else
      local div = self.Create_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 0))
      div.UpdatePos()
    end

    if self.Edit_BUTTON:IsHovered() then
      local div = self.Edit_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 150))
      div.UpdatePos()
    else
      local div = self.Edit_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 0))
      div.UpdatePos()
    end

    if self.Back_BUTTON:IsHovered() then
      local div = self.Back_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 150))
      div.UpdatePos()
    else
      local div = self.Back_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 0))
      div.UpdatePos()
    end

    // Pages
    if self.ShowEdit and !self.Edit:IsVisible() then
      self.ShowCreate = false

      self.Edit:AlphaTo(255, 0.15)
      self.Edit:Show()

      self.VertList:AlphaTo(255, 0.15)
      self.VertList:Show()

      if self.Create:IsVisible() then
        self.Create:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.Create:Hide() end)
      end

      self.Buttons:AlphaTo(0, 0.15)
      timer.Simple(0.16, function() self.Buttons:Hide() end)
    elseif self.ShowCreate and !self.Create:IsVisible() then
      self.ShowEdit = false

      self.Create:AlphaTo(255, 0.15)
      self.Create:Show()

      self.VertList:AlphaTo(255, 0.15)
      self.VertList:Show()

      if self.Edit:IsVisible() then
        self.Edit:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.Edit:Hide() end)
      end

      self.Buttons:AlphaTo(0, 0.15)
      timer.Simple(0.16, function() self.Buttons:Hide() end)
    elseif !self.ShowCreate and !self.ShowEdit and !self.Buttons:IsVisible() then
      if self.Edit:IsVisible() then
        self.Edit:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.Edit:Hide() end)
      end
      if self.Create:IsVisible() then
        self.Create:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.Create:Hide() end)
      end
        self.VertList:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.VertList:Hide() end)
      self.Buttons:Show()
      self.Buttons:AlphaTo(255, 0.15)
    end

	end
}

ZONEE = vgui.RegisterTable( ZONEE, "EditablePanel" )

local TRICKE =
{
	Init = function( self )
    self.ply = LocalPlayer()
    // Main Menu
    self.Main = CreateFrame(self, 65, 41, ScrW()-130, ScrH()-135)

    self.Buttons = CreateFrame(self.Main, 0, 0, 175, 300)
    local bWide = self.Buttons:GetWide()
    SetFont("TE_Text", {
      font = "SIMPLIFICA",
      weight = 700,
      size = 50,
      antialias = true
    })
    self.Title = CreateLabel(self.Buttons, "Trick Editor", bWide/2, 2, "TE_Text", Color(255,255,255), "center")
    self.Divide1 = CreateBox(self.Buttons, bWide/2, 50, 170, 2, Color(255,255,255), "center")
    self.Create_BUTTON = CreateButton(self.Buttons, "Create", bWide/2, 50+10, 150, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Create_BUTTON_DIVIDE = CreateBox(self.Create_BUTTON, 150/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Edit_BUTTON = CreateButton(self.Buttons, "Edit", bWide/2, (50+10)+60, 150, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Edit_BUTTON_DIVIDE = CreateBox(self.Edit_BUTTON, 150/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Back_BUTTON = CreateButton(self.Buttons, "Back", bWide/2, (50+10)+(60*2), 150, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Back_BUTTON_DIVIDE = CreateBox(self.Back_BUTTON, 150/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Back_BUTTON.DoClick = function()
      self:GetParent().ShowTrickE = false
    end
  end,

	PerformLayout = function( self )

		self:SetSize( ScrW(), ScrH() )
		self:SetPos( 0, 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    if self.Create_BUTTON:IsHovered() then
      local div = self.Create_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 150))
      div.UpdatePos()
    else
      local div = self.Create_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 0))
      div.UpdatePos()
    end

    if self.Edit_BUTTON:IsHovered() then
      local div = self.Edit_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 150))
      div.UpdatePos()
    else
      local div = self.Edit_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 0))
      div.UpdatePos()
    end

    if self.Back_BUTTON:IsHovered() then
      local div = self.Back_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 150))
      div.UpdatePos()
    else
      local div = self.Back_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 0))
      div.UpdatePos()
    end
	end
}

TRICKE = vgui.RegisterTable( TRICKE, "EditablePanel" )

local EDITOR =
{
	Init = function( self )
    self.ply = LocalPlayer()
    // Main Menu
    self.Main = CreateFrame(self, 65, 41, ScrW()-130, ScrH()-135)

    self.Buttons = CreateFrame(self.Main, 0, 0, 175, 200)
    local bWide = self.Buttons:GetWide()
    if ( !IsValid( self.ZoneE ) ) then
  		self.ZoneE = vgui.CreateFromTable( ZONEE, self )
      self.ZoneE:SetAlpha(0)
      self.ZoneE:Hide()
  	end

    if ( !IsValid( self.TrickE ) ) then
  		self.TrickE = vgui.CreateFromTable( TRICKE, self )
      self.TrickE:SetAlpha(0)
      self.TrickE:Hide()
  	end

    SetFont("TE_Text", {
      font = "SIMPLIFICA",
      weight = 700,
      size = 50,
      antialias = true
    })
    self.Title = CreateLabel(self.Buttons, "Editor", bWide/2, 2, "TE_Text", Color(255,255,255), "center")
    self.Divide1 = CreateBox(self.Buttons, bWide/2, 50, 170, 2, Color(255,255,255), "center")
    self.Zone_BUTTON = CreateButton(self.Buttons, "Zones", bWide/2, 50+10, 150, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Zone_BUTTON_DIVIDE = CreateBox(self.Zone_BUTTON, 150/2, 50-2, 0, 2, Color(255,255,255), "center")
    self.Tricks_BUTTON = CreateButton(self.Buttons, "Tricks", bWide/2, (50+10)+60, 150, 50, "TE_Text", Color(255,255,255, 0), Color(255,255,255), "center")
    self.Tricks_BUTTON_DIVIDE = CreateBox(self.Tricks_BUTTON, 150/2, 50-2, 0, 2, Color(255,255,255), "center")

    self.Zone_BUTTON.DoClick = function()
      self.ShowZoneE = true
    end

    self.Tricks_BUTTON.DoClick = function()
      self.ShowTrickE = true
    end
    self.ShowZoneE = false
    self.ShowTrickE = false

  end,

	PerformLayout = function( self )

		self:SetSize( ScrW(), ScrH() )
		self:SetPos( 0, 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    if self.Zone_BUTTON:IsHovered() then
      local div = self.Zone_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 150))
      div.UpdatePos()
    else
      local div = self.Zone_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 0))
      div.UpdatePos()
    end

    if self.Tricks_BUTTON:IsHovered() then
      local div = self.Tricks_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 150))
      div.UpdatePos()
    else
      local div = self.Tricks_BUTTON_DIVIDE
      div:SetWide(Lerp(0.2, div:GetWide(), 0))
      div.UpdatePos()
    end

    if self.ShowTrickE and !self.TrickE:IsVisible() then
      self.ShowZoneE = false

      self.TrickE:AlphaTo(255, 0.15)
      self.TrickE:Show()

      if self.ZoneE:IsVisible() then
        self.ZoneE:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.ZoneE:Hide() end)
      end

      self.Buttons:AlphaTo(0, 0.15)
      timer.Simple(0.16, function() self.Buttons:Hide() end)
    elseif self.ShowZoneE and !self.ZoneE:IsVisible() then
      self.ShowTrickE = false

      self.ZoneE:AlphaTo(255, 0.15)
      self.ZoneE:Show()

      if self.TrickE:IsVisible() then
        self.TrickE:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.TrickE:Hide() end)
      end

      self.Buttons:AlphaTo(0, 0.15)
      timer.Simple(0.16, function() self.Buttons:Hide() end)
    elseif !self.ShowZoneE and !self.ShowTrickE and !self.Buttons:IsVisible() then
      if self.TrickE:IsVisible() then
        self.TrickE:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.TrickE:Hide() end)
      end
      if self.ZoneE:IsVisible() then
        self.ZoneE:AlphaTo(0, 0.15)
        timer.Simple(0.16, function() self.ZoneE:Hide() end)
      end
      self.Buttons:Show()
      self.Buttons:AlphaTo(255, 0.15)
    end
	end
}

EDITOR = vgui.RegisterTable( EDITOR, "EditablePanel" )

if ( IsValid( TrickEditor ) ) then
TrickEditor:Remove()

end

function editor.Open()

  if ( !IsValid( TrickEditor ) ) then
		TrickEditor = vgui.CreateFromTable( EDITOR )
    TrickEditor:SetAlpha(0)
	end

	if ( IsValid( TrickEditor ) ) then
    TrickEditor:AlphaTo(255, 0.15)
		TrickEditor:Show()
		--TrickEditor:MakePopup()
		TrickEditor:SetKeyboardInputEnabled( false )
	end
  editor.open = true
end

function editor.Close()
  editor.open = false

  if ( IsValid( TrickEditor ) ) then
		timer.Simple(0.2, function()
			if !editor.open then
				TrickEditor:Hide()
			end
		end)

		TrickEditor:AlphaTo(0, 0.15)
	end
end

hook.Add("Think", "Open/Close Editor", function()
  local ply = LocalPlayer()
  if ply:GetNWBool("Editing") ~= ply:Editing() then
    ply:Editing(ply:GetNWBool("Editing"))
  end


  if ply:Editing() and !editor.open then

    editor.Open()
  elseif !ply:Editing() and editor.open then
    editor.Close()
  end
end)
