font = {}
font.fonts = {}
font.clientList = {
  "MavenPro",
  "SIMPLIFICA",
  "Ostrich Sans Black",
  "Ostrich Sans",
  "Acens",
  "Uni Sans Thin CAPS"
}
font.list = {
  "halflife2",
  "MavenPro",
  "SIMPLIFICA",
  "Ostrich Sans Black",
  "Ostrich Sans",
  "HL2MP",
  "Acens",
  "Uni Sans Thin CAPS"
}
font.setFont = "Trebuchet24"

function InitalizeFonts()
  surface.CreateFont("HUD General", {
    font = "halflife2",
    size = 25,
    antialias = true})

  surface.CreateFont("HUD Player", {
    font = "MavenPro",
    weight = 600,
    size = 15,
    antialias = true})

  surface.CreateFont("HUD Echo2", {
    font = "MavenPro",
    weight = 300,
    size = 26,
    antialias = true})

  surface.CreateFont("HUD Echo", {
    font = "SIMPLIFICA",
    weight = 600,
    size = 60,
    antialias = true})

  surface.CreateFont("HUD Echo2txt", {
    font = "MavenPro",
    weight = 1000,
    size = 17,
    antialias = true})

  surface.CreateFont("HUD 3dEcho", {
    font = "MavenPro",
    weight = 700,
    size = 45,
    antialias = true})

  surface.CreateFont("HUD Menu", {
    font = "MavenPro",
    weight = 700,
    size = 15,
    antialias = true})

  surface.CreateFont("HUD Menu2", {
    font = "MavenPro",
    weight = 700,
    size = 15,
    antialias = true})

  surface.CreateFont("HUD Notification", {
    font = "MavenPro",
    weight = 700,
    size = 15,
    antialias = true})

  surface.CreateFont("HUD Speed", {
    font = "SIMPLIFICA",
    weight = 500,
    size = 60,
    antialias = true})

  surface.CreateFont("HUD Spectators", {
    font = "SIMPLIFICA",
    weight = 600,
    size = 25,
    antialias = true})

  surface.CreateFont("HUD Gear", {
    font = "SIMPLIFICA",
    weight = 600,
    size = 50,
    antialias = true})

  surface.CreateFont("Editor", {
    font = "SIMPLIFICA",
    weight = 600,
    size = 25,
    antialias = true})

  surface.CreateFont("Console", {
    font = "MavenPro",
    weight = 450,
    size = 18,
    antialias = true})
end
function SetFont(unique, tbl)
  if !tbl then
    if font.fonts[unique] then
      if font.setFont != unique then
        surface.SetFont(unique)
        font.setFont = unique
        return
      end
    end
  else
    if font.fonts[unique] && UT:TblCompare(font.fonts[unique],tbl) then
      font.setFont = unique
      surface.SetFont(unique)
    else
      font.fonts[unique] = tbl
      font.setFont = unique
      surface.CreateFont(unique, tbl)
      surface.SetFont(unique)
    end
  end
end

function GetFont(unique)
  if font.fonts[unique] then
    return unique
  end
  return "Trebuchet24"
end
