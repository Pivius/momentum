lobby = lobby or {}

lobby.minigames = {
	["Miscellaneous"]  = Color(155,255,255,255),
	["Hunted"]				 = Color(0,75,255,255),
	["Elimination"] 	 = Color(255,75,0,255),
	["Race"]				 	 = Color(41, 228, 25),
	["Moo5e is gay"]	 = Color(230, 83, 202),
  ["None"]           = Color(255,255,255,255)
}

function lobby.GetColorByTeam(team, lb)
	if type(team) == "string" then
		team = UT:TblgetKey(lb.minigameTeams, team)
	end
	local col = lb.minigameColors[team]
	return col
end

function lobby:SetTeam(ply, primCol, secCol, team)
	if SERVER then
		local plyTeam = lobby:getPlayerLobby(ply).Minigame.players[ply]
		local teamName = lobby:getPlayerLobby(ply).Minigame.teams[plyTeam]
		if !primCol then
			primCol = lobby:getPlayerLobby(ply).Minigame.colors[plyTeam] or Color(255,255,255)
		end
		ply:TrailColor(primCol)

		if !secCol then
			secCol = nil
		end
		local t = {plyTeam, teamName}
		net.Start("lobbyNetworking")
	  	net.WriteString("team")
	  	net.WriteTable( {ply, plyTeam, primCol, secCol} )
	  net.Send(ply)
	else
		hud.customColor = primCol
		if !team then
			team = "None"
		end
		if lobby:getPlayerLobby(ply) != "None" then
			lobby:getPlayerLobby(ply).minigamePlayers[ply] = team
		end
		if !secCol then
			secCol = Color(255,255,255)
		end
		hud.customColor2 = secCol
	end
end



function lobby:getPlayerLobby(ply)
  if SERVER then
		local lb = ply:InLobby()
    if lb == "none" then return "None" end
    return lobby:getByID( ply:InLobby() )
  else
    if lobby.lobbies == {} then return "None" end

    for k, v in pairs(lobby.lobbies) do

      if table.HasValue(v["lobbyPlayers"], ply) then

        return lobby.lobbies[k]
      end
    end
  end
  return "None"
end

net.Receive( "lobbyNetworking", function()
 local type = net.ReadString()
 local varargs = net.ReadTable()
 if type == "sync" then
	 if SERVER then
     local ply = varargs[1]
		 if ply:IsValid() then
	     net.Start("lobbyNetworking")
	       net.WriteString("sync")
	       net.WriteTable( {[1] = lobby:getLobbies( )} )
	     net.Send(ply)
		 end
   else

    lobby.lobbies = varargs[1]
   end
 elseif type == "create" then
	 if SERVER then
		 local ply = varargs[1]
		 local mg = varargs[2]
		 local var = varargs[3]
		 if ply:IsValid() then
		 	lobby:openLobby(ply, mg, var)
		end
	 end
 elseif type == "join" then
	 if SERVER then
		 local ply = varargs[1]
		 local lbID = varargs[2]
		 local spectator = varargs[3]
		 if lobby:isValidLobby( lbID ) and ply:IsValid() then
			 lobby:addPlayerLobby(lbID, ply)
		 end
	 end
 elseif type == "leave" then
	 if SERVER then
		 local ply = varargs[1]
		 local LobbyID = ply:InLobby()
		 if lobby:isValidLobby( LobbyID ) and ply:IsValid() then

			 lobby:removePlayerLobby(LobbyID, ply)
		 end
	 end
 elseif type == "team" then
		if CLIENT then
			local ply = varargs[1]
			local team = varargs[2]
			local color = varargs[3]
			local secondary = varargs[4]
			timer.Simple(0.01, function()
				lobby:SetTeam(ply, color, secondary, team)
			end)
		end
	end
end )
