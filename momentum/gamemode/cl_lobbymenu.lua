lobby = lobby or {}
lobby.lobbies = {}
lobby.keepopen = false

function syncLobbies()
  net.Start("lobbyNetworking")
    net.WriteString("sync")
    net.WriteTable({LocalPlayer()})
  net.SendToServer()
end

local MENU =
{
	Init = function( self )
    self.ply = LocalPlayer()
    // Main Menu
    self.Main= self:Add( "DFrame" )

    self.Main:SetPos( 65, 41 )
    self.Main:SetSize(ScrW()-130, ScrH()-135)
    self.Main:SetTitle("")
    self.Main:SetDraggable(false)
    self.Main:ShowCloseButton( false )
    self.Main.Paint = function(self, w, h )
      --surface.SetDrawColor( Color(15,15,15,170) )
      --surface.DrawRect(0, 0, w, h)
    end
    local ButtonCol = Color(15,15,15,170)
    local ButtonHighCol = Color(255,255,255)

    self.Info= self.Main:Add( "DFrame" )
    self.Info.W = 400
    self.Info:SetPos( 0, 0 )
    self.Info:SetSize(self.Info.W, 250)
    self.Info:SetTitle("")
    self.Info:SetDraggable(false)
    self.Info:ShowCloseButton( false )
    self.Info.Paint = function(self, w, h )
      surface.SetDrawColor( Color(0,0,0, 150) )
      surface.DrawRect(0, 0, w, h)
    end

    self.AvaterBG= self.Main:Add( "DFrame" )
    self.AvaterBG:SetPos( 10, 10 )
    self.AvaterBG:SetSize(68, 68)
    self.AvaterBG:SetTitle("")
    self.AvaterBG:SetDraggable(false)
    self.AvaterBG:ShowCloseButton( false )
    self.AvaterBG.Paint = function(self, w, h )
      surface.SetDrawColor( Color(255,255,255,255) )
      surface.DrawRect(0, 0, w, h)
    end

		self.AvatarButton = self.AvaterBG:Add( "DButton" )
		--self.AvatarButton:Dock( LEFT )
    self.AvatarButton:SetPos( 2, 2 )
		self.AvatarButton:SetSize( 64, 64 )
		self.AvatarButton.DoClick = function()
      self.ply:ShowProfile()
    end

		function self.AvatarButton:Paint(w, h)
			return true
		end

		self.Avatar	= self.AvatarButton:Add( "AvatarImage" )
		self.Avatar:SetSize( 64, 64  )
		self.Avatar:SetMouseInputEnabled( false )
    self.Avatar:SetPlayer( self.ply )

    SetFont("Q_Info", {
      font = "SIMPLIFICA",
      weight = 700,
      size = 30,
      antialias = true
    })
    self.NickL = self.Info:Add( "DLabel" )
    self.NickL.txt_w, self.NickL.txt_h = surface.GetTextSize("Nick")
    self.NickL:SetPos( ((self.Info.W-10+64+5+2+7)/2)-(self.NickL.txt_w)/2, 0 )
    self.NickL:SetSize( self.NickL.txt_w, self.NickL.txt_h  )
		self.NickL:SetFont( GetFont("Q_Info"))
    self.NickL:SetText("Nick")
		self.NickL:SetTextColor(Color(255, 255, 255))

    self.Divide1= self.Main:Add( "DFrame" )
    self.Divide1:SetPos( 10+64+5+2, self.NickL.txt_h )
    self.Divide1:SetSize(self.Info.W-(10+64+5+2+7) , 1)
    self.Divide1:SetTitle("")
    self.Divide1:SetDraggable(false)
    self.Divide1:ShowCloseButton( false )
    self.Divide1.Paint = function(self, w, h )
      surface.SetDrawColor( Color(255,255,255,255) )
      surface.DrawRect(0, 0, w, h)
    end

    self.PName= self.Info:Add( "DLabel" )
    self.PName.txtSize = 30
    local txt_w, txt_h = surface.GetTextSize(self.ply:Nick())
    self.PName:SetPos( ((self.Info.W-10+64+5+2+7)/2)-(txt_w)/2, self.NickL.txt_h+1 )
    self.PName:SetSize( txt_w, txt_h  )
		self.PName:SetFont( GetFont("Q_Info"))
    self.PName:SetText(self.ply:Nick())
		self.PName:SetTextColor(Color(255, 255, 255 ))

    self.Divide2= self.Main:Add( "DFrame" )
    self.Divide2:SetPos( 10+64+5+2, 75 )
    self.Divide2:SetSize(self.Info.W-(10+64+5+2+7) , 3)
    self.Divide2:SetTitle("")
    self.Divide2:SetDraggable(false)
    self.Divide2:ShowCloseButton( false )
    self.Divide2.Paint = function(self, w, h )
      surface.SetDrawColor( Color(255,255,255,255) )
      surface.DrawRect(0, 0, w, h)
    end

    self.Topspeed = self.Info:Add( "DLabel" )
    txt_w, txt_h = surface.GetTextSize("Topspeed")
    self.Topspeed:SetPos( ((self.Info.W)/2)-(txt_w)/2, 78 )
    self.Topspeed:SetSize( txt_w, txt_h  )
		self.Topspeed:SetFont( GetFont("Q_Info"))
    self.Topspeed:SetText("Topspeed")
		self.Topspeed:SetTextColor(Color(255, 255, 255))

    SetFont("Q_Info_Small", {
      font = "SIMPLIFICA",
      weight = 700,
      size = 25,
      antialias = true
    })
    self.Global = self.Info:Add( "DLabel" )
    txt_w, txt_h = surface.GetTextSize("Global")
    self.Global:SetPos( ((self.Info.W)/5)-(txt_w/2), 94 )
    self.Global:SetSize( txt_w, txt_h  )
		self.Global:SetFont( GetFont("Q_Info_Small"))
    self.Global:SetText("Global")
		self.Global:SetTextColor(Color(255, 255, 255))

    self.Session = self.Info:Add( "DLabel" )
    txt_w, txt_h = surface.GetTextSize("Session")
    self.Session:SetPos( ((self.Info.W)/1.25)-(txt_w/2), 94 )
    self.Session:SetSize( txt_w, txt_h  )
		self.Session:SetFont( GetFont("Q_Info_Small"))
    self.Session:SetText("Session")
		self.Session:SetTextColor(Color(255, 255, 255))

    self.Divide3= self.Main:Add( "DFrame" )
    self.Divide3:SetPos( 10, 120 )
    self.Divide3:SetSize(self.Info.W-17 , 1)
    self.Divide3:SetTitle("")
    self.Divide3:SetDraggable(false)
    self.Divide3:ShowCloseButton( false )
    self.Divide3.Paint = function(self, w, h )
      surface.SetDrawColor( Color(255,255,255,255) )
      surface.DrawRect(0, 0, w, h)
    end

    self.SessionStat = self.Info:Add( "DLabel" )
    txt_w, txt_h = surface.GetTextSize(math.Round(LocalPlayer():TSS()))
    self.SessionStat:SetPos( ((self.Info.W)/1.25)-(txt_w/2), 120 )
    self.SessionStat:SetSize( txt_w, txt_h  )
		self.SessionStat:SetFont( GetFont("Q_Info"))
    self.SessionStat:SetText(math.Round(LocalPlayer():TSS()))
		self.SessionStat:SetTextColor(Color(255, 255, 255))

    self.GlobalStat = self.Info:Add( "DLabel" )
    txt_w, txt_h = surface.GetTextSize(math.Round(LocalPlayer():TSA()))
    self.GlobalStat:SetPos( (((self.Info.W)-(10+64+5+2+7)))-(txt_w/2), 120 )
    self.GlobalStat:SetSize( txt_w, txt_h  )
		self.GlobalStat:SetFont( GetFont("Q_Info"))
    self.GlobalStat:SetText(math.Round(LocalPlayer():TSA()))
		self.GlobalStat:SetTextColor(Color(255, 255, 255))
  end,

	PerformLayout = function( self )

		self:SetSize( ScrW(), ScrH() )
		self:SetPos( 0, 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    if self.PName then
      SetFont("Q_Info")
      local txt_w, txt_h = surface.GetTextSize(self.ply:Nick())
      self.PName:SetPos( ((self.Info.W-10+64+5+2+7)/2)-(txt_w)/2, self.NickL.txt_h+1 )
      self.PName:SetSize( txt_w, txt_h  )
  		self.PName:SetFont( GetFont("Q_Info"))
      self.PName:SetText(self.ply:Nick())

      SetFont("Q_Info")
      txt_w, txt_h = surface.GetTextSize(math.Round(LocalPlayer():TSS()))
      self.SessionStat:SetPos( ((self.Info.W)/1.25)-(txt_w/2), 120 )
      self.SessionStat:SetSize( txt_w, txt_h  )
  		self.SessionStat:SetFont( GetFont("Q_Info"))
      self.SessionStat:SetText(math.Round(LocalPlayer():TSS()))
  		self.SessionStat:SetTextColor(Color(255, 255, 255))

      txt_w, txt_h = surface.GetTextSize(math.Round(LocalPlayer():TSA()))
      self.GlobalStat:SetPos( ((self.Info.W)/5)-(txt_w/2), 120 )
      self.GlobalStat:SetSize( txt_w, txt_h  )
  		self.GlobalStat:SetFont( GetFont("Q_Info"))
      self.GlobalStat:SetText(math.Round(LocalPlayer():TSA()))
    end
	end
}

MENU = vgui.RegisterTable( MENU, "EditablePanel" )

if ( IsValid( LobbyMenu ) ) then
	LobbyMenu:Remove()
end
function GM:OnSpawnMenuOpen()
  hookCall("MinigameOpen")
  gui.EnableScreenClicker(true)
	open = true
  /*
	if ( !IsValid( LobbyMenu ) ) then
		LobbyMenu = vgui.CreateFromTable( MENU )
    LobbyMenu:SetAlpha(0)
	end

	if ( IsValid( LobbyMenu ) ) then
    LobbyMenu:AlphaTo(255, 0.15)
		LobbyMenu:Show()
		LobbyMenu:MakePopup()
		LobbyMenu:SetKeyboardInputEnabled( false )
	end*/
end

function GM:OnSpawnMenuClose()
  hookCall("MinigameClose")
  /*
  if lobby.keepopen then return end
	open = false

	if ( IsValid( LobbyMenu ) ) then
		timer.Simple(0.2, function()
			if not open then
				LobbyMenu:Hide()
			end
		end)

		LobbyMenu:AlphaTo(0, 0.15)
	end*/
  gui.EnableScreenClicker(false)
end
