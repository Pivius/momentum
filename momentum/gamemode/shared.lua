
--[[---------------------------------------------------------

  This file should contain variables and functions that are
   the same on both client and server.

  This file will get sent to the client - so don't add
   anything to this file that you don't want them to be
   able to see.

-----------------------------------------------------------]]
include( "player_class/player_sandbox.lua" )


GM.Name			= "Momentum"
GM.Author		= "Pivius"
GM.Email		= ""
GM.Website		= ""
GM.TeamBased	= false

local folders = {
	"main",
	"stats",
	"movement",
	"watchlist",
	"admin",
	"hud",
	"playback",
	"spectate",
	"console",
	"trick",
	"race"
}
local blacklist = {
	"sh_hook"

}

for _, folder in pairs(folders) do
	if folder == "main" then
		local files = file.Find( "momentum/gamemode/*.lua", "LUA" )
		for _,v in pairs( files ) do
			local skip = false
			for _, b in pairs(blacklist) do
				if string.match(v, b) then
					skip = true
				end
			end
			if !skip then
				if string.match(v, "sv_") then
					include( "momentum/gamemode/" .. v )
				elseif string.match(v, "cl_") and not string.match(v, "cl_init") then
					if CLIENT then
						include( "momentum/gamemode/" .. v )
					else
						AddCSLuaFile( "momentum/gamemode/" .. v )
					end
				elseif string.match(v, "sh_") then
					include( "momentum/gamemode/" .. v )
					if SERVER then
						AddCSLuaFile( "momentum/gamemode/" .. v )
					end
				end
			end
		end
	else
		local files = file.Find( "momentum/gamemode/".. folder .."/*.lua", "LUA" )
		for _,v in pairs( files ) do
			if string.match(v, "sv_") then
				include( "momentum/gamemode/".. folder .."/" .. v )
			elseif string.match(v, "cl_") then
				if CLIENT then
					include( "momentum/gamemode/".. folder .."/" .. v )
				else
					AddCSLuaFile( "momentum/gamemode/".. folder .."/" .. v )
				end
			elseif string.match(v, "sh_") then
				if CLIENT then
					include( "momentum/gamemode/".. folder .."/" .. v )
				else
					include( "momentum/gamemode/".. folder .."/" .. v )
					AddCSLuaFile( "momentum/gamemode/".. folder .."/" .. v )
				end
			end
		end
	end
end

--[[---------------------------------------------------------
   Name: gamemode:KeyPress( )
   Desc: Player pressed a key (see IN enums)
-----------------------------------------------------------]]
function GM:KeyPress( player, key )
end

--[[---------------------------------------------------------
   Name: gamemode:KeyRelease( )
   Desc: Player released a key (see IN enums)
-----------------------------------------------------------]]
function GM:KeyRelease( player, key )
end

--[[---------------------------------------------------------
   Name: gamemode:PlayerConnect( )
   Desc: Player has connects to the server (hasn't spawned)
-----------------------------------------------------------]]
function GM:PlayerConnect( name, address )
end


--[[---------------------------------------------------------
   Name: gamemode:PlayerShouldTakeDamage
   Return true if this player should take damage from this attacker
-----------------------------------------------------------]]
function GM:PlayerShouldTakeDamage( ply, attacker )
	return true
end

--[[---------------------------------------------------------
   Name: gamemode:ShouldCollide( Ent1, Ent2 )
   Desc: This should always return true unless you have
		  a good reason for it not to.
-----------------------------------------------------------]]
function GM:ShouldCollide( Ent1, Ent2 )

	return false

end
