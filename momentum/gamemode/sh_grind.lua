function QuickTrace(ply, vec)
  local pos = ply:GetShootPos()
  local td = {}
  td.start = pos
  td.endpos = (pos - Vector(0,0,76)) + vec
  td.filter = ply
  local tr = util.TraceLine(td)
  if(tr.Fraction ~= 1) then
    local Plane, Last = tr.HitNormal, Vector()

    if(0.7 <= Plane.z and Plane.z < 1) then
      return
    end
  end
  return tr.Hit, tr.HitPos
end


function Grind(ply , mv, cmd)
  ply.grindsound = ply.grindsound or CreateSound(ply, "ambient/energy/force_field_loop1.wav")
  local vel = mv:GetVelocity()

  local x = 1
  local y = 1
  if ply.Grinding == nil then
    ply.Grinding = false
    ply.GrindVel = Vector(0,0,0)
    ply.gCooldown = 0
    ply.gNormal = Vector(0,0,0)
    ply.gHeight = 0
  end
  ply.gNormal:Length()
  if mv:KeyDown(IN_SPEED) and !ply.Grinding and CurTime() > ply.gCooldown then
    local trWest = util.TraceLine ( {
    	start = ply:GetPos()+Vector(ply:OBBMins().x+1,0,-4),
    endpos = ply:GetPos()+Vector(-ply:OBBMins().x,0,-4),
    	filter = ply,
    } )

    local trEast = util.TraceLine ( {
      start = ply:GetPos()+Vector(-ply:OBBMins().x+1,0,-2),
    endpos = ply:GetPos()+Vector(ply:OBBMins().x,0,-4),
      filter = ply,
    } )

    local trSouth = util.TraceLine ( {
      start = ply:GetPos()+Vector(0,ply:OBBMins().y+1,-2),
    endpos = ply:GetPos()+Vector(0,-ply:OBBMins().y,-4),
      filter = ply,
    } )

    local trNorth = util.TraceLine ( {
      start = ply:GetPos()+Vector(0,-ply:OBBMins().y+1,-2),
    endpos = ply:GetPos()+Vector(0,ply:OBBMins().y,-4),
      filter = ply,
    })
    ply.gNormal = Vector(0,0,0)
    if (!trNorth.StartSolid and trNorth.Hit) and (trSouth.StartSolid or !trSouth.Hit) then
      ply.gNormal = trNorth.HitNormal
      ply.GrindPos = Vector(trNorth.HitPos.x,trNorth.HitPos.y, trNorth.HitPos.z+3)
    elseif (!trSouth.StartSolid and trSouth.Hit) and (trNorth.StartSolid or !trNorth.Hit) then
      ply.gNormal = trSouth.HitNormal
        ply.GrindPos = Vector(trSouth.HitPos.x,trSouth.HitPos.y, trSouth.HitPos.z+3)
    elseif (!trEast.StartSolid and trEast.Hit ) and (trWest.StartSolid or !trWest.Hit) then
      ply.gNormal = trEast.HitNormal
      ply.GrindPos = Vector(trEast.HitPos.x,trEast.HitPos.y, trEast.HitPos.z+3)
    elseif (!trWest.StartSolid and trWest.Hit) and (trEast.StartSolid or !trEast.Hit) then
      ply.gNormal = trWest.HitNormal
      ply.GrindPos = Vector(trWest.HitPos.x,trWest.HitPos.y, trWest.HitPos.z+3)
    elseif (!trNorth.StartSolid and trNorth.Hit) and (!trSouth.StartSolid and trSouth.Hit) then
      local dif = (trNorth.HitPos-trSouth.HitPos)/2
      local normDif = (trNorth.HitNormal-trSouth.HitNormal)/2
      ply.gNormal = trNorth.HitNormal
      ply.GrindPos = Vector((trNorth.HitPos-dif).x,(trNorth.HitPos-dif).y, trNorth.HitPos.z+3)
    elseif (!trEast.StartSolid and trEast.Hit) and (!trWest.StartSolid and trWest.Hit) then
      local dif = (trEast.HitPos-trWest.HitPos)/2
      local normDif = (trEast.HitNormal-trWest.HitNormal)/2
      ply.gNormal = trEast.HitNormal
      ply.GrindPos = Vector((trEast.HitPos-dif).x,(trEast.HitPos-dif).y, trEast.HitPos.z+3)
    end



    if ply.gNormal:Length() != 0 then
      mv:SetOrigin(Vector(ply.GrindPos.x, ply.GrindPos.y, ply.GrindPos.z+0.5))
      ply.Grinding = true
      ply.grindsound:Play()
      ply.GrindVel = vel--*1.5
      ply.gHeight = ply:GetPos().z
      ply.OldAA = ply.AirAccel
      ply.gTimer = CurTime() + 3
    end
	end

  if ply.Grinding then
    ply.gFriction = math.Clamp(math.TimeFraction( ply.gTimer+10, ply.gTimer-5, CurTime() ), 0.25, 1)
    local velnorm = vel:GetNormalized()

    velnorm.x = velnorm.x * math.abs(ply.gNormal.y)
    velnorm.y = velnorm.y * math.abs(ply.gNormal.x)
    velnorm.z = velnorm.z * math.abs(ply.gNormal.z)
    local grindVel = velnorm * (ply.GrindVel:Length()+250)
    --mv:SetForwardSpeed(0)
    --mv:SetSideSpeed(0)

    --ply.AirAccel = 0

    mv:SetOrigin(Vector(mv:GetOrigin().x, mv:GetOrigin().y, ply.GrindPos.z+0.5))
    mv:SetVelocity(grindVel*ply.gFriction )
    if !mv:KeyDown(IN_SPEED) or ply.gNormal:Length() == 0 or ply:GetPos().z != ply.gHeight then
      ply.gCooldown = CurTime()+1
      ply.Grinding = false
      ply.AirAccel = ply.OldAA
      ply.grindsound:FadeOut(0.5)
    end
    /*
    if mv:KeyDown(IN_JUMP) then
      if !ply:OnGround() then
        mv:SetVelocity(mv:GetVelocity()+Vector(0,0,200))
      end
      ply.gCooldown = CurTime()+1
      ply.Grinding = false
      ply.AirAccel = ply.OldAA
      ply.grindsound:FadeOut(0.5)
    end*/

  end

end
--  hook.Add("SetupMove", "Grine", Grind)
