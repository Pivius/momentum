AddCSLuaFile('cl_init.lua')
AddCSLuaFile('shared.lua')

include('parkour/gamemode/sv_tricks.lua')
include('shared.lua')
function ENT:Initialize()
	local BBOX = (self.max - self.min)
	self:SetSolid( SOLID_BBOX )
	self:PhysicsInitBox( -BBOX, BBOX )
	self:SetCollisionBoundsWS( self.min, self.max )
	self.Surfbool = {}
	self.Groundbool = {}
	self:SetTrigger( true )
	self:DrawShadow( false )
	self:SetNotSolid( true )
	self:SetNoDraw( false )

	self.Phys = self:GetPhysicsObject()
	if self.Phys and self.Phys:IsValid() then
		self.Phys:Sleep()
		self.Phys:EnableCollisions( false )
	end
	self:SetZoneName( self.name )
	self:SetMode(self.mode )
end


function ENT:StartTouch( ent )
	if IsValid( ent ) and ent:IsPlayer() and ent:Team() != TEAM_SPECTATOR then
		if !self.Surfbool[ent] then
			self.Surfbool[ent] = false
			self.Groundbool[ent] = false
		end
		local zone = self:GetZoneName()

		if self:GetMode() == "Enter" then
			--if ent:ZoneOrder() == nil or table.GetLastValue(ent:ZoneOrder()) != self.name then
				--PrintMessage( HUD_PRINTTALK, ent:Nick() .. " has entered " .. zone )
				ent:AddZone( zone, "entered", ent:GetSpeed() )

			end
		--end
	end
end

function Surfing( ent )
        local vPos = ent:GetPos()

        local vMins = ent:OBBMins()

        local vMaxs = ent:OBBMaxs()

        local vEndPos = vPos * 1
        vEndPos.z = vEndPos.z -1

        local tr = util.TraceHull{
            start = vPos,
            endpos = vEndPos,
            mins = vMins,
            maxs = vMaxs,
            mask = MASK_PLAYERSOLID_BRUSHONLY,

            filter = function(e1, e2)
                return not e1:IsPlayer()
            end

        }
        if(tr.Fraction ~= 1) then
            -- Gets the normal vector of the surface under the player
            local vPlane, vLast = tr.HitNormal, Vector()

            -- Make sure it's not flat ground and not a surf ramp (1.0 = flat ground, < 0.7 = surf ramp)
            if(0.2 <= vPlane.z and vPlane.z < 1) then
							return true
						else

							return false
            end
					else
						return false
        end
end
function ENT:Touch( ent )
	if IsValid( ent ) and ent:IsPlayer() and ent:Team() != TEAM_SPECTATOR then
		local zone = self:GetZoneName()
		local surfing = Surfing( ent )


		if self:GetMode() == "Surfing" then
			--if ent:ZoneOrder() == nil or table.GetLastValue(ent:ZoneOrder()) != self.name then
				if surfing == true and self.Surfbool[ent] == false then
					self.Surfbool[ent] = true
					ent:AddZone( zone, "surfing", ent:GetSpeed() )
				elseif surfing == false and self.Surfbool[ent] == true then
					ent:AddZone( zone, "leave", ent:GetSpeed() )
					--self.Surfbool[ent] = false
				end
			--end
		elseif self:GetMode() == "Ground" then
			--if ent:ZoneOrder() == nil or table.GetLastValue(ent:ZoneOrder()) != self.name then
				if ent:OnGround() and self.Groundbool[ent]  == false and !surfing then

					self.Groundbool[ent]  = true
					--PrintMessage( HUD_PRINTTALK, ent:Nick() .. " landed on " .. zone )
					ent:AddZone( zone, "ground", ent:GetSpeed() )

				elseif !ent:OnGround() and self.Groundbool[ent]  == true then
				--PrintMessage( HUD_PRINTTALK, ent:Nick() .. " has left " .. zone )
					--ent:AddZone( zone, "leaveground", ent:GetSpeed() )
					ent:AddZone( zone, "leaveground", ent:GetSpeed() )
					self.Groundbool[ent]  = false
				end
			--end
		elseif self:GetMode() == "WallKick" then
			if table.GetLastValue(ent:ZoneOrder()) != self.name then

				if ent.Kicked then
					ent:AddZone( zone, "kick", ent:GetSpeed() )
				end
			end
		end
	end
end

function ENT:EndTouch( ent )
	if IsValid( ent ) and ent:IsPlayer() and ent:Team() != TEAM_SPECTATOR then
		local zone = self:GetZoneName()
		if self:GetMode() == "Enter" then
			ent:AddZone( zone, "leave", ent:GetSpeed() )
		elseif self:GetMode() == "Surfing" and self.Surfbool[ent]  == true then
			self.Surfbool[ent]  = false
			ent:AddZone( zone, "leave", ent:GetSpeed() )
		elseif self:GetMode() == "Ground" and self.Groundbool[ent]  == true then
			self.Groundbool[ent]  = false
			ent:AddZone( zone, "leave", ent:GetSpeed() )
		end
	end
end
