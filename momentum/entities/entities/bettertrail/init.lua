AddCSLuaFile('cl_init.lua')
AddCSLuaFile('shared.lua')
include('shared.lua')
function ENT:Initialize()
  self:SetTrigger( true )
	self:DrawShadow( false )
	self:SetNotSolid( true )
	self:SetNoDraw( false )

  self.Phys = self:GetPhysicsObject()
	if self.Phys and self.Phys:IsValid() then
		self.Phys:Sleep()
		self.Phys:EnableCollisions( false )
	end
  self:SetStartWidth( 5 )
	self:SetEndWidth(0 )
  self:SetLifeTime( 5 )
  self:SetStartFade(0 )
  self:SetNodeLength( 10 )
  self:SetTrailColor(Vector(255,255,255))
  self:SetMaterial("materials/zone/zone.png")
  self:SetTrailAlpha(255)
  self:SetReset(false)
  self:SetDashed(false)
  self:SetDashDist(10)
  self:SetUpdateRate(10)
end

function ENT:startWidth(w)
  self:SetStartWidth( w )
end

function ENT:endWidth(w)
  self:SetEndWidth( w )
end

function ENT:lifeTime(lt)
  self:SetLifeTime( lt )
end

function ENT:startFade(sf)
  self:SetStartFade( sf )
end

function ENT:nodeLength(sl)
  self:SetNodeLength( sl )
end

function ENT:Color(col)
  self:SetTrailColor( Vector(col.r,col.g,col.b) )
  self:SetTrailAlpha(col.a)
end

function ENT:Reset()
  self:SetReset(true)
end

function ENT:Dashed(d)
  self:SetDashed( d )
end

function ENT:updateRate(rt)
  self:SetUpdateRate( rt )
end

function ENT:dashDist(dd)
  self:SetDashDist( dd )
end

 -- If for some reason my boundingbox code doesn't work correctly for you, try this!
function ENT:UpdateTransmitState()
	return TRANSMIT_PVS
end
