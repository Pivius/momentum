ENT.Type = "anim"
ENT.Base = "base_anim"
ENT.RenderGroup		= RENDERGROUP_TRANSLUCENT
function ENT:SetupDataTables()
	self:NetworkVar( "Float", 0, "StartWidth" )
	self:NetworkVar( "Float", 1, "EndWidth" )
	self:NetworkVar( "Float", 2, "LifeTime" )
  self:NetworkVar( "Float", 3, "StartFade" )
  self:NetworkVar( "Float", 4, "NodeLength" )
  self:NetworkVar( "Vector", 5, "TrailColor" )
  self:NetworkVar( "Float", 6, "TrailAlpha" )
  self:NetworkVar( "String", 7, "Material" )
  self:NetworkVar( "Bool", 8, "Reset" )
	self:NetworkVar( "Bool", 9, "Dashed" )
	self:NetworkVar( "Float", 10, "DashDist" )
	self:NetworkVar( "Float", 11, "UpdateRate" )
end
