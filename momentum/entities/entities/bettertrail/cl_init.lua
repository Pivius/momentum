include('shared.lua')


function ENT:getTrailNode(n)
  if self.nodes[n] == nil then return nil end
  return self.nodes[n]
end

-- Scale renderbound with trail.
function ENT:updateBoundingBox()
  local renderOrigin = self:GetPos()
	local renderMins = renderOrigin
	local renderMaxs = renderOrigin
	local maxs, mins = Vector()
  if !self.renderedMins then
    self.renderedMins = Vector()
    self.renderedMaxs = Vector()
  end
  for k, tNode in next, self.nodes do

    if tNode != nil && !tNode.Rendered then

      -- Node width
      local nWidth = Vector(tNode.Width,tNode.Width,tNode.Width)
      mins = tNode.Pos - nWidth
      maxs = tNode.Pos + nWidth

      -- Gets the vector that is the furthest out.
      renderMins = Vector(math.min(renderMins.x, mins.x),math.min(renderMins.y, mins.y),math.min(renderMins.z, mins.z))
      renderMaxs = Vector(math.max(renderMaxs.x, maxs.x),math.max(renderMaxs.y, maxs.y),math.max(renderMaxs.z, maxs.z))
    end
  end

  -- Get localvector
	renderMins = renderMins - renderOrigin
  renderMaxs = renderMaxs - renderOrigin

  self:SetRenderBounds(renderMins, renderMaxs)
end

function ENT:updateTrails()

  local lifetime   = self:GetLifeTime()
  local endWidth   = self:GetEndWidth()
  local startWidth = self:GetStartWidth()
  local startFade  = self:GetStartFade()*100 -- To avoid values in the 1000s
  local Dashed     = self:GetDashed()

  if startFade == 0 then
    startFade = 1
  end

  for k, tNode in next, self.nodes do

    if tNode == nil then return end

    local nDashed = tNode.Dashed
    local nSolid = tNode.Solid
    local nFO = tNode.fadeOut
    local nAlpha = tNode.Alpha
    local nPos = tNode.Pos
    local nW = tNode.Width
    local nT = tNode.Time

    local dieTime = math.Clamp(( CurTime( ) - nT ) / (lifetime), 0, 1)
    local liveTime = math.Clamp(( CurTime( ) - nT ) / (startFade/lifetime), 0, 1)

    if nAlpha >= 254 then

      tNode.fadeOut = true
    end

    -- Check if the node can fade out
    if nFO == false then
      tNode.Alpha = Lerp( liveTime, nAlpha, self:GetTrailAlpha() )
    else
      if lifetime != 0 then
        tNode.Alpha = Lerp(dieTime, nAlpha, 0 )
      end
    end
    --if Dashed then -- Dashed trail
      if !nDashed and !nSolid then
        tNode.Alpha = 0 -- Sets the alpha of segments in between dashes
      end
    --end
    if k > 1 then
      -- Checks to see if the distance between each nodes is too big
      if nPos:Distance( self:getTrailNode(k-1).Pos) > self:GetNodeLength()+300 then
        --Set the alpha to 0
        tNode.Alpha = 0
        self:getTrailNode(k-1).Alpha = 0
      end
    end

    -- Doesn't fade out trail if lifetime is 0
    if lifetime != 0 then
      tNode.Width = Lerp( dieTime/2, nW, endWidth )
    end

    -- Node width can't be greater than startwidth
    if nW > startWidth then
      tNode.Width = startWidth
    end
    --if #self.nodes > 100 then
      --table.remove( self.nodes, 1 )
    --end
    -- Remove unnecessary nodes
    if math.Round(nAlpha, 1) <= 1 and nDashed and nFO then
      table.remove( self.nodes, k )
    end
    if endWidth < startWidth then
      if math.Round( nW, 2 ) <= endWidth or math.Round( nW, 2 ) <= 0.25  then
        table.remove( self.nodes, k )
      end
    else
      if math.Round( nW, 1 ) >= endWidth then
        table.remove( self.nodes, k )
      end
    end

  end



  if self:GetReset() then
    self:SetReset(false)
    self.nodes = {}
  end

end

function ENT:Initialize()
  self:SetRenderAngles( Angle(0,0,0) )
  self.nodes = {}
  self.wasDashed = false
end

---------- Dashed trail ----------
function ENT:CreateDashed(node, lastnode)
  if !node.wasDashed then
    print("a")
    local nP = {}
    nP.Width = self:GetStartWidth() -- Point start width
    nP.Time  = CurTime() -- Time of creation
    nP.Pos   = self:GetPos()+Vector(0,0,5) -- Position of the trail. +5 in Z vector to avoid clipping in ground
    nP.Color = Color( self:GetTrailColor().x, self:GetTrailColor().y, self:GetTrailColor().z, 0 )
    nP.Solid = true -- Is Solid
    nP.Dashed = false
    nP.wasDashed = false
    nP.Alpha = 0
    nP.Rendered = false
    self:getTrailNode(#self.nodes).Alpha = 0
    table.insert( self.nodes, nP )
  end
  node.Solid = false
  if lastnode == nil then -- Last node gone?
    node.Dashed = true
    table.insert( self.nodes, node )
  else
    local prevDashed = lastnode.Dashed -- Was previous node dashed?
    local Dist =  math.Round((lastnode.Pos - node.Pos):Length()) -- New node and old node distance
    if prevDashed then
      if Dist >= self:GetNodeLength() then -- Invisible dash
        local D = (lastnode.Pos - node.Pos) -- Position correction incase of distance overshooting
        D:Normalize()
        D:Mul(math.Round(Dist-self:GetNodeLength()))
        local t         = node.Pos + D
        node.Pos    = t
        node.Dashed = false

        table.insert( self.nodes, node )
      end
    else
      if Dist >= self:GetDashDist() then -- Visible dash
        local D = (lastnode.Pos - node.Pos) -- Position correction incase of distance overshooting
        D:Normalize()
        D:Mul(math.Round(Dist-self:GetDashDist()))
        local t = node.Pos + D
        node.Pos = t
        node.Dashed = true
        --print((self:GetPos()-lastnode.Pos):Length()-self:GetDashDist())
        table.insert( self.nodes, node )
        --for i=0, math.floor(D:Distance(self:GetPos())) do

        --end
      end
    end
  end
end

--———————— Solid trail ——————————
function ENT:CreateSolid(node, lastnode)
  if node.wasDashed then
    local nP = {}
    nP.Width = self:GetStartWidth() -- Point start width
    nP.Time  = CurTime() -- Time of creation
    nP.Pos   = self:GetPos()+Vector(0,0,5) -- Position of the trail. +5 in Z vector to avoid clipping in ground
    nP.Color = Color( self:GetTrailColor().x, self:GetTrailColor().y, self:GetTrailColor().z, 0 )
    nP.Solid = true -- Is Solid
    nP.Dashed = false
    nP.wasDashed = false
    nP.Alpha = 0
    nP.Rendered = false
    self:getTrailNode(#self.nodes).Alpha = 0
    table.insert( self.nodes, nP )
  end
  if lastnode == nil then
    table.insert( self.nodes, node )
  else
    -- Avoid making nodes shorter than nodelength.
    if lastnode.Pos:Distance( node.Pos ) > self:GetNodeLength() then
      table.insert( self.nodes, node )
    end
  end
end

function ENT:createNode()
  local pLast = #self.nodes -- Amount of nodes / last node

  local lastNode = self.nodes[pLast]

  local newPoint = {
    ["Width"] = self:GetStartWidth(), -- Point start width
    ["Time"] = CurTime(), -- Time of creation
    ["Pos"] = self:GetPos()+Vector(0,0,5), -- Position of the trail. +5 in Z vector to avoid clipping in ground
    ["Color"] = Color( self:GetTrailColor().x, self:GetTrailColor().y, self:GetTrailColor().z, self:GetTrailAlpha() ),
    ["Solid"] = true, -- Is Solid
    ["Dashed"] = false,
    ["wasDashed"] = self.wasDashed,
    ["Alpha"] = 0,
    ["Rendered"] = false
  }
  self.wasDashed = self:GetDashed()
  if self:GetStartFade() == 0 then -- No start fade?
    newPoint.Alpha = self:GetTrailAlpha()
    newPoint.fadeOut = true
  else
    newPoint.Alpha = 0
    newPoint.fadeOut = false
  end

  ---------- Dashed trail ----------
  if self:GetDashed() then -- Is Dashed?
    self:CreateDashed(newPoint, lastNode)
  else
    --———————— Solid trail ——————————
    self:CreateSolid(newPoint, lastNode)
  end
end

function ENT:Think()

  self:updateTrails() -- Update trail alpha and what not.


  if IsValid( self:GetParent() ) then -- Check to see if this node has a parent.
    if self:GetParent():GetVelocity():Length() == 0 and !self:GetDashed() then -- Avoid unnecessary nodes.

      return true
    elseif self:GetParent():GetVelocity():Length() > 0 then
      self:updateBoundingBox() -- Update boundingbox to make it visible when looking at it.
      self:NextThink( CurTime() + 2 )
    end
  else -- Do not need to update anything if it's not parented.

    return
  end
  // Fixing a bug where the dashed trail would struggle to keep up to the player.
  //TODO Create a better way to make dashed trails with half the nodes to make it more optimized
  if self:GetDashed() then
    local lN = self.nodes[#self.nodes-1]

    if !lN then
      self:createNode()
      return
    end
    local curNode = self.nodes[#self.nodes]
    local Dist =  math.Round((lN.Pos - self:GetPos()+Vector(0,0,5)):Length()) -- New node and old node distance
    local D = (lN.Pos - curNode.Pos) -- Position correction incase of distance overshooting
    local length = self:GetNodeLength()
    if !lN.Dashed then
      length = self:GetDashDist()
    end
    for i=1, math.ceil(Dist/length) do

      self:createNode() -- Create new node.
    end
  else
    self:createNode() -- Create new node.
  end


  return true
end

-- Draw trail
function DrawTrail(ent)
  -- Variables for the segment under the player.
  local width = ent:GetStartWidth() -- Trail width.
  local color = Color(255,255,255,0) -- Trail color.
  if ent:getTrailNode(#ent.nodes) != nil then
    width = ent:getTrailNode(#ent.nodes).Width -- Trail width.
    color = ent:getTrailNode(#ent.nodes).Color -- Trail Color.
  end

  render.SetMaterial( Material( ent:GetMaterial() ) )


  ---------- Dashed trail ----------
  for k, v in pairs( ent.nodes ) do
    if v.Dashed and !v.Solid then -- Is Dashed
      if k+1 <= #ent.nodes then
        render.StartBeam( 2 )
          render.AddBeam( v.Pos , v.Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
          render.AddBeam( ent:getTrailNode(k+1).Pos , ent:getTrailNode(k+1).Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
        render.EndBeam()
      end
    elseif !v.Dashed and !v.Solid then
      if k+1 <= #ent.nodes then
        render.StartBeam( 2 )
          render.AddBeam( v.Pos , v.Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
          render.AddBeam( ent:getTrailNode(k+1).Pos , ent:getTrailNode(k+1).Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
        render.EndBeam()
      end
    end
  end

  if !ent:getTrailNode(#ent.nodes).Solid then
    render.StartBeam(2) -- The line segment under the player
      local lastNode = ent:getTrailNode(#ent.nodes)
      if ent:GetStartFade() == 0 and (lastNode and lastNode.Dashed) then -- Should not fade in?
        render.AddBeam( lastNode.Pos, lastNode.Width, 1.0, Color( lastNode.Color.r, lastNode.Color.g, lastNode.Color.b, lastNode.Alpha ) )
        render.AddBeam( ent:GetPos() + Vector( 0, 0, 5 ), width, 1.0, Color( color.r, color.g, color.b, ent:GetTrailAlpha() ) )
      else -- Should fade in
        if (lastNode and lastNode.Dashed) then
          render.AddBeam( lastNode.Pos, lastNode.Width, 1.0, Color( lastNode.Color.r, lastNode.Color.g, lastNode.Color.b, lastNode.Alpha ) )
          render.AddBeam( ent:GetPos() + Vector( 0, 0, 5 ), width, 1.0, Color( lastNode.Color.r, lastNode.Color.g, lastNode.Color.b, lastNode.Alpha ) )
        end
      end
    render.EndBeam()
  end
  --———————— Solid trail ——————————

  render.StartBeam( #ent.nodes + 1 )
  for k, v in pairs( ent.nodes ) do
    if v.Solid then
      render.AddBeam( v.Pos , v.Width, 1.0, Color( v.Color.r, v.Color.g, v.Color.b, v.Alpha ) )
    end
  end
  if ent:getTrailNode(#ent.nodes).Solid then
    if ent:GetStartFade() == 0 then -- Show the trail beneath the player if it doesn't have startfade
      render.AddBeam( ent:GetPos() + Vector( 0, 0, 5 ), width, 1.0, Color( color.r, color.g, color.b, ent:getTrailNode(#ent.nodes).Alpha ) )
    else -- Invisible node, Exists only to get the fade effect on the last node.
      render.AddBeam( ent:GetPos()+Vector(0,0,5), width, 1.0, Color( color.r, color.g, color.b, 0 ) )
    end
  end
  render.EndBeam()
end

function ENT:Draw()

  if #self.nodes > 0 then
    DrawTrail(self)
  end
end
