ENT.Type = "anim"
ENT.Base = "base_gmodentity"

function ENT:Initialize()
  self:DrawShadow( false )
	self:SetNotSolid( false )
	self:SetNoDraw( false )
  self:PhysicsInit(SOLID_VPHYSICS)
  self:SetMoveType(MOVETYPE_FLY)
  self:SetSolid(SOLID_VPHYSICS)
	--self.Entity:SetCollisionGroup(COLLISION_GROUP_NPC)
  self:SetGravity(0)
	local phys = self:GetPhysicsObject()

	if phys and phys:IsValid() then
		phys:Wake()
	end
  self.BlewUp = false
end

function ENT:OnTakeDamage(dmginfo)
	return false
end

function ENT:Use(activator, caller)
	return false
end

function ENT:Think()
  if self.BlewUp then return end
  if SERVER then
    local rf = RecipientFilter()
    rf:AddAllPlayers()
    rf:RemovePlayer(self:GetOwner())
  end
  if IsFirstTimePredicted() then
    local rocketEffect = EffectData()
    rocketEffect:SetOrigin( self:GetPos() )
    rocketEffect:SetEntity( self )
    util.Effect( "rocket", rocketEffect, true, rf )
  end
  self:NextThink( CurTime() + 0.01 )
  return true
end

function ENT:StartTouch(ent)
	local owner = self:GetOwner()
	if ent != owner and ent:GetClass() != "trigger_soundscape" then
		if !ent:IsPlayer() then
			self:Explode()

		end
	end
end

function ENT:Explode()
  if AAS_WeaponJumpZVelocity(self:GetOwner():GetPos(), self:GetPos(), 120):Length() > 0 then
    self:GetOwner():SetVelocity(self:GetOwner():GetVelocity()+AAS_WeaponJumpZVelocity(self:GetOwner():GetPos(), self:GetPos(), 120))
  end
  if self.BlewUp then return end
  self:SetLocalVelocity(Vector(0,0,0))
  if IsFirstTimePredicted() then
    if SERVER then
      local rf = RecipientFilter()
      rf:AddAllPlayers()
      rf:RemovePlayer(self:GetOwner())
    end
    local hitEffect = EffectData()
    hitEffect:SetOrigin( self:GetPos() )
    hitEffect:SetEntity( self )
    util.Effect( "explode", hitEffect, true, rf )
    if SERVER then
      EmitSound("weapons/mortar/mortar_explode1.wav", self:GetPos(), 1, CHAN_WEAPON, 1, 140, 0, 100)
    end
    timer.Simple(5, function()
      SafeRemoveEntity(self)
    end)
    self.BlewUp = true
  end
end
