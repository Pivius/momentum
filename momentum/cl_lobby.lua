lobby = lobby or {}
lobby.lobbies = {}
lobby.keepopen = false


function syncLobbies()
  net.Start("lobbyNetworking")
    net.WriteString("sync")
    net.WriteTable({LocalPlayer()})
  net.SendToServer()
end

local LOBBY_BUTTON =
{
	Init = function( self )
    self.Host = ""
    self.Mode = ""
    self.Name = ""
    self.Players = ""
    self.Settings = {}

  end,

  Setup = function( self, lb, lst, id )

		self.Host = lb["lobbyHost"]
    self.Mode = lb["lobbyMode"]
    self.Name = lb["lobbyName"]
    self.Players = lb["lobbyPlayers"]
    self.Settings = lb["lobbySettings"]
    self.List = lst
    self.LobbyID = id
		self:Think( self )
    local alpha = 0

    self.ButtonColor = self:Add( "DModelPanel" )

    self.ButtonColor:SetAlpha(255)
    self.ButtonColor:SetColor(lobby.minigames[self.Mode])
    self.ButtonColor:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Alpha = vgui.Create("DButton")
    self.Alpha:SetSize(0,0)
    self.Button.Name = self.Name
    self.Button.Host = self.Host
    self.Alpha:SetAlpha(10)

    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, 255) )
      surface.DrawRect(0, 0, w, h)
    end
    self.Button:SetText("")
    local alphabut = self.Alpha
    local colorbut = self.ButtonColor
    self.Button:SetSize(self:GetWide(),self:GetTall())
    --self.Button:Dock(TOP)
    self.Button.DoClick = function()
      alphabut:SetAlpha(250)
      alphabut:AlphaTo( alpha, 0.2, 0)
      colorbut:SetColor(Color(255,255,255))

      colorbut:ColorTo(lobby.minigames[lb["lobbyMode"]], 0.2)
      if !table.HasValue(self.Players, LocalPlayer()) then
        net.Start("lobbyNetworking")
          net.WriteString("join")

          net.WriteTable( {[1] = LocalPlayer(), [2] = self.LobbyID, [3] = false} ) --replace host with lobbyid
        net.SendToServer()
      else
        net.Start("lobbyNetworking")
          net.WriteString("leave")
          net.WriteTable( {[1] = LocalPlayer()} )
        net.SendToServer()
      end
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      alphabut:AlphaTo( 120, 0.3 , 0)
      alpha = 120

    end

    self.Button.OnCursorExited = function(self)
      alphabut:AlphaTo( 10, 0.3 , 0)
      alpha = 10
    end

    self.Button.Paint = function(self, w, h)

      surface.SetDrawColor( Color(colorbut:GetColor().r,colorbut:GetColor().g,colorbut:GetColor().b, alphabut:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "Scoreboard line" )
      self.txtw, self.txth = surface.GetTextSize(self.Name)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self.Name )
    end

	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    if !lobby.lobbies[self.LobbyID] then
      self.Button.Paint = function() end
      self.ButtonColor:Remove()
      self.Button:Remove()
      self:Remove()
    return end

    if lobby.lobbies[self.LobbyID]["lobbyHost"] != self.Host or lobby.lobbies[self.LobbyID]["lobbyMode"] != self.Mode or lobby.lobbies[self.LobbyID]["lobbySettings"] != self.Settings then
      self.Button.Paint = function() end
      self.ButtonColor:Remove()
      self.Button:Remove()
      self:Remove()
			return
		end
  end
}

LOBBY_BUTTON = vgui.RegisterTable( LOBBY_BUTTON, "EditablePanel" )

local MINIGAME_BUTTON =
{
	Init = function( self )
    self.Minigame = "None"
    self.func = function() end
    local alpha = 0
    self.Alpha = self:Add( "DButton" )
    self.Alpha:SetAlpha(0)
    self.Alpha:SetText("")
    self.Alpha.Paint = function(self, w, h)
    end

    self.TextAlpha = self:Add( "DModelPanel" )

    self.TextAlpha:SetAlpha(255)
    self.TextAlpha:SetColor(lobby.minigames[self.Minigame])
    self.TextAlpha:SetSize( 0, 0 )

    self.Button = self:Add( "DButton" )
    self.Button:SetPos( 0, 0 )
    self.Button:SetSize( (850+200 - 210 - 30)/5, 100 )
    self.Button:SetText("")
    self.Button.DoClick = function()
      if self.Minigame == "None" then return end
      self.Alpha:SetAlpha(250)
      self.Alpha:AlphaTo( alpha, 0.2, 0)
      self.funct = self.func(self)
    end
    self.Button:SetAlpha(255)
    self.Button.OnCursorEntered = function(self)
      self:GetParent().Alpha:AlphaTo( 200, 0.3 , 0)
      self:GetParent().TextAlpha:ColorTo(Color(255,255,255), 0.3)
      alpha = 200
    end

    self.Button.OnCursorExited = function(self)
      self:GetParent().Alpha:AlphaTo( 0, 0.3 , 0)
      self:GetParent().TextAlpha:ColorTo(lobby.minigames[self:GetParent().Minigame], 0.3)
      alpha = 0
    end

    self.Button.Paint = function(self, w, h)
      surface.SetDrawColor( Color(25,25,25, self:GetParent().Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(self:GetParent().TextAlpha:GetColor())
      surface.SetFont( "Scoreboard info" )
      self.txtw, self.txth = surface.GetTextSize(self:GetParent().Minigame)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self:GetParent().Minigame )
    end
  end,
  ReColor = function(self)
    self.TextAlpha:SetColor(lobby.minigames[self.Minigame])
  end,
	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
  end
}

MINIGAME_BUTTON = vgui.RegisterTable( MINIGAME_BUTTON, "EditablePanel" )

local LOBBY =
{
	Init = function( self )

    self.Lobbies= self:Add( "DFrame" )

    self.Lobbies:SetPos( 0, 5 )
    self.Lobbies:SetSize(200, ScrH()/1.015)
    self.Lobbies:SetTitle("")
    self.Lobbies:SetDraggable(false)
    self.Lobbies:ShowCloseButton( false )
    self.Lobbies.Paint = function(self, w, h )
      surface.SetDrawColor( Color(25,25,25, 250) )
      surface.DrawRect(0, 0, w, h)
    end

    self.LobbiesTitle = self.Lobbies:Add( "DLabel" )
    self.LobbiesTitle:SetPos( 60, 2 )
    self.LobbiesTitle:SetSize(110, 25)
    self.LobbiesTitle:SetFont( "Scoreboard info" )
    self.LobbiesTitle:SetTextColor(Color(255, 255, 255))
    self.LobbiesTitle:SetText("Lobbies")

    self.Lobbies.List = vgui.Create("DScrollPanel", self.Lobbies)
    --self.Lobbies.List:SetSize(200, ScrH()/1.015-50)
    --self.Lobbies.List:SetPos(20, 5+50)
    self.Lobbies.List:Dock( FILL )
    self.Lobbies.List:SetVerticalScrollbarEnabled(false)

    self.Lobbies.List.VBar:SetAlpha(0)

    self.Lobbies.List.offset = 0

    self.Lobbies.List.Paint = function(self, w, h )

    end

    self.Lobbies.List.OnVScroll = function(self, iOffset )
      self.offset = iOffset
    end

    self.Lobbies.List.Think = function(self )
      self.offsetsmooth = self.offsetsmooth and UT:Lerp(0.03, self.offsetsmooth , self.offset) or self.offset

      self.pnlCanvas:SetPos( 0, self.offsetsmooth )
    end

    self.Lobbies.List.PerformLayout = function(self )
      local wide = self:GetWide()

      self:Rebuild()

      self.VBar:SetUp( self:GetTall(), self.pnlCanvas:GetTall() )
      self.VBar:SetSize(0,0)
      if self.VBar.Enabled then wide = wide end-- self.VBar:GetWide() end

      self.pnlCanvas:SetWide(wide)

      self:Rebuild()
    end

    self.Frame= self:Add( "DFrame" )
    self.Frame:SetPos( 210, 5 )
    self.Frame:SetSize(850+200 - 210, ScrH()/1.62)
    self.Frame:SetTitle("")
    self.Frame:SetDraggable(false)
    self.Frame:ShowCloseButton( false )
    self.Frame.Paint = function(self, w, h )
      surface.SetDrawColor( Color(25,25,25, 250) )
      surface.DrawRect(0, 0, w, h)
    end

    self.MinigameInfo= self:Add( "DFrame" )
    self.MinigameInfo:SetPos( 210, 5 )
    self.MinigameInfo:SetSize(850+200 - 210, ScrH()/1.62)
    self.MinigameInfo:SetTitle("")
    self.MinigameInfo:SetDraggable(false)
    self.MinigameInfo:ShowCloseButton( false )
    self.MinigameInfo:SetAlpha(0)
    self.MinigameInfo.Paint = function(self, w, h )
    end
    local sizex, sizey = self.MinigameInfo:GetSize()

    self.LobbyTitle = self.MinigameInfo:Add( "DLabel" )
    self.LobbyTitle:SetPos( sizex/2 - 25, 10 )
    self.LobbyTitle:SetSize(200, 25)
    self.LobbyTitle:SetFont( "Scoreboard info" )
    self.LobbyTitle:SetTextColor(Color(255, 255, 255))
    self.LobbyTitle:SetText("")

    self.LobbyDesc = self.MinigameInfo:Add( "DLabel" )
    self.LobbyDesc:SetSize(850+200 - 210, 30)
    self.LobbyDesc:SetFont( "Scoreboard line" )
    self.LobbyDesc:SetTextColor(Color(255, 255, 255))
    --self.LobbyDesc:SetAutoStretchVertical( true )
    self.LobbyDesc:SetText("")
    surface.SetFont( "Scoreboard line" )
    self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
    self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

    self.LobbyDesc2 = self.MinigameInfo:Add( "DLabel" )
    self.LobbyDesc2:SetSize(850+200 - 210, 30)
    self.LobbyDesc2:SetFont( "Scoreboard line" )
    self.LobbyDesc2:SetTextColor(Color(255, 255, 255))
    --self.LobbyDesc:SetAutoStretchVertical( true )
    self.LobbyDesc2:SetText("")
    surface.SetFont( "Scoreboard line" )
    self.LobbyDesc2.txtw, self.LobbyDesc2.txth = surface.GetTextSize(self.LobbyDesc2:GetText())
    self.LobbyDesc2:SetPos( sizex/2 - self.LobbyDesc2.txtw/2, 55 )

    self.GravityTitle = self.MinigameInfo:Add( "DLabel" )
    self.GravityTitle:SetPos( sizex/2*1.5 - 145/2 + 30, sizey/2.5 )
    self.GravityTitle:SetSize(145, 35)
    self.GravityTitle:SetFont( "Scoreboard info" )
    self.GravityTitle:SetTextColor(Color(255, 255, 255))
    self.GravityTitle:SetText("Gravity")

    self.Gravity = vgui.Create( "DButton", self.MinigameInfo )
    self.GravityCover= self.Gravity:Add( "DFrame" )
    self.GravityCover:SetTitle("")
    self.GravityCover:SetDraggable(false)
    self.GravityCover:ShowCloseButton( false )
    self.GravityCover:SetSize(0, 0)
    self.GravityCover.Paint = function(self, w, h)
      surface.SetDrawColor( Color(10,10,10, 210) )
      surface.DrawRect(0, 0, w, h)

    end
    self.Gravity.Alpha = self:Add( "DButton" )
    self.Gravity.Alpha:SetAlpha(3)
    self.Gravity.Alpha:SetText("")
    self.Gravity.Alpha.Paint = function(self, w, h)
    end
    self.Gravity.Alpha.alpha = 0
    self.Gravity:SetPos( sizex/2*1.5 - 145/2, sizey/2)
    self.Gravity:SetSize( 145, 35 )
    self.Gravity:SetText("")
    self.Gravity.Gravity = 300
    self.Gravity.OnCursorEntered = function(self)
      self.Alpha:AlphaTo( 15, 0.3 , 0)
      self.Alpha.alpha = 15
    end

    self.Gravity.OnCursorExited = function(self)
      self.Alpha:AlphaTo( 3, 0.3 , 0)
      self.Alpha.alpha = 3
    end

    self.Gravity.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, self.Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "Scoreboard info" )
      self.txtw, self.txth = surface.GetTextSize(self.Gravity)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self.Gravity )
    end

    self.Gravity.DoClick = function(self)
      if not self.editing then
        self.editing = true
        self.Gravity = 0
        self.txt = self.txt or {}
        self.txt = vgui.Create( "DTextEntry" )

        self.txt:SetSize( 0, 0 )
        self.txt:SetPos(0, 0)
        self.txt:SetFont("Scoreboard info")
        self.txt:SetText("")
        self.txt:SetTextColor(Color(255,255,255))
        self.txt:SetAlpha(0)
        self.txt:SetEditable(true)
        self.txt:SetDrawBackground(false)
        self.txt:SetEnterAllowed( true )
        self.txt:SetUpdateOnType( true )

        self.txt:MakePopup()
        lobby.keepopen = true
      end
      self.txt.OnValueChange = function(s, string)
        local pattern = string:find( "[%a%c%p%s]" )
        if pattern then
          self.txt:SetText("300")
        end
        self.Gravity = self.txt:GetValue()
      end

      self.txt.OnEnter = function()
        self.editing = false
        if util.StringToType( self.txt:GetValue(), "float" ) < 75 then
          self.txt:SetText(75)
          self.Gravity = self.txt:GetValue()
        elseif util.StringToType( self.txt:GetValue(), "float" ) > 2000 then
          self.txt:SetText(2000)
          self.Gravity = self.txt:GetValue()
        end
        self.txt:Remove()
        lobby.keepopen = false
      end
    end

    self.AccelerateTitle = self.MinigameInfo:Add( "DLabel" )
    self.AccelerateTitle:SetPos( sizex/2 - 145/2 + 1, sizey/2.5 )
    self.AccelerateTitle:SetSize(145, 35)
    self.AccelerateTitle:SetFont( "Scoreboard info" )
    self.AccelerateTitle:SetTextColor(Color(255, 255, 255))
    self.AccelerateTitle:SetText("Air Accelerate")

    self.Accelerate = vgui.Create( "DButton", self.MinigameInfo )
    self.AccelerateCover= self.Accelerate:Add( "DFrame" )
    self.AccelerateCover:SetTitle("")
    self.AccelerateCover:SetDraggable(false)
    self.AccelerateCover:ShowCloseButton( false )
    self.AccelerateCover:SetSize(0, 0)
    self.AccelerateCover.Paint = function(self, w, h)
      surface.SetDrawColor( Color(10,10,10, 210) )
      surface.DrawRect(0, 0, w, h)

    end
    self.Accelerate.Alpha = self:Add( "DButton" )
    self.Accelerate.Alpha:SetAlpha(3)
    self.Accelerate.Alpha:SetText("")
    self.Accelerate.Alpha.Paint = function(self, w, h)
    end
    self.Accelerate.Alpha.alpha = 0
    self.Accelerate:SetPos( sizex/2 - 145/2, sizey/2)
    self.Accelerate:SetSize( 145, 35 )
    self.Accelerate:SetText("")
    self.Accelerate.Accelerate = 10
    self.Accelerate.OnCursorEntered = function(self)
      self.Alpha:AlphaTo( 15, 0.3 , 0)
      self.Alpha.alpha = 15
    end

    self.Accelerate.OnCursorExited = function(self)
      self.Alpha:AlphaTo( 3, 0.3 , 0)
      self.Alpha.alpha = 3
    end

    self.Accelerate.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, self.Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "Scoreboard info" )
      self.txtw, self.txth = surface.GetTextSize(self.Accelerate)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self.Accelerate )
    end

    self.Accelerate.DoClick = function(self)
      if not self.editing then
        self.editing = true
        self.Accelerate = 0
        self.txt = self.txt or {}
        self.txt = vgui.Create( "DTextEntry" )

        self.txt:SetSize( 0, 0 )
        self.txt:SetPos(0, 0)
        self.txt:SetFont("Scoreboard info")
        self.txt:SetText("")
        self.txt:SetTextColor(Color(255,255,255))
        self.txt:SetAlpha(0)
        self.txt:SetEditable(true)
        self.txt:SetDrawBackground(false)
        self.txt:SetEnterAllowed( true )
        self.txt:SetUpdateOnType( true )

        self.txt:MakePopup()
        lobby.keepopen = true
      end
      self.txt.OnValueChange = function(s, string)
        local pattern = string:find( "[%a%c%p%s]" )
        if pattern then
          self.txt:SetText("10")
        end
        self.Accelerate = self.txt:GetValue()
      end

      self.txt.OnEnter = function()
        self.editing = false
        if util.StringToType( self.txt:GetValue(), "float" ) < 3 then
          self.txt:SetText(3)
          self.Accelerate = self.txt:GetValue()
        elseif util.StringToType( self.txt:GetValue(), "float" ) > 100 then
          self.txt:SetText(100)
          self.Accelerate = self.txt:GetValue()
        end
        self.txt:Remove()
        lobby.keepopen = false
      end
    end

    self.TimeTitle = self.MinigameInfo:Add( "DLabel" )
    self.TimeTitle:SetPos( sizex/4 - 145/2 + 15, sizey/2.5 )
    self.TimeTitle:SetSize(145, 35)
    self.TimeTitle:SetFont( "Scoreboard info" )
    self.TimeTitle:SetTextColor(Color(255, 255, 255))
    self.TimeTitle:SetText("Time Limit")

    self.TimeBut = vgui.Create( "DButton", self.MinigameInfo )
    self.TimeButCover= self.TimeBut:Add( "DFrame" )
    self.TimeButCover:SetTitle("")
    self.TimeButCover:SetDraggable(false)
    self.TimeButCover:ShowCloseButton( false )
    self.TimeButCover:SetSize(0, 0)
    self.TimeButCover.Paint = function(self, w, h)
      surface.SetDrawColor( Color(10,10,10, 210) )
      surface.DrawRect(0, 0, w, h)

    end
    self.TimeBut.Alpha = self:Add( "DButton" )
    self.TimeBut.Alpha:SetAlpha(3)
    self.TimeBut.Alpha:SetText("")
    self.TimeBut.Alpha.Paint = function(self, w, h)
    end
    self.TimeBut.Alpha.alpha = 0
    self.TimeBut:SetPos( sizex/4 - 145/2, sizey/2)
    self.TimeBut:SetSize( 145, 35 )
    self.TimeBut:SetText("")
    self.TimeBut.Time = 300
    self.TimeBut.OnCursorEntered = function(self)
      self.Alpha:AlphaTo( 15, 0.3 , 0)
      self.Alpha.alpha = 15
    end

    self.TimeBut.OnCursorExited = function(self)
      self.Alpha:AlphaTo( 3, 0.3 , 0)
      self.Alpha.alpha = 3
    end

    self.TimeBut.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, self.Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "Scoreboard info" )
      self.txtw, self.txth = surface.GetTextSize(self.Time)
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( self.Time )
    end

    self.TimeBut.DoClick = function(self)
      if not self.editing then
        self.editing = true
        self.Time = 0
        self.txt = self.txt or {}
        self.txt = vgui.Create( "DTextEntry" )

        self.txt:SetSize( 0, 0 )
        self.txt:SetPos(0, 0)
        self.txt:SetFont("Scoreboard info")
        self.txt:SetText(0)
        self.txt:SetTextColor(Color(255,255,255))
        self.txt:SetAlpha(0)
        self.txt:SetEditable(true)
        self.txt:SetDrawBackground(false)
        self.txt:SetEnterAllowed( true )
        self.txt:SetUpdateOnType( true )

        self.txt:MakePopup()
        lobby.keepopen = true
      end
      self.txt.OnValueChange = function(s, string)
        local pattern = string:find( "[%a%c%p%s]" )
        if pattern then
          self.txt:SetText("300")
        end
        self.Time = self.txt:GetValue()
      end

      self.txt.OnEnter = function()
        self.editing = false
        if util.StringToType( self.txt:GetValue(), "float" ) < 60 then
          self.txt:SetText(60)
          self.Time = self.txt:GetValue()
        elseif util.StringToType( self.txt:GetValue(), "float" ) > 9999 then
          self.txt:SetText(9999)
          self.Time = self.txt:GetValue()
        end
        self.txt:Remove()
        lobby.keepopen = false
      end
    end

    self.Autohop = vgui.Create( "DButton", self.MinigameInfo )
    self.AutohopCover= self.Autohop:Add( "DFrame" )
    self.AutohopCover:SetTitle("")
    self.AutohopCover:SetDraggable(false)
    self.AutohopCover:ShowCloseButton( false )
    self.AutohopCover:SetSize(0, 0)
    self.AutohopCover.Paint = function(self, w, h)
      surface.SetDrawColor( Color(10,10,10, 210) )
      surface.DrawRect(0, 0, w, h)

    end
    self.Autohop.toggled = false
    self.Autohop.Alpha = self:Add( "DButton" )
    self.Autohop.Alpha:SetAlpha(3)
    self.Autohop.Alpha:SetText("")
    self.Autohop.Alpha.Paint = function(self, w, h)

    end
    self.Autohop.Alpha.alpha = 0

    self.Autohop:SetPos( sizex/2*1.5 - 145/2, sizey/4 )

    self.Autohop:SetSize( 145, 35 )
    self.Autohop:SetText("")
    self.Autohop.DoClick = function(self)
      self.Alpha:AlphaTo( 150, 0, 0)
      self.Alpha:AlphaTo( self.Alpha.alpha, 0.2, 0)
      if self.toggled then
        self.toggled = false
      else
        self.toggled = true
      end
    end
    self.Autohop:SetAlpha(255)
    self.Autohop.OnCursorEntered = function(self)
      self.Alpha:AlphaTo( 15, 0.3 , 0)
      self.Alpha.alpha = 15
    end

    self.Autohop.OnCursorExited = function(self)
      self.Alpha:AlphaTo( 3, 0.3 , 0)
      self.Alpha.alpha = 3
    end

    self.Autohop.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, self.Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "Scoreboard info" )
      self.txtw, self.txth = surface.GetTextSize("Autohop: Off")
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      if self.toggled == true then
        surface.DrawText( "Autohop: On" )
      else
        surface.DrawText( "Autohop: Off" )
      end
    end

    self.Parkour = vgui.Create( "DButton", self.MinigameInfo )
    self.ParkourCover= self.Parkour:Add( "DFrame" )
    self.ParkourCover:SetTitle("")
    self.ParkourCover:SetDraggable(false)
    self.ParkourCover:ShowCloseButton( false )
    self.ParkourCover:SetSize(0, 0)
    self.ParkourCover.Paint = function(self, w, h)
      surface.SetDrawColor( Color(10,10,10, 210) )
      surface.DrawRect(0, 0, w, h)

    end

    self.Parkour.toggled = true
    self.Parkour.Alpha = self:Add( "DButton" )
    self.Parkour.Alpha:SetAlpha(3)
    self.Parkour.Alpha:SetText("")
    self.Parkour.Alpha.Paint = function(self, w, h)

    end
    self.Parkour.Alpha.alpha = 0

    self.Parkour:SetPos( sizex/4 - 145/2, sizey/4 )

    self.Parkour:SetSize( 145, 35 )
    self.Parkour:SetText("")
    self.Parkour.DoClick = function(self)
      self.Alpha:AlphaTo( 150, 0, 0)
      self.Alpha:AlphaTo( self.Alpha.alpha, 0.2, 0)
      if self.toggled then
        self.toggled = false
      else
        self.toggled = true
      end
    end
    self.Parkour:SetAlpha(255)
    self.Parkour.OnCursorEntered = function(self)
      self.Alpha:AlphaTo( 15, 0.3 , 0)
      self.Alpha.alpha = 15
    end

    self.Parkour.OnCursorExited = function(self)
      self.Alpha:AlphaTo( 3, 0.3 , 0)
      self.Alpha.alpha = 3
    end

    self.Parkour.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, self.Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "Scoreboard info" )
      self.txtw, self.txth = surface.GetTextSize("Parkour: Off")
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      if self.toggled == true then
        surface.DrawText( "Parkour: On" )
      else
        surface.DrawText( "Parkour: Off" )
      end
    end

    self.Shift = vgui.Create( "DButton", self.MinigameInfo )
    self.ShiftCover= self.Shift:Add( "DFrame" )
    self.ShiftCover:SetTitle("")
    self.ShiftCover:SetDraggable(false)
    self.ShiftCover:ShowCloseButton( false )
    self.ShiftCover:SetSize(235, 35)
    self.ShiftCover.Paint = function(self, w, h)
      surface.SetDrawColor( Color(10,10,10, 210) )
      surface.DrawRect(0, 0, w, h)
    end
    self.Shift.toggled = false
    self.Shift.Alpha = self:Add( "DButton" )
    self.Shift.Alpha:SetAlpha(3)
    self.Shift.Alpha:SetText("")
    self.Shift.Alpha.Paint = function(self, w, h)

    end
    self.Shift.Alpha.alpha = 0

    self.Shift:SetPos( sizex/2 - 235/2, sizey/4 )

    self.Shift:SetSize( 235, 35 )
    self.Shift:SetText("")
    self.Shift.DoClick = function(self)
      self.Alpha:AlphaTo( 150, 0, 0)
      self.Alpha:AlphaTo( self.Alpha.alpha, 0.2, 0)
      if self.toggled then
        self.toggled = false
      else
        self.toggled = true
      end
    end
    self.Shift:SetAlpha(255)
    self.Shift.OnCursorEntered = function(self)
      self.Alpha:AlphaTo( 15, 0.3 , 0)
      self.Alpha.alpha = 15
    end

    self.Shift.OnCursorExited = function(self)
      self.Alpha:AlphaTo( 3, 0.3 , 0)
      self.Alpha.alpha = 3
    end

    self.Shift.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, self.Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "Scoreboard info" )
      self.txtw, self.txth = surface.GetTextSize("Super accelerate: Off")
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      if self.toggled == true then
        surface.DrawText( "Super accelerate: On" )
      else
        surface.DrawText( "Super accelerate: Off" )
      end
    end

    self.Minigames= self:Add( "DFrame", self )
    self.Minigames:SetPos( 210, ScrH()/1.59 )
    self.Minigames:SetSize(850+200 - 210, ScrH()/2.75)
    self.Minigames:SetTitle("")
    self.Minigames:SetDraggable(false)
    self.Minigames:ShowCloseButton( false )
    self.Minigames.Paint = function(self, w, h )
      surface.SetDrawColor( Color(225,225,225, 100) )
      surface.DrawRect(0, 0, w, h)
      /*
      surface.SetDrawColor( Color(0,0,0, 220) )
      surface.DrawRect(0, (h/10), w, 2)
      surface.DrawRect(0, (h/10)+300/2, w, 2)
      surface.DrawRect(0, (h/10)+300, w, 2)*/
    end

    self.Minigames.Title = self:Add( "DLabel", self.Minigames )
    self.Minigames.Title:SetPos( (850+200+(210/2))/2, ScrH()/1.59 )
    self.Minigames.Title:SetSize(200, 25)
    self.Minigames.Title:SetFont( "Scoreboard info" )
    self.Minigames.Title:SetTextColor(Color(0, 0, 0))
    self.Minigames.Title:SetText("Minigames")
    local mul1 = 0
    local mul2 = 0
    for k, v in pairs(lobby.minigames) do
      if k == "Hunted" or k == "Elimination" or k == "Tag" or k == "Tricks" or k == "Crushed" then
        self[k] = vgui.CreateFromTable( MINIGAME_BUTTON, self.Minigames )
        self[k]:SetPos(((850+200 - 210 - 30+25)/5*mul1+5), 50)
        self[k]:SetSize((850+200 - 210 - 30)/5,100)
        self[k].Minigame = k
        self[k]:ReColor()
        self[k].func = function()
          self.MinigameInfo:AlphaTo(255,0.3)
          surface.SetFont( "Scoreboard info" )
          self.txtw, self.txth = surface.GetTextSize(k)
          self.LobbyTitle:SetText(k)
          self.LobbyTitle:SetTextColor(v)
          self.LobbyTitle:SetPos( sizex/2 - self.txtw/2, 10 )
          self.Gravity.Gravity = 300
          self.Accelerate.Accelerate = 10
          self.TimeBut.Time = 300

          --Descriptions
          if k == "Hunted" then
            self.LobbyDesc:SetText("One player is the hunted and the rest are hunters. The hunters become the hunted when catching it.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("")
          elseif k == "Elimination" then
            self.LobbyDesc:SetText("A infected type gamemode where the hunters have to infect all the runners.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("")
          elseif k == "Tag" then
            self.LobbyDesc:SetText("The tagger have to tag a runner, that runner then become the next tagger.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("")

          elseif k == "Tricks" then
            self.LobbyDesc:SetText("Gives you access to do tricks aswell as certain commands.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("")
          elseif k == "Crushed" then
            self.LobbyDesc:SetText("You're slowly being crushed. Don't touch the walls closing in! The last man standing wins.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("")
          end
          -- Lock certain adjustments
          if k == "ShadowHop" then
            self.Autohop.toggled = true
            self.Parkour.toggled = false
            self.ParkourCover:SetSize(145, 35)
            self.AutohopCover:SetSize(0,0)
            self.ShiftCover:SetSize(235,35)
            self.AccelerateCover:SetSize(0,0)
            self.GravityCover:SetSize(0,0)
            self.TimeButCover:SetSize(0,0)
          elseif k == "Tricks" then
            self.Autohop.toggled = false
            self.Parkour.toggled = true
            self.TimeBut.Time = 0
            self.ParkourCover:SetSize(145, 35)
            self.AutohopCover:SetSize(145,35)
            self.ShiftCover:SetSize(235,35)
            self.AccelerateCover:SetSize(145,35)
            self.GravityCover:SetSize(145,35)
            self.TimeButCover:SetSize(145,35)
          elseif k == "Miscellaneous" then
            self.TimeBut.Time = 0
            self.TimeButCover:SetSize(145,35)
            self.Autohop.toggled = false
            self.Parkour.toggled = true
            self.ParkourCover:SetSize(0,0)
            self.AutohopCover:SetSize(0,0)
            self.ShiftCover:SetSize(235,35)
            self.AccelerateCover:SetSize(0,0)
            self.GravityCover:SetSize(0,0)
          else
            self.Autohop.toggled = false
            self.Parkour.toggled = true
            self.ParkourCover:SetSize(0,0)
            self.AutohopCover:SetSize(0,0)
            self.ShiftCover:SetSize(235,35)
            self.AccelerateCover:SetSize(0,0)
            self.GravityCover:SetSize(0,0)
            self.TimeButCover:SetSize(0,0)
          end
          self.Shift.toggled = false
        end
        mul1 = mul1 +1

      elseif k == "Race" or k == "Survival" or k == "HnS" or k == "ShadowHop" or k == "Miscellaneous" then
        self[k] = vgui.CreateFromTable( MINIGAME_BUTTON, self.Minigames )
        self[k]:SetPos(((850+200 - 210 - 30+25)/5*mul2+5), 155)
        self[k]:SetSize((850+200 - 210 - 30)/5,100)
        self[k].Minigame = k
        self[k]:ReColor()
        self[k].func = function()
          self.MinigameInfo:AlphaTo(255,0.3)
          surface.SetFont( "Scoreboard info" )
          self.txtw, self.txth = surface.GetTextSize(k)
          self.LobbyTitle:SetText(k)
          self.LobbyTitle:SetTextColor(v)
          self.LobbyTitle:SetPos( sizex/2 - self.txtw/2, 10 )
          self.Gravity.Gravity = 300
          self.Accelerate.Accelerate = 10
          self.TimeBut.Time = 300

          --Descriptions
          if k == "Race" then
            self.LobbyDesc:SetText("Create your own route and race other people. Official ranked routes is work in progress.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("")
          elseif k == "Survival" then
            self.LobbyDesc:SetText("Everytime the timer runs out the player with the lowest speed is eliminated.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("")
          elseif k == "HnS" then
            self.LobbyDesc:SetText("HnS/Hide and Seek is a movement inspired gamemode from css.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("Wallkick power is reduced and you have to rely on strafing. There is one hunter trying to 'infect' the other runners.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc2.txtw, self.LobbyDesc2.txth = surface.GetTextSize(self.LobbyDesc2:GetText())
            self.LobbyDesc2:SetPos( sizex/2 - self.LobbyDesc2.txtw/2, 45 )
          elseif k == "ShadowHop" then
            self.LobbyDesc:SetText("This gamemode plays like hunted where one person is hunted and the rest is hunters.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("Walljumping is disabled and you can only bhop/walk on shadows.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc2.txtw, self.LobbyDesc2.txth = surface.GetTextSize(self.LobbyDesc2:GetText())
            self.LobbyDesc2:SetPos( sizex/2 - self.LobbyDesc2.txtw/2, 45 )
          elseif k == "Miscellaneous" then
            self.LobbyDesc:SetText("This gamemode serves no purpose other than displaying indicators of the people in the lobby.")
            surface.SetFont( "Scoreboard line" )
            self.LobbyDesc.txtw, self.LobbyDesc.txth = surface.GetTextSize(self.LobbyDesc:GetText())
            self.LobbyDesc:SetPos( sizex/2 - self.LobbyDesc.txtw/2, 25 )

            self.LobbyDesc2:SetText("")
          end
          -- Lock certain adjustments
          if k == "ShadowHop" then

            self.Autohop.toggled = true
            self.Parkour.toggled = false
            self.ParkourCover:SetSize(145, 35)
            self.AutohopCover:SetSize(0,0)
            self.ShiftCover:SetSize(235,35)
            self.AccelerateCover:SetSize(0,0)
            self.GravityCover:SetSize(0,0)
            self.TimeButCover:SetSize(0,0)
          elseif k == "Tricks" then
            self.Autohop.toggled = false
            self.Parkour.toggled = true
            self.TimeBut.Time = 0
            self.ParkourCover:SetSize(145, 35)
            self.AutohopCover:SetSize(145,35)
            self.ShiftCover:SetSize(235,35)
            self.AccelerateCover:SetSize(145,35)
            self.GravityCover:SetSize(145,35)
            self.TimeButCover:SetSize(145,35)
          elseif k == "Miscellaneous" then
            self.TimeBut.Time = 0
            self.TimeButCover:SetSize(145,35)
            self.Autohop.toggled = false
            self.Parkour.toggled = true
            self.ParkourCover:SetSize(0,0)
            self.AutohopCover:SetSize(0,0)
            self.ShiftCover:SetSize(235,35)
            self.AccelerateCover:SetSize(0,0)
            self.GravityCover:SetSize(0,0)
          else
            self.Autohop.toggled = false
            self.Parkour.toggled = true
            self.ParkourCover:SetSize(0,0)
            self.AutohopCover:SetSize(0,0)
            self.ShiftCover:SetSize(235,35)
            self.AccelerateCover:SetSize(0,0)
            self.GravityCover:SetSize(0,0)
            self.TimeButCover:SetSize(0,0)
          end
          self.Shift.toggled = false
        end
        mul2 = mul2 +1

      end

    end

    self.CreateLobby = vgui.Create( "DButton", self.MinigameInfo )
    self.CreateLobby.Alpha = self:Add( "DButton" )
    self.CreateLobby.Alpha:SetAlpha(3)
    self.CreateLobby.Alpha:SetText("")
    self.CreateLobby.Alpha.Paint = function(self, w, h)
    end
    self.CreateLobby.Alpha.alpha = 0

    self.CreateLobby:SetPos( sizex/2 - 42.5, sizey-35-20 )

    self.CreateLobby:SetSize( 85, 35 )
    self.CreateLobby:SetText("")
    self.CreateLobby.DoClick = function(self)
      self:GetParent():AlphaTo(0,0.3)
      self.Alpha:AlphaTo( 150, 0, 0)
      self.Alpha:AlphaTo( self.Alpha.alpha, 0.2, 0)
      local par = self:GetParent():GetParent()
      local Settings = {
      		["shift"] = par.Shift.toggled,
      		["time"] = par.TimeBut.Time,
      		["gravity"] = par.Gravity.Gravity,
      		["airaccelerate"] = par.Accelerate.Accelerate,
      		["autohop"] = par.Autohop.toggled,
      		["parkour"] = par.Parkour.toggled
      }
      net.Start("lobbyNetworking")
        net.WriteString("create")
        net.WriteTable({LocalPlayer(), par.LobbyTitle:GetText(), Settings})
      net.SendToServer()
    end
    self.CreateLobby:SetAlpha(255)
    self.CreateLobby.OnCursorEntered = function(self)
      self.Alpha:AlphaTo( 15, 0.3 , 0)
      self.Alpha.alpha = 15
    end

    self.CreateLobby.OnCursorExited = function(self)
      self.Alpha:AlphaTo( 3, 0.3 , 0)
      self.Alpha.alpha = 3
    end

    self.CreateLobby.Paint = function(self, w, h)
      surface.SetDrawColor( Color(255,255,255, self.Alpha:GetAlpha()) )
      surface.DrawRect(0, 0, w, h)
      surface.SetTextColor(Color(255,255,255))
      surface.SetFont( "Scoreboard info" )
      self.txtw, self.txth = surface.GetTextSize("Create")
      surface.SetTextPos( (w/2) - self.txtw/2,(h/2) - self.txth/2 )
      surface.DrawText( "Create" )
    end

		self.Header = self:Add( "Panel" )
		self.Header:Dock( TOP )
		self.Header:SetHeight( 30 )
  end,

	PerformLayout = function( self )

		self:SetSize( 850+200, ScrH() )
		self:SetPos( ScrW()/2 - ((850+200)/2), 0 )
	end,

	Paint = function( self, w, h )
	end,

	Think = function( self, w, h )
    for id, lb in pairs( lobby.lobbies ) do

			if ( IsValid( lb.Entry ) ) then continue end
      if ( IsValid( lb.Entry2 ) ) then continue end

			lb.Entry = vgui.CreateFromTable( LOBBY_BUTTON, lb.Entry )
      lb.Entry:SetSize(self.Lobbies.List:GetWide(), 30)
      lb.Entry:Dock(TOP)
      lb.Entry:DockMargin(0,0,0,5)
			lb.Entry:Setup( lb, self.Lobbies.List, id )

      self.Lobbies.List:AddItem( lb.Entry  )


		end

	end
}

LOBBY = vgui.RegisterTable( LOBBY, "EditablePanel" )

if ( IsValid( LobbyMenu ) ) then
	LobbyMenu:Remove()
end
function GM:OnSpawnMenuOpen()
	open = true

	if ( !IsValid( LobbyMenu ) ) then
		LobbyMenu = vgui.CreateFromTable( LOBBY )
    LobbyMenu:SetAlpha(0)
	end

	if ( IsValid( LobbyMenu ) ) then
    LobbyMenu:AlphaTo(255, 0.15)
		LobbyMenu:Show()
		LobbyMenu:MakePopup()
		LobbyMenu:SetKeyboardInputEnabled( false )
	end
end

function GM:OnSpawnMenuClose()
  if lobby.keepopen then return end
	open = false

	if ( IsValid( LobbyMenu ) ) then
		timer.Simple(0.2, function()
			if not open then
				LobbyMenu:Hide()
			end
		end)

		LobbyMenu:AlphaTo(0, 0.15)
	end

end
